<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $table = 'codes';

    public function user(){
    	return $this->belongsTo('\App\Models\User');
    }

    public function profile(){
    	return $this->belongsTo('\App\Models\Profile', 'user_id', 'user_id');
    }

    public function organization() {
    	return $this->belongsTo('\App\Models\Organization', 'organization_id', 'id');
    }

    public function courses(){
        return explode(",", $this->courses);
    }
}