<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Decision extends Model
{
    protected $table = 'decisions';

    public function user(){
    	return $this->belongsTo('\App\Models\User');
    }

    public function application(){
    	return $this->belongsTo('\App\Models\Application', 'app');
    }
}