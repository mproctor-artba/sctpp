<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prolydian;
use Accredible;

class Certificate extends Model
{
    protected $table= 'certificates';

    protected $fillable = [
        'user_id', 'certificate', 'status' , 'created_at', 'expires_on',
    ];


    public function user(){
    	return $this->belongsTo('\App\Models\User');
    }

    public function profile($user){
    	return \App\Models\Profile::where('user_id', $user)->get();
    }

    public function scopeLink(){
    	return "https://www.credential.net/" . $this->certificate;
    }

    public function scopeExpiration(){
    	return strtotime(convertTimeStamp($this->expires_on));
    }

    public function scopeDaysToRecertify(){
    	//dd(convertTimeStamp(Date("Y-m-d"), "monthdayyear"));
    	//dd(Date("Y-m-d", $this->expiration()));
    	//dd($this->expiration());
    	return str_replace("+","",DateDifference(Date("Y-m-d"), Date("Y-m-d",$this->expiration())));

    }

    public function scopeCheckStatus(){
        $cert = Accredible::viewCredential($this->certificate);
        return $cert["credential"];
    }
}