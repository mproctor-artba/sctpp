<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use \App\Models\Organization as Organizations;
use Accredible;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $softDelete = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin(){
        if($this->admin >= 1){
            return true;
        }

        return false;
    }


    public function age(){
        return \Carbon\Carbon::parse($this->created_at)
        ->diffInDays(\Carbon\Carbon::now());
    }

    public function canReview(){
        if($this->admin >= 2){
            return true;
        }
        return false;
    }

    public function isOrgAdmin(){
        if(Organizations::where('admin', $this->id)->count() > 0){
            return true;
        }

        return false;
    }

    public function isNew(){
        if($this->type == 0){
            return true;
        }
        return false;
    }

    public function profile()
    {
        return $this->hasOne('\App\Models\Profile');
    }

    public function certificate()
    {
        return $this->hasOne('\App\Models\Certificate');
    }

    public function organization()
    {
        return $this->hasOne('Organization');
    }

    public function application()
    {
        return $this->hasOne('\App\Models\Application');
    }

    public function signedApplication()
    {
        return $this->hasOne('\App\Models\Application')->where('signature', '!=', '');
    }

    public function paidApplication()
    {
        return $this->hasOne('\App\Models\Application')->where('status', 2);
    }

    public function deniedApplication()
    {
        return $this->hasOne('\App\Models\Application')->where('status', 3);
    }

    public function approvedApplication()
    {
        return $this->hasOne('\App\Models\Application')->where('status', 4);
    }

    public function exportedApplication()
    {
        return $this->hasOne('\App\Models\Application')->where('status', 5);
    }

    public function enrollment()
    {
        return $this->hasMany('\App\Models\Enrollment');
    }

    public function messages()
    {
        return $this->hasMany('\App\Models\Message', 'recipient');
        //return \App\Models\Message::where('to', Auth::user()->id)->get();
    }

    public function payments()
    {
        return $this->hasMany('Payment');
    }

    public function codes(){
        return $this->hasMany('\App\Models\Code');
    }

    public function documents(){
        return $this->hasMany('\App\Models\Document')->where('status', 1);
    }

    public function pdhs(){
        return $this->hasMany('\App\Models\PDH')->where('deleted', 0);
    }

    public function results(){
        // poly
        return $this->hasMany('\App\Models\Result', 'ClientCandidateID');
    }

    public function experiences(){
        return $this->hasMany('\App\Models\Experience')->where('deleted', 0)->where('type', 1);
    }
    public function recertExperiences(){
        return $this->hasMany('\App\Models\Experience')->where('deleted', 0)->where('type', 2);
    }

    public function completedReferences(){
        return $this->hasMany('\App\Models\Experience')->where('status', 1)->where('deleted', 0);
    }

    public function IncompleteReferences(){
        return $this->hasMany('\App\Models\Experience')->where('status', 0)->where('deleted', 0);
    }

    /* Not sure how to address this in the future, but we will be able to monitor the "progress of a user" here */

    public function step1(){
        if(\App\Models\Profile::where('user_id', $this->id)->count() == 1){
            return true;
        }
        return false;
    }

    public function step2(){
        if(\App\Models\Application::where('user_id', $this->id)->count() == 1){
            return true;
        }
        return false;
    }

    public function step3(){
        if(\App\Models\Document::where('user_id', $this->id)->where('status', 1)->count() >= 2){
            return true;
        }
        return false;
    }

    public function step4(){
        if(\App\Models\Experience::where('user_id', $this->id)->where('deleted',0)->where('type',1)->count() >0){
            return true;
        }
        return false;
    }
    public function step5(){
        if(\App\Models\Application::where('user_id', $this->id)->where('signature', '!=', '')->count() == 1){
            return true;
        }
        return false;
    }

    public function step6(){
        if(\App\Models\Application::where('user_id', $this->id)->where('status', '>=', '2')->count() == 1){
            return true;
        }
        return false;
    }

    public function step7(){
        if(\App\Models\Experience::where('user_id', $this->id)->count() == \App\Models\Experience::where('user_id', $this->id)->where('status', 1)->count() && $this->step6()){
            return true;
        }
        return false;
    }
    public function step8(){
        if(\App\Models\Application::where('user_id', $this->id)->where('status', 4)->count() == 1){
            return true;
        }
        return false;
    }
    public function step9(){
        if(\App\Models\Application::where('user_id', $this->id)->where('status', 5)->count() == 1){
            return true;
        }
        return false;
    }
    public function step10(){
        if(\App\Models\Result::where('ClientCandidateID', $this->profile->ClientCandidateID)->count() >= 1){
            return true;
        }
        return false;
    }

    public function step11(){
        $app = \App\Models\Application::where('user_id', $this->id)->where('recert_option','!=', NULL)->first();
        if(!isset($app)){
            return NULL;
        }
        return $app->recert_option;
    }

    public function step12(){
        if(\App\Models\Payment::where('user_id', $this->id)->where('type','>=', 6)->count() > 0){
            return true;
        }
        return false;
    }

    public function step13(){
        if(\App\Models\Experience::where('user_id', $this->id)->where('deleted',0)->where('type',2)->count() > 0){
            return true;
        }
        return false;
    }

    public function step14(){

        if(\App\Models\Application::where('user_id', $this->id)->whereBetween('stage', [1, 4])->count() > 0){
            return true;
        }
        return false;
    }

    public function step15(){
        if(\App\Models\Application::where('user_id', $this->id)->where('stage',5)->count() > 0){
            return true;
        }
        return false;
    }

    public function scopeGoalOne($query){
        return $query->has('profile');
    }

    public function scopegoalTwo($query){
        return $query->has('application');
    }

    public function scopegoalThree($query){
        return $query->has('documents');
    }

    public function scopegoalFour($query){
        return $query->has('experiences');
    }

    public function scopegoalFive($query){
        return $query->has('signedApplication');
    }

    public function scopegoalSix($query){
        return $query->has('paidApplication');
    }

    public function scopegoalSeven($query){
        return $query->has('experiences', '=' ,'users.completedReferences');
    }

    public function scopegoalEight($query){
        return $query->has('approvedApplication');
    }

    public function scopegoalNine($query){
        return $query->has('exportedApplication');
    }


    

    public function currentStep(){
        $step = 0;
        if($this->step1()){
            $step = 1;
        }
        if($this->step2()){
            $step = 2;
        }
        if($this->step3()){
            $step = 3;
        }
        if($this->step4()){
            $step = 4;
        }
        if($this->step5()){
            $step = 5;
        }
        if($this->step6()){
            $step = 6;
        }
        if($this->step7()){
            $step = 7;
        }
        if($this->step8()){
            $step = 8;
        }
        if($this->step9()){
            $step = 9;
        }
        if($this->step10()){
            $step = 10;
        }
        if(is_object($this->application)){
            if($this->application->status == 3){
                return "Awaiting Resubmission";
            }
        }
        if($this->step14()){
            $step = 14;
        }
        if($this->step15()){
            $step = 15;
        }
        return $this->stepName($step);
    }

    public function currentStepNum(){
        $step = 0;
        if($this->step1()){
            $step = 1;
        }
        if($this->step2()){
            $step = 2;
        }
        if($this->step3()){
            $step = 3;
        }
        if($this->step4()){
            $step = 4;
        }
        if($this->step5()){
            $step = 5;
        }
        if($this->step6()){
            $step = 6;
        }
        if($this->step7()){
            $step = 7;
        }
        if($this->step8()){
            $step = 8;
        }
        if($this->step9()){
            $step = 9;
        }
        if($this->step10()){
            $step = 10;
        }
        if(is_object($this->application)){
            if($this->application->status == 3){
                return "Awaiting Resubmission";
            }
        }
        return $step;
    }

    public function stepName($step){
        switch ($step) {
            case 0:
                return "Working on profile";
                break;
            case 1:
                return "Selecting eligibility";
                break;
            case 2:
                return "Uploading documents";
                break;
            case 3:
                return "Identifying references";
                break;
            case 4:
                return "Agreeing to terms";
                break;
            case 5:
                return "Submitting payment";
                break;
            case 6:
                return "Awaiting references";
                break;
            case 7:
                return "Awaiting review";
                break;
            case 8:
                return "Awaiting export";
                break;
            case 9:
                return "Awaiting test results";
                break;
            case 10:
                return "Exam complete";
                break;
            case 14:
                return "Recertifying";
                break;
            case 15:
                return "Recertified";
                break;
            default:
                return "No action";
                break;
        }
    }

    public function currentRecertStep(){
        $step = 10;

        if($this->step11() > 0){
            $step = 11;
        }
        if($this->step12()){
            $step = 12;
        }
        if($this->step13()){
            $step = 13;
        }

        return $step;
    }

    public function scopePDHStatus(){
        // get all PDHs submitted withtin the recertification period
        $allPDHs = $this->pdhs;
        

        $certificate = Accredible::viewCredential($this->certificate->certificate);

        $beginning = date('Y-m-d', strtotime($certificate["credential"]["issued_on"]));
        $end = date('Y-m-d', strtotime($certificate["credential"]["expired_on"]));

        $hours = 0;

       
        foreach ($allPDHs as $allPDH){



            $pdhDate = convertTimestamp($allPDH->activityDate, "yearmonthday");

            

            if (($pdhDate >= $beginning) && ($pdhDate <= $end)){
                // the PDH falls within the expiry of the current cert. Fill the array with the amount of hours in each bucket
                
                $hours += $allPDH->hours;

            }


        }

        return $hours;
    }
}
