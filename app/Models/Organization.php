<?php

namespace App\Models;
use App\Models\User as Users;
use App\Code as Codes;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $table = 'organizations';

    public function codes(){
        return $this->hasMany('\App\Models\Code', 'organization_id');
    }

    public function users($organization){
    	return \App\Models\User::where('organization_id', $organization)->get();
    }

    public function admin(){
    	return \App\Models\User::where('id', $this->admin)->first();
    }

    public function scopePassed(){
        $passed = 0;
        foreach($this->codes as $code){
            if(isset($code->user)){
                if(isset($code->user->certificate)){
                    $passed++;
                }


            }
        }
        foreach(Users::where('organization_id', $this->id)->get() as $monitoredUser){
            if(Codes::where('organization_id', $this->id)->where('user_id', $monitoredUser->id)->count() == 0 && isset($monitoredUser->certificate)){
                $passed++;
            }
        }
                
        return $passed;
    }

    public function scopeFailed(){
        $failed = 0;
        foreach($this->codes as $code){
            if(isset($code->user)){
                if(!isset($code->user->certificate)){
                    $failed++;
                }
            }
        }
        return $failed;
    }
    public function scopeNoShows(){
        $noshows = 0;
        return $noshows;
        foreach($this->codes as $code){
            if(isset($code->user)){
                if(!isset($code->user->certificate)){
                    $failed++;
                }
            }
        }
    }
}