<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PDH extends Model
{
    protected $table = 'pdhs';

    protected $fillable = [
        'id', 'user_id', 'name', 'activityDate', 'city', 'state', 'organization', 'contactName', 'phone', 'email', 'type', 'file', 'hours', 'status',
    ];

    public function user(){
    	return $this->belongsTo('\App\Models\User');
    }

    public function decisions()
    {
        return $this->hasMany('\App\Models\PDHDecision', 'pdh_id');
    }

    public function lastestDecision()
    {
        return \App\Models\PDHDecision::where('pdh_id', $this->id)->orderBy('created_at', 'DESC')->first();
    }
}