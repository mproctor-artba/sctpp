<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
    protected $table = 'enrollment';

    public function user()
    {
    	return $this->belongsTo('\App\Models\User');
    }

        public function course()
    {
    	return $this->hasOne('\App\Models\Course');
    }
}
