<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use SoftDeletes;

    protected $table = 'applications';
    protected $softDelete = true;

    protected $fillable = [
        'user_id', 'type', 'status', 'eligibility', 'signature','form', 'stage', 'recert_option', 'date', 'updated_at'
    ];

    public function user()
    {
    	return $this->belongsTo('\App\Models\User');
    }

    public function profile($user)
    {
    	return \App\Models\Profile::where('user_id', $user)->get();
    }

    public function decisions()
    {
        return $this->hasMany('\App\Models\Decision', 'app')->orderBy('date', 'desc');
    }
    public function scopeApprovalDate()
    {
        if(\App\Models\Decision::where('app', $this->id)->where('decision', 1)->count() > 0){
            echo "ID: " . $this->id;
            return \App\Models\Decision::where('app', $this->id)->where('decision', 1)->orderBy('date', 'desc')->first()->date;
        } 
        echo "ID: " . $this->id . "[NULL] ";        
        return NULL;       
    }

    public function scopeTestDate()
    {
        $ApprovalDate = $this->ApprovalDate();
        if(is_string($ApprovalDate)){
echo " Time: ";
print_r($ApprovalDate);
echo "
";

            return date('Y-m-d H:m:s', strtotime($ApprovalDate . ' + 180 days'));
        }
        return NULL;
    }

    public function ready()
    {
        foreach($this->get() as $application)
        {

        }
    }

}