<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Message extends Model
{
    protected $table= 'messages';

    public function user(){
    	return $this->belongsTo('\App\Models\User');
    }

    public function content($type, $message = NULL){
		$profile = Auth::user()->profile;

		switch ($type) {
			case '1':
				return "Hello, $profile->first_name. Thank you for your interest in becoming a Safety Certified Transportation Project Professional&trade;. If you have any questions, please send an email to <a href='mailto:certificationteam@sctpp.org'>certificationteam@sctpp.org</a>";
				break;
			case '2':
				return "We sent an email to your recently added reference to confirm your employment experience. Remember, all references must electronically verify your experience for your application to be reviewed.";
				break;
			case '3':
				return "Your signature looks great! The last step is to submit payment for your application.";
				break;
			case '4':
				return "Your SCTPP&trade; application has been fully submitted. You will receive a decision within 15 business days once all of your references have verified your experience with them (if they haven't already).";
				break;
			case '5':
				return "Your SCTPP&trade; application has been fully resubmitted. You will receive a decision within 15 business days.";
				break;
			case '6':
				return "Approved! You qualify to sit for the SCTPP&trade; exam. Please visit <a href='http://pearsonvue.com/sctpp/' class='label label-info' target='_blank' ><i class='fa fa-external-link'></i> Pearson Vue</a> in order to schedule your exam date.";
			case '7':
				return "We could not approve your application for the following reason(s): $message";
				break;
			case '8':
				return "Your testing profile as been created and sent to Pearson. They will contact you via email with further instructions.";
				break;
			case '9':
				return "Your test results have been received and unfortunately you did not pass your exam. We strongly encourage you to try again.";
				break;
			case '10':
				return "You did not appear for your exam according to an attendance report received by the test center you registered at.";
				break;
			case '11':
				return "Contratulations! You have passed your exam and have been awarded the SCTPP designation. Your certificate will be emailed to you.";
				break;
			/* for recertification */
			case '12':
				return "Hello, $profile->first_name. Thank you for your interest in renewing your SCTPP&trade; certification. A receipt has been sent to your email. If you have any questions, please send an email to <a href='mailto:certificationteam@sctpp.org'>certificationteam@sctpp.org</a>";
				break;
			case '13':
				return "We sent an email to your recently added reference to confirm your employment experience. Remember, all references must electronically verify your experience for your recertification application to be reviewed.";
				break;
			/* PDH Route */
			case '14':
				return "Your evidence for professional development hours has been denied for the following reason(s): $message";
				break;
			case '15':
				return "Your evidence for professional development hours has been approved.";
				break;
			case '16':
				return "Your SCTPP&trade; recertification application has been fully submitted. You will receive a decision within 15 business days.";
				break;
			case '17':
				return "We could not approve your recertification application for the following reason(s): $message";
				break;
			case '18':
				return "Approved! Your SCTPP&trade; certificate will automatically renew on $message.";
			/* exam route */
			case '19':
				return "Approved! You qualify to sit for the SCTPP&trade; exam. Please visit <a href='http://pearsonvue.com/sctpp/' class='label label-info' target='_blank' ><i class='fa fa-external-link'></i> Pearson Vue</a> in order to schedule your exam date. You must complete the exam before: $message";
			case '20':
				return "Contratulations! You passed your exam and your SCTPP certificate has been renewed for three more years.";			
			default:
				# code...
				break;
		}
	}
}