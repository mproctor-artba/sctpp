<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use \App\Models\Profile as Profiles;
use Auth;

class State extends Model
{
	protected $table = 'us_states';

	public function scopeResults($query){
		$passed = 0;
		$failed = 0;

		foreach(Profiles::where('state', $this->abbr)->get() as $profile){
			if($profile->passed()){
				$passed++;
			}
			if($profile->failed()){
				$failed++;
			}
		}

		return [$passed, $failed];
	}
}