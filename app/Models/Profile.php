<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'company', 'title', 'address', 'address2', 'city', 'state', 'zip', 'ClientAuthorizationID'
    ];


    
    protected $table = 'profiles';
    protected $softDelete = true;

    public function user(){
    	return $this->belongsTo('\App\Models\User')->withTrashed();
    }

    public function applications(){
        return $this->belongsToMany('\App\Models\Application')->withTrashed();
    }

    public function address(){
    	return "$this->address $this->city, $this->state, $this->zip";
    }

    public function firstExport(){
        if(empty($this->ClientCandidateID)){
            return true;
        }
        return false;
    }

    public function scopeCompanies($query){
        return $query->select('company')->distinct('company')->get();
    }

    public function results(){
        return $this->hasMany('\App\Models\Result', 'ClientCandidateID', 'ClientCandidateID' );
    }
    public function scopeCompanyResult($query, $company){
        $passed = 0;
        $failed = 0;

        foreach($query->where('company', $company)->get() as $profile){
            if($profile->passed()){
                $passed++;
            }
            if($profile->failed()){
                $failed++;
            }
        }

        return [$passed, $failed];
    }
    public function companyResultsFailed(){

    }

    public function scopegoalTen($query){
        return true;
    }
    public function scopeComplete($query){
        return $query->has('results');
    }
    public function progress(){
        // poly
        return $this->hasMany('\App\Models\Progress', 'user_id', 'user_id');
    }

    public function countTitle($title){
        return $this->where('title','LIKE', $title)->count();
    }

    public function countCompanies($company){
        return $this->where('company','LIKE', '%' . $company . '%')->count();
    }
    public function countStates($state){
        return $this->where('state', $state)->count();
    }

    public function titlePassRate($title){
        $profiles = $this->where('title','LIKE', $title)->get();
        $passed = 0;
        $failed = 0;
        foreach($profiles as $profile){
            if($profile->results->where('Grade','PASS')->count() > 0){
                $passed++;
            } 
            if($profile->results->where('Grade','FAIL')->count() > 0){
                $failed++;
            }
        }
        if(($passed + $failed) > 0){
            return number_format(($passed / ($passed + $failed)) * 100, 2) . "%";
        }
        return "N/A";
    }

    public function companyPassRate($company){
        $profiles = $this->where('company','LIKE', $company)->get();
        $passed = 0;
        $failed = 0;
        foreach($profiles as $profile){
            if($profile->results->where('Grade','PASS')->count() > 0){
                $passed++;
            } 
            if($profile->results->where('Grade','FAIL')->count() > 0){
                $failed++;
            }
        }
        if(($passed + $failed) > 0){
            return number_format(($passed / ($passed + $failed)) * 100, 2) . "%";
        }
        return "N/A";
    }

    public function statePassRate($state){
        $profiles = $this->where('state', $state)->get();
        $passed = 0;
        $failed = 0;
        foreach($profiles as $profile){
            if($profile->results->where('Grade','PASS')->count() > 0){
                $passed++;
            } 
            if($profile->results->where('Grade','FAIL')->count() > 0){
                $failed++;
            }
        }
        if(($passed + $failed) > 0){
            return number_format(($passed / ($passed + $failed)) * 100, 2) . "%";
        }
        return "N/A";
        
    }

    public function avgDaystoTest(){
        $profiles = $this->all();

        $i = 1;
        $days = 0;

        foreach($profiles as $profile){
            if(isset($profile->results->first()->created_at)){
                $days += DateDifference(convertTimestamp($profile->created_at), convertTimestamp($profile->results->first()->created_at));
                $i++;
            }
        }

        return number_format(($days / $i), 2);
    }

    public function expired(){
        $profiles = $this->all();

        $i = 1;
        $days = 0;

        foreach($profiles as $profile){
            if(isset($profile->results->first()->created_at)){
                $days = DateDifference(convertTimestamp($profile->created_at), convertTimestamp($profile->results->first()->created_at));
                if($days > 180){
                    $i++;
                }
            }
        }

        return $i;
    }

    public function scopePassed($query){
        if(\App\Models\Result::where('ClientCandidateID', $this->ClientCandidateID)->where('Grade', 'PASS')->count() > 0){
            return true;
        }

        return false;
    }

    public function scopeFailed($query){
        if(\App\Models\Result::where('ClientCandidateID', $this->ClientCandidateID)->where('Grade', 'FAIL')->count() > 0){
            return true;
        }

        return false;
    }

}