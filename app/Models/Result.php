<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Result extends Model
{
    protected $table = 'results';

    public function user(){
    	return $this->belongsTo('\App\Models\Profile', 'ClientCandidateID', 'ClientCandidateID');
    }

    public function profile($user){
    	return \App\Models\Profile::where('user_id', $user)->get();
    }

    public function attrition(){
    	$i = 0;

    	$failed = $this->failed();

    	$twoexams = sizeof(DB::select(DB::raw('select count(*) from `results` group by `ClientCandidateID` having COUNT(ClientCandidateID) > 1')));

    	return (1 - ($twoexams / $failed)) * 100;
    	
    }

    public function passrate(){

    	return ($this->passed() / $this->count()) * 100;
    }

    public function failed(){
    	return $this->where('Grade', 'FAIL')->count();
    }

    public function passed(){
    	return $this->where('Grade','PASS')->count();
    }


    public function popularMonths(){

        $years = array(2021 => [], 2020 => [], 2019 => [], 2018 => [], 2017 => [], 2016 => []);

        foreach($this->all() as $result){
            if(!$result->user->isAdmin){
                $stamp = convertTimestamp($result->date, $pull = "monthyear");
                $stamp = explode("-", $stamp);
                array_push($years[$stamp[1]], $stamp[0]);
            }
        }
        return $years;
    }

}