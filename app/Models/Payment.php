<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Stripe;

class Payment extends Model
{
    protected $table = 'payments';

    public function user(){
    	return $this->belongsTo('\App\Models\User');
    }

}