<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PDHDecision extends Model
{
    protected $table = 'pdh_decisions';

    public function pdh(){
    	return $this->belongsTo('\App\Models\PDH', 'pdh_id', 'id');
    }
}
