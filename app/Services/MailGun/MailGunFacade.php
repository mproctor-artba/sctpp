<?php

namespace app\Services\MailGun;

use Illuminate\Support\Facades\Facade;

class MailGunFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'MailGun';
    }
}