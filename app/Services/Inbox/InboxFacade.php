<?php

namespace App\Services\Inbox;

use Illuminate\Support\Facades\Facade;

use Auth;
use DB;
use App\Models\Message as Messages;
use App\Models\Profile as Profiles;

class InboxFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Inbox';
    }

    public static function all($user){
        return Messages::where('recipient', $user)->orderBy('date','desc')->get();
    }

    public static function new($user, $type, $message = NULL){
        DB::table('messages')->insert([
            'recipient' => $user,
            'type' => $type,
            'content' => $message
        ]);
    }
}