<?php

namespace App\Services\Inbox;

use Illuminate\Support\ServiceProvider;
use App\Services\Inbox;

class InboxServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Inbox', function($app) {
            return new Inbox();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
