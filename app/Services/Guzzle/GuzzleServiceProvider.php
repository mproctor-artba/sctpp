<?php

namespace app\Services\Guzzle;

use Illuminate\Support\ServiceProvider;

class GuzzleServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Guzzle', function($app) {
            return new Guzzle();
        });
    }
}