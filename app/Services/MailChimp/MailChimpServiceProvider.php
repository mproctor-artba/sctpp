<?php

namespace app\Services\MailChimp;

use Illuminate\Support\ServiceProvider;

class MailChimpServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('MailChimp', function($app) {
            return new MailChimp();
        });
    }
}