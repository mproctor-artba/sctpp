<?php

namespace app\Services\MailChimp;

use Illuminate\Support\Facades\Facade;

class MailChimpFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'MailChimp';
    }
}