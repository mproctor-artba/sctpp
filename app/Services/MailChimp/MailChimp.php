<?php

namespace app\Services\MailChimp;
use Auth;
use DB;
use App\Models\Message as Messages;
use App\Models\Profile as Profiles;
use App\Models\Result as Results;
use Newsletter;

class MailChimp
{
	public function updateCandidatesExamStatus(){

		foreach (Results::where('Grade', 'PASS')->get() as $result) {
			$profile = $result->user;

			Newsletter::subscribe($profile->user->email, 
				[
					'FNAME'=> $profile->first_name, 
					'LNAME'=> $profile->last_name,
					'TITLE' => $profile->title,
					'COMPANY' => $profile->company,
					'STATE' => $profile->state,
					'STATUS' => $result->Grade
				],
				'SCTPP'
			);


			if(Newsletter::isSubscribed($profile->user->email) != 1){
				// 
			}
		}

		foreach (Results::where('Grade', 'FAIL')->get() as $result) {
			$profile = $result->user;

			Newsletter::subscribe($profile->user->email, 
				[
					'FNAME'=> $profile->first_name, 
					'LNAME'=> $profile->last_name,
					'TITLE' => $profile->title,
					'COMPANY' => $profile->company,
					'STATE' => $profile->state,
					'STATUS' => $result->Grade
				],
				'SCTPP'
			);

			if(Newsletter::isSubscribed($profile->user->email) != 1){
				//
			}
		}

		foreach (Results::where('Grade', 'NOSHOW')->get() as $result) {
			$profile = $result->user;

			Newsletter::subscribe($profile->user->email, 
				[
					'FNAME'=> $profile->first_name, 
					'LNAME'=> $profile->last_name,
					'TITLE' => $profile->title,
					'COMPANY' => $profile->company,
					'STATE' => $profile->state,
					'STATUS' => $result->Grade
				],
				'SCTPP'
			);

			if(Newsletter::isSubscribed($profile->user->email) != 1){
				//
			}
		}
		
	}
}