<?php

namespace app\Services\Sisense;

use Illuminate\Support\ServiceProvider;

class SisenseServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Sisense', function($app) {
            return new Sisense();
        });
    }
}