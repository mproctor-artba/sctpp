<?php

namespace app\Services\Sisense;

use Illuminate\Support\Facades\Facade;

class SisenseFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Sisense';
    }
}