<?php

namespace app\Services\Sisense;
use Auth;
use DB;
use App\Models\User as Users;
use App\Models\Profile as Profiles;
use App\Models\Certificate as Certificates;
use Accredible;

class Sisense
{
	public function users(){

		$users = Users::where('admin', 0)->get();
		
		$certificates = Certificates::all();

		DB::table('sisense_users')->delete();
					
		$skippedCerts = [];

		foreach ($users as $user) {
			$state = $title = $company = NULL;
			$is_certified = $is_expired = $is_recertified = 0;

			if(isset($user->profile)){
				$state = $user->profile->state;
				$title = $user->profile->title;
				$company = $user->profile->company;
			}

			$progress = ["disposition" => ""];

            if(isset($user->profile) && !empty($user->profile->progress()->first())){
                $progress = $user->profile->progress()->orderBy('id', 'DESC')->first()->toArray();

            }

            if($progress["disposition"] == "Recertified"){
            	$is_certified = 1;
            	$is_recertified = 1;
            }

			$progress = $progress["disposition"];


			DB::table('sisense_users')->insert([
				"id" => $user->id,
				"state" => $state,
				"company" => $company,
				"title" => $title,
				"status" => $progress,
				"is_certified" => $is_certified,
				"is_recertified" => $is_recertified
			]);
		}

		foreach($certificates as $cert){
			$is_certified = $is_expired = 0;

			if(strtotime(Date('Y-m-d')) > strtotime($cert->expires_on)){
				$is_expired = 1;
			} else {
				$is_certified = 1;
			}

			DB::table('sisense_users')->where("id", $cert->user_id)->update([
				"is_certified" => $is_certified,
				"is_expired" => $is_expired
			]);
		}
	}

	public function certs(){
		$certificates = Certificates::all();

		DB::table('sisense_certs')->delete();

		foreach($certificates as $cert){

			$status = "Certified";

			if(strtotime(Date('Y-m-d')) > strtotime($cert->expires_on)){
				$status = "Expired";
			}

			if(str_replace("+","",DateDifference($cert->created_at, $cert->expires_on)) > 1096 ){
				$status = "Recertified";
			}

			DB::table('sisense_certs')->insert([
				"id" => $cert->certificate,
				"user_id" => $cert->user_id,
				"status" => $status,
				"created_at" => $cert->created_at
			]);
		}
	}
}