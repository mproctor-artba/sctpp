<?php

namespace app\Services\Edvance360;

use Illuminate\Support\Facades\Facade;

class Edvance360Facade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Edvance360';
    }
}