<?php

namespace app\Services\Edvance360;
use App\Models\Message as Messages;
use App\Models\User as Users;
use App\Models\Profile as Profiles;
use App\Models\Experience as Experiences;
use App\Models\Payment as Payments;
use Auth;
use DB;
use Mail;

class Edvance360
{
	public function auth(){
		$callback = [];
		$account_id = env('EDVANCE360_ACCOUNT_ID');
		$key = env('EDVANCE360_KEY');
		$time = time();
		$string = $account_id.$key.$time;
		$secret = env('EDVANCE360_SECRET');
		$sig = base64_encode ( hash_hmac ( 'sha256', $string, $secret, "true") );

		$header = ['exceptions' => true, 'clientId' => $account_id];

		$body = [
            'clientId' => $account_id,
	        'key' => $key,
	        'signature' => $sig,
	        'time' => $time
    	];

    	$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, env('EDVANCE360_URL') . "/token"); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('clientId:566'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body)); 

		$response = json_decode(curl_exec($ch));
		$err = curl_error($ch);

		curl_close($ch);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
			return $response->token;
		}


	}

	public function data(){
		$auth = $this->auth();
		$edvance360 = new \stdClass();

		// create curl resource 
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, env('EDVANCE360_URL') . "/user/1/1000"); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
   		curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("token: $auth", 'clientId: 566'));

        // $output contains the output string 
       	$edvance360->students = json_decode(curl_exec($ch));

       	curl_close($ch);

       	// create curl resource 
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, env('EDVANCE360_URL') . "/course/1/100"); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
   		curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("token: $auth", 'clientId: 566'));

        // $output contains the output string 
       	$edvance360->courses = json_decode(curl_exec($ch));

       	curl_close($ch);

       	return $edvance360;
	}

	public function courses($student){
		$auth = $this->auth();
		$url = env('EDVANCE360_URL') . "/courseuser/$student/completedcourses";
		// create curl resource 
    $ch = curl_init(); 

        // set url 
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
   	curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("token: $auth", 'clientId: 566'));

        // $output contains the output string 
    $courses = json_decode(curl_exec($ch));

    curl_close($ch);



    return $courses;  
	}

	public function course($id){
		$auth = $this->auth();
		$url = env('EDVANCE360_URL') . "/course/$id";
		// create curl resource 
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("token: $auth", 'clientId: 566'));

        // $output contains the output string 
       	$course = json_decode(curl_exec($ch));

       	curl_close($ch);

       	return $course;
	}

  public function newStudent($user){
    $body = '[
      {
      "userId": "string",
      "username": "string",
      "firstName": "string",
      "lastName": "string",
      "middleName": "string",
      "email": "string",
      "password": "string",
      "userRole": "Student",
      "status": "1",
      "address": "string",
      "address2": "string",
      "city": "string",
      "state": "string",
      "zip": 0,
      "country": "string",
      "homePhone": "string",
      "workPhone": "string",
      "cellPhone": "string",
      "companyName": "string",
      "parentId": "string"
}
    ]';
  }

  public function students($id){
    $auth = $this->auth();
    $url = env('EDVANCE360_URL') . "/courseuser/$id";
    // create curl resource 
    $ch = curl_init(); 

    // set url 
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("token: $auth", 'clientId: 566'));

    // $output contains the output string 
    $course = json_decode(curl_exec($ch));

    curl_close($ch);

    return $course;
  }

  public function enroll($student, $courseID){
    $auth = $this->auth();

    echo $auth;
  }
}