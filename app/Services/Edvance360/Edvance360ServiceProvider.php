<?php

namespace app\Services\Edvance360;

use Illuminate\Support\ServiceProvider;

class Edvance360ServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Edvance360', function($app) {
            return new Edvance360();
        });
    }
}