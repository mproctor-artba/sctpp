<?php

namespace app\Services\Email;

use Illuminate\Support\Facades\Facade;

use Auth;
use DB;
use Mail;
use App\Models\Message as Messages;
use App\Models\User as Users;
use App\Models\Application as Applications;
use App\Models\Profile as Profiles;
use App\Models\Experience as Experiences;
use App\Models\Payment as Payments;
use App\Models\Organization as Organizations;
use App\Models\Notification as Notifications;
use App\Models\Code as Codes;

class EmailFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Email';
    }

    public static function send($user, $subject, $header, $content){
        $user = Users::where('id', $user)->first();
        $profile = Profiles::where('user_id', $user->id)->first();
        $name = "$profile->first_name $profile->last_name";
        $email = $user->email;

        $data = [
            "name" => $name,
            "header" => $header,
            "body" => $content
        ];

        Mail::send(['html' => 'emails.generic'], $data, function($message) use ($subject, $email, $name){
        $message->to($email, $name)
        ->subject("$subject");
            $message->from("certificationteam@sctpp.org",'ARTBA Certification Team');
            $message->sender("certificationteam@sctpp.org",'ARTBA Certification Team');
        });
    }

    public static function receipt($user, $invoice){
        $user = Users::where('id', $user)->first();
        $profile = Profiles::where('user_id', $user->id)->first();
        $name = "$profile->first_name $profile->last_name";
        $email = $user->email;
        $subject = "Your SCTPP Application Receipt";

        $data = [
            "name" => $name,
            "invoice" => $invoice
        ];

        Mail::send(['html' => 'emails.receipt'], $data, function($message) use ($subject, $email, $name){
        $message->to($email, $name)
        ->subject("$subject");
            $message->from("certificationteam@sctpp.org",'ARTBA Certification Team');
            $message->sender("certificationteam@sctpp.org",'ARTBA Certification Team');
        });
    }

    public static function accessCodesReceipt($email, $organization, $quantity, $invoice, $name){
        date_default_timezone_set('America/New_York');
        $codes = [];
        $prefix = Organizations::find($organization)->prefix;

        for($i = 0; $i < $quantity; $i++){
            $code = "$prefix-" . strtoupper(generateRandomString(12));
            DB::table('codes')->insert([
                "code" => $code,
                "organization_id" => $organization
            ]);
            array_push($codes, $code);
        }

        $subject = "Receipt for Your SCTPP Access Codes";

        $data = [
            "name" => $name,
            "invoice" => $invoice,
            "quantity" => $quantity,
            "amount" => $quantity * 400,
            "codes" => $codes,
            "date" => Date('F d, Y')
        ];

        Mail::send(['html' => 'emails.receiptAccessCodes'], $data, function($message) use ($subject, $email, $name){
        $message->to($email, $name)
        ->subject("$subject");
            $message->from("certificationteam@sctpp.org",'ARTBA Certification Team');
            $message->sender("certificationteam@sctpp.org",'ARTBA Certification Team');
        });

    }

    public static function retestReceipt($user, $invoice){
        $user = Users::where('id', $user)->first();
        $profile = Profiles::where('user_id', $user->id)->first();
        $name = "$profile->first_name $profile->last_name";
        $email = $user->email;
        if(sandbox()){
            $email = "mproctor@artba.org";
        }
        $subject = "Your SCTPP Testing Receipt";

        $data = [
            "name" => $name,
            "invoice" => $invoice
        ];

        Mail::send(['html' => 'emails.retest-receipt'], $data, function($message) use ($subject, $email, $name){
        $message->to($email, $name)
        ->subject("$subject");
            $message->from("certificationteam@sctpp.org",'ARTBA Certification Team');
            $message->sender("certificationteam@sctpp.org",'ARTBA Certification Team');
        });
    }

    public static function recertReceipt($user, $invoice, $recertOption){
        $user = Users::where('id', $user)->first();
        $profile = Profiles::where('user_id', $user->id)->first();
        $name = "$profile->first_name $profile->last_name";
        $email = $user->email;
        if(sandbox()){
            $email = "mproctor@artba.org";
        }
        $subject = "Your SCTPP Recertification Payment Receipt";

        $data = [
            "name" => $name,
            "invoice" => $invoice,
            "recertOption" => $recertOption
        ];

        Mail::send(['html' => 'emails.recert-receipt'], $data, function($message) use ($subject, $email, $name){
        $message->to($email, $name)
        ->subject("$subject");
            $message->from("certificationteam@sctpp.org",'ARTBA Certification Team');
            $message->sender("certificationteam@sctpp.org",'ARTBA Certification Team');
        });
    }

    public static function verification($user, $experience){
        $user = Users::where('id', $user)->first();
        $profile = Profiles::where('user_id', $user->id)->first();
        $experience = Experiences::where('secret', $experience)->first();

        $name = "$experience->reference";
        $subject = "Please Verify Work Experience for $profile->first_name $profile->last_name";
        $email = $experience->email;
        if(sandbox()){
            $email = "mproctor@artba.org";
        }
        $data = [
            "user" => $user,
            "experience" => $experience,
            "profile" => $profile
        ];

        Mail::send(['html' => 'emails.verification'], $data, function($message) use ($subject, $email, $name){
        $message->to($email, $name)
        ->subject("$subject");
            $message->from("certificationteam@sctpp.org",'ARTBA Certification Team');
            $message->sender("certificationteam@sctpp.org",'ARTBA Certification Team');
        });
    }

    public static function applicationReminder($email, $type){

        $user = Users::where('email', $email)->first();
        $subject = "Please complete your SCTPP application ";
        
        $data = [
                "header" => "Thank you for your interest in applying to take the Safety Certification Transportation Project Professional Exam.",
                "body" => "We noticed that you have not submitted an application for review. Your application will allow us to verify your eligibility to take this exam.",
                "company" => NULL,
                "first_name" => NULL,
                "colleagues" => NULL,
                "references" => NULL,
                "daysToTest" => NULL
            ];

        if($type == 1){
            // remind users without an application
            
            Mail::send(['html' => 'emails.reminders.incomplete-application-v1'], $data, function($message) use ($subject, $email){
            $message->to($email, $email)
            ->subject("$subject");
                $message->from("certificationteam@sctpp.org",'ARTBA Certification Team');
                $message->sender("certificationteam@sctpp.org",'ARTBA Certification Team');
            });
            
        }

        else{
            
            // remind users that started and finished step 1 but stopped
            $profile = $user->profile;
            $name = "$profile->first_name $profile->last_name";
            $subject = $profile->first_name . ", Complete your SCTPP Application";
            $data["first_name"] = $profile->first_name;
            $data["company"] = $profile->company;

            if($type == 2){
                if(companyEmployeeCount($profile->company) == 0){
                     Mail::send(['html' => 'emails.reminders.incomplete-application-v1'], $data, function($message) use ($subject, $email, $name){
                        $message->to($email, $name)
                        ->subject("$subject");
                            $message->from("certificationteam@sctpp.org",'ARTBA Certification Team');
                            $message->sender("certificationteam@sctpp.org",'ARTBA Certification Team');
                        });
                        
                } else{
                    $data["colleagues"] = companyEmployeeCount($profile->company);
                    Mail::send(['html' => 'emails.reminders.incomplete-application-v2'], $data, function($message) use ($subject, $email, $name){
                        $message->to($email, $name)
                        ->subject("$subject");
                            $message->from("certificationteam@sctpp.org",'ARTBA Certification Team');
                            $message->sender("certificationteam@sctpp.org",'ARTBA Certification Team');
                        });
                        
                }
            }
            if($type == 3){
                // remind users that have outstanding references
                $subject = $profile->first_name . ", We are waiting on confirmation from your employment references";
                $data["references"] = $user->IncompleteReferences;
                 Mail::send(['html' => 'emails.reminders.references-outstanding'], $data, function($message) use ($subject, $email, $name){
                    $message->to($email, $name)
                    ->subject("$subject");
                        $message->from("certificationteam@sctpp.org",'ARTBA Certification Team');
                        $message->sender("certificationteam@sctpp.org",'ARTBA Certification Team');
                    });
                    
            }
            
            if($type == 4 && Date('d') >= 15 && !is_null($user->application->TestDate()) && !is_object($user->application->TestDate())){
                // remind users that have not taken their exam yet
                // we only want to send this once a month

                $daysToTest = DateDifference(convertTimestamp(Date("Y-m-d H:m:s")), convertTimestamp($user->application->TestDate()));

                if($daysToTest > 0){
                    $data["daysToTest"] = substr($daysToTest, 1);


                $subject = $profile->first_name . ", you have " . $data["daysToTest"] . " days left to take your SCTPP exam";

                Mail::send(['html' => 'emails.reminders.exam-days-remaining'], $data, function($message) use ($subject, $email, $name){
                    $message->to($email, $name)
                    ->subject("$subject");
                        $message->from("certificationteam@sctpp.org",'ARTBA Certification Team');
                        $message->sender("certificationteam@sctpp.org",'ARTBA Certification Team');
                    });
                }
            }
        }
    }

    public static function codeNotify($code, $user){
        $user = Users::where('id', $user)->first();
        $profile = $user->profile;
        $name = "$profile->first_name $profile->last_name";

        $organization = Organizations::where('id', $code->organization_id)->first();
        $notifications = Notifications::where('organization_id', $organization->id)->get();

        foreach($notifications as $notification){
            $admin = Users::where('id', $notification->user_id)->first();

            $content ="
            $name from $organization->name has successfully activated their SCTPP access code.
            ";
            $this->send($admin->id, "New SCTPP Code Activated", "Code Activated", $content);
        }
    }
}