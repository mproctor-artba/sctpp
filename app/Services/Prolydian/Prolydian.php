<?php

namespace app\Services\Prolydian;

use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use App\Models\Result as Results;
use App\Models\User as Users;
use App\Models\Profile as Profiles;
use App\Models\Application as Applications;
use App\Models\Certificate as Certificates;
use Auth;
use DB;
use Inbox;
use Redirect; 
use Zipper;
use Storage;
use Accredible;
use File;

/*

The Disposition mappings are as follows:

Not scheduled = Authorized
Scheduled & Rescheduled = Scheduled
Delivered = Scored
No Show = No Show
Cancelled = Cancelled

*/

class Prolydian
{
	public function auth(){

        $client = new Client([
            'base_uri' => env('PROLYDIAN_TOKEN')
        ]);

		// This request authenticates the user

        $response = $client->request('POST', 'token', [
            'form_params' => [
                'username' => env('PROLYDIAN_USERNAME'),
                'password' => env('PROLYDIAN_PASSWORD'),
                'client_id' => 'achieve-rest-api',
                'grant_type' => 'password'
            ]
        ]);

	    $data = json_decode($response->getBody());
	    $token = $data->access_token;
	    
	    // Recreate the client with the Authorization header containing the token

	    $client = new Client([
        	'base_uri' => env('PROLYDIAN_URL'),
	        'headers' => ['Authorization' => 'Bearer ' . $token]
    	]);

	    return $client;
        
	}

	public function export(){

		$client = $this->auth();

		// define file parameters

        $start = date("m/d/Y");
        $end = date("m/d/Y", strtotime("+180 days"));
        $filelocation = public_path() . "/uploads/exports/";
        $filename = 'CDDexport-'.date('Y-m-d H.i.s').'.txt';
        $file_export  =  $filelocation . $filename;

        // CSV column names

        $csv_fields = array();
            
        $csv_fields[] = "ClientCandidateID";
        $csv_fields[] = "FirstName";
        $csv_fields[] = "LastName";
        $csv_fields[] = "MiddleName";
        $csv_fields[] = "Salutation";
        $csv_fields[] = "Email";
        $csv_fields[] = "Address1";
        $csv_fields[] = "Address2";
        $csv_fields[] = "Address3";
        $csv_fields[] = "City";
        $csv_fields[] = "State";
        $csv_fields[] = "PostalCode";
        $csv_fields[] = "Country";
        $csv_fields[] = "Phone";
        $csv_fields[] = "PhoneExtension";
        $csv_fields[] = "PhoneCountryCode";
        $csv_fields[] = "Fax";
        $csv_fields[] = "FaxExtension";
        $csv_fields[] = "Company Name";
        $csv_fields[] = "AuthorizationTransactionType";
        $csv_fields[] = "ClientAuthorizationID";
        $csv_fields[] = "ExamAuthorizationCount";
        $csv_fields[] = "ExamSeriesCode";
        $csv_fields[] = "EligibilityApptDateFirst";
        $csv_fields[] = "EligibilityApptDateLast";
        
        $data = fopen($file_export, 'w');
        fwrite($data, str_replace(",","\t",implode(",", $csv_fields)));

        foreach(Applications::where('status', 4)->orWhere('stage', 3)->get() as $export){
            $user = $export->user;
            $profile = $user->profile;
            $ClientAuthorizationID = "PTIARTBA" . generateRandomNumString(12);

            if($profile->firstExport()){
                DB::table('profiles')->where('user_id', $export->user_id)->update([
                    "ClientCandidateID" => "PTIARTBA" . generateRandomNumString(12),
                    "ClientAuthorizationID" => $ClientAuthorizationID
                ]);
            }

            $profile = Profiles::where('user_id', $export->user_id)->first();

            $csv_field[] = $profile->ClientCandidateID;
            $csv_field[] = str_replace(","," ",$profile->first_name);
            $csv_field[] = str_replace(","," ",$profile->last_name);
            $csv_field[] = str_replace(","," ",$profile->middle);
            $csv_field[] = "";
            $csv_field[] = $profile->user->email;
            $csv_field[] = str_replace(","," ",$profile->address);
            $csv_field[] = str_replace(","," ",$profile->address2);
            $csv_field[] = "";
            $csv_field[] = $profile->city;
            $csv_field[] = $profile->state;
            $csv_field[] = $profile->zip;
            $csv_field[] = "USA";
            $csv_field[] = $profile->phone;
            $csv_field[] = "";
            $csv_field[] = 1;
            $csv_field[] = "";
            $csv_field[] = "";
            $csv_field[] = str_replace(","," ",$profile->company);
            $csv_field[] = "A";
            $csv_field[] = $ClientAuthorizationID;
            $csv_field[] = 1;
            $csv_field[] = "SCTPP";
            $csv_field[] = $start;
            $csv_field[] = $end;

            fwrite($data, "\n" . str_replace(",","\t",implode(",", $csv_field)));
            unset($csv_field);
            unset($content);

            Inbox::new($profile->user_id, 8);

        }

        try {
            if(env('APP_SANDBOX') == 'disabled'){

        		$response = $client->request('POST', 'testadministrations/' . env('PROLYDIAN_ADMIN') . '/import-roster-cdd', 
        			[
        		        'multipart' => [            
        		            [
        		                'name'     => 'import_file',
        		                'contents' => fopen(public_path() ."/uploads/exports/$filename", 'r')
        		            ],
        		        ],
        		    ]);

        		$data = json_decode($response->getBody());

            }

        DB::table('applications')->where('status', 4)->update([
            "status" => 5 // exported
        ]);
        DB::table('applications')->where('stage', 3)->update([
            "stage" => 4 // exported and awaiting results
        ]);

    	} catch (RequestException $e) {
        echo 'Exception: ' . $e->getMessage();
        #throw $e;
    		echo "error!";
		}
	}

	public function import($start, $end){

		$client = $this->auth();
        $zipped = "";
        $continue = false;

		try {

            $zip = public_path() . '/uploads/results/Scored_Test_Takers_' . str_replace("/","-",$start) . '_' . str_replace("/","-",$end) . '_data';

            $response = $client->request('POST',
                'pearson/A2975554-8C1F-4EAA-AD4C-869629E41673/test-groups/ARTBA/performance-data', [

                'verify' => false,

                'json' => [

                'TransmittedAfter' => $start,

                'TransmittedBefore' => $end,

                ],

                'save_to' => $zip . ".zip"

            ]);

            $filesize = filesize($zip . ".zip");

            if($filesize > 0){
                try{
                    $zipper = new \Chumper\Zipper\Zipper;
                    $fileName = str_replace("/","-",$start) . '_' . str_replace("/","-",$end);
                    $zipper->make($zip . ".zip")->extractTo(public_path() . "/uploads/results/$fileName",array("$fileName.csv"));
                    $zipperStatus = $zipper->getStatus();
                    $zipper->close();

                    // move files
                    $parentDir = public_path() . "/uploads/results";
                    $dir = public_path() . "/uploads/results/$fileName";
                    $files = scandir($dir);
                    foreach ($files as $key => $value) {
                        if(array_key_exists(2, $files)){
                            rename($dir . "/" . $files[2], $parentDir . "/". "$fileName.csv");
                            $continue = true;
                            break;
                        }else{
                            echo "Deleting folder: $dir \n";
                        }
                        
                    }
                    File::deleteDirectory($dir);
                    
                    if($zipperStatus == "No error"){

                       echo "EXTRACT RESULT: $zipperStatus \n";
                       unlink($zip . ".zip");
                    } else {
                        unlink($zip . ".zip");
                    }
                } 
                catch (RequestException $e) {
                    print_r($e);
                }

            }
            else {
                echo "ERROR: $zip is empty \n";
                unlink($zip . ".zip");
            }


            if($continue){
                
                foreach(Storage::disk('exams')->files() as $exam){
                    if($exam != ".DS_Store"){
                        $line = str_getcsv(Storage::disk('exams')->get($exam), "\n");
                        
                        $i = 0;
                        foreach($line as $row){
                            $row = explode(",", $row);
                            if($i > 0 && array_key_exists(21, $row)){

                                $testID = $row[0];
                                $clientID = $row[2];
                                $authID = $row[21];
                                $score = $row[13];
                                $date = $row[10];

                            
                                if(Profiles::where('ClientCandidateID', $row[2])->count() == 1){
                                    // the test belongs to someone in our database
                                    echo "Analyzing record: " . $row[2] . "\n";

                                    if(Results::where('RegistrationID', $testID)->count() == 0){
                                        // only unique results will be allowed entry
                                        $profile = Profiles::where('ClientCandidateID', $row['2'])->first();
                                        $application = $profile->application;
                                        $grade = "PASS";

                                        echo $profile->first_name . " " . $profile->last_name . " testing record was found. \n";

                                        if($score < env('SCTPP_PASSING_SCORE')){
                                            $grade = "FAIL";
                                        }
                                        if($row['15'] == "True"){
                                            $grade = "NOSHOW";
                                        }

                                        $type = 0;

                                        if(isset($application) && $application->status == 8 && $application->stage == 3){
                                            // this exam result is for recertification
                                            $type = 1;
                                        }

                                        DB::table('results')->insert([
                                            'RegistrationID' => $row['0'], 
                                            'CandidateID' => $row['1'], 
                                            'ClientCandidateID' => $row['2'], 
                                            'TCID' => $row['3'], 
                                            'ExamSeriesCode' => $row['4'], 
                                            'ExamName' => $row['5'], 
                                            'ExamRevision' => $row['6'], 
                                            'Form' => $row['7'], 
                                            'ExamLanguage' => $row['8'], 
                                            'Attempt' => $row['9'], 
                                            'ExamDate' => $row['10'], 
                                            'TimeUsed' => $row['11'], 
                                            'PassingScore' => env('SCTPP_PASSING_SCORE'), 
                                            'Score' => $row['13'], 
                                            'Grade' => strtoupper($grade), 
                                            'NoShow' => strtoupper($row['15']), 
                                            'NDARefused' => $row['16'], 
                                            'Correct' => $row['17'], 
                                            'Incorrect' => $row['18'], 
                                            'Skipped' => $row['19'], 
                                            'Unscored' => $row['20'], 
                                            'ClientAuthorizationID' => $row['21'],
                                            'Voucher' => $row['22'],
                                            'D1_project_risk' => $row['23'],
                                            'D2_safety_plan' => $row['24'],
                                            'D3_op_plan' => $row['25'],
                                            'D4_eval' => $row['26'],
                                            'D5_incident' => $row['27'],
                                            'type' => $type
                                        ]);


                                        if(strtoupper($row['15']) == "TRUE"){
                                            // alert the user

                                            Inbox::new($profile->user_id, 10);

                                            // update the application

                                            DB::table('applications')->where('user_id', $profile->user_id)->update([
                                                'status' => 6 // no-show applicant, restesting needed
                                            ]);
                                        }

                                        if($grade == "FAIL"){
                                            // alert the user

                                            Inbox::new($profile->user_id, 9);

                                            // update the application

                                            DB::table('applications')->where('user_id', $profile->user_id)->update([
                                                'status' => 7 // failed applicant, restesting needed
                                            ]);

                                            echo $profile->first_name . " " . $profile->last_name . " failed their exam \n";
                                        }
                                        
                                        
                                        if($grade == "PASS"){
                                            if($type == 0){
                                                // issue the certificate

                                                echo $profile->user_id . " became certified \n";

                                                if(Certificates::where('user_id', $profile->user_id)->count() == 0){
                                                    

                                                    // test if the app is in dev mode or not

                                                    if(env('APP_ENV') == 'local'){
                                                        $cert = rand(9999,999999);
                                                    } else {
                                                        // this is their first certificate
                                                        $cert = Accredible::create("$profile->first_name $profile->last_name", $profile->user->email);
                                                    }
                                                    
                                                    DB::table('certificates')->insert(['user_id' => $profile->user_id, 'certificate' => $cert]);
                                                    echo $profile->first_name . " " . $profile->last_name . " awarded certificate $cert \n";

                                                    // alert the user

                                                    Inbox::new($profile->user_id, 11);

                                                    // update the application status

                                                    DB::table('applications')->where('user_id', $profile->user_id)->update([
                                                        'status' => 8 // passed applicant, prep for recertification
                                                    ]);
                                                    
                                                } else {
                                                    Accredible::renew($profile->user->certificate->certificate);
                                                    // alert the user

                                                    Inbox::new($profile->user_id, 12);
                                                }
                                            }
                                            elseif($type == 1){
                                                DB::table('applications')->where('user_id', $profile->user_id)->update([
                                                    'stage' => 4 // pending cert renewal by cron job on recert date
                                                ]);

                                                echo $profile->user_id . " became recertified \n";
                                            }
                                        }
                                    }

                                    if(array_key_exists(23, $row)){
                                        DB::table('results')->where('RegistrationID', $testID)->update([
                                            'D1_project_risk' => $row[23],
                                            'D2_safety_plan' => $row[24],
                                            'D3_op_plan' => $row[25],
                                            'D4_eval' => $row[26],
                                            'D5_incident' => $row[27]
                                        ]);
                                    }
                                }
                            }
                    
                            $i++;
                        }
                    }
                    // delete files

                    // cleanout the directory
                    $Allfiles = Storage::allFiles(public_path() . '/uploads/results');
                    // Delete Files
                    Storage::delete($Allfiles);
                }
            
            }
    	} catch (RequestException $e) {
            echo $e . "\n";
    	}
        
	}

    public function sandboxImport(){

        $client = new Client([
            'base_uri' => 'http://prodtest.prolydian.com/api/', 
            'debug' => true
        ]);

        try {
            // This request authenticates the user
            $response = $client->request('POST', 'token', [
                'verify' => false,
                'form_params' => [
                    'userName' => 'mproctor@artba.org',
                    'password' => 'Markus1234',
                    'confirmPassword' => 'Markus1234',
                    'grant_type' => 'password'
                ]
            ]);

            $data = json_decode($response->getBody());
            $token = $data->access_token;
            
            // Recreate the client with the Authorization header containing the token
            $client = new Client([
            'base_uri' => 'http://prodtest.prolydian.com/api/', 
            'debug' => true, 
                'headers' => ['Authorization' => 'Bearer ' . $token]
        ]);
            
            
            // This request exports filtered PearsonVue result data from the specified time range
            //  and saves it to the filesystem as "data.zip"
            //

            $zip = public_path() . '/uploads/results/Sandbox-Scored_Test_Takers' . rand(0,999) . 'data.zip';

            $response = $client->request('POST', 'pearson/A2975554-8C1F-4EAA-AD4C-869629E41673/test-groups/ARTBA/performance-data', [
                'verify' => false,
                'json' => [            
                    "TransmittedAfter" => "2010-01-1T00:00:00Z",
                    "TransmittedBefore" => "2020-04-31T00:00:00Z",
                ],
                'save_to' => $zip
            ]);

            echo "<br><br><br> " . $response->getStatusCode();

            Zipper::make($zip)->extractTo(public_path()  . '/uploads/results');

            

        } catch (RequestException $e) {
                echo 'Exception: ' . $e->getMessage();
                #throw $e;
        }
    }

    public function profiles(){
        
        $client = $this->auth();
        $profilesFound = [];
        try{
            $response = $client->request('POST', 'TestTakers/', 
                [
                'json' =>            
                    [
                        'SourceTestId' => '0365c46a-23f2-47be-93eb-a8a8522f6ecf',
                        'OnOrBefore'     => '2020-01-22T14:23:05.0005627Z'
                    ]
                ]
            );

            $records = json_decode($response->getBody(), true)["Records"];
            
            foreach($records as $record){
                $profile = Profiles::where("ClientCandidateID", $record["PearsonCandidate"]["ClientCandidateId"])->first();
                if(isset($profile)){
                    $user = $profile->user;
                    $scheduled = $record["PearsonScheduledOn"];

                    $disposition = $record["RegistrationStatus"];

                    if($disposition == "Scored"){
                        $disposition = $profile->user->currentStep();
                    }

                    DB::table('progress')->insert([
                        'user_id' => $profile->user_id,
                        'disposition' => $disposition,
                        'sent_date' => $record["PearsonCandidate"]["TransmittedToPearsonOn"],
                        "processed_date" => $record["PearsonCandidate"]["PearsonConfirmedOn"],
                        "location" => $record["ScheduledLocationName"],
                        "test_date" => $record["ScheduledFor"]
                    ]);
                    array_push($profilesFound, $profile->user_id);
                }
            }

        } catch (RequestException $e) {
            echo 'Exception: ' . $e->getMessage();
            #throw $e;
        }

        // update status of all profiles that do not have a candidate

        $profiles = Profiles::all();

        foreach ($profiles as $profile) {
            if(!in_array($profile->user_id, $profilesFound)){
                DB::table('progress')->insert([
                    'user_id' => $profile->user_id,
                    'disposition' => $profile->user->currentStep()
                ]);
            }
        }
    }
}