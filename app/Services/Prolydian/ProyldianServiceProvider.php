<?php

namespace app\Services\Prolydian;

use Illuminate\Support\ServiceProvider;

class ProlydianServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Prolydian', function($app) {
            return new Prolydian();
        });
    }
}