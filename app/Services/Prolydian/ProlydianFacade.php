<?php

namespace app\Services\Prolydian;

use Illuminate\Support\Facades\Facade;

class ProlydianFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Prolydian';
    }
}