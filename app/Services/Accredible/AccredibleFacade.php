<?php

namespace app\Services\Accredible;

use Illuminate\Support\Facades\Facade;

use Auth;
use DB;
use Mail;
use App\Models\Message as Messages;
use App\Models\User as Users;
use App\Models\Profile as Profiles;
use App\Models\Experience as Experiences;
use App\Models\Payment as Payments;
use App\Models\Certificate as Certificates;

class AccredibleFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Accredible';
    }

    public static function test(){
        echo "ready";
    }
    public static function create($name, $email){
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_URL, "https://api.accredible.com/v1/credentials");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    
    curl_setopt($ch, CURLOPT_POST, TRUE);
    
    $fields = json_encode(array(
    "credential" => array(
        "recipient" => array(
        "name" => $name,
        "email" => $email
        ),
    "group_name" => "SCTPP",
    "name" => "Safety Certification Transportation Project Professional Certification",
    "description" => "You passed your exam!",
    "issued_on" => date('Y-m-d'),
    "expired_on" => date('Y-m-d', strtotime('+3 years'))
    )));
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Content-Type: application/json",
      "Authorization: Token token=" . env('ACCREDIBLE_KEY')
    ));
    
    $response = curl_exec($ch);
    curl_close($ch);
    
    // format data
    
    $r = json_decode($response, true);
    $cert = [];
    foreach($r as $key => $value){
    $cert[$key] = $value;
    }
    
    return $cert["credential"]["id"];
}

public static function viewCredential($credentialID){
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.accredible.com/v1/credentials/$credentialID");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Content-Type: application/json",
      "Authorization: Token token=" . env('ACCREDIBLE_KEY')
    ));
    
    $response = curl_exec($ch);
    curl_close($ch);
    
    $r = json_decode($response, true);

    return $r;
}

public static function renew($credentialID){
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.accredible.com/v1/credentials/$credentialID");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    
    $fields = json_encode(array(
        "credential" => array(
            "approved" => TRUE,
            "expired_on" => date('Y-m-d', strtotime('+3 years'))
            )
    ));
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Content-Type: application/json",
      "Authorization: Token token=" . env('ACCREDIBLE_KEY')
    ));
    
    $response = curl_exec($ch);
    
    curl_close($ch);
    
    return $response;
}

    public static function allCerts(){ 
        $certs = [];
        for($i = 1; $i <= 20; $i++){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.accredible.com/v1/all_credentials?group_id=62424&page=$i");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              "Content-Type: application/json",
              "Authorization: Token token=" . env('ACCREDIBLE_KEY')
            ));

            $response = curl_exec($ch);
            $credentials = json_decode($response, true);
            curl_close($ch);

            foreach ($credentials["credentials"] as $credential) {
                array_push($certs, $credential);
            }
        }
        return $certs;
    }

    public static function allCertificates(){  
        $certs = [];
        for($i = 1; $i <= 20; $i++){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.accredible.com/v1/all_credentials?group_id=292714&page=$i");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              "Content-Type: application/json",
              "Authorization: Token token=" . env('ACCREDIBLE_KEY')
            ));

            $response = curl_exec($ch);
            $credentials = json_decode($response, true);
            curl_close($ch);

            foreach ($credentials["credentials"] as $credential) {
                array_push($certs, $credential);
            }
        }
        return $certs;
    }

    public static function validateCertDates(){
        $certificates = Certificates::where('status', 2)->get();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.accredible.com/v1/all_credentials?group_id=62424&page_size=306");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: Token token=" . env('ACCREDIBLE_KEY')
        ));

        $certificates = json_decode(curl_exec($ch), true);
        curl_close($ch);

        foreach ($certificates["credentials"] as $certificate) {

            $issued_on = $certificate["issued_on"];
            $credentialID = $certificate["id"];

            $sctppCert = Certificates::where('certificate', $credentialID)->first();
            $dateAdded = date('Y-m-d', strtotime($sctppCert->created_at));
            if($issued_on != $dateAdded){
                echo "<br> $credentialID issued on $issued_on but added on $dateAdded <br>";
            }
        }
    }

    public static function expireCert($credentialID){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.accredible.com/v1/credentials/$credentialID");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        
        $fields = json_encode(array(
            "credential" => array(
                "approved" => TRUE,
                "expired_on" => date('Y-m-d')
                )
        ));
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: Token token=" . env('ACCREDIBLE_KEY')
        ));
        
        $response = curl_exec($ch);
        
        curl_close($ch);
        
        return $response;
    }
}