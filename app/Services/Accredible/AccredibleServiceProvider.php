<?php

namespace app\Services\Accredible;

use Illuminate\Support\ServiceProvider;

class AccredibleServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Accredible', function($app) {
            return new Accredible();
        });
    }
}