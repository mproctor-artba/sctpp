<?php

namespace app\Services\Stripe;

use Illuminate\Support\Facades\Facade;

$public = base_path();
require_once($public . '/vendor/stripe/stripe-php/init.php');

\Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

use App\Models\Payment as Payments;
use App\Models\Application as Applications;
use DB;
use Auth;
use Email;


class StripeFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Stripe';
    }

    /*
        5 = Application and Testing Joint Fee
        2 = Retesting Fee
        6 = PDH Recert Fee
        7 = PDH to Exam Upgrade Fee
        8 = Recertification Exam Fee


        fees:   2.2% + .30 for non AMEX
                3.5% for amex
    */

    public static function testConnection(){
        echo "Status OK";
    }

    public static function getBalance(){
        foreach (\Stripe\Balance::retrieve()->available as $key => $value) {
            $balance[$key] = $value;
        }
        return $balance[0]->amount / 100;
    }

    public static function retrieveCard($customer, $cardID){
        $customer = $this->retrieveCustomer($customer);
        return $customer->sources->retrieve($cardID);
    }

    public static function createCharge($amount, $source, $last4){
        $fee = "9.10";

        $chargeParameters = [
            'amount' => $amount, 
            'currency' => 'usd',
            'source' => $source,
            'description' => "Safety Certification Application: " . getFullName(Auth::user()->id),
            'statement_descriptor' => "SCTPP Application Fee"
        ];

        $charge = \Stripe\Charge::create($chargeParameters);

        if(strtoupper($charge->source->brand) == "AMERICAN EXPRESS"){
            $fee = "14.00";
        }

        // save proof of transaction

        DB::table('payments')->insert([
            'user_id' => Auth::user()->id,
            'amount' => ($amount / 100),
            'token' => $charge->id,
            'type' => 5, // 5 = application and testing fee
            'last4' => $last4,
            'cardType' => strtoupper($charge->source->brand),
            'fee' => $fee,
            'captured' => 1
        ]);

        $invoiceNumber = DB::getPdo()->lastInsertId();

        Email::receipt(Auth::user()->id, $invoiceNumber);

        if(isset($charge->id)){
            return true;
        }
        
        return false;
    }

    public static function createChargeforAccessCodes($quantity, $source, $last4, $name, $email, $organization, $title){
        $amount = $quantity * 40000;
        $price = $quantity * 400;
        $fee = ($price * env("STRIPE_FEE")) + 0.30;

        $chargeParameters = [
            'amount' => $amount, 
            'currency' => 'usd',
            'source' => $source,
            'description' => "SCTPP Access Codes: $name",
            'statement_descriptor' => "SCTPP Access Codes"
        ];

        $charge = \Stripe\Charge::create($chargeParameters);

        if(strtoupper($charge->source->brand) == "AMERICAN EXPRESS"){
            $fee = $price * env("STRIP_AMEX_FEE");
        }

        // save proof of transaction

        DB::table('orders')->insert([
            'quantity' => $quantity,
            'name' => $name,
            'email' => $email,
            'organization' => $organization,
            'title' => $title,
            'amount' => ($amount / 100),
            'token' => $charge->id,
            'fee' => $fee,
            'last4' => $last4,
            'cardType' => strtoupper($charge->source->brand),
            'captured' => 1
        ]);

        $invoiceNumber = DB::getPdo()->lastInsertId();

        Email::accessCodesReceipt($email, $organization, $quantity, $invoiceNumber, $name);

        if(isset($charge->id)){
            return true;
        }
        
        return false;
    }

    public static function createRetestCharge($amount, $source, $last4){

        $fee = "3.60";

        $chargeParameters = [
            'amount' => $amount, 
            'currency' => 'usd',
            'source' => $source,
            'description' => "Safety Certification Testing Fee: " . getFullName(Auth::user()->id),
            'statement_descriptor' => "SCTPP Exam Fee"
        ];

        $charge = \Stripe\Charge::create($chargeParameters);

        if(strtoupper($charge->source->brand) == "AMERICAN EXPRESS"){
            $fee = "5.25";
        }

        // save proof of transaction

        DB::table('payments')->insert([
            'user_id' => Auth::user()->id,
            'amount' => ($amount / 100),
            'token' => $charge->id,
            'type' => 2, // 2 = testing fee
            'last4' => $last4,
            'cardType' => strtoupper($charge->source->brand),
            'fee' => $fee,
            'captured' => 1
        ]);

        $invoiceNumber = DB::getPdo()->lastInsertId();

        Email::retestReceipt(Auth::user()->id, $invoiceNumber);

        if(isset($charge->id)){
            return true;
        }
        
        return false;
    }

    public static function createRecertificationCharge($amount, $source, $last4){
        $fee = "3.60";

        $chargeParameters = [
            'amount' => $amount, 
            'currency' => 'usd',
            'source' => $source,
            'description' => "Safety Recertification Fee: " . getFullName(Auth::user()->id),
            'statement_descriptor' => "SCTPP Recert Fee"
        ];

        $charge = \Stripe\Charge::create($chargeParameters);

        if(strtoupper($charge->source->brand) == "AMERICAN EXPRESS"){
            $fee = "5.25";
        }

        $application = Applications::where('user_id', Auth::user()->id)->first();

        if($application->recert_option == 1){
            $type = 6;
        }
        if($application->recert_option == 2){
            $type = 8;
        }
        if($application->recert_option == 3){
            $type = 7;
        }

        // save proof of transaction

        DB::table('payments')->insert([
            'user_id' => Auth::user()->id,
            'amount' => ($amount / 100),
            'token' => $charge->id,
            'type' => $type, // defined at top of page
            'last4' => $last4,
            'cardType' => strtoupper($charge->source->brand),
            'fee' => $fee,
            'captured' => 1
        ]);

        $invoiceNumber = DB::getPdo()->lastInsertId();

        Email::recertReceipt(Auth::user()->id, $invoiceNumber, $application->recert_option);

        if(isset($charge->id)){
            return true;
        }
        
        return false;
    }

    public static function createCustomer($source, $last4, $cardID, $description){

        $customer = \Stripe\Customer::create(
            array(
                "description" => $description,
                "source" => $source,
                "email" => Auth::user()->email
            ));

        // save customer for our records

        DB::table('customers')->insert([
            'user' => Auth::user()->id,
            'customerID' => $customer->id,
            'source' => $source,
            'last4' => $last4,
            'cardID' => $cardID
        ]);   
    }

    public static function allInvoices(){
        
        $invoices = \Stripe\Invoice::all(["limit" => 50]);
        $invoiceList = [];
        $due = 0;
        $paid = 0;

        foreach ($invoices->data as $invoice) {
            if($invoice->status != "void" && $invoice->status != "draft"){
                array_push($invoiceList, $invoice);

                if($invoice->status == "paid"){
                    $paid += $invoice->amount_due/ 100;
                }else{
                    $due += $invoice->amount_due / 100;
                }
            }
        }

        return ["all" => $invoiceList, "due" => number_format($due, 2), "paid" => number_format($paid,2)];

    }

    public static function retrieveCustomer($customer){
        return \Stripe\Customer::retrieve($customer);
    }

    public static function updateCustomer($customer, $token, $cardnumber, $card){
        if (isset($token)){
          try {
            $cu = \Stripe\Customer::retrieve($customer); // stored in your application
            $cu->source = $token; // obtained with Checkout
            $cu->save();

            $success = "Your card details have been updated!";
          }
          catch(\Stripe\Error\Card $e) {

            // Use the variable $error to save any errors
            // To be displayed to the customer later in the page
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $error = $err['message'];
          }
          // Add additional error handling here as needed
        }

        DB::table('customers')->where('user', Auth::user()->id)->update([
            'source' => $token,
            'last4' => $cardnumber,
            'cardID' => $card
        ]);   
    }

    public static function captureCharge($chargeID){

        $ch = \Stripe\Charge::retrieve("$chargeID");

        $ch->capture();

        DB::table('payments')->where('token', $chargeID)->update([
            'captured' => 1
        ]);

        return $ch;

    }

    public static function lmsRevenue(){
        $oldSystem = 26550; // we switched API keys in November 2017
        $newSystem = 5770;
        $i = 0;
        $charges = [];
        $lastID = NULL;


        $ch = \Stripe\Charge::all(array("limit" => 100));

        foreach($ch->data as $charge){
            if (strpos($charge->description, 'register.artba.org') !== false && $charge->amount_refunded == 0) {
                $charges{$i} = ["amount" => [], "date" => [], "description" => [], "refunded" => []];
                $charges{$i}["amount"] = $charge->amount / 100;
                $charges{$i}["refunded"] = $charge->amount_refunded / 100;
                $charges{$i}["date"] = Date("m-d-Y", $charge->created);
                $charges{$i}["description"] = $charge->id;
                
            }   
            $i++;
            $lastID = $charge->id;
        }

        $ch = \Stripe\Charge::all(array("limit" => 100, "starting_after" => $lastID));

        foreach($ch->data as $charge){
            if(strpos($charge->description, 'register.artba.org') !== false) {
                $charges{$i} = ["amount" => [], "date" => [], "description" => [], "refunded" => []];
                $charges{$i}["amount"] = $charge->amount / 100;
                $charges{$i}["refunded"] = $charge->amount_refunded / 100;
                $charges{$i}["date"] = Date("m-d-Y", $charge->created);
                $charges{$i}["description"] = $charge->id;
                
            }   
            $i++;
            $lastID = $charge->id;
        }

        $lastID = 0;

        foreach ($charges as $charge) {
            $lastID += $charge["amount"];
        }

        return $lastID + 26550;
    }
}