<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Webhook as Webhooks;
use DB;

class WebhooksController extends Controller
{
    public function index(Request $request){
    	$payload = @file_get_contents('php://input');

    	$payload = json_decode($payload, true);

    	$data = $payload["event-data"];
    	$signature = $payload["signature"];
    	$subject = $severity = $reason = NULL;

    	if(array_key_exists("headers", $data["message"]) && array_key_exists("subject", $data["message"]["headers"])){
    		$subject = $data["message"]["headers"]["subject"];
    	}

    	if(array_key_exists("severity", $data)){
    		$severity = $data["severity"];
    		$reason = $data["reason"];
    	}

    	DB::table('mailgun')->insert([
			"token" => $signature["token"],
			"signature" => $signature["signature"],
			"severity" => $severity,
			"response" => $data["event"],
			"reason" => $reason,
			"email" => $data["recipient"],
			"subject" => $subject
		]);
    }
}
