<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as Users;
use App\Models\Profile as Profiles;
use App\Models\Application as Applications;
use App\Models\Experience as Experiences;
use App\Models\Certificate as Certificates;
use App\Models\Result as Results;
use App\Models\Organization as Organizations;
use App\Models\Code as Codes;
use Auth;
use Redirect;
use Session;
use DB;
use Email;
use Carbon;
use Prolydian;
use Exception;
use Zipper;
use Storage;
use Inbox;
use DateTime;
use Stripe;
use Edvance360;
use Accredible;

class PublicController extends Controller
{
    public function directory($output){
        // returns all active certificates
         header("Access-Control-Allow-Origin: *");
        $body = ["data" => []];

        $i = 0;
        foreach (Certificates::where('status', 2)->get() as $certificate) {

            $issued = new DateTime($certificate->created_at);
            $issued1 = $issued->format('m-d-Y');
            $issued2 = $issued->format('Ymd');
            $expiry = new DateTime($certificate->expires_on);
            $expiry1 = $expiry->format('m-d-Y');
            $expiry2 = $expiry->format('Ymd');
            $expiry3 = $expiry->format('Y-m-d');

            $today = new DateTime("now");
            $dateDiff = date_diff($today, $expiry);
            $dateDiff = $dateDiff->format('%R%a');
            $dateDiff = str_replace("+", "", $dateDiff);


            if($dateDiff >= 0){
                array_push($body["data"], array(
                $certificate->user->profile->last_name . ", " . $certificate->user->profile->first_name,
                "<span style='display:none;'>" . $issued2 . "</span>" . $issued1,
                "<span style='display:none;' diff='" . $dateDiff. "' >" . $expiry2 . "</span>" . $expiry1,
                "<a target='_blank' href='https://www.credential.net/" . $certificate->certificate . "'>https://www.credential.net/" . $certificate->certificate . "</a>"));
                $i++;
            }
        }


        echo json_encode($body, 128);
    }

    public function certificates($output){
        // returns all active certificates
         header("Access-Control-Allow-Origin: *");
        $body = ["data" => []];

        $i = 0;

        $certificates = Accredible::allCertificates();


        foreach ($certificates as $certificate) {

            $issued = new DateTime($certificate["issued_on"]);
            $issued1 = $issued->format('m-d-Y');
            $issued2 = $issued->format('Ymd');
            $expiry = new DateTime($certificate["expired_on"]);
            $expiry1 = $expiry->format('m-d-Y');
            $expiry2 = $expiry->format('Ymd');
            $expiry3 = $expiry->format('Y-m-d');

            $today = new DateTime("now");
            $dateDiff = date_diff($today, $expiry);
            $dateDiff = $dateDiff->format('%R%a');
            $dateDiff = str_replace("+", "", $dateDiff);

            array_push($body["data"], array(
            $certificate["recipient"]["name"],
            "<span style='display:none;'>" . $issued2 . "</span>" . $issued1,
            "<a target='_blank' href='" . $certificate['url'] . "'>" . $certificate['url'] . "</a>"));
            $i++;
        }


        echo json_encode($body, 128);
    }

    public function about(){
        return view('about');
    }

    public function verify($secret, $spoof){
        
        if(Experiences::where('secret', $secret)->where('status', 0)->where('deleted', 0)->count() == 1){
        	$experience = Experiences::where('secret', $secret)->first();

        	$data = [
        		"profile" => Profiles::where('user_id', $experience->user_id)->first(),
        		"experience" => $experience,

        	];

        	return view('application.verify', $data);
        }

        return "<h1 style='width:400px; margin:0 auto; margin-top:20%; text-align:center;'>Invalid or expired verification link!</h1>";
    }

    public function submitVerify($secret, $spoof){
        
        if(Experiences::where('secret', $secret)->count() == 1){
        	$experience = Experiences::where('secret', $secret)->first();

        	DB::table('experiences')->where('secret', $secret)->update([
        		'status' => 1 // verified
    		]);

            $data = [
                "profile" => Profiles::where('user_id', $experience->user_id)->first(),
                "experience" => $experience
            ];

        	return view('verifycomplete', $data);
        }
    }

    public function testverifyComplete(){
        $data = [
                "profile" => Profiles::where('user_id', 5)->first(),
                "experience" => Experiences::where('user_id', 5)->first(),
            ];

        return view('verifycomplete', $data);
    }

    public function confirmResetComplete($email){
        $resetCutOff = new Carbon('04-04-2018');

        //dd($resetCutOff);

        if(Users::where('email', $email)->where('created_at', '<', $resetCutOff)->where('resetComplete', 0)->count() == 1){
                return "reset";
        }
        
    }
    public function pushResetComplete($email){
        DB::table('users')->where('email', $email)->update([
            'resetComplete' => 1
        ]);
        
    }

    public function receipt(){
        Email::receipt(Auth::user()->id);
    }

    public function order(){
        $data = [ 
            "profile" => false,
            "organizations" => Organizations::all()
        ];

        if (Auth::check()) {
            $data["profile"] = Auth::user()->profile;
        }

        return view('order', $data);
    }

    public function submitOrder(Request $request){

        $organization = $request->organization;

        // if organization does not exist, create it

        if(!is_numeric($organization)){

            $prefix = str_replace(" ","",strtoupper(substr($request->organization, 0, 5)));

            DB::table('organizations')->insert([
                "name" => $organization,
                "prefix" => $prefix,
                "notes" => NULL,
                "active" => 1,
                "admin" => 0
            ]);

            $organization = DB::getPdo()->lastInsertId();
        }

        $charge = Stripe::createChargeforAccessCodes($request->quantity, $request->stripeToken, $request->cardnumber, $request->name, $request->email, $organization, $request->title);

        if($charge){
            Session::flash("confirmation","Order complete! Your receipt containing your access codes has been emailed to you.");

        }
        sleep(1);
        return Redirect::to('/order');
        
    }

    public function edvance(){
        Accredible::validateCertDates();
        //Prolydian::profiles();
    }

    public function customers(){
        Stripe::retrieveAllInvoices();
    }
}