<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as Users;
use App\Models\Certificate as Certificates;
use App\Models\Application as Applications;
use App\Models\Document as Documents;
use App\Models\Experience as Experiences;
use App\Models\Profile as Profiles;
use App\Models\Code as Codes;

use Edvance360;

class LMSController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	// pulls latest users from Edvance360
    	$edvanceData = Edvance360::data();

    	$data = [
    		"page" => "LMS",
            "title" => "LMS Console",
            "students" => $edvanceData->students,
            "courses" => $edvanceData->courses,
            "codes" => Codes::all()
    	];

    	return view('admin.lms.index', $data);
    }

    public function enroll(Request $request){

    }
}
