<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as Users;
use App\Models\Profile as Profiles;
use App\Models\Certificate as Certificates;
use App\Models\Application as Applications;
use App\Models\Result as Results;
use App\Models\Organization as Organizations;
use App\Models\Payment as Payments;
use App\Models\Code as Codes;
use App\Models\State as States;
use App\Models\Experience as Experiences;
use App\Models\Course as Courses;
use App\Models\Enrollment as Enrollment;
use App\Models\PDH as PDHs;
use App\Models\Decision as Decisions;
use App\Models\Order as Orders;
use App\Models\Progress as Progress;
use Auth;
use Stripe;
use Carbon;
use DB;
use Inbox;
use Redirect;
use Response;
use Accredible;
use Session;
use Prolydian;
use Edvance360;
use Email;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            "page" => "Analytics",
            "title" => "Overview",
            "results" => $this->barChartData(),
            "progress" => $this->pieChartData(),
            "certificates" => Certificates::all(),
            "applications" => Applications::all(),
            "examresults" => Results::count()
        ];
        
        return view('admin.index', $data);
    }

    public function performance(){
        // returns all necessary data for the performance section of the admin dashboard
        $months = [];
        if(is_object(Results::first())){
            $months = Results::first()->popularMonths();
        }

        $data = [
            "page" => "Analytics",
            "title" => "Performance",
            "results" => Results::all(),
            "certificates" => Certificates::all(),
            "applications" => Applications::all(),
            "start" => new Carbon('first day of this month'),
            "thirty" => Carbon::now()->subDays(30),
            "year" => new Carbon('first day of January ' . Date('Y')),
            "years" => $months,
            "states" => States::all(),
            "companies" => Profiles::companies(),
            "progress" => $this->pieChartData(),
            "examResultsChartData" => $this->barChartData()

        ];

        return view('admin.analytics.performance', $data);
    }

    public function demographics(){
        // returns all necessary data for the demographics section of the admin dashboard
        $users = new Users;
        $profiles = new Profiles;

        $data = [
            "page" => "Analytics",
            "title" => "Demographics",
            "users" => Users::where('admin', 0)->get(),
            "profiles" => Profiles::orderBy('created_at', 'DESC')->get(),
            "applications" => Applications::all(),
            "avgDaystoTest" => Profiles::first()->avgDaystoTest(),
            "expired" => Profiles::first()->expired(),
            "titles" => Profiles::where('title','!=', '')->groupBy('title')->get(['title']),
            "states" => Profiles::where('state','!=', '')->groupBy('state')->get(['state']),
            "step1" => $users::goalOne(),
            "step2" => $users::goalTwo(),
            "step3" => $users::goalThree(),
            "step4" => $users::goalFour(),
            "step5" => $users::goalFive(),
            "step6" => $users::goalSix(),
            "step7" => $users::goalSeven(),
            "step8" => $users::goalEight(),
            "step9" => $users::goalNine(),
            "step10" => $profiles::goalTen(),
            "complete" => $profiles::complete(),
            "organizations" => Organizations::all(),
            "companies" => Profiles::where('company','!=', '')->groupBy('company')->get(['company']),
            "codes" => Codes::all(),
            "start" => new Carbon('first day of this month'),
            "thirty" => Carbon::now()->subDays(30),
            "year" => new Carbon('first day of January ' . Date('Y'))
        ];

        return view('admin.analytics.demographics', $data);
    }

    public function financials(){
        // returns all necessary data for the financials section of the admin dashboard

        // We are pulling order records from WooCommerce that relate to SCTPP
/*
        $sales = json_decode(file_get_contents('https://store.artba.org/applications/SCTPP/stats.php?x6vuGA3cZuPkDR=zlQxTQLVNk3rOW'), true);
        $salesTotal = number_format($sales["product"][0]["total"],2 );
        $vouchersTotal = number_format($sales["product"][1]["total"],2 );
*/
        $data = [
            "page" => "Analytics",
            "title" => "Financials",
            "payments" => Payments::all(),
            "orders" => Orders::all()
            /*
            "codes" => Codes::all(),
            //"startYear2017" => new Carbon('first day of January 2017' . Date('Y')),
            //"endYear2017" => new Carbon('last day of December 2017' . Date('Y')),
            //"year2018" => new Carbon('first day of January' . Date('Y')),
            "invoices" => Stripe::allInvoices(),
            "lmsRevenue" => Stripe::lmsRevenue(),
            "salesTotal" => $salesTotal,
            "vouchersTotal" => $vouchersTotal*/
        ];

        return view('admin.analytics.financials', $data);
    }

    public function emails(){
        $data = [
            "page" => "Analytics",
            "title" => "Email Log",
            "emails" => DB::table('mailgun')->get()
        ];

        return view('admin.analytics.emails', $data);
    }

    public function sisense(){
        
        $data = [
            "page" => "Sisense",
            "title" => "Sisense"
        ];

        return view('admin.analytics.sisense', $data);
    }

    public function examResults(){
        $data = [
            "page" => "Users",
            "title" => "Exam Results"
        ];

        return view('admin.analytics.exam-results', $data);
    }

    public function latestResults($type = 0){

        $data = [
            "page" => "Users",
            "title" => "Exam Results"
        ];

        $profiles = Profiles::all();
        
        $results = [];

        foreach($profiles as $profile){
            if(!is_null(Results::where('ClientCandidateID', $profile->ClientCandidateID)->where('type', $type)->orderBy('date','DESC')->first())){
                array_push($results, Results::where('ClientCandidateID', $profile->ClientCandidateID)->where('type', $type)->orderBy('date','DESC')->first());
            }
        }

        $data = [
            "page" => "Users",
            "title" => "Exam Results",
            "results" => $results
        ];

        return view('admin.management.results', $data);
    }

    public function users(){
        // returns all user profiles in the system. Users without profiles will not appear here

        $data = [
            "page" => "Users",
            "title" => "All Users",
            "profiles" => Profiles::all()
        ];

        return view('admin.management.users', $data);
    }

    public function usersTable($page){
        $profiles = Profiles::paginate(4);

        $row = array("data" => []);
        foreach ($profiles as $profile) {
            if(!$profile->user->isAdmin()) {
                $CandidateID = $profile->ClientCandidateID;
                if(empty($CandidateID)){
                    $CandidateID = "n/a";
                }
                $newrow = [
                    "clientCandidateID" => $CandidateID,
                    "name" => $profile->first_name . " " . $profile->last_name,
                    "title" => $profile->title,
                    "company" => $profile->company,
                    "state" => $profile->state,
                    "progress" => $profile->user->currentStep(),
                    "joined" => '<span style="display: none;">' . convertTimestamp($profile->user->created_at, "tostring")  . '</span>' . convertTimestamp($profile->user->created_at) . '</td>',

                    "controls" => '<table >
                                        <tr style="background: none !important;">
                                            <td style="border:none !important;"><a href="mailto:' . $profile->user->email . '" class="text-center"><i class="fa fa-envelope-o"></i></a></td>
                                            <td style="border:none !important;"><a href="' . route('user', ['id' => $profile->user_id]) . '" class="text-center"><i class="fa fa-external-link"></i></a></td>
                                            </tr>
                                        </table>'
                ];
            }

            array_push($row["data"], $newrow);
        }
        
        return json_encode($row);
    }

    public function references(){
        // returns all user references in the system.

        $data = [
            "page" => "Users",
            "title" => "All References",
            "experiences" => Experiences::all()
        ];

        return view('admin.management.references', $data);
    }

    public function editReferences($id){
        // returns all user references in the system.

        $data = [
            "page" => "Users",
            "title" => "Edit Reference",
            "reference" => Experiences::find($id)
        ];

        return view('admin.management.reference-edit', $data);
    }

    public function pdhs(){
        // returns all user pdhs in the system.

        $data = [
            "page" => "Users",
            "title" => "All Professional Development Hours",
            "pdhs" => PDHs::orderBy('created_at','DESC')->get()
        ];

        return view('admin.management.pdhs', $data);
    }

    public function user($id){

        $user =  Users::find($id);
        $code = Codes::where('user_id', $id)->first();
        $email = $user->email;
        $courses = [];

        $data = [
            "page" => "Users",
            "title" => $user->profile->first_name . " " . $user->profile->last_name,
            "user" => $user,
            "documents" => $user->documents,
            "courses" => [],
            "code" => NULL
        ];


        foreach ($user->enrollment as $enrollment) {
            array_push($courses, Courses::find($enrollment->course_id)->name);
            $data["courses"] = $courses;
        }

        if(!array_filter($data["courses"]) && isset($code)){
            $data["code"] = $code->code;
            if(isset($code->courses)){
                foreach (explode(",", $code->courses) as $course) {
                    array_push($courses, Courses::find($course));
                    $data["courses"] = $courses;
                }
            }
        }



        return view('admin.management.user', $data);
    }

    public function editUser(Request $request){
        $profile = Users::find($request->user_id)->profile();

        $profile->update($request->except(['_token', 'submit']));

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "User updated success"
                ]);

        return Redirect::back();
    }

    public function applications(){
        // returns all necessary data for the applications section of the admin dashboard

        $data = [
            "page" => "Users",
            "title" => "Review Applications",
            "applications" => Applications::where('status', 2)->get(),
            "recertApplications" => Applications::where('recert_option','>=', 3)->where('stage','<', 3)->get()
        ];

        return view('admin.management.review', $data);
    }

    public function reviewHistory(){
        $data = [
            "page" => "Users",
            "title" => "Application Decision History",
            "decisions" => Decisions::all(),
        ];

        return view('admin.management.history', $data);
    }

    public function exam(){
        // returns all necessary data for the exams / exam results section of the admin dashboard

        $data = [
            "page" => "Users",
            "title" => "Examination",
            "exports" => Applications::where('status', 4)->get(), // 4 = approved and not exported
            "results" => Results::with('User')->where('type', 0)->get(),
            "recertResults" => Results::with('User')->where('type',1)->get(),
            "schedules" => Progress::where('disposition', "scheduled")->get()
        ];

        return view('admin.management.exam', $data);
    }

    public function organizations(){
        $data = [
            "page" => "Organizations",
            "title" => "All Organizations",
            "organizations" => Organizations::all(),
            "users" => Users::where('admin', 0)->get()
        ];

        return view('admin.organizations.manage', $data);
    }

    public function new_organization(Request $request){

        DB::table('organizations')->insert([
            "name" => $request->name,
            "prefix" => $request->prefix,
            "notes" => $request->notes,
            "admin" => $request->admin,
            "active" => 1
        ]);

        $orgID = DB::getPdo()->lastInsertId();

        for($i = 0; $i < $request->codes; $i++){
            DB::table('codes')->insert([
                "code" => strtoupper($request->prefix) . "-" . strtoupper(generateRandomString(12)),
                "organization_id" => $orgID
            ]);
        }

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "$request->name organization profile has been created"
                ]);

        return Redirect::route('new-organization');
    }

    public function organization($org){
        $organization = Organizations::where('id', $org)->first();
        $data = [
            "page" => "Organizations",
            "title" => "All Organizations",
            "organization" => $organization,
            "title" => $organization->name,
            "profiles" => Profiles::all(),
            "codes" => Codes::where('organization_id', $organization->id)->get(),
            "courses" => Courses::all()
        ];

        return view('admin.organizations.profile', $data);
    }

    public function organizationNotifications($organization){
        if(!notificationsEnabled($organization)){
            DB::table('notifications')->insert([
                'organization_id' => $organization,
                'user_id' => Auth::user()->id
            ]);
        } else {
            DB::table('notifications')->where('organization_id', $organization)->where('user_id', Auth::user()->id)->delete();
        }
    }

    public function updateOrganization(Request $request, $org){
        DB::table('organizations')->where('id', $org)->update([
            "name" => $request->name,
            "prefix" => $request->prefix,
            "notes" => $request->notes,
            "admin" => $request->admin
        ]);

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "$request->name organization has been updated"
                ]);

        return Redirect::back();
    }

    public function updateOrganizationCourses(Request $request, $org){
        if(!empty($request->organizationcourses)){
            DB::table('organizations')->where('id', $org)->update([
                'courses' => implode(",", $request->organizationcourses)
            ]);
        } else {
            DB::table('organizations')->where('id', $org)->update([
                'courses' => NULL
            ]);
        }
    }

    public function accessCodes(){
        // pulls latest users from Edvance360

        $data = [
            "page" => "Organizations",
            "title" => "Access Codes",
            "codes" => Codes::with('User')->get(),
            "organizations" => Organizations::all(),
            "courses" => Courses::all()
        ];

        return view('admin.organizations.codes', $data);
    }

    public function createCodes(Request $request){
        $organization = Organizations::where('id', $request->org)->first();

        for($i = 0; $i < $request->codes; $i++){
            if($request->course != ""){
                DB::table('codes')->insert([
                    "code" => strtoupper($organization->prefix) . "-" . strtoupper(generateRandomString(12)),
                    "organization_id" => $organization->id,
                    "courses" => implode(",", $request->course)
                ]);
            } else {
                DB::table('codes')->insert([
                    "code" => strtoupper($organization->prefix) . "-" . strtoupper(generateRandomString(12)),
                    "organization_id" => $organization->id
                ]);
            }
            
        }

        Session::flash("message", [
            "alert" => "success",
            "header" => "Success",
            "body" => "$request->codes codes were created for $organization->name"
        ]);

        return Redirect::back();
    }

    public function updateCourses(Request $request, $code){
        if(!empty($request->codecourses)){
            DB::table('codes')->where('code',$code)->update([
                'courses' => implode(",", $request->codecourses)
            ]);
        } else {
            DB::table('codes')->where('code',$code)->update([
                'courses' => NULL
            ]);
        }
        
    }

    public function deleteCode($code){
        DB::table('codes')->where('code', $code)->delete();
    }

    public function deleteReference($reference){
         DB::table('experiences')->where('id', $reference)->update([
            "deleted" => 1
        ]);
    }
    public function restoreReference($reference){
         DB::table('experiences')->where('id', $reference)->update([
            "deleted" => 0
        ]);
    }

    public function adminModifyReference(Request $request){

        $finished = "present";

        if($request->currently != 1){
            $finished = $request->endMonth . "/" . $request->endYear;
        }
        
        DB::table('experiences')->where('id', $request->referenceID)->update([
            "organization" => $request->organization,
            "start" => $request->startMonth . "/" . $request->startYear,
            "finished" => $finished,
            "experiences" => implode(",", $request->experiences),
            "reference" => $request->reference,
            "email" => $request->email,
            "phone" => $request->phone
        ]);

        Session::flash("message", [
            "alert" => "success",
            "header" => "Success",
            "body" => "You successfully modified this reference!"
        ]);

        return Redirect::back();
    }

    public function certificates(){
        $data = [
            "page" => "Users",
            "title" => "Certificates",
            "certificates" => Certificates::with('User')->get()
        ];

        return view('admin.management.certificates', $data);
    }

    public function export(){
        // this function allows the admin to manually generate a CSV export file for Prolydian. This is a backup incase the API is down

        // define file parameters

        $start = date("m/d/Y");
        $end = date("m/d/Y", strtotime("+180 days"));
        $filelocation = public_path() . "/uploads/exports/";
        $filename = 'CDDexport-'.date('Y-m-d H.i.s').'.txt';
        $file_export  =  $filelocation . $filename;

        // CSV column names

        $csv_fields = array();
            
        $csv_fields[] = "ClientCandidateID";
        $csv_fields[] = "FirstName";
        $csv_fields[] = "LastName";
        $csv_fields[] = "MiddleName";
        $csv_fields[] = "Salutation";
        $csv_fields[] = "Email";
        $csv_fields[] = "Address1";
        $csv_fields[] = "Address2";
        $csv_fields[] = "Address3";
        $csv_fields[] = "City";
        $csv_fields[] = "State";
        $csv_fields[] = "PostalCode";
        $csv_fields[] = "Country";
        $csv_fields[] = "Phone";
        $csv_fields[] = "PhoneExtension";
        $csv_fields[] = "PhoneCountryCode";
        $csv_fields[] = "Fax";
        $csv_fields[] = "FaxExtension";
        $csv_fields[] = "Company Name";
        $csv_fields[] = "AuthorizationTransactionType";
        $csv_fields[] = "ClientAuthorizationID";
        $csv_fields[] = "ExamAuthorizationCount";
        $csv_fields[] = "ExamSeriesCode";
        $csv_fields[] = "EligibilityApptDateFirst";
        $csv_fields[] = "EligibilityApptDateLast";
        
        $data = fopen($file_export, 'w');
        fwrite($data, str_replace(",","\t",implode(",", $csv_fields)));

        foreach(Applications::where('status', 4)->get() as $export){
            $user = $export->user;
            $profile = $user->profile;
            $ClientAuthorizationID = "PTIARTBA" . generateRandomNumString(12);

            if($profile->firstExport()){
                DB::table('profiles')->where('user_id', $export->user_id)->update([
                    "ClientCandidateID" => "PTIARTBA" . generateRandomNumString(12),
                    "ClientAuthorizationID" => $ClientAuthorizationID
                ]);
            }

            $profile = Profiles::where('user_id', $export->user_id)->first();

            $csv_field[] = $profile->ClientCandidateID;
            $csv_field[] = str_replace(","," ",$profile->first_name);
            $csv_field[] = str_replace(","," ",$profile->last_name);
            $csv_field[] = str_replace(","," ",$profile->middle);
            $csv_field[] = "";
            $csv_field[] = $profile->user->email;
            $csv_field[] = str_replace(","," ",$profile->address);
            $csv_field[] = str_replace(","," ",$profile->address2);
            $csv_field[] = "";
            $csv_field[] = $profile->city;
            $csv_field[] = $profile->state;
            $csv_field[] = $profile->zip;
            $csv_field[] = "USA";
            $csv_field[] = $profile->phone;
            $csv_field[] = "";
            $csv_field[] = 1;
            $csv_field[] = "";
            $csv_field[] = "";
            $csv_field[] = str_replace(","," ",$profile->company);
            $csv_field[] = "A";
            $csv_field[] = $ClientAuthorizationID;
            $csv_field[] = 1;
            $csv_field[] = "SCTPP";
            $csv_field[] = $start;
            $csv_field[] = $end;

            fwrite($data, "
" . str_replace(",","\t",implode(",", $csv_field)));
            unset($csv_field);
            unset($content);

            DB::table('applications')->where('user_id', $export->user_id)->update([
                "status" => 5 // exported
            ]);

            Inbox::new($profile->user_id, 8);

        }

        return Response::download(public_path() . "/uploads/exports/$filename");
    }

    public function exportLMS($organization){
        // this allows the admin to export a company's users to a CSV file for the Edvance360 system

        $codes = Codes::where('organization_id', $organization)->get();
        $organization = Organizations::where('id', $organization)->first();

        $filelocation = public_path() . "/uploads/lms/";
        $filename = 'LMS' . $organization->name . 'export-'.date('Y-m-d').'.csv';
        $file_export  =  $filelocation . $filename;

        // CSV column names

        $csv_fields = array();
            
        $csv_fields[] = "Id";
        $csv_fields[] = "Title";
        $csv_fields[] = "FirstName";
        $csv_fields[] = "MiddleName";
        $csv_fields[] = "LastName";
        $csv_fields[] = "Suffix";
        $csv_fields[] = "Email Address";
        $csv_fields[] = "Address";
        $csv_fields[] = "Address2";
        $csv_fields[] = "City";
        $csv_fields[] = "State";
        $csv_fields[] = "Zip";
        $csv_fields[] = "Country";
        $csv_fields[] = "HomePhone";
        $csv_fields[] = "WorkPhone ";
        $csv_fields[] = "CellPhone";
        $csv_fields[] = "UserType";
        $csv_fields[] = "Administrator";
        $csv_fields[] = "Username";
        $csv_fields[] = "Password";
        $csv_fields[] = "Delete";
        $csv_fields[] = "ParentId";
        $csv_fields[] = "UserTitle";
        $csv_fields[] = "CostCenter";
        $csv_fields[] = "Sites";
        $csv_fields[] = "CompanyName";
        $csv_fields[] = "HireDate";
        $csv_fields[] = "EmploymentType";

        $data = fopen($file_export, 'w');
                fwrite($data, implode(",", $csv_fields));

                $names = [];
                foreach($codes as $code){
                    if(is_object($code->user)){
                        array_push($names, $code->user->profile->first_name[0] . " " . $code->user->profile->last_name);
                    }
                }

                foreach($codes as $code){
                    if(is_object($code->user)){

                        $username = generateUsername($names, $code->user->profile->first_name, $code->user->profile->last_name);

        $csv_field[] = generateRandomString();
        $csv_field[] = "";
        $csv_field[] = $code->user->profile->first_name;
        $csv_field[] = $code->user->profile->middle;
        $csv_field[] = $code->user->profile->last_name;
        $csv_field[] = "";
        $csv_field[] = $code->user->email;
        $csv_field[] = $code->user->profile->address;
        $csv_field[] = $code->user->profile->address2;
        $csv_field[] = $code->user->profile->city;
        $csv_field[] = $code->user->profile->state;
        $csv_field[] = $code->user->profile->zip;
        $csv_field[] = "";
        $csv_field[] = "";
        $csv_field[] = $code->user->profile->phone;
        $csv_field[] = "";
        $csv_field[] = "Student";
        $csv_field[] = "";
        $csv_field[] = $username;
        $csv_field[] = "";
        $csv_field[] = "";
        $csv_field[] = "";
        $csv_field[] = "";
        $csv_field[] = "";
        $csv_field[] = "";
        $csv_field[] = $code->user->profile->company;
        $csv_field[] = "";
        $csv_field[] = "";


        fwrite($data, "\n" . implode(",", $csv_field));
                    unset($csv_field);
                    }
        }

        return Redirect::to("/uploads/lms/$filename");
    }

    public function importTestResults(Request $request){
        // prevent admin from uploading results
        

        $destination = public_path() . "/uploads/results";
        $file = $request->file('file');
        $fileName = generateRandomString() . "-" . $file->getClientOriginalName();
        $file->move($destination, $fileName); 

        $csv = file_get_contents("$destination/$fileName");

        $line = array_map('str_getcsv', file("$destination/$fileName"));
        $i = 0;
        
                
        foreach($line as $row){
            if($i > 0){
                $testID = $row[0];
                $clientID = $row[2];
                $authID = $row[21];
                $score = $row[13];
                $date = $row[10];
                $type = 0;
                
                if(Profiles::where('ClientCandidateID', $row[2])->count() == 1){
                    // the test belongs to someone in our database
                    echo "Analyzing record: " . $row[2] . "\n";

                    if(Results::where('RegistrationID', $testID)->count() == 0){
                        // only unique exam results will be allowed entry
                        $profile = Profiles::where('ClientCandidateID', $row['2'])->first();
                        $application = $profile->application;
                        $grade = "PASS";

                        echo $profile->first_name . " " . $profile->last_name . " testing record was found. \n";

                        if($score < env('SCTPP_PASSING_SCORE')){
                            $grade = "FAIL";
                        }
                        if($row['15'] == "TRUE"){
                            $grade = "NOSHOW";
                        }

                        if(isset($application)){
                            if($application->recert_option == 4 && $application->stage == 3){
                                $type = 1;
                            }
                            if($application->recert_option == 4 && $application->stage == 5){
                                // already recertified...just confirming with exam result if they can keep it
                                $type = 2;
                            }
                        }

                        DB::table('results')->insert([
                            'RegistrationID' => $row['0'], 
                            'CandidateID' => $row['1'], 
                            'ClientCandidateID' => $row['2'], 
                            'TCID' => $row['3'], 
                            'ExamSeriesCode' => $row['4'], 
                            'ExamName' => $row['5'], 
                            'ExamRevision' => $row['6'], 
                            'Form' => $row['7'], 
                            'ExamLanguage' => $row['8'], 
                            'Attempt' => $row['9'], 
                            'ExamDate' => $row['10'], 
                            'TimeUsed' => $row['11'], 
                            'PassingScore' => env('SCTPP_PASSING_SCORE'), 
                            'Score' => $row['13'], 
                            'Grade' => strtoupper($grade), 
                            'NoShow' => strtoupper($row['15']), 
                            'NDARefused' => $row['16'], 
                            'Correct' => $row['17'], 
                            'Incorrect' => $row['18'], 
                            'Skipped' => $row['19'], 
                            'Unscored' => $row['20'], 
                            'ClientAuthorizationID' => $row['21'],
                            'Voucher' => $row['22'],/*,
                            'D1_project_risk' => $row['23'],
                            'D2_safety_plan' => $row['24'],
                            'D3_op_plan' => $row['25'],
                            'D4_eval' => $row['26'],
                            'D5_incident' => $row['27'],*/
                            'type' => $type
                        ]);



                        if($grade == "NOSHOW"){
                            // no-show first-time applicant, restesting needed
                            
                            if($type == 0){
                                DB::table('applications')->where('user_id', $profile->user_id)->update(['status' => 6]);

                                Inbox::new($profile->user_id, 10);

                                echo $profile->first_name . " " . $profile->last_name . " failed to appear \n";
                            }

                            

                            if($type == 1 || $type == 2){
                                // recertification folks that fail to appear will need to retest
                                // DB::table('applications')->where('user_id', $profile->user_id)->update(['recert_option' => 2]);
                            }

                            if($type == 2){
                                // expire this person's certificate immediately
                                //Accredible::expireCert($profile->user->certificate->certificate);
                              //  echo $profile->first_name . " " . $profile->last_name . "'s certificate is now expired \n";
                            }
                        }

                        if($grade == "FAIL"){
                            if($type == 0){
                                // new candidate failed their exam
                                
                                Inbox::new($profile->user_id, 9);
                                DB::table('applications')->where('user_id', $profile->user_id)->update(['status' => 7]);
                                echo $profile->first_name . " " . $profile->last_name . " failed their exam \n";
                            }
                            
                            if($type == 1){
                                // recertification folks that fail will need to retest

                                //DB::table('applications')->where('user_id', $profile->user_id)->update(['status' => 7,'recert_option' => 2]);
                                
                                //Inbox::new($profile->user_id, 9);
                            }

                            if($type == 2){
                                // expire this person's certificate immediately

                                //Accredible::expireCert($profile->user->certificate->certificate);

                                // echo $profile->first_name . " " . $profile->last_name . "'s certificate is now expired \n";

                                // Inbox::new($profile->user_id, 9);
                            }                            
                        }
                        
                        
                        if($grade == "PASS"){
                            
                            if($type == 0){
                                if(Certificates::where('user_id', $profile->user_id)->count() == 0){
                                    // test if the app is in dev mode or not

                                    if(env('APP_ENV') == 'local'){
                                        $cert = rand(9999,999999);
                                    } else {
                                        // this is their first certificate
                                        $cert = Accredible::create("$profile->first_name $profile->last_name", $profile->user->email);
                                    }
                                    
                                    DB::table('certificates')->insert(['user_id' => $profile->user_id, 'certificate' => $cert]);
                                    
                                    echo $profile->first_name . " " . $profile->last_name . " became certified \n";

                                    // alert the user

                                    Inbox::new($profile->user_id, 11);

                                    // update the application status
                                    // passed applicant, prep for recertification
                                    DB::table('applications')->where('user_id', $profile->user_id)->update(['status' => 8]);
                                    
                                } else {

                                    //Accredible::renew($profile->user->certificate->certificate);
                                    //echo $profile->first_name . " " . $profile->last_name . " certificate has been renewed \n";

                                    // alert the user

                                    //Inbox::new($profile->user_id, 12);
                                }
                            }
                            if($type == 1){
                                // pending cert renewal by cron job on recert date
                                //DB::table('applications')->where('user_id', $profile->user_id)->update(['stage' => 4]);

                                //echo $profile->first_name . " " . $profile->last_name . " became scheduled to auto recertify \n";
                            }

                            if($type == 2){
                                
                            }
                        }
                    }

                    //DB::table('results')->where('RegistrationID', $testID)->update(['D1_project_risk' => $row[23],'D2_safety_plan' => $row[24],'D3_op_plan' => $row[25],'D4_eval' => $row[26],'D5_incident' => $row[27]]);
                }
            }
            $i++;
        }

                // update stage and level based on test result
        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);
    }

    public function barChartData(){
        $results = ["passed" => [], "failed" => [], "noshow" => []];
        for($i = 2016; $i <= 2021; $i++){
            $yearStart = new Carbon('first day of January ' . $i);
            $yearEnd = new Carbon('last day of December ' . $i);
            $passed[$i] = Results::where('Grade', 'PASS')->where('date','>=', $yearStart)->where('date','<=', $yearEnd)->count();
            $failed[$i] = Results::where('Grade', 'FAIL')->where('date','>=', $yearStart)->where('date','<=', $yearEnd)->count();
            $noshow[$i] = Results::where('Grade', 'NOSHOW')->where('date','>=', $yearStart)->where('date','<=', $yearEnd)->count();
            array_push($results["passed"] , $passed[$i]);
            array_push($results["failed"] , $failed[$i]);
            array_push($results["noshow"] , $noshow[$i]);
        }
        
        return $results;
    }

    public function pieChartData(){
        $applications = Applications::all();

        $Incomplete = $applications->where('status', 0)->count();
        $InProgress = $applications->where('status', 1)->count();
        $FinishingUp = $applications->where('status', 2)->count();
        $Denied = $applications->where('status', 3)->count();
        $AwaitingTestResults = $applications->where('status', 5)->count();
        $sum = $Incomplete + $InProgress + $FinishingUp + $Denied +$AwaitingTestResults;

        $results = [
        "values" => [ $Incomplete, $InProgress, $FinishingUp, $Denied, $AwaitingTestResults], 
        "percentages" => [ percent($Incomplete, $sum), percent($InProgress, $sum), percent($FinishingUp, $sum), percent($Denied, $sum), percent($AwaitingTestResults, $sum)] 
        ];


        return $results;
    }

    public function enrollStudent(Request $request){
        // double check to make sure student doesn't exist
        $data = Edvance360::data();
        $students = $data->students;
        $count = 0;
        $user = Users::find($request->user_id);

        if(newStudent($user->email, $students)){
            // create student profile
            Edvance360::newStudent($user);
        }
    }

    public function pdhDecide(Request $request){

        $pdh = PDHs::find($request->pdh_id);

        DB::table('pdh_decisions')->insert([
            "pdh_id" => $request->pdh_id,
            "user_id" => Auth::user()->id,
            "decision" => $request->decision,
            "reason" => $request->reason
        ]);


        DB::table('pdhs')->where('id', $request->pdh_id)->update([
            "status" => $request->decision
        ]);

        if($request->decision == 1){
            // rejected
            Inbox::new($pdh->user_id, 14, $request->reason);

            // email applicant

            Email::send($pdh->user_id, "Your PDH Evidence Has Been Rejected", "Your PDH Evidence Has Been Rejected.", $pdh->user->profile->first_name . ", <br>
We regret to inform you that the evidence you submitted for <strong>" . $pdh->name . "</strong> has not been approved to add " . $pdh->hours . " Professional Development Hours towards your recertification application due to the following reason(s):<br>
<i>" . $request->reason . "</i>
<br><br>
Please login to your <a href='https://portal.sctpp.org'>SCTPP account</a> to make necessary adjustments to your application. If you have any questions regarding this decision, you may contact certificationteam@sctpp.org
<br>
<br>
Sincerely,
<br>
ARTBA Foundation's Safety Certification Team
");

        }
        elseif ($request->decision == 2) {
            // approved
            Inbox::new($pdh->user_id, 15);

            // email applicant

            Email::send($pdh->user_id, "Your PDH Evidence Has Been Approved", "Your PDH Evidence Has Been Approved.", $pdh->user->profile->first_name . ", <br>
Your evidence for <strong>" . $pdh->name . "</strong> has been approved and we credited " . $pdh->hours . " Professional Development Hours (PDHs) towards your recertification application. We encourage you to continue earning your PDHs until you have reached 30 total hours and have satisfied the necessary hours for all five PDH categories.<br>
<br><br>
Please login to your <a href='https://portal.sctpp.org'>SCTPP account</a> to add more PDHs to your recertification application. If you have any questions, please contact certificationteam@sctpp.org
<br>
<br>
Sincerely,
<br>
ARTBA Foundation's Safety Certification Team
");

        }

        return Redirect::back();
    }

    public function getUserProgress($user_id){
        return Users::find($user_id)->currentStep();
    }

    public function verifyUser($method, $marker){
        if($method == "email"){
            if(Users::where('email', $marker)->count() > 0){
                $user = Users::where('email', $marker)->first();
                if(isset($user->certificate)){
                    return $user->id . "," . $user->certificate->certificate;
                } else{
                    return $user->id;
                }
            }
            return "false";
        }
    }

    public function verifyCertificate($method, $marker){
        if($method == "email"){

        }
    }

    public function deletedUsers(){
        // returns all deleted user profiles in the system. Users without profiles will not appear here. 

        $data = [
            "page" => "Users",
            "title" => "All Archived Users",
            "profiles" => Profiles::onlyTrashed()->get()
        ];

        return view('admin.management.deleted-users', $data);
    }

    public function deleteUser($id){
        $user = Users::findOrFail($id);

        $user->delete();

        $profile = Profiles::where('user_id', $id)->first();
        if(isset($profile)){
            $profile->delete();
        }
        
        $application = Applications::where('user_id', $id)->first();

        if(isset($application)){
            $application->delete();
        }

        $experiences = Experiences::where('user_id', $id)->get();

        foreach ($experiences as $experience) {
            $experience->delete();
        }

        Session::flash("message", [
            "alert" => "success",
            "header" => "Success",
            "body" => "This user has been deleted"
        ]);

        return Redirect::to('/admin/users');

    }

    public function restoreUser($id){
        Users::withTrashed()->find($id)->restore();

        $profile = Profiles::withTrashed()->where('user_id', $id)->first();
        if(isset($profile)){
            $profile->restore();
        }
        
        $application = Applications::withTrashed()->where('user_id', $id)->first();
        
        if(isset($application)){
            $application->restore();
        }

        $experiences = Experiences::withTrashed()->where('user_id', $id)->get();

        foreach ($experiences as $experience) {
            $experience->restore();
        }


        Session::flash("message", [
            "alert" => "success",
            "header" => "Success",
            "body" => "This user has been restored"
        ]);

        return Redirect::to('/admin/users/view/' . $id);
    }
}
