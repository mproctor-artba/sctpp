<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as Users;
use App\Models\Profile as Profiles;
use App\Models\Certificate as Certificates;
use App\Models\Application as Applications;
use App\Models\Result as Results;
use App\Models\Organization as Organizations;
use App\Models\Payment as Payments;
use App\Models\Code as Codes;
use App\Models\State as States;
use App\Models\Experience as Experiences;
use App\Models\Course as Courses;
use App\Models\Enrollment as Enrollment;
use App\Models\PDH as PDHs;
use Auth;
use Stripe;
use Carbon;
use DB;
use Inbox;
use Redirect;
use Response;
use Accredible;
use Session;
use Prolydian;
use Edvance360;
use Email;
use Sisense;
use DatePeriod;
use DateTime;
use DateInterval;


class TestingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function prolydianProfile()
    {
        //Prolydian::sandboxImport();
        Prolydian::profiles();
    }

    public function prolydianResults()
    {
        echo "Starting exam data pull \n";


        $period = new DatePeriod(
             new DateTime('2019-01-01'),
             new DateInterval('P7D'),
             new DateTime('2020-12-30')
        );

        foreach ($period as $key => $value) {
            $from = $value->format('m/d/Y');
            $to = date('m/d/Y', strtotime("+7 days", strtotime($from)));
            echo "From: $from To: $to \n";
            Prolydian::import($from, $to);
            
        }
    }

    public function edvance360Data(){

        $table = [];

        $results = Results::all();

        foreach ($results as $result) {
            $registration = $result->RegistrationID;

            echo $registration . ",D1: Assessing Project Risk," . $result->D1_project_risk . ",$result->Grade<br>";
            echo $registration . ",D2: Create Safety Plan Based on Project Risk Assessment," . $result->D2_safety_plan . ",$result->Grade<br>";
            echo $registration . ",D3: Implement Operational Safety Plan," . $result->D3_op_plan . ",$result->Grade<br>";
            echo $registration . ",D4: Conducting Ongoing Evaluation of Operational Safety Plan," . $result->D4_eval . ",$result->Grade<br>";
            echo $registration . ",D5: Conduct Incident Investigations," . $result->D5_incident . ",$result->Grade<br>";
        }

       
    }

    public function pdhStatus(){
        $apps = Applications::where('status', '>', 4)->where('stage', 1)->get();

        foreach($apps as $app){
            
            $hours = $app->user->PDHStatus();

            exposeArray($hours);
        }
    }

    public function sisenseUsers(){
        Prolydian::profiles();
        Sisense::users();
        Sisense::certs();
    }

    public function cleanupcerts(){
        //
    }

}