<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as Users;
use App\Models\Profile as Profiles;
use App\Models\Certificate as Certificates;
use App\Models\Application as Applications;
use App\Models\Result as Results;
use App\Models\Organization as Organizations;
use App\Models\Payment as Payments;
use App\Models\Code as Codes;
use Carbon;
use DB;
use Inbox;
use Redirect;
use Response;
use Accredible;
use Session;

class ConvertController extends Controller
{
	public function start(){
		$users = DB::table('users')->get();
		$applications = DB::table('applications')->where('status', '>=', 2)->get();
		foreach ($applications as $application) {
			DB::table('users')->where('id', $application->user_id)->update([
                "type" => 1
            ]);
		}
		echo "<h2>Applications</h2>";
		/*
		foreach ($users as $user) {
		 	$application = DB::table('safety_apps')->where('author', $user->id)->first();

		 	if(is_object($application)){

		 		$form = json_decode($application->form);
		 		$signature = NULL;

		 		if(isset($form->signature)){
		 			$signature = $form->signature;
		 		}

		 		echo "$application->id, $application->author, 1, NULL, $form->option, \"$signature\", \"$application->date\"<br>";
		 	}
		}

		
		echo "<h2>Documents</h2>";
		$d = 1;
		foreach ($users as $user) {
		 	$application = DB::table('safety_apps')->where('author', $user->id)->first();

		 	if(is_object($application)){
		 		// resume = 1, osha = 2, diploma = 3

		 		if(!empty($application->file) && $application->file != "./uploads/-"){
		 			echo "$d, $application->author, \"$application->file\", 1, 1, \"$application->date\"<br>";
		 			$d++;
		 		}
		 		if(!empty($application->file2) && $application->file2 != "./uploads/-"){
		 			echo "$d, $application->author, \"$application->file2\", 2, 1, \"$application->date\"<br>";
		 			$d++;
		 		}
		 		if(!empty($application->file3) && $application->file3 != "./uploads/-"){
		 			echo "$d, $application->author, \"$application->file3\", 3, 1, \"$application->date\"<br>";
		 			$d++;
		 		}
		 	}
		}

		/*
		

		echo "<h2>Employment</h2>";
		
		$i = 1;
		foreach ($users as $user) {
		 	$application = DB::table('applications')->where('user_id', $user->id)->first();

		 	if(is_object($application)){
		 		// resume = 1, osha = 2, diploma = 3
		 		$form = json_decode($application->form);
		 		if(isset($form->employer)){
			 		$e = 0;
			 		//dd($form);

			 		foreach($form->employer as $employment){
			 			//dd($form->employerName[$e]);
			 			if(!empty($form->employerName[$e])){
			 				$experiences = [];
			 				if(isset($form->employerExperience0)){
			 					$experiences = $form->employerExperience0;
			 				}

			 				echo "$i, \"NULL\", \"" . $form->employerName[$e] . "\", \"" . $form->employerPhone[$e] . "\", \"" . $form->employerEmail[$e] . "\", $application->user_id, \"" . $form->employer[$e] . "\", \"" . $form->beginMonth[$e] . "/" . $form->beginYear[$e] . "\", \"" . $form->endMonth[$e] . "/" . $form->endYear[$e] . "\", \"" . implode(",", $experiences) . "\", 1, \"$application->date\"<br>";
			 			// id, secret, reference, phone, email
			 			$e++;
			 			$i++;
			 			}
			 		}
			 		
		 		}
		 	}
		}
		/*

		echo "<h2>Codes</h2>";

		$codes = DB::table('safety_voucher_codes')->orderBy('code', 'asc')->get();
		$i = 1;
		foreach($codes as $code){
			if(DB::table('safety_voucher_uses')->where('code', $code->code)->count() == 1){
				$use = DB::table('safety_voucher_uses')->where('code', $code->code)->first();
				echo "$i, \"" . $code->code . "\", " . $code->org_id . ", " . $use->user_id . ", \"" . $code->date . "\", \"" . $use->date . "\"<br>";
					$i++;
			}
			else {
				//echo "<br>" . $code->code . "<br>";
				echo "$i, \"" . $code->code . "\", " . $code->org_id . ", \"NULL\", \"" . $code->date . "\", \"0000-00-00 00:00:00\"<br>";
				$i++;
			}
		}
		*/

	}
}