<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as Users;
use App\Models\Organization as Organizations;
use App\Models\Message as Messages;
use App\Models\Profile as Profiles;
use App\Models\State as States;
use App\Models\Code as Codes;
use Auth;
use Redirect;
use Session;
use Hash;
use DB;
use Inbox;
use Stripe;


class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $data = [
            "page" => "Account",
            "title" => "Overview",
            "user" => Users::find($id),
            "mine" => false,
            "organizations" => Profiles::where('company', '!=', '')->distinct()->get(['company']),
        "titles" => Profiles::where('title', '!=', '')->distinct()->get(['title']),
        "messages" => Inbox::all(Auth::user()->id),
        "profile" => Profiles::where('user_id', Auth::user()->id)->first(),
        "states" => States::all()
        ];

        if(Auth::user()->id == $id){
            $data["mine"] = true;
        }
        
        return view('profile.index', $data);
    }

    public function profile(){
        $data = [
        "page" => "profile",
        "title" => "Profile",
        "messages" => \app\Services\Inbox::all(Auth::user()->id),
        "profile" => Profiles::where('user_id', Auth::user()->id)->first(),
        "states" => States::all(),
        "organizations" => Profiles::where('company', '!=', '')->distinct()->get(['company']),
        "titles" => Profiles::where('title', '!=', '')->distinct()->get(['title'])
        ];

        
        return view('index', $data);
    }

    public function saveProfile(Request $request){
        if(Profiles::where()->count() == 1){

            DB::table('profiles')->update([
                "first_name" => $request->f_name,
                "last_name" => $request->l_name,
                "middle" => $request->middle,
                "address" => $request->address,
                "city" => $request->city,
                "state" => strtoupper($request->state),
                "zip" => $request->zip,
                "company" => $request->company,
                "title" => $request->title,
                "phone" => $request->phone
            ])->where('user_id', Auth::user()->id);

            return Redirect::route('profile');
        }

        DB::table('profiles')->insert([
                "user_id" => Auth::user()->id,
                "first_name" => $request->f_name,
                "last_name" => $request->l_name,
                "middle" => $request->middle,
                "address" => $request->address,
                "city" => $request->city,
                "state" => $request->state,
                "zip" => $request->zip,
                "company" => $request->company,
                "title" => $request->title,
                "phone" => $request->phone
        ]);

        return Redirect::route('eligibility');

    }

    public function updateEmail($email){
        if(Auth::user()->email != $email && $email != ""){
            // user is updating their email
            if(Users::where('id', '!=' ,Auth::user()->id)->where('email', $email)->count() == 0){
                // number of users that have this email (other than logged in user) should be 0
                DB::table('users')->where('id', Auth::user()->id)->update([
                    "email" => $email
                ]);

                Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "Your email has been updated to $email"
                ]);

                return true;
            }

            Session::flash("message", [
                    "alert" => "error",
                    "header" => "Error",
                    "body" => "This email is already in use"
                ]);


            return false;
        }
        return true;
    }

    public function updatePassword($current, $new){
 
        if (!(Hash::check($current, Auth::user()->password))) {
            // The passwords matches

            Session::flash("message", [
                "alert" => "error",
                "header" => "Error",
                "body" => "Your account password does not match what you provided"
            ]);

            return false;
        }
 
        if(strcmp($current, $new) == 0){
            //Current password and new password are same

            Session::flash("message", [
                "alert" => "error",
                "header" => "Error",
                "body" => "Your passwords did not match"
            ]);

            return false;
        }
 
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($new);
        $user->save();
 
        return true;
 
    }

    public function updateAccount(Request $request){

        if($this->updateEmail($request->email)){
            if(!empty($request->password)){
                if($this->updatePassword($request->password, $request->newpassword)){
                    Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "Your password has been updated"
                ]);
                }
            }
        }

        if(!empty($request->monitor)){
            DB::table('users')->where('id', Auth::user()->id)->update([
                    "organization_id" => $request->organization_id
            ]);

            Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "Progress monitoring has been enabled"
                ]);
        } else {
            DB::table('users')->where('id', Auth::user()->id)->update([
                    "organization_id" => null
            ]);

            Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "Progress monitoring has been disabled"
                ]);
        }

        return Redirect::route('home');
    }

    public function organizations(){
        $organization = Organizations::where('admin', Auth::user()->id)->first();

        $data = [
            "page" => "Profile",
            "title" => "My Organizations",
            "organization" => $organization,
            "monitoredUsers" => Users::where('organization_id', $organization->id)->get()
        ];

        return view('profile.organization.index', $data);
    }

    public function organization(){

    }

    public function orgdocuments(){
        $data = [
            "page" => "Document-Center",
            "title" => "My Organizations",
            "organization" => Organizations::where('admin', Auth::user()->id)->first()
        ];

        return view('profile.organization.documents', $data);
    }

    public function organalytics(){
        $organization = Organizations::where('admin', Auth::user()->id)->first();
        $data = [
            "page" => "Analytics",
            "title" => "My Organizations",
            "organization" => $organization,
            "passed" => $organization->passed(),
            "failed" => $organization->failed(),
            "noshows" => $organization->noshows(),
            "codes" => Codes::where('organization_id', $organization->id)->get(),
            "monitoredUsers" => Users::where('organization_id', $organization->id)->get()
        ];

        return view('profile.organization.analytics', $data);
    }

    public function orgbilling(){
        $data = [
            "page" => "Billing",
            "title" => "My Organizations",
            "organization" => Organizations::where('admin', Auth::user()->id)->first()
        ];

        return view('profile.organization.billing', $data);
    }

    public function orgsupport(){
        $data = [
            "page" => "Support",
            "title" => "My Organizations",
            "organization" => Organizations::where('admin', Auth::user()->id)->first()
        ];

        return view('profile.organization.support', $data);
    }

    public function user($user){
        $profile = ["name" => Profiles::where('user_id', $user)->first()->first_name . " " . Profiles::where('user_id', $user)->first()->last_name, "id" => Profiles::where('user_id', $user)->first()->user_id];
        return json_encode($profile);
    }

    public function newimport($user, Request $request){
        // upload this document with type "4"

        $destination = public_path() . "/uploads/documents";
        
        if($request->file('file') !== null){
            $document = $request->file('file');
            $documentName = str_replace(" ","-",generateRandomString() . "-" . $document->getClientOriginalName());
            $document->move($destination, $documentName); 

            DB::table('documents')->insert([
                "user_id" => $user,
                "name" => $documentName,
                "type" => 4, // resume = 1, osha = 2, diploma = 3, recert = 4
                "status" => 1 // active = 1, deleted = 0
            ]);
        }

        return "success";
    }


}