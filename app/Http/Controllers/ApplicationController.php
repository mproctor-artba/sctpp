<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User as Users;
use App\Models\Certificate as Certificates;
use App\Models\Application as Applications;
use App\Models\Document as Documents;
use App\Models\Experience as Experiences;
use App\Models\Profile as Profiles;
use App\Models\Code as Codes;
use App\Models\State as States;
use App\Models\PDH as PDH;
use Edvance360;
use DB;
use Auth;
use Response;
use Redirect;
use Session;
use Stripe;
use Email;
use Inbox;
use Prolydian;

class ApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    
    status definition

    0 = started and incomplete
    1 = signed
    2 = paid / pending
    3 = denied
    4 = approved
    5 = exported
    6 = no show
    7 = failed
    8 = passed

    recert_option definition

    1 = PDH
    2 = Exam
    3 = Paid PDH
    4 = Paid Exam

    stage definitions

    NULL = application started
    1 = recert application submitted and pending admin
    2 = denied
    3 = pending exam results
    4 = pending cert renewal
    5 = recertified
    */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function eligibility(){
        $data = [
            "page" => "Eligibility",
            "title" => "Select Eligibility",
            "application" => Auth::user()->application
        ];

        return view('application.eligibility', $data);
    }

    public function documents(){
        $data = [
            "page" => "Documents",
            "title" => "Manage Documents",
            "application" => Auth::user()->application,
            "documents" => Auth::user()->documents
        ];

        return view('application.documents', $data);
    }

    public function experience(){
        $data = [
            "page" => "Experience",
            "title" => "Verify Experience",
            "eligibility" => Auth::user()->application->eligibility,
            "experiences" => Auth::user()->experiences
        ];

        return view('application.experience', $data);
    }

    public function terms(){
        $data = [
            "page" => "Terms",
            "title" => "",
            "application" => Auth::user()->application
        ];

        return view('application.terms', $data);
    }

    public function submit(){

        if(Auth::user()->application->status > 1){
            return view('application.complete', [
                "page" => "Fees",
                "title" => ""
            ]);
        }

        return view('application.submit', [
            "page" => "Complete",
            "title" => ""
        ]);
    }

    public function complete(){
        $data = [
            "page" => "Complete",
            "title" => ""
        ];

        return view('application.complete', $data);
    }

    public function application($id){
        $application = Applications::find($id);
        
        $status = "";

        if($application->status == 2){
            $status = "<label class='label label-info'>Requires Review</label>";
        }
        elseif($application->status == 3){
            $status = "<label class='label label-danger'>Application Rejected</label>";
        }
        elseif($application->status >= 4){
            $status = "<label class='label label-success'>Application Approved</label>";
        }

    	$data = [
    		"page" => "Users",
    		"title" => "SCTPP Application for " . getFullName($application->user_id) . ": $status",
    		"application" => $application
    	];

    	return view('application.view', $data);
    }

    public function saveProfile(Request $request){
        if(Profiles::where('user_id', Auth::user()->id)->count() == 1){

            DB::table('profiles')->where('user_id', Auth::user()->id)->update([
                "first_name" => $request->f_name,
                "last_name" => $request->l_name,
                "middle" => $request->middle,
                "address" => $request->address,
                "address2" => $request->address2,
                "city" => $request->city,
                "state" => $request->state,
                "zip" => $request->zip,
                "company" => $request->company,
                "title" => $request->title,
                "phone" => $request->phone
            ]);

            Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "Your profile has been updated"
                ]);

            if(Auth::user()->isNew()){

                return Redirect::route('eligibility');
            }

            return Redirect::back();
        }

        DB::table('profiles')->insert([
            "user_id" => Auth::user()->id,
            "first_name" => $request->f_name,
            "last_name" => $request->l_name,
            "middle" => $request->middle,
            "address" => $request->address,
            "address2" => $request->address2,
            "city" => $request->city,
            "state" => $request->state,
            "zip" => $request->zip,
            "company" => $request->company,
            "title" => $request->title,
            "phone" => $request->phone
        ]);

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "Your profile has been updated"
                ]);

        Inbox::new(Auth::user()->id, '1');

        return Redirect::route('eligibility');

    }

    public function saveEligibility(Request $request){
        if(Applications::where('user_id', Auth::user()->id)->count() == 0){
            DB::table('applications')->insert([
                "user_id" => Auth::user()->id,
                "type" => 1, // certification app, 2 = recertification for now
                "status" => 0, // 0 = incomplete
                "eligibility" => $request->eligibility
            ]);
        } else {
            DB::table('applications')->where('user_id', Auth::user()->id)->update([
                "eligibility" => $request->eligibility
            ]);
        }

        return Redirect::route('documents');
    }


    public function upload(Request $request){
        // grab files based on eligibility selected

        $destination = public_path() . "/uploads/documents";

        $userID = Auth::user()->id;

        if(Auth::user()->isAdmin()){
            $application = Applications::find($request->appID);
            $userID = $application->user->id;
        }

        
        if($request->file('resume') !== null){
            $resume = $request->file('resume');
            $resumeName = cleanFileName(generateRandomString() . "-" . $resume->getClientOriginalName());
            $resume->move($destination, $resumeName); 

            DB::table('documents')->insert([
                "user_id" => $userID,
                "name" => $resumeName,
                "type" => 1, // resume = 1, osha = 2, diploma = 3
                "status" => 1 // active = 1, deleted = 0
            ]);
        }

        if($request->eligibility == 1){
            if($request->file('osha') !== null){

                $osha = $request->file('osha');
                $oshaName = cleanFileName(generateRandomString() . "-" . $osha->getClientOriginalName());
                $osha->move($destination, $oshaName); 

                DB::table('documents')->insert([
                    "user_id" => $userID,
                    "name" => $oshaName,
                    "type" => 2, // resume = 1, osha = 2, diploma = 3
                    "status" => 1 // active = 1, deleted = 0
                ]);

            }
            
        }
        elseif($request->eligibility == 2){

            if($request->file('diploma') !== null){
                $diploma = $request->file('diploma');
                $diplomaName = cleanFileName(generateRandomString() . "-" . $diploma->getClientOriginalName());
                $diploma->move($destination, $diplomaName); 

                DB::table('documents')->insert([
                    "user_id" => $userID,
                    "name" => $diplomaName,
                    "type" => 3, // resume = 1, osha = 2, diploma = 3
                    "status" => 1 // active = 1, deleted = 0
                ]);
            }
            
            if($request->file('osha') !== null){

                $osha = $request->file('osha');
                $oshaName = cleanFileName(generateRandomString() . "-" . $osha->getClientOriginalName());
                $osha->move($destination, $oshaName); 

                DB::table('documents')->insert([
                    "user_id" => $userID,
                    "name" => $oshaName,
                    "type" => 2, // resume = 1, osha = 2, diploma = 3
                    "status" => 1 // active = 1, deleted = 0
                ]);

            }

            
        }
        elseif($request->eligibility == 3) {
            if($request->file('diploma') !== null){
                $diploma = $request->file('diploma');
                $diplomaName = cleanFileName(generateRandomString() . "-" . $diploma->getClientOriginalName());
                $diploma->move($destination, $diplomaName); 

                DB::table('documents')->insert([
                    "user_id" => $userID,
                    "name" => $diplomaName,
                    "type" => 3, // resume = 1, osha = 2, diploma = 3
                    "status" => 1 // active = 1, deleted = 0
                ]);
            }
        }

        Session::flash("message", [
                "alert" => "success",
                "header" => "Success",
                "body" => "Your files were uploaded successfully"
            ]);

        if(empty($request->file('diploma')) && empty($request->file('resume')) && empty($request->file('osha'))){
            Session::flash("message", [
                "alert" => "error",
                "header" => "Error",
                "body" => "No files were uploaded"
            ]);
        }

        return Redirect::back();

    }

    public function saveExperience(Request $request){

        $secret = generateRandomString(); // ensures a secure link

        $finished = "present";

        if($request->currently != 1){
            $finished = $request->endMonth . "/" . $request->endYear;
        }

        DB::table('experiences')->insert([
            "user_id" => Auth::user()->id,
            "organization" => $request->organization,
            "start" => $request->startMonth . "/" . $request->startYear,
            "finished" => $finished,
            "experiences" => implode(",", $request->experiences),
            "reference" => $request->reference,
            "email" => $request->email,
            "phone" => $request->phone,
            "secret" => $secret
        ]);

        // send the notification email to the reference to verify
        Email::verification(Auth::user()->id, $secret);
        Inbox::new(Auth::user()->id, 2);

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "We sent " . $request->reference . " an email to confirm your work experience."
                ]);

        return Redirect::route('experience');
    }

    public function removeExperience($reference){
        DB::table('experiences')->where('id', $reference)->where('user_id', Auth::user()->id)->update([
            "deleted" => 1
        ]);
    }

    public function saveTerms(Request $request){
        DB::table('applications')->where('user_id', Auth::user()->id)->update([
            "signature" => $request->signature,
            "status" => 1
        ]);

        Inbox::new(Auth::user()->id, 3);

        return Redirect::route('submit');
    }

    public function verifyCode($code){
        if(Codes::where('code', $code)->count() == 1){
            // code exists, check if in use
            if(Codes::where('code', $code)->where('user_id', '!=', 0)->where('user_id', "!=" , Auth::user()->id)->where('expired', 0)->count() > 0){
                return "TAKEN";
            } else{
                return "OK";
            }
        }

        return "INVALID";
    }

    public function submitWaived($code){
        if($this->verifyCode($code) == "OK"){
            
            DB::table('codes')->where('code', $code)->update([
                "user_id" => Auth::user()->id,
                "used_on" => Date("Y-m-d H:m:s")
            ]);
            
            DB::table('applications')->where('user_id', Auth::user()->id)->update([
                "status" => 2 // paid application
            ]);
            DB::table('users')->where('id', Auth::user()->id)->update([
                "type" => 1
            ]);

            Inbox::new(Auth::user()->id, 4);

            // notify admins of code use

            $code = Codes::where('code',$code)->first();
        
            Email::codeNotify($code, Auth::user()->id);

            // check if code is tied to a course

            if($code->courses != ""){
                
            }

            return Redirect::route('complete');
        } 
        else {
            return Redirect::back();
        }
    }

    public function submitApplication(Request $request){

        $charge = Stripe::createCharge("40000", $request->stripeToken, $request->cardnumber);

        if($charge){
            DB::table('applications')->where('user_id', Auth::user()->id)->update([
                "status" => 2 // paid application
            ]);
            DB::table('users')->where('id', Auth::user()->id)->update([
                "type" => 1
            ]);

            Inbox::new(Auth::user()->id, 4);

            return Redirect::route('complete');
        }
    }

    public function decide(Request $request, $id){

        DB::table('decisions')->insert([
            'user_id' => Auth::user()->id,
            'app' => $id,
            'decision' => $request->decision,
            'reason' => $request->reason
        ]);

        $application = Applications::find($id);

        if($request->decision == 1){

            Inbox::new($application->user->id, 6);

            DB::table('applications')->where('id', $id)->update([
            'status' => 4 // approved
            ]);

            // send profile to Pearson

            

            Email::send($application->user->id, "Your SCTPP application has been approved!", "Your SCTPP application has been approved!", "Congratulations, " . $application->user->profile->first_name . "! <br>
We are pleased to inform you that your application for the ARTBA Foundation's Safety Certification for Transportation Project Professionals (SCTPP) has been approved. Soon, you will receive an email from Pearson with instructions on how to register for your exam. Please contact certificationteam@sctpp.org with any questions.
<br><br>
Again, congratulations!
<br>
Sincerely,
<br>
ARTBA Foundation's Safety Certification Team
");

            Prolydian::export();

            Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "This application has been approved"
                ]);
        }
        elseif($request->decision == 0){
            Inbox::new(Applications::where('id', $id)->first()->user_id, 7, $request->reason);

            DB::table('applications')->where('id', $id)->update([
                'status' => 3 // rejected
            ]);

            Email::send($application->user->id, "Your SCTPP application has been denied", "Your SCTPP application has been denied.", $application->user->profile->first_name . ", <br>
We regret to inform you that your application for the ARTBA Foundation's Safety Certification for Transportation Project Professionals (SCTPP) program was not approved for the following reason(s):<br>
<i>" . $request->reason . "</i>
<br><br>
Please login to your <a href='https://portal.sctpp.org'>SCTPP account</a> to make necessary adjustments to your application. If you have any questions regarding this decision, you may contact certificationteam@sctpp.org
<br>
<br>
Sincerely,
<br>
ARTBA Foundation's Safety Certification Team
");

            Session::flash("message", [
                    "alert" => "error",
                    "header" => "Denied",
                    "body" => "This application has been denied"
                ]);
        }

        return Redirect::back();
    }

    public function resubmit(){
        DB::table('applications')->where('user_id', Auth::user()->id)->update([
            'status' => 2 // pending
        ]);

        Inbox::new(Auth::user()->id, 5);

        return Redirect::route('complete');
    }

    public function retest(){
        $data = [
            "page" => "Retest",
            "title" => "",
            "attempts" => 3 - Profiles::where('user_id', Auth::user()->id)->first()->results->count()
        ];

        if(Applications::where('user_id', Auth::user()->id)->first()->status == 6 || Applications::where('user_id', Auth::user()->id)->first()->status == 7){
            return view('application.retest', $data);
        }

        return Redirect::route('home');
    }

    public function submitRetest(Request $request){
        $charge = Stripe::createRetestCharge("15000", $request->stripeToken, $request->cardnumber);

        if($charge){
            DB::table('applications')->where('user_id', Auth::user()->id)->update([
                "status" => 4 // approved application
            ]);

            return Redirect::route('complete');
        }
    }

    public function enroll(){
        $edvanceData = Edvance360::data();
        $courses = $edvanceData->courses;
        $code_courses = explode(",",Auth::user()->codes->first()->courses);
        $courseNames = [];

        foreach ($courses as $course) {
            foreach ($code_courses as $code_course) {
                if($course->identifier == $code_course){
                    array_push($courseNames, $course->name);
                }
            }
        }

        $data = [
            "page" => "enroll",
            "title" => "Enroll",
            "courseNames" => $courseNames
        ];

        return view('application.enroll', $data);
    }

    public function removeDocument($name){
        if(Auth::user()->isOrgAdmin() || Auth::user()->isAdmin()){
            DB::table('documents')->where('name', $name)->update([
                'status' => 0 // removed
            ]);
        } else {

            DB::table('documents')->where('user_id', Auth::user()->id)->where('name', $name)->update([
                'status' => 0 // removed
            ]);
        }

        return "OK";
    }



    public function sendReminder($secret){
        $experience = Experiences::where('secret', $secret)->first();
        $user = Users::find($experience->user_id);
        Email::verification($user->id, $secret);
        Inbox::new($user->id, 2);

        DB::table('experiences')->where('secret', $secret)->update([
                'last_contacted' => Date("Y-m-d H:m:s")
        ]);
    }

    public function recertify(){

        $data = [
            "page" => "recertify",
            "title" => "Recertification",
            "states" => States::all()
        ];

        return view('application.recertification.index', $data);
    }

    public function recertifyPDHForm(){
        $data = [
            "page" => "recertifyPDF",
            "title" => "Manage PDHs for Recertification",
            "states" => States::all(),
            "pdhs" => Auth::user()->pdhs,
            "letters" => [1 => "A", 2 => "B", 3 => "C", 4 => "D", 5 => "E"]
        ];

        $option = Auth::user()->application->recert_option;

        if($option == 1 || $option == 3){
            return view('application.recertification.pdh', $data);
        }

        Session::flash("message", [
            "alert" => "error",
            "header" => "Error",
            "body" => "The PDH option is not available for your profile. If you need assistance changing, please contact certificationteam@sctpp.org"
        ]);

        return Redirect::route('recertify-experience');
        
    }

    public function getPDHForm(){
        $data = [
            "states" => States::all()
        ];

        return view('layouts.pdh', $data);
    }

    public function resumeApp(){
        $step = Auth::user()->currentRecertStep();

        switch ($step) {
            case $step == 10:
                return Redirect::route('recertify');
                break;
            case $step == 11:
                return Redirect::route('recertify-checkout');
                break;
            case $step == 12:
                return Redirect::route('recertify-experience');
                break;
            case $step == 13:
                return Redirect::route('recertify-pdh');
                break;
            default:
                return Redirect::back();
                break;
        }
    }

    public function recertifyEligibility(Request $request){
        $application = Applications::where('user_id', Auth::user()->id)->first();
        $errors = true;
        if($application->recert_option != 2){
            DB::table('applications')->where('user_id', Auth::user()->id)->update([
                'recert_option' => $request->recertOption
            ]);
            $errors = false;
        }
        elseif($application->recert_option == 1 && $request->recertOption == 2){
            DB::table('applications')->where('user_id', Auth::user()->id)->update([
                'recert_option' => 2
            ]);
            $errors = false;
        }
        
        if(!$errors){

            Session::flash("message", [
                "alert" => "success",
                "header" => "Success",
                "body" => "Your profile has been updated"
            ]);

            return Redirect::route('recertify-checkout');
        }
        Session::flash("message", [
                "alert" => "error",
                "header" => "Error",
                "body" => "You cannot switch to Option 1"
        ]);
        return Redirect::back();
    }

    public function getCheckout(){
            $application = Applications::where('user_id', Auth::user()->id)->first();

            $data = [
                "page" => "checkout",
                "title" => "Submit Payment",
                "application" => $application
            ];

            return view('application.recertification.checkout', $data);
    }

    public function payCheckout(Request $request){
        $application = Applications::where('user_id', Auth::user()->id)->first();

        if($application->recert_option == 1){
            $charge = Stripe::createRecertificationCharge("19000", $request->stripeToken, $request->cardnumber);

            if($charge){
                DB::table('applications')->where('user_id', Auth::user()->id)->update([
                    "recert_option" => 3 // paid pdh recertify application
                ]);
            }
        }
        if($application->recert_option == 2) {
            $charge = Stripe::createRecertificationCharge("35000", $request->stripeToken, $request->cardnumber);

            if($charge){
                DB::table('applications')->where('user_id', Auth::user()->id)->update([
                    "recert_option" => 4 // paid test recertify application
                ]);
            }
        }

        if($charge){
            Session::flash("message", [
                "alert" => "success",
                "header" => "Success",
                "body" => "Thank you, your payment was successful!"
            ]);
        }

        return Redirect::route('recertify-experience');
    }

    public function recertifyExperience(){
        $data = [
            "page" => "Experience",
            "title" => "Verify Experience",
            "eligibility" => Auth::user()->application->recert_option,
            "experiences" => Auth::user()->recertExperiences
        ];

        return view('application.recertification.experience', $data);
    }

    public function saveRecertifyExperience(Request $request){
        $secret = generateRandomString(); // ensures a secure link

        $finished = "present";

        if($request->currently != 1){
            $finished = $request->endMonth . "/" . $request->endYear;
        }

        DB::table('experiences')->insert([
            "user_id" => Auth::user()->id,
            "organization" => $request->organization,
            "start" => $request->startMonth . "/" . $request->startYear,
            "finished" => $finished,
            "experiences" => implode(",", $request->experiences),
            "reference" => $request->reference,
            "email" => $request->email,
            "phone" => $request->phone,
            "secret" => $secret,
            "type" => $request->type
        ]);

        // send the notification email to the reference to verify
        Email::verification(Auth::user()->id, $secret);
        Inbox::new(Auth::user()->id, 2);

        Session::flash("message", [
                    "alert" => "success",
                    "header" => "Success",
                    "body" => "We sent " . $request->reference . " an email to confirm your work experience."
                ]);

        return Redirect::route('recertify-experience');
    }

    public function recertifySubmit(){
        $data = [
            "page" => "Experience",
            "title" => "Submit Application for Recertification",
            "eligibility" => Auth::user()->application->recert_option,
            "experiences" => Auth::user()->recertExperiences,
            "pdhs" => Auth::user()->pdhs
        ];

        return view('application.recertification.submit', $data);
    }

    public function savePDHform(Request $request){

        $destination = public_path() . "/uploads/pdhs";

        if($request->file('activityDocument') !== null){

            $activityDocument = $request->file('activityDocument');
            $activityDocumentName = generateRandomString() . "-" . $activityDocument->getClientOriginalName();
            $activityDocument->move($destination, $activityDocumentName); 

            PDH::updateOrCreate([
                "id" => $request->pdhID
                ],[
                "user_id" => Auth::user()->id,
                "name" => $request->activityName,
                "activityDate" => date("Y-m-d", strtotime($request->activityDate)),
                "city" => $request->city,
                "state" => $request->state,
                "organization" => $request->sponsor,
                "contactName" => $request->contactName,
                "phone" => $request->telephone,
                "email" => $request->email,
                "type" => $request->activityType,
                "file" => $activityDocumentName,
                "hours" => $request->pdhsEarned,
                "status" => 0
            ]);

            return 200;
        } else {
            PDH::updateOrCreate([
                "id" => $request->pdhID
                ],[
                "user_id" => Auth::user()->id,
                "name" => $request->activityName,
                "activityDate" => date("Y-m-d", strtotime($request->activityDate)),
                "city" => $request->city,
                "state" => $request->state,
                "organization" => $request->sponsor,
                "contactName" => $request->contactName,
                "phone" => $request->telephone,
                "email" => $request->email,
                "type" => $request->activityType,
                "hours" => $request->pdhsEarned,
                "status" => 0
            ]);
            return 200;
        }

        return 400;
    }

    public function removePDHform($id){
        DB::table('pdhs')->where('id', $id)->update([
            'deleted' => 1
        ]);
    }

    public function submitRecertification(Request $request){
        $application = Auth::user()->application;

        DB::table('applications')->where('id', $application->id)->update([
            "stage" => 1 // ready for recert decision
        ]);

        return view('application.recertification.complete', [
            "page" => "Complete",
            "title" => ""
        ]);
    }

    public function recertificationApplication($id){

        $application = Applications::find($id);
        $pdhs = PDH::where('user_id', $application->user_id)->where('deleted', 0)->get();
        $pdhApprovedSum = $pdhs->where('status',2)->sum('hours');
        $pdhallhours = $pdhs->sum('hours');
        $experiences = Experiences::where('user_id', $application->user_id)->where('type', 2)->get();



        $data = [
            "page" => "Recertification Application",
            "title" => "Recertification Application for " . getFullName($application->user_id),
            "application" => $application,
            "pdhs" => $pdhs,
            "pdhStatus" => [],
            "experiences" => $experiences,
            "approvedSum" => $pdhApprovedSum,
            "allhours" => $pdhallhours
        ];


        return view('application.recertification.view', $data);
    }

    public function decideRecertification(Request $request, $id){

        DB::table('decisions')->insert([
            'user_id' => Auth::user()->id,
            'app' => $id,
            'decision' => $request->decision,
            'reason' => $request->reason,
            'type' => 2 // 2 = recert 1 = normal
        ]);

        $application = Applications::find($id);
        $applicant = Users::find($application->user_id);

        if($request->decision == 1){

            // send profile to Pearson

            $expirationDate = Date("m-d-Y", $applicant->certificate->expiration()); // gets current expiration of certificate

            if($application->recert_option == 3){
                Inbox::new($application->user_id, 18, $expirationDate);
                $customMessage = "Your certificate will automatically renew on: $expirationDate.";

                DB::table('applications')->where('id', $application->id)->update([
                    "stage" => 4 // pending renewal
                ]);
            }
            elseif($application->recert_option == 4){
                Inbox::new($application->user_id, 19, $expirationDate);
                $customMessage = "Please take the exam before $expirationDate.";

                DB::table('applications')->where('id', $application->id)->update([
                    "stage" => 3 // pending test results
                ]);

                Prolydian::export();
            }

            Email::send($application->user->id, "Your SCTPP recertification application has been approved!", "Your SCTPP recertification application has been approved!", "Congratulations, " . $application->user->profile->first_name . "! <br>
                We are pleased to inform you that your recertification application for the ARTBA Foundation's Safety Certification for Transportation Project Professionals (SCTPP) has been approved. $customMessage
                <br><br>
                Again, congratulations!
                <br>
                Sincerely,
                <br>
                ARTBA Foundation's Safety Certification Team
                ");

            Session::flash("message", [
                "alert" => "success",
                "header" => "Success",
                "body" => "This application has been approved"
            ]);
        }
        elseif($request->decision == 0){

            DB::table('applications')->where('id', $application->id)->update([
                "stage" => 2// denied
            ]);
            
            Inbox::new($application->user_id, 17, $request->reason);

            Email::send($application->user_id, "Your SCTPP recertification application has been denied", "Your SCTPP recertification application has been denied.", $application->user->profile->first_name . ", <br>
                We regret to inform you that your recertification application for the ARTBA Foundation's Safety Certification for Transportation Project Professionals (SCTPP) program was not approved for the following reason(s):<br>
                <i>" . $request->reason . "</i>
                <br><br>
                Please login to your <a href='https://portal.sctpp.org'>SCTPP account</a> to make necessary adjustments to your recertification application. If you have any questions regarding this decision, you may contact certificationteam@sctpp.org
                <br>
                <br>
                Sincerely,
                <br>
                ARTBA Foundation's Safety Certification Team
                ");

            Session::flash("message", [
                "alert" => "error",
                "header" => "Denied",
                "body" => "This application has been denied"
            ]);
        }

        return Redirect::back();
    }
}