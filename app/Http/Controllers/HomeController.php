<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message as Messages;
use App\Models\Profile as Profiles;
use App\Models\State as States;
use Redirect;
use Auth;
use Inbox;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!sandbox() && Auth::user()->isAdmin()){
            return Redirect::to('/admin');
        }

        if(Auth::user()->isOrgAdmin()){
            return Redirect::route('myorgs');
        }

        $data = [
            "page" => "profile",
            "title" => "Profile",
            "messages" => Inbox::all(Auth::user()->id),
            "profile" => Profiles::where('user_id', Auth::user()->id)->first(),
            "states" => States::all(),
            "organizations" => Profiles::where('company', '!=', '')->distinct()->get(['company']),
            "titles" => Profiles::where('title', '!=', '')->distinct()->get(['title'])
        ];

        return view('index', $data);
    }
}
