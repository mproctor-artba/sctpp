<?php

use Carbon\Carbon;

use App\Models\User as Users;
use App\Models\Profile as Profiles;
use App\Models\Organization as Organizations;
use App\Models\Notification as Notifications;
use App\Models\Experience as Experiences;
use App\Models\Code as Codes;

function sandbox(){
    if(env('APP_SANDBOX') == 'enabled'){
        return true;
    }
    return false;
}

function getFullName($id)
{
    $profile = Profiles::where('user_id', $id)->first();
    return $profile->first_name . " " . $profile->last_name;
}

function convertHTMLTime($time)
{
    return date("m-d-Y", strtotime($time));
}

function convertTimestamp($stamp, $pull = null)
{
    if($pull == "month"){
        return date('m', strtotime($stamp));
    }
    if($pull == "monthyear"){
        return date('m-Y', strtotime($stamp));
    }
    if($pull == "monthdayyear"){
        return date('m-d-Y', strtotime($stamp));
    }
    if($pull == "tostring"){
        return strtotime($stamp);
    }
    if($pull == "yearmonthday"){
        return date('Y-m-d', strtotime($stamp));
    }
    if($pull == "unix"){
        return date('Y-m-d 00:00:00', strtotime($stamp));
    }
    return date('M. j, Y', strtotime($stamp));
}

function DateDifference($start, $end){
    $datetime1 = new DateTime($start);
    $datetime2 = new DateTime($end);
    $interval = $datetime1->diff($datetime2);
    return $interval->format('%R%a');
}

function convertHTMLTime2($date){
    $parts = explode('-',$date);
    $yyyy_mm_dd = $parts[1] . '-' . $parts[2] . '-' . $parts[0];
    return $yyyy_mm_dd;
}

function month($m){
    $monthNum  = $m;
    $dateObj   = DateTime::createFromFormat('!m', $monthNum);
    return $dateObj->format('F'); // March
}

function exposeArray($array){
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function sumPDHArray(&$array){
    $sumArray = ["total" => 0, "completed" => 0];
    $i = 1;
    foreach ($array as $k=>$subArray) {
        $sumArray["total"] += $subArray["b$i"];
        $sumArray["completed"] += $subArray["c$i"];
        $i++;
    }

    return $sumArray;
}

function validateEmail($email){
    if(Users::where('id','!=', Auth::user()->id)->where('email', $email)->count() == 0){
        return true;
    }
    return false;
}

function newStudent($email, $roster){
    $count = 0;

    foreach ($roster as $student) {
        if($student->email == $email){
            $count++;
        }
    }
    if($count > 0){
        return false;
    }

    return true;
}

function getCompanyAdmin($company){
    $organization = Organizations::where('name', $company)->where('admin', '>', 0)->first();

    if(isset($organization)){
        return ["company" => $organization, "email" => Users::find($organization->admin)->email, "profile" => Profiles::where('user_id', $organization->admin)->first()];
    }

    return null;
}

function companyEmployeeCount($company){
    return Profiles::where('company', $company)->count();
}

function usersByOrgCode($organization){
    return Codes::where('organization_id', $organization)->where('user_id','!=','0')->count();
}

function lastContacted($experience_id){
    $experience = Experiences::find($experience_id);
    if(is_null($experience->last_contacted)){
        return convertTimestamp($experience->date);
    }
    return convertTimestamp($experience->last_contacted);
}

function notificationsEnabled($organization){
    if(Notifications::where('organization_id', $organization)->where('user_id', Auth::user()->id)->count() > 0){
        return true;
    }
    return false;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateRandomNumString($length = 10) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateUsername(&$usernames, $fname, $lname){
    $username = str_replace(" ","",$fname[0] . "" . $lname);

    for($i = 1; $i <= 9; $i++){
        if(!in_array($username, $usernames)){
            array_push($usernames, $username);
            break;
        } else {
            $username = str_replace(" ","",$fname[0] . "" . $lname . "0" . $i);
            if(!in_array($username, $usernames)){
                break;
            }
        }
    }

    return strtolower($username);
}

function percent($value, $sum){
    return round((($value/$sum) * 100)) . "%";
}

function cleanFileName($string){
    return str_replace(" ", "-", $string);
}

function convertRecertActivity($i){
    switch ($i) {
        case $i == 1:
            return "College-level classroom course in relevant material completed with passing grade";
            break;
        case $i == 2:
            return "Continuing Education Course complete with certificate";
            break;
        case $i == 3:
            return "Short courses, tutorials and distance-education courses through video, webinar or internet learning";
            break;
        case $i == 4:
            return "Attended qualifying seminars, industry courses, workshops, or professional presentations, meetings, conventions or conferences";
            break;
        case $i == 5:
            return "Presented at qualifying seminars, industry courses, workshops, or professional presentations, meetings, conventions or conferences";
            break;
        case $i == 6:
            return "Published paper or article on transportation construction health and safety or traffic control and roadway maintenance";
            break;
        case $i == 7:
            return "Published book on safety- transportation, construction, environmental, occupational";
            break;
        default:
            return "";
            break;
    }
}

function convertRecertCategory($i){
    switch ($i) {
        case $i == 1:
            return 1;
            break;
        case $i > 1 && $i < 4:
            return 2;
            break;
        case $i == 4:
            return 3;
            break;
        case $i > 4 && $i < 8:
            return 4;
            break;
        case $i > 7:
            return 5;
            break;
        default:
            return 0;
            break;
    }
}


function convertRecertContent($i){
    switch ($i) {
        case $i == 1:
            return "Transportation Construction Health and Safety";
            break;
        case $i == 2:
            return "Traffic Control";
            break;
        case $i == 3:
            return "Maintenance of roadways for users";
            break;
        case $i == 4:
            return "Management skills- project, people or organizational";
            break;
        case $i == 5:
            return "Incident Investigation and Corrective Measures";
            break;
        case $i == 6:
            return "Emergency preparedness";
            break;
        case $i == 7:
            return "Emergency management";
            break;
        case $i == 8:
            return "Transportation construction issues";
            break;
        case $i == 9:
            return "Environmental regulations";
            break;
        case $i == 10:
            return "OSHA regulations";
            break;
        case $i == 11:
            return "New construction technology";
            break;
        case $i == 12:
            return "Creating health and safety training";
            break;
        default:
            return "";
            break;
    }
}

