<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('FetchExamData:fetch')
                 ->everyMinute();
        $schedule->command('UserReminders:fetch')
                 ->everyMinute();
        $schedule->command('reporting:weekly-summary')
                 ->everyMinute();
        $schedule->command('FetchEnrollment:fetch')
                 ->everyMinute();
        $schedule->command('FetchProfileData:fetch')
                 ->everyMinute();
        $schedule->command('FetchCertificates:fetch')
                 ->hourly();
        $schedule->command('backup:clean')->daily()->at('01:00');
        $schedule->command('backup:run')->daily()->at('02:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
