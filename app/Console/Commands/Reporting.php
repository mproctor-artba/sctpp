<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use App\Result as Results;
use App\Models\User as Users;
use App\Profile as Profiles;
use App\Application as Applications;
use App\Certificate as Certificates;
use App\Code as Codes;
use Carbon\Carbon;
use Prolydian;
use Zipper;
use Storage;
use DB;
use Inbox;
use DatePeriod;
use DateTime;
use DateInterval;
use MailChimp;
use Email;

class Reporting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reporting:weekly-summary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will email admins a summary of system activity and system status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
              $years = [2016,2017,2018,2019, 2020, 2021];
        $users = Users::where('admin', 0)->get();
        $applications = Applications::all();
        $exams = Results::all();
        $certificates = Certificates::all();
        $codes = Codes::all();
        $lastWeek = new Carbon('last week');
       
       $usersbyYear = [
            2016 => $users->where('created_at', '>=', '2016-01-01 00:00:00')->where('created_at', '<=', '2016-12-31 00:00:00'),
            2017 => $users->where('created_at', '>=', '2017-01-01 00:00:00')->where('created_at', '<=', '2017-12-31 00:00:00'),
            2018 => $users->where('created_at', '>=', '2018-01-01 00:00:00')->where('created_at', '<=', '2018-12-31 00:00:00'),
            2019 => $users->where('created_at', '>=', '2019-01-01 00:00:00')->where('created_at', '<=', '2019-12-31 00:00:00'),
            2020 => $users->where('created_at', '>=', '2020-01-01 00:00:00')->where('created_at', '<=', '2020-12-31 00:00:00'),
            2021 => $users->where('created_at', '>=', '2021-01-01 00:00:00')->where('created_at', '<=', '2021-12-31 00:00:00')
          ];

       $applicationsbyYear = [
        2021 => $applications->where('date', '>=', '2021-01-01 00:00:00')->where('date', '<=', '2021-12-31 00:00:00')
       ];

       $examResultsByYear = [
         2021 => $exams->where('date', '>=', '2021-01-01 00:00:00')->where('date', '<=', '2021-12-31 00:00:00')
       
       ];

       $certificatesByYear = [
         2021 => $certificates->where('created_at', '>=', '2021-01-01 00:00:00')->where('created_at', '<=', '2021-12-31 00:00:00')
       ];

       $codesByYear = [
         2021 => $codes->where('used_on', '>=', '2021-01-01 00:00:00')->where('used_on', '<=', '2021-12-31 00:00:00')
       ];
       
       $usersSinceLastWeek = $users->where('created_at', '>=', $lastWeek);
       $applicationsSinceLastWeek = $applications->where('date', '>=', $lastWeek);
       $examsSinceLastWeek = $exams->where('date', '>=', $lastWeek);
       $certificatesSinceLastWeek = $certificates->where('created_at', '>=', $lastWeek);
       $codesSinceLastWeek = $codes->where('used_on', '>=', $lastWeek);

       $content = "
       <table class='centered-cells' style='width:100%'>
            <thead>
                <tr>
                    <th width='50%'></th>
                    <th class='text-center'>YTD</th>
                    <th class='text-center'>Since <br> Last Week</th>
                </tr>
            <thead>
            <tbody>
       ";


       $content .= "
        <tr>
            <td style='width:50%'><strong>Total Users</strong></td>
            <td class='text-center'>" . $usersbyYear["2021"]->count() . "</td>
            <td class='text-center'>" . $usersSinceLastWeek->count() . "</td>
        </tr>
       ";
       $content .= "
        <tr>
            <td style='width:50%'><strong>Total Applications</strong></td>
            <td class='text-center'>" . $applicationsbyYear["2021"]->count() . "</td>
            <td class='text-center'>" . $applicationsSinceLastWeek->count() . "</td>
        </tr>
       ";
       $content .= "
        <tr><td colspan='3' style='border-bottom:1px solid black;'></td></tr>
       ";

       $content .= "
        <tr>
            <td style='width:50%'><strong>Exam Activity</strong></td>
            <td colspan='2'>
            </td>
        </tr>
        <tr>
            <td style='width:50%' class='text-indent-left'><strong>Failed</strong></td>
            <td class='text-center'>" . $examResultsByYear['2021']->where('Grade','FAIL')->count() . "</td>
            <td class='text-center'>" . $examsSinceLastWeek->where('Grade','FAIL')->count() . "</td>
        </tr>
        <tr>
            <td style='width:50%'  class='text-indent-left'><strong>No Show</strong></td>
            <td class='text-center'>" . $examResultsByYear['2021']->where('Grade','NOSHOW')->count()  . "</td>
            <td class='text-center'>" .$examsSinceLastWeek->where('Grade','NOSHOW')->count()  . "</td>
        </tr>
        <tr>
            <td style='width:50%'  class='text-indent-left'><strong>Passed</strong></td>
            <td class='text-center'>" . $examResultsByYear['2021']->where('Grade','PASS')->count() . "</td>
            <td class='text-center'>" . $examsSinceLastWeek->where('Grade','PASS')->count() . "</td>
        </tr>

       ";
       $content .= "
        <tr><td colspan='3' style='border-bottom:1px solid black;'></td></tr>
       ";

       $content .= "
        <tr>
            <td style='width:50%'><strong>Codes Activated</strong></td>
            <td class='text-center'>" . $codesByYear[2021]->count() . "</td>
            <td class='text-center'>" . $codesSinceLastWeek->count() . "</td>
        </tr>
       ";
       $content .= "
        <tr><td colspan='3' style='border-bottom:1px solid black;'></td></tr>
       ";

       foreach($codesSinceLastWeek as $code){
            $profile = $code->profile;
            $content .= "
                <tr>
                    <td style='width:50%; font-size:12px; border-bottom: 1px solid whitesmoke;'>" . getFullName($code->user_id) . ": " . $code->profile->company . "</td>
                    <td class='text-center' style='font-size:12px; border-bottom: 1px solid whitesmoke;'>" . $code->code  . "</td>
                    <td class='text-center' style='font-size:12px; border-bottom: 1px solid whitesmoke;'>" . convertTimestamp($code->used_on) . "</td>
                </tr>
            ";

       }

       
       $content .= "
       </tbody>
       </table>
       ";

        foreach(array(5,21,220,250,705) as $adminID){
            $subject = "SCTPP Weekly Summary Since " . convertTimestamp($lastWeek, "monthdayyear");
            Email::send($adminID, $subject , $subject, $content);
            sleep(1);
        }
    }
}
