<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Accredible;
use App\Certificate as Certificates;

class FetchCertificates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FetchCertificates:Fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will pull all certificate data from accredible.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $certs = Accredible::allCerts();
        $i = 0;
        foreach ($certs as $cert) {
            if($cert["id"] != 14392476){
                $certificate = Certificates::where('certificate', $cert["id"])->first();

                if(!is_null($certificate)){
                    $certificate->update([
                        "created_at" => convertTimestamp($cert["issued_on"], "unix"),
                        "expires_on" => convertTimestamp($cert["expired_on"], "unix")
                    ]);

                    $i++;
                } else {
                    dd($cert);
                }
            }
        }
        echo "$i Certificates updated";
    }
}
