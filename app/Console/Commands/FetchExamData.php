<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use App\Result as Results;
use App\Models\User as Users;
use App\Profile as Profiles;
use App\Application as Applications;
use Accredible;
use Prolydian;
use Zipper;
use Storage;
use DB;
use Inbox;
use DatePeriod;
use DateTime;
use DateInterval;
use MailChimp;

class FetchExamData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FetchExamData:Fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will pull all exam data from Prolydian and import unique results.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // create date array to pull 15 days worth of data beginning installation of cron

        

        echo "Starting exam data pull \n";


        $period = new DatePeriod(
             new DateTime('2021-01-01'),
             new DateInterval('P7D'),
             new DateTime('2021-12-31')
        );

        foreach ($period as $key => $value) {
            $from = $value->format('m/d/Y');
            $to = date('m/d/Y', strtotime("+7 days", strtotime($from)));
            echo "From: $from To: $to \n";
            Prolydian::import($from, $to);
        }


        // update mailing lists

        MailChimp::updateCandidatesExamStatus();

        

        echo "Exam pull complete \n";

        $applications = Applications::where('status',8)->where('stage', 4)->get();
        // all candidates that are due for renewal on their certificate

        foreach($applications as $application){
            $certificate = Accredible::viewCredential($application->user->certificate->certificate);

            echo $certificate["credential"]["id"] . " is due for recert on " . $certificate["credential"]["expired_on"] . " \n";

            if($certificate["credential"]["expired_on"] == Date("Y-m-d")){
                
                $application->stage = 5;
                $application->save();

                Accredible::renew($certificate["credential"]["id"]);

                echo "Credential:" . $certificate["credential"]["id"] . " has been renewed";
            }
        }

        // get latest disposition after clearing what is in the database

        DB::table('progress')->delete();

        Prolydian::profiles();

        echo "Profile refresh complete \n";
    }
}
