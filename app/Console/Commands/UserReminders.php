<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use App\Result as Results;
use App\Models\User as Users;
use App\Profile as Profiles;
use App\Application as Applications;
use Prolydian;
use Storage;
use DB;
use Inbox;
use DatePeriod;
use DateTime;
use DateInterval;
use Email;
use MailChimp;

class UserReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UserReminders:Fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will organize users by inactivity type and send appropriate reminders.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // these reminders target users on the 15th and 30th of every month
        $i = 0;
        DB::enableQueryLog();
        ini_set('memory_limit', '-1');
        $noApplication = 0;
        $stoppedAtStepOne = 0;
        $awaitingReferences = 0;
        $awatingExam = 0;

        foreach(Users::where('admin', 0)->get() as $user){
            $inactive = false;
            $age = $user->age();

            if(!isset($user->profile)){
                $inactive = true;
                
                if($age <= 180){
                    Email::applicationReminder($user->email, 1);
                    $noApplication++;
                }
            }
            else {

                //
                $step = $user->currentStepNum();

                if(!isset($user->application)){
                        if($age <= 365){
                            $profile = $user->profile;
                            Email::applicationReminder($user->email, 2);
                            $stoppedAtStepOne++;
                        }
                }
                elseif($step == 6){
                    // awaiting references
                    Email::applicationReminder($user->email, 3);
                    $awaitingReferences++;
                }
                elseif($step >= 7 && $step <= 9 ){
                        // awaiting test results
                        $profile = $user->profile;
                        Email::applicationReminder($user->email, 4);
                        $awatingExam++;
                }
            }

            unset($profile);
        }
       
       // send summary to admin(s)
$today = Date("m-d-Y");
$totalReminders = $noApplication + $stoppedAtStepOne + $awaitingReferences + $awatingExam;
$content = "
<table style='width:100%;'>
<tr>
<td>Total reminders sent:</td><td style='text-align:center;'><strong>$totalReminders</strong></td>
</tr>
<tr>
<td>Users with no application:</td><td style='text-align:center;'><strong>$noApplication</strong></td>
</tr>
<tr>
<td>Users that started application but did NOT select eligibility:</td><td style='text-align:center;'><strong>$stoppedAtStepOne</strong></td>
</tr>
<tr>
<td>Users awaiting references:</td><td style='text-align:center;'><strong>$awaitingReferences</strong></td>
</tr>
<tr>
<td>Users awaiting exam:</td><td style='text-align:center;'><strong>$awatingExam</strong></td>
</tr>
</table>
";
        foreach(array(5,21,220,250,705) as $adminID){
            Email::send($adminID, "$totalReminders SCTPP Notification(s) Sent on $today", "SCTPP Notifcation Breakdown: $today", $content);
            sleep(1);
        }
    }
}
