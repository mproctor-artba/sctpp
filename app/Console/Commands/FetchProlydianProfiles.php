<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Prolydian;

class FetchProlydianProfiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FetchProlydianProfiles:Fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pulls all candidate data from Pearson';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Prolydian::profiles();
    }
}
