<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use App\Result as Results;
use App\Models\User as Users;
use App\Profile as Profiles;
use App\Application as Applications;
use App\Course as Courses;
use App\Enrollment as Enrollment;
use Edvance360;
use Storage;
use DB;
use Inbox;
use DatePeriod;
use DateTime;
use DateInterval;
use Email;
use MailChimp;

class FetchEnrollment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FetchEnrollment:Fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will sync SCTPP enrollment data with Edvance360';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // download student and course data from Edvance360

        $data = Edvance360::data();

        $courses = $data->courses;
        $students = $data->students;

        // sync Edvance360 courses into SCTPP Database

        foreach ($courses as $course) {
            if(Courses::where('identifier', $course->identifier)->count() == 0){
                DB::table('courses')->insert([
                    'identifier' => $course->identifier,
                    'termIdentifier' => $course->termIdentifier,
                    'instructorId' => $course->instructorId,
                    'courseId' => $course->courseId,
                    'name' => $course->name,
                    'description' => $course->description,
                    'categoryIdentifier' => $course->categoryIdentifier,
                    'campusIdentifier' => $course->campusIdentifier,
                    'sectionIdentifier' => $course->sectionIdentifier
                ]);
            } else {
                DB::table('courses')->where('identifier', $course->identifier)->update([
                    'identifier' => $course->identifier,
                    'termIdentifier' => $course->termIdentifier,
                    'instructorId' => $course->instructorId,
                    'courseId' => $course->courseId,
                    'name' => $course->name,
                    'description' => $course->description,
                    'categoryIdentifier' => $course->categoryIdentifier,
                    'campusIdentifier' => $course->campusIdentifier,
                    'sectionIdentifier' => $course->sectionIdentifier
                ]);
            }

            // 

            // sync Edvance360 enrollment data into SCTPP database
            if(!empty($course->identifier)){
                $enrolledStudents = Edvance360::students($course->identifier);

                foreach ($enrolledStudents as $enrolledStudent) {
                    if(is_object($enrolledStudent)){
                        $user = Users::where('email', $enrolledStudent->email)->first();
                        $courseID = Courses::where('identifier', $course->identifier)->first();
                        $courseID = $courseID->id;
                        if(isset($user)){
                            if(Enrollment::where('user_id', $user->id)->where('course_id', $courseID)->count() == 0){
                                // enrollment record not found, populate the database

                                DB::table('enrollment')->insert([
                                    'user_id' => $user->id,
                                    'course_id' => $courseID,
                                    'status' => $enrolledStudent->userRole
                                ]);
                            } else {
                                DB::table('enrollment')->where('user_id', $user->id)->where('course_id', $courseID)->update([
                                    'status' => $enrolledStudent->userRole
                                ]);
                            }
                        }
                    } else {
                        echo "ERROR FOUND!!!!!!!! with $course->identifier";
                        var_dump($enrolledStudent);
                    }
                }
            }
        }
    }
}
