/**
Template Name: Ubold Dashboard
Author: CoderThemes
Email: coderthemes@gmail.com
File: Chartjs
*/


!function($) {
    "use strict";

    var ChartJs = function() {};

    ChartJs.prototype.respChart = function(selector,type,data, options) {
        // get selector by context
        var ctx = selector.get(0).getContext("2d");
        // pointing parent container to make chart js inherit its width
        var container = $(selector).parent();

        // enable resizing matter
        $(window).resize( generateChart );

        // this function produce the responsive Chart JS
        function generateChart(){
            // make chart width fit with its container
            var ww = selector.attr('width', $(container).width() );
            switch(type){
                case 'Pie':
                    new Chart(ctx, {type: 'pie', data: data, options: options});
                    break;
                case 'Bar':
                    new Chart(ctx, {type: 'bar', data: data, options: options});
                    break;
            }
            // Initiate new chart or Redraw

        };
        // run function - render chart at first load
        generateChart();
    },
    //init
    ChartJs.prototype.init = function() {
        //Pie chart
        var pieChart = {
            labels: [
                "Review Needed",
                "Rejected",
                "Exam Pending",
                "Payment Required"
            ],
            datasets: [
                {
                    data: [300, 50, 100, 10],
                    backgroundColor: [
                        "#ff8800",
                        "#ef4554",
                        "#5fbeaa",
                        "#ebeff2"
                    ],
                    hoverBackgroundColor: [
                        "#5d9cec",
                        "#5fbeaa",
                        "#ebeff2",
                        "#ebeff2"
                    ],
                    hoverBorderColor: "#fff"
                }]
        };
        this.respChart($("#pie"),'Pie',pieChart);


        //barchart
        var barChart = {
            labels: ["2016", "2017", "2018"],
            datasets: [
                {
                    label: "Passed",
                    backgroundColor: "rgba(95, 190, 170, 0.5)",
                    borderColor: "#5fbeaa",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(95, 190, 170, 0.8)",
                    hoverBorderColor: "#5fbeaa",
                    data: [50, 90, 84]
                },
                {
                    label: "Failed",
                    backgroundColor: "rgba(239, 69, 84, 0.5)",
                    borderColor: "#ef4554",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(239, 69, 84, 0.8)",
                    hoverBorderColor: "#ef4554",
                    data: [5, 25, 16]
                },
                {
                    label: "No show",
                    backgroundColor: "rgba(255, 136, 0, 0.5)",
                    borderColor: "#ff8800",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(255, 136, 0, 0.8)",
                    hoverBorderColor: "#ff8800",
                    data: [0, 3, 4]
                }
            ]
        };
        this.respChart($("#bar"),'Bar',barChart);

    },
    $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs

}(window.jQuery),

//initializing
function($) {
    "use strict";
    $.ChartJs.init()
}(window.jQuery);

