<?php

Route::prefix('application')->group(function () {
	
	Route::get('', 'ApplicationController@eligibility')->name('eligibility');
	/* document center */
	
    Route::get('/view/{id}', 'ApplicationController@application')->name('application');


    Route::prefix('documents')->group(function () {
        Route::get('/', 'ApplicationController@documents')->name('documents');
        Route::post('/', 'ApplicationController@upload')->name('upload');
        Route::get('/remove/{name}', 'ApplicationController@removeDocument')->name('removeDocument');
    });

    Route::get('/experience', 'ApplicationController@experience')->name('experience');
    Route::post('/experience', 'ApplicationController@saveExperience')->name('saveExperience');
    Route::get('/experience/remove/{reference}', 'ApplicationController@removeExperience')->name('removeExperience');
    Route::get('/experience/remind/{secret?}', 'ApplicationController@sendReminder')->name('remindReference');

    Route::get('/terms', 'ApplicationController@terms')->name('terms');

    Route::get('/submit', 'ApplicationController@submit')->name('submit');

    Route::get('/complete', 'ApplicationController@complete')->name('complete');
    Route::get('/enroll', 'ApplicationController@enroll')->name('enroll');
    
    Route::post('/', 'ApplicationController@saveProfile')->name('saveProfile');

    Route::post('/eligibility', 'ApplicationController@saveEligibility')->name('saveEligibility');

    Route::post('/terms', 'ApplicationController@saveTerms')->name('saveTerms');

    Route::post('/submit', 'ApplicationController@submitApplication')->name('submitApplication');

    Route::get('/access/{code}', 'ApplicationController@verifyCode')->name('verifyCode');

    Route::get('/waived/{code}', 'ApplicationController@submitWaived');

    Route::get('/resubmit', 'ApplicationController@resubmit')->name('resubmit');
    Route::get('/retest', 'ApplicationController@retest')->name('retest');
    Route::post('/retest', 'ApplicationController@submitRetest')->name('submitRetest');

    Route::post('/decide/{id}', 'ApplicationController@decide')->name('decide');
    
    Route::prefix('recertification')->group(function(){
        Route::get('/', 'ApplicationController@recertify')->name('recertify');
        Route::get('/experience', 'ApplicationController@recertifyExperience')->name('recertify-experience');
        Route::get('/checkout', 'ApplicationController@getCheckout')->name('recertify-checkout');
        Route::get('/submit', 'ApplicationController@recertifySubmit')->name('recertify-submit');
        Route::get('/pdh', 'ApplicationController@recertifyPDHForm')->name('recertify-pdh');
        Route::get('/pdh/form', 'ApplicationController@getPDHForm')->name('getPDHForm');
        Route::get('/resume', 'ApplicationController@resumeApp')->name('resumeApp');
        Route::get('/pdh/form/remove/{id?}','ApplicationController@removePDHform')->name('removePDHform');
        Route::get('/view/{id}', 'ApplicationController@recertificationApplication')->name('recertificationApplication');
        
        Route::post('/checkout', 'ApplicationController@payCheckout')->name('recertify-payment');
        Route::post('/submit', 'ApplicationController@submitRecertification')->name('submit-recertification');
        Route::post('/', 'ApplicationController@recertifyEligibility');
        Route::post('/experience','ApplicationController@saveRecertifyExperience')->name('saveRecertifyExperience');
        Route::post('/pdf', 'ApplicationController@savePDHform')->name('savePDHform');
        Route::post('/decide/{id}', 'ApplicationController@decideRecertification')->name('decideRecertification');
    });
});