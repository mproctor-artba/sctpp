<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/', 'HomeController@index')->name('home');

Route::post('/', function(){
echo "ok";
});

// application routes
include('application.php');

// admin routes
include('admin.php');

// profile routes
include('profile.php');

// testing routes
include('testing.php');

// webhook routes for services like mailgun and stripe
include('webhooks.php');

// routes to push data for the Sisense Dashboard
include('sisense.php');

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/confirmResetComplete/{email}', 'PublicController@confirmResetComplete')->name('confirmResetComplete');
Route::get('/pushResetComplete/{email}', 'PublicController@pushResetComplete')->name('pushResetComplete');

Route::get('/verification/{secret}/{spoof}', 'PublicController@verify')->name('verify');
Route::post('/verification/{secret}/{spoof}', 'PublicController@submitVerify')->name('submitVerify');

Route::get('/about', 'PublicController@about')->name('about');
Route::get('/verifyComplete', 'PublicController@testverifyComplete')->name('verifyComplete');
Route::post('/about', 'PublicController@about');

Route::get('/order', 'PublicController@order')->name('order');
Route::post('/order', 'PublicController@submitOrder')->name('submitOrder');

Route::get('/directory/{output}', 'PublicController@directory');
Route::get('/certificates/{output}', 'PublicController@certificates');

Route::get('user/{user}', 'ProfileController@user');

Route::get('/edvance', 'PublicController@edvance');





