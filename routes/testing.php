<?php 



Route::prefix('testing')->group(function () {

	Route::get('/',function(){
		$links = [
			"prolydian/profile",
			"edvance360/data"
		];
		echo '<ul style="list-style:none;">';
		foreach($links as $link){
			echo '<li><a href="/testing/' . $link . '">' . $link . '</a></li>';
		}
		echo '</ul>';
	});

	Route::prefix('prolydian')->group(function () {
		Route::get('/profile', 'TestingController@prolydianProfile')->name('prolydianProfile');
		Route::get('/results', 'TestingController@prolydianResults')->name('prolydianResults');
	});

	Route::prefix('edvance360')->group(function () {
		Route::get('/data', 'TestingController@edvance360Data')->name('edvance360Data');
	});

	Route::prefix('pdh')->group(function () {
		Route::get('/status', 'TestingController@pdhStatus')->name('pdhStatus');
	});

	Route::prefix('sisense')->group(function () {
		Route::get('/users', 'TestingController@sisenseUsers')->name('sisenseUsers');
	});

	Route::prefix('accredible')->group(function () {
		Route::get('/clean', 'TestingController@cleanupcerts')->name('cleanupcerts');
	});
});

?>