<?php

Route::prefix('profile')->group(function () {
    Route::get('/', 'ProfileController@profile')->name('profile');
    Route::get('/view/{id}', 'ProfileController@index')->name('account');

    Route::prefix('organizations')->group(function () {
    	Route::get('/', 'ProfileController@organizations')->name('myorgs');
    	Route::get('/documents', 'ProfileController@orgdocuments')->name('myorgdocuments');
    	Route::get('/analytics', 'ProfileController@organalytics')->name('myorganalytics');
    	Route::get('/billing', 'ProfileController@orgbilling')->name('myorgbilling');
    	Route::get('/support', 'ProfileController@orgsupport')->name('myorgsupport');
    	Route::get('/{id}', 'ProfileController@organization');
        Route::post('/documents/upload/{user}', 'ProfileController@newimport')->name('newimport');
    });
    

    Route::post('/', 'ProfileController@updateAccount')->name('updateAccount');

});