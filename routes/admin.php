<?php

/* 
Controllers:

AdminController
LMSController

*/

Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('overview');

    Route::prefix('performance')->group(function () {
    	Route::get('/', 'AdminController@performance')->name('performance');
    	Route::get('/exam-results', 'AdminController@examResults')->name('exam-results');
        Route::get('/exam-results/uniques/{type}', 'AdminController@latestResults')->name('latest-results');
    });

    Route::prefix('demographics')->group(function () {
    	Route::get('/', 'AdminController@demographics')->name('demographics');
    });

    Route::prefix('financials')->group(function () {
    	Route::get('/', 'AdminController@financials')->name('financials');
    });

    Route::prefix('emails')->group(function () {
        Route::get('/', 'AdminController@emails')->name('emails');
    });

    Route::prefix('sisense')->group(function () {
        Route::get('/', 'AdminController@sisense')->name('sisense');
    });

    Route::prefix('users')->group(function () {
        Route::get('/', 'AdminController@users')->name('users');
        Route::get('/table?page={page}', 'AdminController@usersTable')->name('usersTable');
        Route::get('/references', 'AdminController@references')->name('references');
        Route::get('/review', 'AdminController@applications')->name('review');
        Route::get('/review/history', 'AdminController@reviewHistory')->name('reviewHistory');
        Route::get('/view/{id?}', 'AdminController@user')->name('user');
        Route::get('/reference/edit/{id}', 'AdminController@editReferences')->name('edit-reference');
        Route::get('/reference/delete/{reference}', 'AdminController@deleteReference')->name('delete-reference');
        Route::get('/reference/restore/{reference}', 'AdminController@restoreReference')->name('restore-reference');
        Route::get('/progress/{id}', 'AdminController@getUserProgress')->name('user-progress');
        Route::get('/pdhs','AdminController@pdhs')->name('pdh-submissions');
        Route::get('/verify/{method}/{marker}','AdminController@verifyUser');

        Route::post('/', 'AdminController@editUser')->name('edit-user');

        Route::post('/pdhs/decide','AdminController@pdhDecide')->name('decide-pdh');
        
        Route::post('/enroll', 'LMSController@enroll')->name('enroll');
        Route::post('/reference/edit', 'AdminController@adminModifyReference')->name('adminModifyReference');

        Route::get('/archived', 'AdminController@deletedUsers');
        Route::get('/delete/{id}', 'AdminController@deleteUser');
        Route::get('/restore/{id}', 'AdminController@restoreUser');
    });

    Route::prefix('examination')->group(function () {
        Route::get('/', 'AdminController@exam')->name('exam');
        Route::get('/export', 'AdminController@export')->name('export');
        Route::post('/import', 'AdminController@importTestResults')->name('import');
    });

    Route::prefix('organizations')->group(function () {
        Route::get('/', 'AdminController@organizations')->name('organizations');
        Route::get('/manage/{id}', 'AdminController@organization')->name('organization');
        Route::get('/manage/{id}/lms', 'AdminController@exportLMS')->name('exportLMS');
        Route::get('/upload-results', 'AdminController@examResults')->name('upload-results');
        Route::get('/notifications/{organization}', 'AdminController@organizationNotifications')->name('organization-notifications');

        Route::post('/', 'AdminController@new_organization')->name('new-organization');        
        Route::post('/manage/{id}', 'AdminController@updateOrganization')->name('updateOrganization');

        Route::prefix('courses')->group(function () {
            Route::post('/update/{org}', 'AdminController@updateOrganizationCourses')->name('change-courses'); 
        });

        Route::prefix('codes')->group(function () {
            Route::get('/', 'AdminController@accessCodes')->name('access-codes');
            Route::get('/new', 'AdminController@accessCodes')->name('new-codes');
            Route::get('/delete/{code}', 'AdminController@deleteCode')->name('delete-code');
            Route::post('/new', 'AdminController@createCodes')->name('create-codes');
            Route::post('/updateCourses/{code}', 'AdminController@updateCourses')->name('updateCourses');
        });
    });

    Route::prefix('certificates')->group(function () {
        Route::get('/', 'AdminController@certificates')->name('certificates');
        Route::get('/verify/{method}/{marker}','AdminController@verifyCertificate');
    });

    include('lms.php');
});