<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="The ARTBA Foundation’s Safety Certification for Transportation Project Professionals™ (SCTPP) program was established in 2016 to make safety top-of-mind for all professionals involved in the planning, design, management, materials delivery and construction of transportation projects from inception through completion.">
        <meta name="author" content="ARTBA Certification Team">

        <link rel="shortcut icon" href="/assets/images/favicon.png">

        <title>SCTPP | About</title>

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">

        <!-- Bootstrap core CSS -->
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Owl Carousel CSS -->
        <link href="/css/owl.carousel.css" rel="stylesheet">
        <link href="/css/owl.theme.default.min.css" rel="stylesheet">

        <!-- Icon CSS -->
        <link href="/css/font-awesome.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="/css/style.css" rel="stylesheet">



        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->

        <style type="text/css">
            .bg-img-1{
                background: url('/images/bg-header.jpg') fixed;
                background-size: cover;

            }
            .hide{
                display: none;
            }
            .section{
                padding-top: 50px;
                padding-bottom: 40px;
            }
            .pricing-column .plan-stats{
                padding: 0px 20px 15px;
            }
            @media (max-width: 767px) {
                .logo img{
                    position: absolute;
                    margin-top:-10px;
                    width:300px;
                }
            }
            .bg-overlay{
                background-color: rgba(60,64,70,0.7);
            }
            .text-muted{
                color: black !important;
            }
            .home-wrapper h4 {
                color: rgba(255, 255, 255, 0.9) !important;
            }
            .features-box{
                padding: 15px;
            }
            .navbar-brand{
                margin: 0 auto;
            }
            html,body,.home{
                height:100%;
            }
        </style>

    </head>


    <body data-spy="scroll" data-target="#navbar-menu">

        <!-- Navbar -->
        <nav class="navbar navbar-custom navbar-expand-lg navbar-light">
            <div class="container">
                <a class="navbar-brand logo" href="https://sctpp.org"><img src="/images/artba-tdf-2.jpg" style="max-width: 350px;"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsMenu" aria-controls="navbarsMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
        <!-- End navbar-custom -->



        <!-- HOME -->
        <section class="home bg-img-1" id="home">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="home-fullscreen">
                            <div class="full-screen">
                                <div class="home-wrapper home-wrapper-alt" style="padding-top: 150px;">
                                    <h1 class="text-white" style="font-size: 3rem;">Thank You!</h1>
                                    <h4 class="" style="width:82%;">{{ $profile->first_name }} is one step closer to becoming a Safety Certified Transportation Project Professional<sup>&trade;</sup>. <br>Click below to learn more about how your employees can become safety certified and how {{ $experience->organization }} can benefit.</h4>
                                    <a href="{{ route('about') }}" class="btn btn-white-bordered">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END HOME -->
        

        <!-- Back to top -->    
        <a href="#" class="back-to-top" id="back-to-top"> <i class="fa fa-angle-up"></i> </a>


        <!-- js placed at the end of the document so the pages load faster -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/popper.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>

        <!-- Jquery easing -->                                                      
        <script type="text/javascript" src="/js/jquery.easing.1.3.min.js"></script>

        <!-- Owl Carousel -->                                                      
        <script type="text/javascript" src="/js/owl.carousel.min.js"></script>

        <!--common script for all pages-->
        <script src="/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:false,
                autoplay: true,
                autoplayTimeout: 4000,
                responsive:{
                    0:{
                        items:1
                    }
                }
            })
        </script>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83551434-1', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>