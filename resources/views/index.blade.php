@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="https://selectize.github.io/selectize.js/css/selectize.default.css" data-theme="default">
	<style>
	.tab-content{
		width:100%;
	}
    form .form-group .control-label{
        position: absolute;
        top: 0.25rem;
        pointer-events: none;
        padding-left: 0.125rem;
        z-index: 1;
        color: #98a6ad;
        font-weight: normal;
        -webkit-transition: all 0.28s ease;
        transition: all 0.28s ease;
    }
    .page-title{
        display: none;
    }
    .select2-selection{
        text-align: left;
    }
    .selectize-control.single .selectize-input{
            background-color: white !important;
            background-image: none !important;
            border: none;
            border-bottom: 1px solid #999;
            border-radius: 0 !important;
    }
    .selectize-control{
        padding-top: 2px !important;
        padding-left: 0 !important;
    }
    .selectize-control .item{
        font-size: 1rem !important;
        color: #565656 !important;
    }
	</style>
@endsection


@section('content')

<div class="row">
    <div class="col-lg-10 offset-md-1">
    @csrf
    
    @if(isset(Auth::user()->certificate))
        @php
            $statusClass = "bg-success";
            $statusMessage = "Congratulations, You're Safety Certified!";
            $statusDays = Auth::user()->certificate->daysToRecertify();
            $statusExpiry = "Your SCTPP certificate expires on ";
            if(Auth::user()->certificate->daysToRecertify() <= 0){
                $statusClass = "bg-danger";
                $statusMessage = "You're Safety Certification has Expired";
                $statusDays = 0;
                $statusExpiry = "Your SCTPP certificate expired on ";
            }
        @endphp
        <div class="card text-white {{ $statusClass }} text-xs-center" style="margin-bottom: 10px;">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">
                        <h4 class="text-white">{{ $statusMessage }}</h4>
                        <p> {{ $statusExpiry . Date("m-d-Y",Auth::user()->certificate->expiration()) }}</p>
                        <footer><a href="{{ Auth::user()->certificate->link() }}" style="color: white; border: 1px solid white; padding: 10px 20px; border-radius: 3px;" target="_blank"><strong>View Your Certificate <i class="fa fa-certificate"></i></strong></a>
                        </footer>
                    </div>
                    <div class="col-md-2 text-center">
                        <h2 class="text-white">{{ number_format($statusDays) }}</h2>
                        <p>Days left to recertify</p>
                        @if($statusDays > 0)
                            @if(Auth::user()->step11() == NULL)
                                <a class="btn btn-white" href="{{ route('recertify') }}"> Get Started</a>
                            @else
                                @if(Auth::user()->application->stage == NULL || Auth::user()->application->stage > 1 && Auth::user()->application->stage < 4)
                                <a class="btn btn-white" href="{{ route('resumeApp') }}"> Resume Application</a>
                                @else
                                    @if(Auth::user()->application->stage == 5)
                                        @if(Auth::user()->certificate->daysToRecertify() >= 365)
                                            <a class="btn btn-white" href="#"><i class="fa fa-check"></i> Recertified</a>
                                        @else
                                            <a class="btn btn-white" href="{{ route('recertify') }}"><i class="fa fa-check"></i> Start Recertification</a>
                                        @endif
                                    @endif
                                @endif
                            @endif
                            
                        @else
                            <a class="btn btn-white" href="mailto:certificationteam@sctpp.org?subject=Cerrtification Expired">Contact Us</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="tabs-vertical-env">
        <ul class="nav tabs-vertical">
            <li class="nav-item">
                <a href="#v-profile" data-toggle="tab" aria-expanded="false" class="nav-link active">
                    Home
                </a>
            </li>
            <li class="nav-item">
                <a href="#v-settings" data-toggle="tab" aria-expanded="false" class="nav-link">
                    My Inbox
                </a>
            </li>
            <li class="nav-item">
                <a href="#v-account" data-toggle="tab" aria-expanded="false" class="nav-link">
                    Account
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('logout') }}"  aria-expanded="false" class="nav-link">
                    Logout
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="v-profile" aria-expanded="false">
            
            <form method="POST" action="{{ route('saveProfile') }}">
                @csrf
            	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>All fields are required. Your name must match your photo identification.</b></h4>
                <div class="form-row clearfix">
                    <div class="form-group col-md-5">
                        <label>First Name</label>
                        <input type="text" name="f_name" value="{{ $profile->first_name or old('f_name') }}" class="form-control material required " required="">
                    </div>
                    <div class="form-group col-md-5">
                        <label>Last Name</label>
                        <input type="text" name="l_name" value="{{ $profile->last_name or  old('l_name') }}" class="form-control material required" required>
                        
                    </div>
                    <div class="form-group col-md-2">
                        <label>Middle Initial</label>
                        <input type="text" name="middle" value="{{ $profile->middle or old('middle') }}" class="form-control material">
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-group col-md-4">
                        <label>Job Title</label>
                        <select id="title" class="demo-default required material" placeholder="Please Select or Enter Job Title" name="title" required="">
                            <option value="">Please Select or Enter Job Title</option>
                            @foreach($titles as $jobtitle)
                                    <option value="{{ $jobtitle->title }}"
                                    @if(isset($profile->title))
                                        @if($profile->title == $jobtitle->title)
                                            selected
                                        @endif
                                    @elseif(old('title') == $jobtitle->title)
                                        selected
                                    @endif
                                    >{{ $jobtitle->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Company</label>
                        <select id="company" class="demo-default required material" placeholder="Please Select or Enter Company Name" name="company" required="">
                            <option value="">Please Select or Enter Company Name</option>
                            @foreach($organizations as $organization)
                                    <option value="{{ $organization->company }}"
                                    @if(isset($profile->company))
                                        @if($profile->company == $organization->company)
                                            selected
                                        @endif
                                    @elseif(old('company') == $organization->company)
                                        selected
                                    @endif
                                    >{{ $organization->company }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Phone Number</label>
                        <input type="text" name="phone" value="{{ $profile->phone or old('phone') }}" minlength="4" class="form-control material required phone" required>
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-group col-md-6">
                        <label>Address</label>
                        <input type="text" name="address" value="{{ $profile->address or old('address') }}" class="form-control material required" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Address Line 2</label>
                        <input type="text" name="address2" value="{{ $profile->address2 or old('address2') }}" class="form-control material">
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-group col-md-5">
                        <label>City</label>
                        <input type="text" name="city" value="{{ $profile->city or old('city') }}" class="form-control material required" required>
                    </div>
                    <div class="form-group col-md-5">
                        <label>State</label>
                        <select class="form-control material required" name="state" required="">
                        <option value="">Please Select</option>
                        @foreach($states as $state)
                            <option value="{{ $state->abbr }}" 
                                @if(old('state') !== null)
                                    @if(old('state') == $state->abbr)
                                        selected=""
                                    @endif
                                @endif
                                @if(is_object($profile))
                                    @if($profile->state == $state->abbr)
                                        selected=""
                                    @endif
                                @endif
                            >
                        {{ $state->name }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Zip</label>
                        <input type="number" name="zip" value="{{ $profile->zip or old('zip') }}" minlength="4" class="form-control material required" required>
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-group col-md-6">
                        <button type="submit" class="btn btn-primary">Save @if(Auth::user()->isNew()) and Move to Step 2 <span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span>@else Changes @endif</button>
                    </div>
                </div>
                </form>
            </div>
            <div class="tab-pane" id="v-settings">
                <h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>System generated messages for you to keep track of your progress</b></h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-left">Message</th>
                            <th width="15%">Date</th>
                        </tr>
                    </thead>
                    @foreach($messages as $message)
                        <tr>
                            <td class="text-left">{!! $message->content($message->type, $message->content) !!}</td>
                            <td>{{ convertTimestamp($message->date) }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="tab-pane" id="v-account">
            <h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>We recommend changing your password at least once a year.</b></h4>
                <form method="POST" action="{{ route('updateAccount') }}">
                    @csrf
                    <div class="form-row clearfix">
                        <div class="form-group col-md-12">
                            <label>Email</label>
                            <input type="text" name="email" value="{{ Auth::user()->email }}" class="form-control material required" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label><strong>Leave the below fields blank if not updating password</strong></label>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Current Password</label>
                            <input type="password" name="password" value="" class="form-control material">
                        </div>
                        <div class="form-group col-md-12">
                            <label>New Password</label>
                            <input type="password" name="newpassword" value="" class="form-control material">
                        </div>
                        <div class="form-group col-md-12">
                            @if(is_object($profile))
                                @php $companyAdmin = getCompanyAdmin($profile->company); @endphp
                                @if(isset($profile->company) && $companyAdmin !== null)
                                    <label class="form-label"><strong>Give {{ $profile->company }} Permission to Monitor Your Progress</strong></label>
                                    <br>
                                    <input type="hidden" name="organization_id" value="{{ $companyAdmin['company']->id }}">
                                    <input type="checkbox" name="monitor" value="1" @if(isset(Auth::user()->organization_id)) checked @endif> Checking this box will allow <a href="mailto:{{ $companyAdmin['email'] }}">{{ $companyAdmin["profile"]->first_name . " " . $companyAdmin["profile"]->last_name }}</a> to track your certification progress from {{ $profile->company }}'s dashboard. <br><strong>Your test scores will never be shared with anyone.</strong>
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="form-row clearfix">
                        <button type="submit" class="btn btn-primary">Update Account</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    </form>
    </div>
</div>

@endsection

@section('js')
    <script src="https://selectize.github.io/selectize.js/js/selectize.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    $('#title').selectize({
        create: true,
        sortField: 'text'
    });
    $('#company').selectize({
        create: true,
        sortField: 'text'
    });


        $(".phone").keyup(function(){
            var val = $(this).val();
            var last = val.substr(val.length - 1);
            if(Math.floor(last) != last && !$.isNumeric(last)){
                $(this).val(val.slice(0,-1));
            }
            if(last == " "){
                $(this).val(val.slice(0,-1));
            }
        });
    });
    </script>
@endsection