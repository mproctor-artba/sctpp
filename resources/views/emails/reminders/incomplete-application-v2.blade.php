@extends('layouts.email-primary')


@section('content')
	<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align:center;" align="center" valign="top">
			<img src="{{ URL('/images/sctpp-ansi.png') }}" width="100%" style="max-width: 400px;">
			</td>
		</tr>
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; font-weight: bold;" valign="top">
			<b>Dear {!! $first_name !!}:</b>
			</td>
		</tr>
	<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
Thanks for your interest in joining {{ $colleagues }} of your colleagues at {{ $company }} in becoming a Safety Certified Transportation Project Professional™ (SCTPP). We noticed  you started your application, but did not submit it for final review. 
<br><br>
Your application will allow us to verify your eligibility for the exam. Please <a href='https://portal.sctpp.org'>login</a> and complete the process.

@include('emails.reminders.signature')
<br><br>
P.S. Check out our Study Guide & Practice Questions (there are 80 of them) to help with exam prep.  It can be purchased at: <a href="https://store.artba.org/product/sctpp-study-guide-exam-practice-questions">https://store.artba.org/product/sctpp-study-guide-exam-practice-questions</a>.
		</td>
	</tr>
	</table>
@endsection