@extends('layouts.email-primary')


@section('content')
	<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align:center;" align="center" valign="top">
			<img src="{{ URL('/images/sctpp-ansi.png') }}" width="100%" style="max-width: 400px;">
			</td>
		</tr>
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; font-weight: bold;" valign="top">
			<b>Dear {{ $first_name  }}:</b>
			</td>
		</tr>
	<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
Your application to take the Safety Certification for Transportation Project Professional™ (SCTPP) exam is nearly complete, but we still need to verify your employment.    
<br><br>
We recommend you contact your indicated reference(s) to make sure they received our email on this matter: 
<br><br>
<ul>
	@foreach($references as $reference)
		<li><a href="mailto:{{ $reference->email }}">{{ $reference->reference }} | {{ $reference->organization }}</a></li>
	@endforeach
</ul>
For assistance, please contact <a href="mailto:certificationteam@sctpp.org">certificationteam@sctpp.org</a><br>
Thank you,
<br>
ARTBA Certification Team
		</td>
	</tr>
	</table>
@endsection