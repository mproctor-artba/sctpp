@extends('layouts.app')

@section('css')
	<!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

	<style>
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">User Management</li>
    <li class="breadcrumb-item active">{{ $user->profile->first_name  or "N/A" }} {{ $user->profile->last_name or "" }}</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="card-box">
			<div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Profile Information</b></h4>
                    </div>
                    <div class="col-md-12">
                        <ul style="list-style: none; padding: 0;">
                            <li>Email:  {{ $user->email }} </li>
                        </ul>
                        <form method="POST" class="row" action="{{ route('edit-user') }}">
                            @CSRF
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <div class="form-group col-md-3">
                                <label class="form-label">Phone:</label>
                                <input type="text" class="form-control" name="phone" value="{{ $user->profile->phone }}" required="">
                            </div>
                            <div class="form-group col-md-5">
                                <label class="form-label">Company:</label>
                                <input type="text" class="form-control" name="company" value="{{ $user->profile->company }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label">Job Title:</label>
                                <input type="text" class="form-control" name="title" value="{{ $user->profile->title }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Address 1:</label>
                                <input type="text" class="form-control" name="address" value="{{ $user->profile->address}}" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label">Address 2:</label>
                                <input type="text" class="form-control" name="address2" value="{{ $user->profile->address2 }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label">City:</label>
                                <input type="text" class="form-control" name="city" value="{{ $user->profile->city }}" required="">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label">State:</label>
                                <input type="text" class="form-control" name="state" value="{{ $user->profile->state}}" required="">
                            </div>
                            <div class="form-group col-md-4">
                                <label class="form-label">Zip Code:</label>
                                <input type="text" class="form-control" name="zip" value="{{ $user->profile->zip}}" required="">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="submit" name="submit" value="Save Changes" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                    @if(is_object($user->application))
                        <a href="{{ route('application', ['id' => $user->application->id]) }}" class="btn btn-primary">Application</a>
                    @endif
                    </div>
                    <div class="col-md-4">
                    @if(is_object($user->application))
                        @if($user->application->stage == NULL || $user->application->stage > 1)
                        <a href="{{ route('recertificationApplication', ['id' => $user->application->id]) }}" class="btn btn-default">Recertification Application</a>
                        @endif
                    @endif
                    </div>
                </div>
			</div>
		</div>
        <div class="card-box">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Documents</b></h4>
                    </div>
                    <div class="col-md-12">
                        @if(is_object($user->application))
                            <form method="POST" action="{{ route('upload') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="eligibility" value="{{ $user->application->eligibility }}">
                                <input type="hidden" name="appID" value="{{ $user->application->id }}">
                                <table class="table" width="100%">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label class="form-label">Resume</label>
                                                <input type="file" name="resume" style="max-width:175px;">
                                            </td>
                                            <td>
                                                <label class="form-label">Certificate</label>
                                                <input type="file" name="osha" style="max-width:175px;">
                                            </td>
                                            <td>
                                                <label class="form-label">Diploma</label>
                                                <input type="file" name="diploma" style="max-width:175px;">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    
                                </div>
                                <div class="form-group">
                                    
                                </div>
                                <div class="form-group">
                                    
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="submit" value="Upload Document(s)" class="btn btn-success">
                                </div>
                            </form>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <ul style="list-style: none; padding: 0;">
                        <table class="table">
                            <tbody>
                                @foreach($documents as $document)
                                    <tr>
                                        <td class="vertical-align:middle;"><a href="/uploads/documents/{{ $document->name }}" target="_blank">{{ $document->name }}</a></td>
                                        <td><label class="deleteDocument btn btn-danger" data-name="{{ $document->name }}"><i class="fa fa-times"></i></label></td>
                                    </tr>
                            @endforeach
                            </tbody>
                        </table>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <div class="col-lg-6 col-md-6">
        <div class="card-box">
            <div class="card-body" >
                <h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>User Timeline</b></h4>
                <div class="row">
                    <div style="max-height: 400px; overflow-y: auto;">
                    <table class="table" >
                    <thead>
                        <tr>
                            <th class="text-left">Message</th>
                            <th width="15%">Date</th>
                        </tr>
                    </thead>
                    @foreach($user->messages as $message)
                        <tr>
                            <td class="text-left">{!! str_replace(Auth::user()->profile->first_name,$user->profile->first_name,$message->content($message->type)) !!}</td>
                            <td>{{ convertTimestamp($message->date) }}</td>
                        </tr>
                    @endforeach
                </table>
                </div>
                </div>
            </div>
        </div>
        @if(isset($courses) && array_filter((array)$courses) && !isset($code))
        <div class="card-box">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Course Enrollment</b></h4>
                    </div>
                        <div class="col-md-12">
                            <p>This student has an Edvance360 Profile and is enrolled into the following courses:</p>
                            <ul>
                            @foreach($courses as $course)
                                <li>{{ $course }}</li>
                            @endforeach
                            </ul>
                        </div>        
                </div>
            </div>
        </div>
        @endif
        @if(isset($code) && array_filter($courses))
            <div class="card-box">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Course Enrollment</b></h4>
                        </div>
                        <div class="col-md-12">
                            <p>Access Code <strong>{{ $code }}</strong> allows this student to be auto-enrolled into the following courses:</p>
                            <form method="POST" action="{{ route('enroll-student') }}">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <ul>
                            @foreach($courses as $course)
                                <li>{{ $course->name }}</li>
                                <input type="hidden" name="courseID[]" value="{{ $course->id }}">
                            @endforeach
                            </ul>
                            
                            <label>Create a password for this user</label>
                            <input type="password" name="password" class="form-control" placeholder="New Password">
                            <br>
                            <button type="submit" name="submit" class="btn btn-primary">Enroll Student</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

<div class="row">
<a href="/admin/users/delete/{{ $user->id }}" class="btn btn-white " onclick="confirm('Are you sure you want to delete this user?');">Archive User <i class="fa fa-trash-o"></i></a>
</div>


@endsection

@section('js')

    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

                        $(".deleteDocument").click(function(){
            if(confirm("Are you sure you want to delete this document?")){
                var filename = $(this).attr("data-name");

                if(filename != ""){
                    $(this).parent().parent().remove();
                    $.get("{{ route('removeDocument', ['name' => NULL]) }}/" + filename, function(data, status){
                        $.Notification.notify('success','bottom right', 'Done', 'Your document has been removed.');
                    });
                }
            }
        });
            } );
        </script>
@endsection