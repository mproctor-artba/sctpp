@extends('layouts.app')

@section('css')
    <!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/dropzone/dropzone.css" rel="stylesheet" type="text/css" />
    <style>
    .tab-content{
        width:100%;
    }

    #datatable-buttons_wrapper, .dataTables_wrapper{
        width:100% !important;
        padding: 0 !important;
        max-width: 100% !important;
    }
    </style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">User Management</li>
    <li class="breadcrumb-item active">{{ $title }}</li>
</ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

        <div class="tabs-vertical-env">
            <ul class="nav tabs-vertical">
                <li class="nav-item">
                    <a href="#test-results" data-toggle="tab" aria-expanded="true" class="nav-link active" id="test-results-tab">
                        Certification Test Results
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#recert-test-results" data-toggle="tab" aria-expanded="false" class="nav-link" id="test-results-tab">
                        Recertification Test Results
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#v-scheduled" data-toggle="tab" aria-expanded="false" class="nav-link" id="scheduled-tab">
                        Scheduled Exams
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#v-profile" data-toggle="tab" aria-expanded="false" class="nav-link" id="export-tab">
                        Pearson Export
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#upload" data-toggle="tab" aria-expanded="false" class="nav-link" id="update-tab">
                        Import Exam Data
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" aria-expanded="true" id="test-results">
                        <div class="row">
                            <ul class="pull-right">
                                <li><a href="{{ route('latest-results', ['type' => 0]) }}" target="_blank">Unique Certification Results</a></li>
                                <li><a href="{{ route('latest-results', ['type' => 1]) }}" target="_blank">Unique Re-Certification Results</a>
                                </li>
                            </ul>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%" style="font-size: 10.5px;">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Candidate ID</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>State</th>
                                <th>Result</th>
                                <th>Score</th>
                                <!--
                                <th>Assessing Project Risk</th>
                                <th>Create Safety Plan Based on Project Risk Assessment</th>
                                <th>Implement Operational Safety Plan</th>
                                <th>Conducting Ongoing Evaluation of Operational Safety Plan</th>
                                <th>Conduct Incident Investigations</th>
                                -->
                                <th>Date</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($results as $result)
                            <tr>
                                <td>{{ $result->user->first_name . " " . $result->user->last_name }}</td>
                                <td>{{ $result->user->ClientCandidateID }}</td>
                                <td>{{ $result->user->title }}</td>
                                <td>{{ $result->user->company }}</td>
                                <td>{{ $result->user->state }}</td>
                                <td>{{ $result->Score }}</td>
                                <td>{{ strtoupper($result->Grade) }}</td>
                                <!--
                                <td>{{ $result->D1_project_risk }}</td>
                                <td>{{ $result->D2_safety_plan }}</td>
                                <td>{{ $result->D3_op_plan }}</td>
                                <td>{{ $result->D4_eval }}</td>
                                <td>{{ $result->D5_incident }}</td>
                                -->
                                <td><span style="display: none;">{{ convertTimestamp($result->date, "tostring") }}</span>{{ convertTimestamp($result->date) }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                </div>
                <div class="tab-pane" aria-expanded="false" id="recert-test-results">
                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%" style="font-size: 10.5px;">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Candidate ID</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>State</th>
                                <th>Result</th>
                                <th>Score</th>
                                <!--
                                <th>Assessing Project Risk</th>
                                <th>Create Safety Plan Based on Project Risk Assessment</th>
                                <th>Implement Operational Safety Plan</th>
                                <th>Conducting Ongoing Evaluation of Operational Safety Plan</th>
                                <th>Conduct Incident Investigations</th>
                                -->
                                <th>Date</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recertResults as $result)
                            <tr>
                                <td>{{ $result->user->first_name . " " . $result->user->last_name }}</td>
                                <td>{{ $result->user->ClientCandidateID }}</td>
                                <td>{{ $result->user->title }}</td>
                                <td>{{ $result->user->company }}</td>
                                <td>{{ $result->user->state }}</td>
                                <td>{{ $result->Score }}</td>
                                <td>{{ strtoupper($result->Grade) }}</td>
                                <!--
                                <td>{{ $result->D1_project_risk }}</td>
                                <td>{{ $result->D2_safety_plan }}</td>
                                <td>{{ $result->D3_op_plan }}</td>
                                <td>{{ $result->D4_eval }}</td>
                                <td>{{ $result->D5_incident }}</td>
                                -->
                                <td><span style="display: none;">{{ convertTimestamp($result->date, "tostring") }}</span>{{ convertTimestamp($result->date) }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                </div>
                <div class="tab-pane" id="v-profile" aria-expanded="false">
                    @if($exports->count() > 0)
                        <a href="{{ route('export') }}" class="btn btn-success" target="_blank"><span class="btn-label"><i class="fa fa-download"></i></span> Export {{ $exports->count() }} Candidates</a>
                        <br><br>
                    @endif
                    <table class="datatable table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Title</th>
                            <th>Company</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($exports as $export)

                                <tr>
                                    <td>{{ $export->user->profile->first_name . " " . $export->user->profile->last_name }}</td>
                                    <td>{{ $export->user->profile->title }}</td>
                                    <td>{{ $export->user->profile->company }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="v-scheduled" aria-expanded="false">
                    <table id="datatable-buttons-scheduled" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Test Date</th>
                                <th>Test Location</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($schedules as $scheduled)
                                @php 
                                    $user = \App\Models\User::find($scheduled->user_id);
                                    $date = substr($scheduled->test_date, 0, 10);
                                @endphp
                                <tr>
                                    <td>{{ $user->profile->first_name . " " . $user->profile->last_name }}</td>
                                    <td>{{ $user->profile->title }}</td>
                                    <td>{{ $user->profile->company }}</td>
                                    <td><span style="display:none">{{ strtotime($date) }}</span>{{ date("m-d-Y", strtotime($date)) }}</td>
                                    <td>{{ $scheduled->location }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="upload">
                     <form method="POST" action="{{ route('import') }}" class="dropzone" id="drop">
                     @csrf
                        <div class="form-row">
                            <div class="fallback">
                                <input name="file" type="file" multiple />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
            
        </div>
    </div>

@endsection

@section('js')
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Page Specific JS Libraries -->
    <script src="/assets/plugins/dropzone/dropzone.js"></script>

    <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('.datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                var tabletwo = $('#datatable-buttons-scheduled').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

                tabletwo.buttons().container()
                        .appendTo('#datatable-buttons-scheduled_wrapper .col-md-6:eq(0)');

                $("#test-results-tab").click(function(){
                    //$(this).addClass("active");
                    $("#upload").removeClass("active");
                });

                $("#export-tab").click(function(){
                    //$(this).addClass("active");
                    $("#upload").removeClass("active");
                });

                $("#uploadFiles").click(function(){
                    $("#dropzone").submit();
                });

                var myDropzone = new Dropzone("#drop", { url: "{{ route('import') }}"});

            } );
    </script>
@endsection