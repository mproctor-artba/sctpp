@extends('layouts.app')

@section('css')
	<!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

	<style>
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">User Management</li>
    <li class="breadcrumb-item active">References</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
            <div class="row">
				<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Applicant</th>
                            <th>Reference</th>
                            <th>Organization</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Days Outstanding</th>
                            <th class="text-center">Date Added</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($experiences as $experience)
                                <tr>
                                    <td>{{ getFullName($experience->user_id) }}</td>
                                    <td>{{ $experience->reference }}<br>
                                    <a href="mailto:{{ $experience->email }}">{{ $experience->email }}</a><br>
                                    <a href="tel:{{ $experience->phone }}">{{ $experience->phone }}</a></td>
                                    <td>{{ $experience->organization }}</td>
                                    @if($experience->deleted == 0)
                                        @if($experience->status == 1)
                                            <td class="text-center"><h4>✓</h4></td>
                                            <td class="text-center">0</td>
                                        @else
                                            <td class="text-center"><label class="btn btn-info sendReminder" data-tracker="{{ $experience->secret }}"><i class="md md-email"></i> Send Reminder</label><br>
                                            <small style="font-size:70%;">Last Contacted: {{ lastContacted($experience->id) }}</small></td>
                                            <td class="text-center">{{ substr(DateDifference(convertTimestamp($experience->date), convertTimestamp(Date("Y-m-d H:m:s"))), 1) }}</td>
                                        @endif
                                        @else
                                        <td class="text-center">REMOVED</td>
                                        <td class="text-center">--</td>
                                    @endif
                                    </td>
                                    <td class="text-center">
                                        {{ convertTimestamp($experience->date) }}
                                    </td>
                                    <td class="text-center"> 
                                        @if($experience->deleted == 0)
                                            <button type="button" class="btn btn-danger deleteReference" data-reference="{{ $experience->id }}"><i class="fa fa-times"></i></button>
                                            &nbsp;
                                            <button type="button" class="btn btn-info copyURL" data-url="{{ route('verify', ['secret' => $experience->secret, 'spoof' => generateRandomString() ]) }}"><i class="fa fa-clipboard"></i></button>
                                            <a href="{{ route('edit-reference', ['id' => $experience->id]) }}" target="_blank" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                        @else
                                            <button type="button" class="btn btn-primary restoreReference" data-reference="{{ $experience->id }}">RESTORE</button>
                                        @endif
                                    </td>
                                </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')

    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

$("body").on("click", ".sendReminder", function(){
            var tracker = $(this).attr("data-tracker");
            if(confirm("Are you sure you want to send this contact a reminder email?")){
                $.get("{{ route('remindReference', ['secret' => NULL]) }}/" + tracker, function(data, status){
                    $.Notification.notify('success','bottom right', 'Done', 'This reference has been sent a reminder email.');
                });
            }
        });

$("body").on("click" , ".deleteReference", function(){
                            if(confirm("Are you sure you want to delete this reference? This can be undone by restoration.")){
                                var reference = $(this).attr("data-reference");

                                $.get('/admin/users/reference/delete/' + reference, function(data, status){
                                    
                                });
                                $(this).parent().parent().remove();
                            }
                        });
$("body").on("click" , ".restoreReference", function(){
                            if(confirm("Are you sure you want to restore this reference?")){
                                var reference = $(this).attr("data-reference");

                                $.get('/admin/users/reference/restore/' + reference, function(data, status){
                                    
                                });
                                $(this).parent().remove();
                            }
                        });

$("body").on("click" , ".copyURL", function(){
    var str = $(this).attr("data-url");
    copyStringToClipboard (str);
    $.Notification.notify('success','bottom right', 'Copied!', 'The URL for this verification has been copy to your clipboard');
}); 


            function copyStringToClipboard (str) {
   // Create new element
   var el = document.createElement('textarea');
   // Set value (string to be copied)
   el.value = str;
   // Set non-editable to avoid focus and move outside of view
   el.setAttribute('readonly', '');
   el.style = {position: 'absolute', left: '-9999px'};
   document.body.appendChild(el);
   // Select text inside element
   el.select();
   // Copy text to clipboard
   document.execCommand('copy');
   // Remove temporary element
   document.body.removeChild(el);
}
            
            } );
        </script>
@endsection