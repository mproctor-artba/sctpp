@extends('layouts.app')

@section('css')
	<!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

	<style>
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">User Management</li>
    <li class="breadcrumb-item active">All</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
            <div class="row">
				<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Title</th>
                            <th>Company</th>
                            <th>State</th>
                            <th>Joined</th>
                            <th>Deleted On</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($profiles as $profile)
                                <tr>
                                    
                                    <td>{{ $profile->first_name . " " . $profile->last_name }}</td>
                                    <td>{{ $profile->title }}</td>
                                    <td>{{ $profile->company }}</td>
                                    <td>{{ $profile->state }}</td>

                                    <td><span style="display: none;">{{ convertTimestamp($profile->user->created_at, "tostring") }}</span>{{ convertTimestamp($profile->user->created_at) }}</td>
                                    <td><span style="display: none;">{{ convertTimestamp($profile->user->deleted_at, "tostring") }}</span>{{ convertTimestamp($profile->user->deleted_at) }}</td>
                                    <td class="text-center"><a href="/admin/users/restore/{{ $profile->user_id }}" class="btn btn-white" onclick="confirm('Are you sure you want to restore this user profile?');">Restore <i class="fa fa-refresh"></i></a></td>
                                    <td class="text-center">
                                        <a href="mailto:{{ $profile->user->email }}" class="text-center"><i class="fa fa-envelope-o"></i></a>
                                    </td>
                                </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')

    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf'],
                    "fnDrawCallback": function( oSettings ) {
                        getFields()
                    }
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

              
            } );
        </script>
@endsection