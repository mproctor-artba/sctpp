@extends('layouts.app')

@section('css')
	<!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

	<style>
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">User Management</li>
    <li class="breadcrumb-item active">{{ $title }}</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
                <div class="row">
    				<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" style="width:100%;">
                        <thead>
                        <tr>
                            <th>Holder</th>
                            <th>Awarded</th>
                            <th>Title</th>
                            <th>Company</th>
                            <th>City</th>
                            <th>State</th>
                            <th class="text-center" width="10%">Credential ID</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($certificates as $certificate)
                        <tr>
                            <td>{{ $certificate->user->profile->first_name . " " . $certificate->user->profile->last_name }}</td>
                            <td><span style="display: none;">{{ convertTimestamp($certificate->created_at, "tostring") }}</span>{{ convertTimeStamp($certificate->created_at) }}</td>
                            <td>{{ $certificate->user->profile->title }}</td>
                            <td>{{ $certificate->user->profile->company }}</td>
                            <td>{{ $certificate->user->profile->city }}</td>
                            <td>{{ $certificate->user->profile->state }}</td>
                            <td width="10%" class="text-center"><a href="https://www.credential.net/{{ $certificate->certificate }}" target="_blank" class="text-center">{{ $certificate->certificate }}</a></td>
                            <td style="border:none !important;" width="10%"><div class="btn-group" style="margin-bottom: 20px;">
                                <button type="button" class="btn btn-success dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Active <span class="caret"></span></button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Active</a>
                                    <a class="dropdown-item" href="#">Suspended</a>
                                    <a class="dropdown-item" href="#">Revoked</a>
                                </div>
                            </div>
                            </td>
                        </tr>
                        @endforeach
                        <tfoot>
                            <tr>
                                <th>Holder</th>
                                <th>Awarded</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>City</th>
                                <th>State</th>
                                <th class="text-center" width="10%">Credential ID</th>
                                <th>Status</th>
                            </tr>
                        </tfoot>
                        </tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')

    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <script type="text/javascript">
            $(document).ready(function() {

                $('#datatable-buttons tfoot th').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input type="text" style="width:100%;" placeholder="Search '+title+'" />' );
                } );

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

                        // Apply the search
                table.columns().every( function () {
                    var that = this;
             
                    $( 'input', this.footer() ).on( 'keyup change', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
            } );

            $(".dropdown-item").on("click", function(){
                $(this).parent().parent().find("button").val($(this).text());
                $(this).parent().parent().find("button").text($(this).text());
            });
        </script>
@endsection