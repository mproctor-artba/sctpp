@extends('layouts.app')

@section('css')
	<!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

	<style>
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">User Management</li>
    <li class="breadcrumb-item active">All</li>
</ol>
@endsection

@section('content')
<a href="/admin/users/archived">Archived Users</a>
<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
            <div class="row">
				<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Pearson ID</th>
                            <th>Name</th>
                            <th>Title</th>
                            <th>Company</th>
                            <th>State</th>
                            <th>Progress</th>
                            <th>Join Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($profiles as $profile)
                            @if(!$profile->user->isAdmin())
                            @php
                                $progress = ["disposition" => ""];
                                if(!empty($profile->progress()->first())){
                                    $progress = $profile->progress()->orderBy('id', 'DESC')->first()->toArray();
                                }
                            @endphp
                                <tr>
                                    <td>{{ $profile->ClientCandidateID or "n/a" }}</td>
                                    <td>{{ $profile->first_name . " " . $profile->last_name }}</td>
                                    <td>{{ $profile->title }}</td>
                                    <td>{{ $profile->company }}</td>
                                    <td>{{ $profile->state }}</td>
                                    <td class="user-progres" data-user="{{ $profile->user_id }}">{{ $progress["disposition"] }}</td>
                                    <td><span style="display: none;">{{ convertTimestamp($profile->user->created_at, "tostring") }}</span>{{ convertTimestamp($profile->user->created_at) }}</td>
                                    <td class="text-center">
                                        <table >
                                        <tr style="background: none !important;">
                                            <td style="border:none !important;"><a href="mailto:{{ $profile->user->email }}" class="text-center"><i class="fa fa-envelope-o"></i></a></td>
                                            <td style="border:none !important;"><a href="{{ route('user', ['id' => $profile->user_id]) }}" class="text-center"><i class="fa fa-external-link"></i></a></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
                </div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')

    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf'],
                    "fnDrawCallback": function( oSettings ) {
                        getFields()
                    }
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

                getFields();


                function getFields(){

                        $(".user-progress").each(function(){
                            var user_id = $(this).attr("data-user");
                            var cell = $(this);
                            $.get('/admin/users/progress/' + user_id, function(data, status){
                                cell.text(data);
                                console.log(cell);
                            });

                            
                        });
                }
            } );
        </script>
@endsection