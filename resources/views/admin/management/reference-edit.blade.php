@extends('layouts.app')

@section('css')
	<link href="/assets/plugins/custombox/custombox.css" rel="stylesheet">
	<link href="/assets/plugins/select2/select2.min.css"  rel="stylesheet" type="text/css" />
	<style>
	.tab-content{
		width:100%;
	}
	.page-title{
		display: none;
	}
	.select2-container{
		z-index:10002;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">User Management</li>
    <li class="breadcrumb-item">References</li>
    <li class="breadcrumb-item active">Edit</li>
</ol>
@endsection

@section('content')
	<form method="POST" action="{{ route('adminModifyReference') }}">
    	@csrf
    	<input type="hidden" name="referenceID" value="{{ $reference->id }}">
        <div class="form-row">
        	<div class="form-group-custom text-left col-md-12">
                <input type="text" required="" name="organization" value="{{ $reference->organization }}">
                <label class="control-label">Organization Name</label><i class="bar"></i>
            </div>
        </div>
        <div class="form-row">
        	<div class="form-group-custom text-left col-md-6">
                <select required="" name="startMonth" class="form-control material">
                	<option value="">Please Select</option>
                    @for($i = 1; $i <= 12; $i++)
                    	@if($i < 10)
                    		<option value="0{{ $i }}" @if(explode('/', $reference->start)[0] == $i) selected @endif>{{ $i }}</option>
                    	@else
                    		<option value="{{ $i }}" @if(explode('/', $reference->start)[0] == $i) selected @endif>{{ $i }}</option>
                    	@endif
                    @endfor
                </select>
                <label class="control-label">Start Month</label><i class="bar"></i>
            </div>
            <div class="form-group-custom text-left col-md-6">
                <input type="number" required="" name="startYear" value="{{ explode('/', $reference->start)[1]}}" step="1" minlength="4" maxlength="4" min="1950" max="{{ Date('Y') }}">
                <label class="control-label">Start Year (YYYY)</label><i class="bar"></i>
            </div>
        </div>
        <div class="form-row">
        	<div class="form-group col-md-12 text-left">
        		<div class="checkbox">
                    <input id="currently" type="checkbox" name="currently" value="1" @if($reference->finished == "present") checked="" @endif>
                    <label for="currently">
                        I currently work here
                    </label>
                </div>
        	</div>
        </div>
        <div class="form-row endtime">
        	<div class="form-group-custom text-left col-md-6">
                <select required="" name="endMonth" class="form-control material">
                	<option value="">Please Select</option>
                    @for($i = 1; $i <= 12; $i++)
                    	@if($i < 10)
                    		<option value="0{{ $i }}">{{ $i }}</option>
                    	@else
                    		<option value="{{ $i }}">{{ $i }}</option>
                    	@endif
                    @endfor
                </select>
                <label class="control-label">End Month</label><i class="bar"></i>
            </div>
            <div class="form-group-custom text-left col-md-6">
                <input type="number" required="" name="endYear" value="{{ $reference->finished }}" step="1" minlength="4" maxlength="4" min="1950" max="{{ Date('Y') }}">
                <label class="control-label">End Year (YYYY)</label><i class="bar"></i>
            </div>
        </div>
        <div class="form-row" style="margin-bottom: 20px;">
        	<div class="form-group-custom text-left col-md-12">
                <label class="control-label">Experiences</label>
            </div>
            <div class="col-md-12">
            	<select class="select2 select2-multiple" multiple="multiple" name="experiences[]" multiple data-placeholder="Choose ..." required="">
            		<option @if(in_array('Construction Manager', explode(",",$reference->experiences))) selected="" @endif>Construction Manager</option>
            		<option @if(in_array('Designer', explode(",",$reference->experiences))) selected="" @endif>Designer</option>
            		<option @if(in_array('Engineer', explode(",",$reference->experiences))) selected="" @endif>Engineer</option>
            		<option @if(in_array('Foreperson', explode(",",$reference->experiences))) selected="" @endif>Foreperson</option>
                    <option @if(in_array('Laborer', explode(",",$reference->experiences))) selected="" @endif>Laborer</option>
                    <option @if(in_array('Operator', explode(",",$reference->experiences))) selected="" @endif>Operator</option>
                    <option @if(in_array('Owner / Executive', explode(",",$reference->experiences))) selected="" @endif>Owner / Executive</option>
                    <option @if(in_array('Project Manager', explode(",",$reference->experiences))) selected="" @endif>Project Manager</option>
                    <option @if(in_array('Risk Manager', explode(",",$reference->experiences))) selected="" @endif>Risk Manager</option>
                    <option @if(in_array('Safety Professional', explode(",",$reference->experiences))) selected="" @endif>Safety Professional</option>
                    <option @if(in_array('Superintendent', explode(",",$reference->experiences))) selected="" @endif>Superintendent</option>
                </select>
            </div>
        </div>
        <div class="form-row">
        	<div class="form-group-custom text-left col-md-4">
                <input type="text" required="" name="reference" value="{{ $reference->reference }}">
                <label class="control-label">Reference Name</label><i class="bar"></i>
            </div>
            <div class="form-group-custom text-left col-md-4">
                <input type="text" required="" name="email" value="{{ $reference->email }}">
                <label class="control-label">Email</label><i class="bar"></i>
            </div>
            <div class="form-group-custom text-left col-md-4">
                <input type="number" required="" name="phone" value="{{ $reference->phone }}">
                <label class="control-label">Phone</label><i class="bar"></i>
            </div>
        </div>
        <div class="form-row text-left">
        	<em>Note: Once you press submit, your reference will receive an email from certificationteam@sctpp.org asking them to verify your experience. Please be sure beforehand that they are expecting an email from us. Generally, we recommend providing the contact information for someone that currently works in Human Resources.</em>
        	<br>
        </div>
        <div class="form-row text-left">
        	<button class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection

@section('js')

<!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script src="/assets/plugins/select2/select2.min.js" type="text/javascript" ></script>
    <script type="text/javascript">
    @if($reference->finished == "present")
    		hideEndTime();
    	@endif
    	$(".select2").select2();



    	$("#currently").click(function(){
    		if($(this).is(":checked")){
    			
				hideEndTime();

    		} else {
    			$(".endtime ").show();
    			$(".endtime select").each(function(){
    				$(this).attr("required", "required");
    			});
    			$(".endtime input").each(function(){
    				$(this).attr("required", "required");
    			});
    		}
    	});

    	$(".removeReference").click(function(){
    		var reference = $(this).attr("data-reference");
    		if(confirm("Are you sure you want to remove this reference from your application?")){
    			$(this).parent().parent().remove();
                $.get("{{ route('removeExperience', ['reference' => NULL]) }}/" + reference, function(data, status){
                    $.Notification.notify('success','bottom right', 'Done', 'Your reference has been removed.');
                });
    		}
    	});

    	$(".sendReminder").click(function(){
    		var tracker = $(this).attr("data-tracker");
    		if(confirm("Are you sure you want to send this contact a reminder email?")){
                $.get("{{ route('remindReference', ['secret' => NULL]) }}/" + tracker, function(data, status){
                    $.Notification.notify('success','bottom right', 'Done', 'Your reference has been sent a reminder email.');
                });
    		}
    	});

    	function hideEndTime(){
    		$(".endtime ").hide();
    			$(".endtime select").each(function(){
    				$(this).removeAttr("required");
    			});
    			$(".endtime input").each(function(){
    				$(this).removeAttr("required");
    			});
    	}

    </script>
@endsection

