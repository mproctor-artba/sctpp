@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	.tab-content{
		width:100%;
	}
	.table{
		margin-top:10px;
		
	}
	.table, .first-row td{
		border: 0 !important;
	}
	.table tr td, {
		padding-left:0;
	}
	.table thead tr th{
		font-size: 12px;
		font-weight: bold;
	}
    #datatable-buttons tr td{
        font-size: 10px;
    }
    .table.data tr td, .table.data tr th{
        text-align: center;
    }
    .legend{
        background:whitesmoke;
    }
    .dataTables_wrapper{
        max-width: 95% !important;
        padding:0 !important;
    }
    .dataTables_wrapper .col-md-7{
        display: contents !important;
    } 
    #datatable-button_filter input, #datatable-button_filter label{
        width: 100%;
    }
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
	<li class="breadcrumb-item">Analytics</li>
	<li class="breadcrumb-item active">Email Log</li>
</ol>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="widget-bg-color-icon card-box">
            <div class="text-right">
                <h3 class="text-dark"><span class="counter"></span>Mailgun Email Log</h3>
                <p class="text-muted mb-0"></p>
            </div>
            <div class="clearfix"></div>
            <table id="datatable-button" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Activity</th>
                        <th>Email</th>
                        <th>Subject</th>
                        <th>Severity</th>
                        <th>Reason</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($emails as $email)
                    <tr>
                        <td>{{ ucfirst($email->response) }}</td>
                        <td>{{ $email->email }}</td>
                        <td>{{ $email->subject }}</td>
                        <td>{{ $email->severity }}</td>
                        <td>{{ $email->reason }}</td>
                        <td>{{ convertTimeStamp($email->created_at) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection

@section('js')
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

                        //Buttons examples
                var table = $('#datatable-button').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-button_wrapper .col-md-6:eq(0)');


            } );
        </script>

@endsection