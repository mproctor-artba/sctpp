@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	.tab-content{
		width:100%;
	}
	.table{
		margin-top:10px;
		
	}
	.table, .first-row td{
		border: 0 !important;
	}
	.table tr td, {
		padding-left:0;
	}
	.table thead tr th{
		font-size: 12px;
		font-weight: bold;
	}
    #datatable-buttons tr td{
        font-size: 10px;
    }
    .table.data tr td, .table.data tr th{
        text-align: center;
    }
    .legend{
        background:whitesmoke;
    }
    .dataTables_wrapper{
        max-width: 95% !important;
        padding:0 !important;
    }
    .dataTables_wrapper .col-md-7{
        display: contents !important;
    } 
    #datatable-button_filter input, #datatable-button_filter label{
        width: 100%;
    }
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
	<li class="breadcrumb-item">Analytics</li>
	<li class="breadcrumb-item active">Email Log</li>
</ol>
@endsection

@section('content')
<!--
<iframe style="position:fixed; width:100%; height: 100%; padding:0; margin:0; top:64px; left:0; border:0;" src="https://dashboard.artba.org/api/demoSession/start/5d8b8eec261f9b3a348434b3?embed=true&r=false"></iframe>
-->

<div id="sisenseApp">
<div id="widget1"></div>
</div>


@endsection

@section('js')
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script type="text/javascript" src="https://dashboard.artba.org/js/sisense.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            Sisense.connect('https://dashboard.artba.org', false).then(function(app) {
                // Load the dashboard model
                app.dashboards.load('5d8b8eec261f9b3a348434b3').then(function(dash) {
                    // Render an existing widget from the dashboard.
                    // You need to have a DIV element with the ID "widget1" in your page.
                    // You can also use libraries like jQuery to make DOM manipulation & querying easier.
                    dash.widgets.get('5d8b8f27261f9b3a348434b6').container = document.getElementById("widget1");
                   
                    // This command will cause the widget to actually render
                    dash.refresh();
                });
            });
        });
    </script>

@endsection