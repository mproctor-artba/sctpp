@extends('layouts.app')

@section('css')
    <!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
	<style>
	.tab-content{
		width:100%;
	}
	.table{
		margin-top:10px;
		
	}
	.table, .first-row td{
		border: 0 !important;
	}
	.table tr td, {
		padding-left:0;
	}
	.table thead tr th{
		font-size: 12px;
		font-weight: bold;
	}
    .table.data tr td, .table.data tr th{
        text-align: center;
    }
    .badge{
        cursor: pointer;
    }
        .dataTables_wrapper{
        max-width: 95% !important;
        padding:0 !important;
    }
    .dataTables_wrapper .col-md-7{
        display: contents !important;
    } 
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
	<li class="breadcrumb-item"><a href="{{ route('home') }}">Analytics</a></li>
	<li class="breadcrumb-item active">Performance</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-md-6 col-lg-6 col-xl-4">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-warning pull-left">
                <i class="fa fa-trophy text-white"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><span class="counter">{{ $certificates->count() }}</span></h3>
                <p class="text-muted mb-0">Total Certificates <sup data-container="body" title="" data-toggle="popover" data-placement="top" data-content="The number above represents all active certificates since the launch of the program." data-original-title="" class="badge badge-inverse">?</sup></p>
            </div>
            <div class="clearfix"></div>
            <table class="table data"> 
            	<thead>
            		<tr>
            			<th  class="text-left"><strong>Since</strong></th>
            			<th>Active</th>
            			<th>Suspended</th>
            			<th>Revoked</th>
            		</tr>
            	</thead>
            	<tr class="first-row">
	            	<td class="text-left">{{ Date('m') }}/01</td>
	            	<td>{{ $certificates->where('status',2)->where('created_at', '>=', $start)->count() }}</td>
	            	<td>{{ $certificates->where('status',1)->where('created_at', '>=', $start)->count() }}</td>
	            	<td>{{ $certificates->where('status',0)->where('created_at', '>=', $start)->count() }}</td>
            	</tr>
            	<tr>
            		<td class="text-left">30 days</td>
            		<td>{{ $certificates->where('status',2)->where('created_at', '>=', $thirty)->count() }}</td>
            		<td>{{ $certificates->where('status',1)->where('created_at', '>=', $thirty)->count() }}</td>
	            	<td>{{ $certificates->where('status',0)->where('created_at', '>=', $thirty)->count() }}</td>
            	</tr>
            	<tr>
            		<td class="text-left">YTD</td>
            		<td>{{ $certificates->where('status',2)->where('created_at', '>=', $year)->count() }}</td>
            		<td>{{ $certificates->where('status',1)->where('created_at', '>=', $year)->count() }}</td>
	            	<td>{{ $certificates->where('status',0)->where('created_at', '>=', $year)->count() }}</td>
            	</tr>
            	</tr>
            </table>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-success pull-left">
                <i class="fa fa-list text-white"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><span class="counter">{{ $results->count() }}</span></h3>
                <p class="text-muted mb-0">Total Exams Completed <sup data-container="body" title="" data-toggle="popover" data-placement="top" data-content="The number above represents all exams completed since the launch of the program." data-original-title="" class="badge badge-inverse">?</sup></p>
            </div>
            <div class="clearfix"></div>
            <table class="table data"> 
            	<thead>
            		<tr>
            			<th class="text-left"><strong>Since</strong></th>
            			<th>Passed</th>
            			<th>Failed</th>
            			<th>No Show</th>
            		</tr>
            	</thead>
            	<tr class="first-row">
	            	<td class="text-left">{{ Date('m') }}/01</td>
	            	<td>{{ $results->where('Grade','PASS')->where('date', '>=', $start)->count() }}</td>
	            	<td>{{ $results->where('Grade','FAIL')->where('date', '>=', $start)->count() }}</td>
	            	<td>{{ $results->where('NoShow', 'TRUE')->where('date', '>=', $start)->count() }}</td>
            	</tr>
            	<tr>
            		<td class="text-left">30 days</td>
            		<td>{{ $results->where('Grade','PASS')->where('date', '>=', $thirty)->count() }}</td>
            		<td>{{ $results->where('Grade','FAIL')->where('date', '>=', $thirty)->count() }}</td>
	            	<td>{{ $results->where('NoShow', 'TRUE')->where('date', '>=', $thirty)->count() }}</td>
            	</tr>
            	<tr>
            		<td class="text-left">YTD</td>
            		<td>{{ $results->where('Grade','PASS')->where('date', '>=', $year)->count() }}</td>
            		<td>{{ $results->where('Grade','FAIL')->where('date', '>=', $year)->count() }}</td>
	            	<td>{{ $results->where('NoShow', 'TRUE')->where('date', '>=', $year)->count() }}</td>
            	</tr>
            </table>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-5">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-primary pull-left">
                <i class="fa fa-file text-white"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><span class="counter">{{ $applications->count() }}</span></h3>
                <p class="text-muted mb-0">Total Applications Received <sup data-container="body" title="" data-toggle="popover" data-placement="top" data-content="The number above represents all applications we have received since the launch of the program." data-original-title="" class="badge badge-inverse">?</sup></p>
            </div>
            <div class="clearfix"></div>
            <table class="table data"> 
            	<thead>
            		<tr>
            			<th class="text-left"><strong>Since</strong></th>
            			<th>Started</th>
                        <th>Completed</th>
            			<th>Paid</th>
            			<th>Approved</th>
            			<th>Denied</th>
            		</tr>
            	</thead>
            	<tr class="first-row">
	            	<td class="text-left">{{ Date('m') }}/01</td>
	            	<td>{{ $applications->where('date','>=', $start)->count() }}</td>
	            	<td>{{ $applications->where('status', 1)->where('date','>=', $start)->count() }}</td>
	            	<td>{{ $applications->where('status', 2)->where('date','>=', $start)->count() }}</td>
	            	<td>{{ $applications->where('status','>=', 4)->where('date','>=', $start)->count() }}</td>
                    <td>{{ $applications->where('status', 3)->where('date','>=', $start)->count() }}</td>
            	</tr>
            	<tr>
            		<td class="text-left">30 days</td>
            		<td>{{ $applications->where('date','>=', $start)->count() }}</td>
                    <td>{{ $applications->where('status', 1)->where('date','>=', $thirty)->count() }}</td>
                    <td>{{ $applications->where('status', 2)->where('date','>=', $thirty)->count() }}</td>
                    <td>{{ $applications->where('status','>=', 4)->where('date','>=', $thirty)->count() }}</td>
                    <td>{{ $applications->where('status', 3)->where('date','>=', $thirty)->count() }}</td>
            	</tr>
            	<tr>
            		<td class="text-left">YTD</td>
            		<td>{{ $applications->where('date','>=', $start)->count() }}</td>
                    <td>{{ $applications->where('status', 1)->where('date','>=', $year)->count() }}</td>
                    <td>{{ $applications->where('status', 2)->where('date','>=', $year)->count() }}</td>
                    <td>{{ $applications->where('status','>=', 4)->where('date','>=', $year)->count() }}</td>
                    <td>{{ $applications->where('status', 3)->where('date','>=', $year)->count() }}</td>
            	</tr>
            </table>
        </div>
    </div>
</div>
<div class="page-title-box">
    <h4 id="page-title" class="page-title">Statistics</h4>
</div>



<div class="row">
    <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="card-box">
        <canvas id="bar" height="300"></canvas>
        <div style="display: none;">
            <canvas id="pie" height="300" style="display: none;"></canvas>
        </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-6">
                 <div class="widget-bg-color-icon card-box">
                    <div class="bg-icon bg-success pull-left">
                        <i class="fa fa-check text-white"></i>
                    </div>
                    <div class="text-right">
                        <h3 class="text-dark"><span class="counter">@if(is_object($results)){{ number_format($results->first()->passrate(), 2) }} @else 0 @endif</span>%</h3>
                        <p class="text-muted mb-0">Exam Pass Rate <sup data-container="body" title="" data-toggle="popover" data-placement="top" data-content="The ratio of all passed exams to completed exams represented as a percentage." data-original-title="" class="badge badge-inverse">?</sup></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-6">
                 <div class="widget-bg-color-icon card-box">
                    <div class="bg-icon bg-danger pull-left">
                        <i class="fa fa-times text-white"></i>
                    </div>
                    <div class="text-right">
                        <h3 class="text-dark"><span class="counter">@if(is_object($results)){{ number_format($results->first()->attrition(), 2) }} @else 0 @endif</span>%</h3>
                        <p class="text-muted mb-0">Exam Attrition <sup data-container="body" title="" data-toggle="popover" data-placement="top" data-content="Describes the percentage of people who do not retest after failing their exam." data-original-title="" class="badge badge-inverse">?</sup></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="widget-bg-color-icon card-box">
                    <div class="bg-icon bg-warning pull-left">
                        <i class="fa fa-percent text-white"></i>
                    </div>
                    <div class="text-right">
                        <h3 class="text-dark"><span class="counter">
                        @if(is_object($results))
                            {{ number_format($results->where('NoShow','!=','TRUE')->avg('Score'), 2) 
                            }}
                        @else 
                            0 
                        @endif</span></h3>
                        <p class="text-muted mb-0">Average Score <sup data-container="body" title="" data-toggle="popover" data-placement="top" data-content="No Shows are not included in this calculation." data-original-title="" class="badge badge-inverse">?</sup></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-success pull-left">
                <i class="fa fa-map text-white"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><span class="counter">Exam Results by State</span></h3>
                <p class="text-muted mb-0"><sup data-container="body" title="" data-toggle="popover" data-placement="top" data-content="For all exam data since the inception of the program." data-original-title="" class="badge badge-inverse">?</sup></p>
            </div>
            <div class="clearfix"></div>
            <table>
                <tr>
                        <td>Minimum Passed:</td>
                        <td><input type="text" id="min" name="min"><input type="hidden" id="max" name="min" value="10000000"></td>
                        <td></td>
                    </tr>
            </table>
            <table class="table datatable" id="datatable-buttons">
                <thead>
                    <tr>
                        <th>State</th>
                        <th class="text-center">Passed</th>
                        <th class="text-center">Failed</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($states as $state)
                    <tr>
                        <td>{{ $state->name }}</td>
                        <td class="text-center">{{ $state->results()[0] }}</td>
                        <td class="text-center">{{ $state->results()[1] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-primary pull-left">
                <i class="fa fa-building text-white"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><span class="counter">Exam Completions by Company</span></h3>
                <p class="text-muted mb-0"><sup data-container="body" title="" data-toggle="popover" data-placement="top" data-content="For all exam data since the inception of the program." data-original-title="" class="badge badge-inverse">?</sup></p>
            </div>
            <div class="clearfix"></div>
            <table>
                    <tr>
                        <td>Minimum Passed:</td>
                        <td><input type="text" id="min2" name="min2"><input type="hidden" id="max2" name="min" value="10000000"></td>
                        <td></td>
                    </tr>
            </table>
            <table class="table datatable"  id="datatable-buttons2" >
                <thead>
                    <tr>
                        <th>Company</th>
                        <th class="text-center">Passed</th>
                        <th class="text-center">Failed</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($companies as $company)
                    <tr>
                        <td>{{ $company->company }}</td>
                        <td class="text-center">{{ $company->companyResult($company->company)[0] }}</td>
                        <td class="text-center">{{ $company->companyResult($company->company)[1] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-lg-12 col-xl-12">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-pink pull-left">
                <i class="fa fa-calendar text-white"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><span class="counter">Exam Completions by Month</span></h3>
                <p class="text-muted mb-0"><sup data-container="body" title="" data-toggle="popover" data-placement="top" data-content="For all exam data since the inception of the program." data-original-title="" class="badge badge-inverse">?</sup></p>
            </div>
            <div class="clearfix"></div>
            <table class="table datatable" >
                <thead>
                    <tr>
                        <th class="text-center">Exam Year</th>
                        <th class="text-center">January</th>
                        <th class="text-center">February</th>
                        <th class="text-center">March</th>
                        <th class="text-center">April</th>
                        <th class="text-center">May</th>
                        <th class="text-center">June</th>
                        <th class="text-center">July</th>
                        <th class="text-center">August</th>
                        <th class="text-center">September</th>
                        <th class="text-center">October</th>
                        <th class="text-center">November</th>
                        <th class="text-center">December</th>   
                        <th class="text-center">Total</th>                 
                    </tr>
                </thead>
                <tbody>
                    @php
                        $yearNum = 2020;
                    @endphp
                    @foreach($years as $year)
                        @php
                            $total = 0;
                            $year = array_count_values($year);
                            ksort($year);
                        @endphp
                        <tr>
                            <td class="text-center">{{ $yearNum }}</td>
                            @for($i = 1; $i <= 12; $i++)
                                @php
                                    if($i < 10){
                                        $i = "0$i";
                                    }
                                @endphp
                                @if(array_key_exists($i, $year))
                                    @php $total = $total + $year[$i]; @endphp
                                    <td class="text-center">{{ $year[$i] }}</td>
                                    @else
                                    <td class="text-center">0</td>
                                @endif
                            @endfor
                            <td class="text-center">{{ $total }}</td>
                        </tr>
                        @php
                            $yearNum--;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/waves.js"></script>
    <script src="/assets/js/jquery.slimscroll.js"></script>
    <script src="/assets/js/jquery.scrollTo.min.js"></script>

    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Chart JS -->
    <script src="/assets/plugins/chart.js/Chart.bundle.min.js"></script>
    <script type="text/javascript">
        /**
Template Name: Ubold Dashboard
Author: CoderThemes
Email: coderthemes@gmail.com
File: Chartjs
*/


!function($) {
    "use strict";

    var ChartJs = function() {};

    ChartJs.prototype.respChart = function(selector,type,data, options) {
        // get selector by context
        var ctx = selector.get(0).getContext("2d");
        // pointing parent container to make chart js inherit its width
        var container = $(selector).parent();

        // enable resizing matter
        $(window).resize( generateChart );

        // this function produce the responsive Chart JS
        function generateChart(){
            // make chart width fit with its container
            var ww = selector.attr('width', $(container).width() );
            switch(type){
                case 'Pie':
                    new Chart(ctx, {type: 'pie', data: data, options: options});
                    break;
                case 'Bar':
                    new Chart(ctx, {type: 'bar', data: data, options: options});
                    break;
            }
            // Initiate new chart or Redraw

        };
        // run function - render chart at first load
        generateChart();
    },
    //init
    ChartJs.prototype.init = function() {
        //Pie chart

        var pieChart = {
            labels: [
                "Incomplete {{ $progress['percentages'][0] }}",
                "In Progress {{ $progress['percentages'][1] }}",
                "Finishing Up {{ $progress['percentages'][2] }}",
                "Denied {{ $progress['percentages'][3] }}",
                "Awaiting Test Results {{ $progress['percentages'][4] }}"
            ],
            datasets: [
                {
                    data: {{ json_encode($progress["values"]) }},
                    backgroundColor: [
                        "#ff8800",
                        "#ef4554",
                        "#5fbeaa",
                        "#566676",
                        "#007bff"
                    ],
                    hoverBackgroundColor: [
                        "#ff8800",
                        "#ef4554",
                        "#5fbeaa",
                        "#566676",
                        "#007bff"
                    ],
                    hoverBorderColor: "#fff"
                }]
        };
        this.respChart($("#pie"),'Pie',pieChart);

        //barchart
        var barChart = {
            labels: ["2016", "2017", "2018", "2019"],
            datasets: [
                {
                    label: "Passed",
                    backgroundColor: "rgba(95, 190, 170, 0.5)",
                    borderColor: "#5fbeaa",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(95, 190, 170, 0.8)",
                    hoverBorderColor: "#5fbeaa",
                    data: {{ json_encode($examResultsChartData["passed"]) }}
                },
                {
                    label: "Failed",
                    backgroundColor: "rgba(239, 69, 84, 0.5)",
                    borderColor: "#ef4554",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(239, 69, 84, 0.8)",
                    hoverBorderColor: "#ef4554",
                    data: {{ json_encode($examResultsChartData["failed"]) }}
                },
                {
                    label: "No show",
                    backgroundColor: "rgba(255, 136, 0, 0.5)",
                    borderColor: "#ff8800",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(255, 136, 0, 0.8)",
                    hoverBorderColor: "#ff8800",
                    data: {{ json_encode($examResultsChartData["noshow"]) }}
                }
            ]
        };
        this.respChart($("#bar"),'Bar',barChart);

    },
    $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs

}(window.jQuery),

//initializing
function($) {
    "use strict";
    $.ChartJs.init()
}(window.jQuery);

    </script>

    <!-- App js -->
    <script src="/assets/js/jquery.core.js"></script>
    <script src="/assets/js/jquery.app.js"></script>
    <script>
    // Default Datatable
    $(document).ready(function() {

            $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    var min = parseInt( $('#min').val(), 10 );
                    var max = parseInt( $('#max').val(), 10 );
                    var min2 = parseInt( $('#min2').val(), 10 );
                    var max2 = parseInt( $('#max2').val(), 10 );
                    var passed = parseFloat( data[1] ) || 0; // use data for the age column
             
                    if(min2 == 0 || isNaN( min2 ) ){
                    if ( ( isNaN( min ) && isNaN( max ) ) ||
                         ( isNaN( min ) && passed <= max ) ||
                         ( min <= passed   && isNaN( max ) ) ||
                         ( min <= passed   && passed <= max )
                         )
                    {
                        return true;
                    }
                    return false;
                }
                else {
                    if ( ( isNaN( min2 ) && isNaN( max2 ) ) ||
                         ( isNaN( min2 ) && passed <= max2 ) ||
                         ( min2 <= passed   && isNaN( max2 ) ) ||
                         ( min2 <= passed   && passed <= max2 )
                         )
                    {
                        return true;
                    }
                    return false;
                }
                }
            );

                //Buttons examples
            var table = $('#datatable-buttons').DataTable( {
                buttons: [
                    'copy', 'excel', 'pdf'
                ]
            } );

            var table2 = $('#datatable-buttons2').DataTable( {
                buttons: [
                    'copy', 'excel', 'pdf'
                ]
            } );

            $('#datatable-buttons tfoot th').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
                } );

            $('#datatable-buttons2 tfoot th').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
                } );

                // Key Tables

            table.buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

            table2.buttons().container().appendTo('#datatable-buttons2_wrapper .col-md-6:eq(0)');

            $('#min').keyup( function() {
                table.draw();
            } );

            $('#min2').keyup( function() {
                table2.draw();
            } );

    });     
    $("body").click(function(event){
        if($(event.target).attr('class') != "badge badge-inverse"){
            console.log("hiding: " + $(event.target).attr('class'));
            if($(this).find(".popover").hasClass("show")){
                $(".popover").each(function(){
                    $(this).remove();
                });
                $("sup.badge").each(function(){
                    $(this).removeAttr("aria-describedby");
                })
            }  
        }

    })
    </script>
@endsection