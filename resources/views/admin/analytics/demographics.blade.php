@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	.tab-content{
		width:100%;
	}
	.table{
		margin-top:10px;
		
	}
	.table, .first-row td{
		border: 0 !important;
	}
	.table tr td, {
		padding-left:0;
	}
	.table thead tr th{
		font-size: 12px;
		font-weight: bold;
	}
    .table.data tr td, .table.data tr th{
        text-align: center;
    }
    .legend{
        background:whitesmoke;
    }
    .dataTables_wrapper{
        max-width: 95% !important;
        padding:0 !important;
    }
    .dataTables_wrapper .col-md-7{
        display: contents !important;
    } 
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
	<li class="breadcrumb-item">Analytics</li>
	<li class="breadcrumb-item active">Demographics</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-md-6 col-lg-6 col-xl-12">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-primary pull-left">
                <i class="fa fa-users text-white"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><span class="counter">{{ $users->count() }}</span></h3>
                <p class="text-muted mb-0">Total Users</p>
            </div>
            <div class="clearfix"></div>
            <table class="table data"> 
            	<thead>
            		<tr>
            			<th class="text-left"><strong>Since</strong></th>
            			<th class="legend">New</th>
            			<th class="legend">Active</th>
            			<th class="legend">Inactive</th>
                        <th>Profile</th>
                        <th>Application</th>
                        <th>Documents</th>
                        <th>References</th>
                        <th>Terms</th>
                        <th>Payment</th>
                        <th>References Pending</th>
                        <th>Review Pending</th>
                        <th>Test Pending</th>
                        <th>Complete</th>
            		</tr>
            	</thead>
                @for($i = 0; $i < 3; $i++)
                    
                        @if($i == 0)
                        <tr class="first-row">
                            <td class="text-left">03/01</td>
                            <td class="legend">{{ $users->where('created_at', '>=', $start)->count() }}</td>
                            <td class="legend">{{ $users->where('type',1)->where('created_at', '>=', $start)->count() }}</td>
                            <td class="legend">{{ $users->where('type',0)->where('created_at', '>=', $start)->count() }}</td>
                            <td>{{ $step1->where('created_at', '>=', $start)->count() }}</td>
                            <td>{{ $step2->where('created_at', '>=', $start)->count() }}</td>
                            <td>{{ $step3->where('created_at', '>=', $start)->count() }}</td>
                            <td>{{ $step4->where('created_at', '>=', $start)->count() }}</td>
                            <td>{{ $step5->where('created_at', '>=', $start)->count() }}</td>
                            <td>{{ $step6->where('created_at', '>=', $start)->count() }}</td>
                            <td>{{ $step7->where('created_at', '>=', $start)->count() }}</td>
                            <td>{{ $step8->where('created_at', '>=', $start)->count() }}</td>
                            <td>{{ $step9->where('created_at', '>=', $start)->count() }}</td>
                            <td>{{ $complete->where('created_at', '>=', $start)->count() }}</td>
                            </tr>
                        @elseif($i == 1)
                        <tr class="first-row">
                            <td class="text-left">30 days</td>
                            <td class="legend">{{ $users->where('created_at', '>=', $thirty)->count() }}</td>
                            <td class="legend">{{ $users->where('type',1)->where('created_at', '>=', $thirty)->count() }}</td>
                            <td class="legend">{{ $users->where('type',0)->where('created_at', '>=', $thirty)->count() }}</td>
                            <td>{{ $step1->where('created_at', '>=', $thirty)->count() }}</td>
                            <td>{{ $step2->where('created_at', '>=', $thirty)->count() }}</td>
                            <td>{{ $step3->where('created_at', '>=', $thirty)->count() }}</td>
                            <td>{{ $step4->where('created_at', '>=', $thirty)->count() }}</td>
                            <td>{{ $step5->where('created_at', '>=', $thirty)->count() }}</td>
                            <td>{{ $step6->where('created_at', '>=', $thirty)->count() }}</td>
                            <td>{{ $step7->where('created_at', '>=', $thirty)->count() }}</td>
                            <td>{{ $step8->where('created_at', '>=', $thirty)->count() }}</td>
                            <td>{{ $step9->where('created_at', '>=', $thirty)->count() }}</td>
                            <td>{{ $complete->where('created_at', '>=', $thirty)->count() }}</td>
                            </tr>
                        @elseif($i == 2)
                        <tr class="first-row">
                            <td class="text-left">YTD</td>
                            <td class="legend">{{ $users->where('created_at', '>=', $year)->count() }}</td>
                            <td class="legend">{{ $users->where('type',1)->where('created_at', '>=', $year)->count() }}</td>
                            <td class="legend">{{ $users->where('type',0)->where('created_at', '>=', $year)->count() }}</td>
                            <td>{{ $step1->where('created_at', '>=', $year)->count() }}</td>
                            <td>{{ $step2->where('created_at', '>=', $year)->count() }}</td>
                            <td>{{ $step3->where('created_at', '>=', $year)->count() }}</td>
                            <td>{{ $step4->where('created_at', '>=', $year)->count() }}</td>
                            <td>{{ $step5->where('created_at', '>=', $year)->count() }}</td>
                            <td>{{ $step6->where('created_at', '>=', $year)->count() }}</td>
                            <td>{{ $step7->where('created_at', '>=', $year)->count() }}</td>
                            <td>{{ $step8->where('created_at', '>=', $year)->count() }}</td>
                            <td>{{ $step9->where('created_at', '>=', $year)->count() }}</td>
                            <td>{{ $complete->where('created_at', '>=', $year)->count() }}</td>
                            </tr>
                        @endif
                    </tr>
                @endfor
            </table>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-pink pull-left">
                <i class="fa fa-users text-white"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><span class="counter">{{ $codes->count() }}</span></h3>
                <p class="text-muted mb-0">Total Access Codes</p>
            </div>
            <div class="clearfix"></div>
            <table class="table"> 
                <thead>
                    <tr>
                        <th><strong>Since</strong></th>
                        <th>Created</th>
                        <th>Used</th>
                    </tr>
                </thead>
                <tr class="first-row">
                    <td>03/01</td>
                    <td>100</td>
                    <td></td>
                </tr>
                <tr>
                    <td>30 days</td>
                    <td>200</td>
                    <td></td>
                </tr>
                <tr>
                    <td>YTD</td>
                    <td>400</td>
                    <td></td>
                </tr>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-pink pull-left">
                        <i class="fa fa-building text-white"></i>
                    </div>
                    <div class="text-right">
                        <h3 class="text-dark"><span class="counter">{{ $organizations->count() }}</span></h3>
                        <p class="text-muted mb-0">Total Organizations</p>
                    </div>
                    <div class="clearfix"></div>
                    <table class="table"> 
                        <thead>
                            <tr>
                                <th><strong>Since</strong></th>
                                <th>Active</th>
                            </tr>
                        </thead>
                        <tr class="first-row">
                            <td>03/01</td>
                            <td>{{ $organizations->where('created_at', '>=', $start)->count() }}</td>
                        </tr>
                        <tr>
                            <td>30 days</td>
                            <td>{{ $organizations->where('created_at', '>=', $thirty)->count() }}</td>
                        </tr>
                        <tr>
                            <td>YTD</td>
                            <td>{{ $organizations->where('created_at', '>=', $year)->count() }}</td>
                        </tr>
                    </table>
        </div>
    </div>
    <div class="col-md-3 col-lg-3 col-xl-3">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="widget-bg-color-icon card-box">
                    <div class="bg-icon bg-pink pull-left">
                        <i class="fa fa-times text-white"></i>
                    </div>
                    <div class="text-right">
                        <h3 class="text-dark"><span class="counter">{{ $expired }}</span></h3>
                        <p class="text-muted mb-0">Expired Candidates</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="widget-bg-color-icon card-box">
                    <div class="bg-icon bg-pink pull-left">
                        <i class="fa fa-calendar text-white"></i>
                    </div>
                    <div class="text-right">
                        <h3 class="text-dark"><span class="counter">{{ $avgDaystoTest }}</span></h3>
                        <p class="text-muted mb-0">Avg Days to Test</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-lg-3 col-xl-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-pink pull-left">
                <i class="fa fa-calendar text-white"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><span class="counter"></span></h3>
                <p class="text-muted mb-0">Eligibility Options</p>
            </div>
            <div class="clearfix"></div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Option</th>
                        <th>Users</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>{{ $applications->where('eligibility', 1)->count() }}</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>{{ $applications->where('eligibility', 2)->count() }}</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>{{ $applications->where('eligibility', 3)->count() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-pink pull-left">
                        <i class="fa fa-map text-white"></i>
                    </div>
                    <div class="text-right">
                        <h3 class="text-dark"><span class="counter">{{ $profiles->groupBy('state')->count() }}</span></h3>
                        <p class="text-muted mb-0">Total U.S States</p>
                    </div>
                    <div class="clearfix"></div>
                    <table class="table datatable"> 
                        <thead>
                            <tr>
                                <th><strong>State</strong></th>
                                <th>Users</th>
                                <th>Pass Rate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($states as $state)
                                <tr>
                                    <td>{{ $state->state }}</td>
                                    <td>{{ $state->countStates($state->state) }}</td>
                                    <td>{{ $state->statePassRate($state->state) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-5">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-primary pull-left">
                <i class="fa fa-briefcase text-white"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><span class="counter">{{ $users->count() }}</span></h3>
                <p class="text-muted mb-0">Job Titles</p>
            </div>
            <div class="clearfix"></div>
            <div style="overflow-y: auto;">
                <table class="datatable table table-bordered table-bordered dt-responsive nowrap"> 
                    <thead>
                        <tr>
                            <td width="50%">Title</td>
                            <td>Users</td>
                            <td>Pass Rate</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($titles as $jobtitle)
                        @if(is_object($jobtitle) && !empty($jobtitle->title))
                        <tr class="first-row">
                            <td>{{ $jobtitle->title }}</td>
                            <td>{{ $jobtitle->countTitle($jobtitle->title) }}</td>
                            <td>{{ $jobtitle->titlePassRate($jobtitle->title) }}</td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-4">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-primary pull-left">
                <i class="fa fa-building text-white"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><span class="counter">{{ $profiles->groupBy('company')->count() }}</span></h3>
                <p class="text-muted mb-0">Companies Listed</p>
            </div>
            <div class="clearfix"></div>
            <div style="overflow-y: auto;">
                <table class="datatable table table-bordered table-bordered dt-responsive nowrap"> 
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td>Users</td>
                            <td>Pass Rate</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                        <tr>
                            <td>{{ $company->company }}</td>
                            <td>{{ $company->countCompanies($company->company) }}</td>
                            <td>{{ $company->companyPassRate($company->company) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">

</div>

@endsection

@section('js')
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Page Specific JS Libraries -->
    <script src="/assets/plugins/dropzone/dropzone.js"></script>

    <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('.datatable').each(function(){
                    $(this).DataTable();
                });
                $('.datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

                $("#test-results-tab").click(function(){
                    //$(this).addClass("active");
                    $("#upload").removeClass("active");
                });

                $("#export-tab").click(function(){
                    //$(this).addClass("active");
                    $("#upload").removeClass("active");
                });

                $("#uploadFiles").click(function(){
                    $("#dropzone").submit();
                });

                var myDropzone = new Dropzone("#drop", { url: "{{ route('import') }}"});

            } );
    </script>
@endsection