@extends('layouts.app')

@section('css')
	<!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/custombox/custombox.css" rel="stylesheet">
    <link href="/assets/plugins/select2/select2.min.css"  rel="stylesheet" type="text/css" />
	<style>
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
    .select2-container{
        z-index:10002;
    }
    td .select2-container{
        z-index:0 !important;
    }
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">Organizations</li>
    <li class="breadcrumb-item active">{{ $title }}</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
            <div class="row">
                <div class="btn-group" style="margin-bottom: 20px;">
                    <a href="#create" class="btn btn-primary waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Codes</a>
                </div>
				<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Code</th>
                        <th>Owned By</th>
                        <th>Used By</th>
                        <th>Select Courses for Auto Enroll</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($codes as $code)
                        <tr>
                            <td>{{ $code->code }}</td>
                            <td>@if(is_object($code->organization)) {{ $code->organization->name }} @endif</td>
                            <td>
                            @if(is_object($code->user))
                                {{ $code->user->profile->first_name . " " . $code->user->profile->last_name }} 
                            @endif
                            </td>
                            <td>
                                <p style="display:none;">{{ $code->course_id }}</p>
                                <select class="select2 select2-multiple change-course" data-code="{{ $code->code }}" data-user="{{ $code->user }}" multiple="" name="codecourses[]" placeholder="Select Course">
                                    <option value="">Select Course</option>
                                    @foreach($courses as $course)
                                        @php $courseArray = explode(",", $code->courses) @endphp
                                        @if($course->identifier != "")
                                            <option value="{{ $course->id }}" @if(in_array($course->id, $courseArray)) selected @endif>{{ $course->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td class="text-center">@if(!is_object($code->user))<button type="button" class="btn btn-danger deleteCode" data-code="{{ $code->code }}"><i class="fa fa-times"></i></button>@endif</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
        <div id="create" class="modal-demo">
            <button type="button" class="close" onclick="Custombox.close();">
                <span>&times;</span><span class="sr-only">Close</span>
            </button>
            <h4 class="custom-modal-title">Create New Access Code</h4>
            <div class="custom-modal-text">
                <form method="POST" action="{{ route('create-codes') }}" id="createnewcode">
                    @csrf
                    <div class="form-group-custom">
                        <select class="form-control material" name="org" required="">
                            <option value="">Select Organization</option>
                            @foreach($organizations as $organization)
                                <option value="{{ $organization->id }}">{{ $organization->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group-custom">
                        <input type="number" required="required" value="1" step="1" min="1" name="codes">
                        <label class="control-label">Number of access codes to create</label><i class="bar"></i>
                    </div>
                    <div class="form-group-custom">
                        <p class="text-left">Use the dropdown below if you wish for these access codes to auto enroll students into an LMS course.</p>
                        <select name="course[]" class="select2 select2-multiple" data-code="{{ $code->code }}" data-user="{{ $code->user }}" multiple placeholder="Select Course">
                            <option>Select Course</option>
                            @foreach($courses as $course)
                                @if($course->id != "")
                                    <option value="{{ $course->id }}">{{ $course->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="checkbox text-left" style="margin-bottom: 30px;">
                        <input type="checkbox" name="alertadmin" checked=""> Automatically alert the organization admin of new codes
                    </div>
                    <div class="form-group-custom">
                        <input type="submit" class="btn btn-success" name="">
                    </div>
                </form>
            </div>
        </div>

@endsection

@section('js')

    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script src="/assets/plugins/select2/select2.min.js" type="text/javascript" ></script>

    <script type="text/javascript">
            $(document).ready(function() {
                $(".select2").select2();
                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');


                        $("body").on("click" , ".deleteCode", function(){
                            if(confirm("Are you sure you want to delete this access code? This cannot be undone.")){
                                var code = $(this).attr("data-code");

                                $.get('/admin/organizations/codes/delete/' + code, function(data, status){
                                    
                                });
                                $(this).parent().parent().remove();
                            }
                        })

                        $(".change-course").on("change", function(){
                            var code = $(this).attr("data-code");

                            var courses = $(this).serialize();
                            
                            $.ajaxSetup({
                              headers: {
                                'X-CSRF-TOKEN': $("#createnewcode input:eq(0)").val()
                              }
                            });

                            $.ajax({
                              type: "POST",

                              url: "/admin/organizations/codes/updateCourses/" + code,
                              data: courses,
                              dataType: "text/javascript"
                            });
                            
                        });
            } );
        </script>
@endsection