@extends('layouts.app')

@section('css')
	<link href="/assets/plugins/select2/select2.min.css"  rel="stylesheet" type="text/css" />
	<style>
	.tab-content{
		width:100%;
	}
    #datatable-buttons_wrapper{
        width:100% !important;
        padding: 0 !important;
        max-width: 100% !important;
    }
    .dataTables_filter{
        text-align: right;
    }
    .select2-container{
        z-index:1029;
    }
    .select2-selection{
        text-align: left;
    }
    .tab-content{
        box-shadow: none;
    }
    #datatable-buttons_wrapper{
        width:100% !important;
        padding: 0 !important;
        max-width: 100% !important;
    }
    td .select2-container{
        z-index:0 !important;
    }
    </style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
	<li class="breadcrumb-item active">Overview</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-6">
		<div class="card-box">
			<div class="card-body">
                <div class="row">
                	<form method="POST" class="form" style="width:100%;" action="{{ route('updateOrganization', ['id' => $organization->id]) }}" id="organizationProfile">
                	@csrf
                		<div class="form-row mb-10">
                            <div class="col-md-6">
    	                        <label>Organization Name:</label>
    	                        <input type="text" name="name" value="{{ $organization->name }}" class="form-control material required" required>
                            </div>
                            <div class="col-md-6">
                                <label>Organization Prefix:</label>
                                <input type="text" name="prefix" value="{{ $organization->prefix }}" class="form-control material required" required>
                            </div>
	                    </div>
                        <!--
	                    <div class="form-row mb-10">
    	                    <div class="form-group col-md-12">
    	                    	<label>Notes</label>
    	                    	<textarea name="notes" class="form-control">{{ $organization->noted }}</textarea>
    	                    </div>
                        </div>
                        -->
	                    <div class="form-row mb-20">
                            <div class="col-md-12">
                                <label>Company Admin</label>
                                <select class="select2 text-left" name="admin" data-placeholder="Select Organization Admin (Not Required)">
                                    <option value=""></option>
                                    @foreach($profiles as $profile)
                                            <option value="{{ $profile->user_id }}" @if($organization->admin == $profile->user_id) selected @endif>{{ $profile->first_name }} {{ $profile->last_name }} | {{ $profile->user->email }}</option>
                                    @endforeach
                                </select>
                            </div>
	                    </div>
	                    <div class="form-group">
	                    	<input type="submit" name="submit" class="btn btn-primary" value="Save Changes">
	                    </div>
                	</form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card-box">
            <div class="card-body">
                <div class="row mb-10">
                    <div class="col-md-12">
                        <label>Organization Courses</label>
                        <br>
                        <small><em>Courses added here will be auto-enroll all candidates that activate one of {{ $organization->name }}'s access codes.</em></small>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <select class="select2 select2-multiple change-organization-courses" multiple="" name="organizationcourses[]" placeholder="Select Course">
                            <option value="">Select Course</option>
                            @foreach($courses as $course)
                                @php $courseArray = explode(",", $organization->courses) @endphp
                                @if($course->identifier != "")
                                    <option value="{{ $course->id }}" @if(in_array($course->id, $courseArray)) selected @endif>{{ $course->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <ul class="nav nav-pills navtab-bg nav-justified" style="margin-bottom:10px;">
                <li class="nav-item">
                    <a href="#home1" id="home1-tab" data-toggle="tab" aria-expanded="true" class="nav-link active">
                        Codes
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#profile1" id="profile1-tab" data-toggle="tab" aria-expanded="false" class="nav-link">
                        Candidates
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane show active" id="home1" aria-expanded="true">
                    <div class="btn-group" style="margin-bottom: 20px;">
                        <a href="#create" class="btn btn-primary waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Codes</a>
                    </div>
                    <div class="btn-group pull-right" style="margin-bottom: 20px; margin-left:20px;">
                        <a href="{{ route('exportLMS', ['id' => $organization->id]) }}" target="_blank" class="btn btn-warning waves-effect waves-light" data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-download"></i> LMS Export</a>
                    </div>
                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="20%">Code</th>
                            <th>Used By</th>
                            <th>Courses</th>
                            <th width="10%">Created On</th>
                            <th width="10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($codes as $code)
                            <tr>
                                <td>{{ $code->code }}</td>
                                <td>
                                @if(is_object($code->user))
                                    {{ $code->user->profile->first_name . " " . $code->user->profile->last_name }} 
                                @endif
                                </td>
                                <td>
                                <p style="display:none;">{{ $code->course_id }}</p>
                                <select class="select2 select2-multiple change-course" data-code="{{ $code->code }}" data-user="{{ $code->user }}" multiple="" name="codecourses[]" placeholder="Select Course">
                                    <option value="">Select Course</option>
                                    @foreach($courses as $course)
                                        @php $courseArray = explode(",", $code->courses) @endphp
                                        @if($course->identifier != "")
                                            <option value="{{ $course->id }}" @if(in_array($course->id, $courseArray)) selected @endif>{{ $course->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                                <td>{{ convertTimestamp($code->created_at) }}</td>
                                <td class="text-center">@if(!is_object($code->user))<button type="button" class="btn btn-danger deleteCode" data-code="{{ $code->code }}"><i class="fa fa-times"></i></button>@endif</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="profile1" aria-expanded="false">
                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Candidate</th>
                                <th>Progress</th>
                                <th class="text-center">Result</th>
                                <th class="text-center">Certificate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($codes as $code)
                                @if(is_object($code->user))
                                    @php $step = $code->user->currentStep(); @endphp
                                    <tr>
                                        <td>                                
                                            {{ $code->user->profile->first_name . " " . $code->user->profile->last_name }} 
                                        </td>
                                        <td>
                                            {{ $step }}
                                        </td>
                                        @if($step == "Exam complete")
                                            <td class="text-center">
                                                @if($step == "Exam complete")
                                                    @php 
                                                        $profile = $code->profile;
                                                        $result = $profile->results()->orderBy('date', 'desc')->first();
                                                        $grade = strtoupper($result->Grade);
                                                    @endphp
                                                        {{ $grade }}
                                                    @else
                                                        N/A
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($grade == "PASS")
                                                    <a href="https://www.credential.net/{{ $profile->user->certificate->certificate }}" target="_blank" class="text-center">{{ $profile->user->certificate->certificate }}</a>
                                                    @else
                                                        N/A
                                                @endif
                                            </td>
                                        @else
                                            <td class="text-center">N/A</td>
                                            <td class="text-center">N/A</td>
                                        @endif
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end card-box-->
    </div>
</div>
<!-- Modal -->
    <div id="create" class="modal-demo">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only">Close</span>
        </button>
        <h4 class="custom-modal-title">Create New Access Code</h4>
        <div class="custom-modal-text">
            <form method="POST" action="{{ route('create-codes') }}">
                @csrf
                <input type="hidden" name="org" value="{{ $organization->id }}">
                <div class="form-group-custom">
                    <input type="number" required="required" value="1" step="1" min="1" name="codes">
                    <label class="control-label">Number of access codes to create</label><i class="bar"></i>
                </div>
                <div class="checkbox text-left" style="margin-bottom: 30px;">
                    <input type="checkbox" name="alertadmin" checked=""> Automatically alert the organization admin of new codes
                </div>
                <div class="form-group-custom">
                    <input type="submit" class="btn btn-success" name="">
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')

<script src="https://selectize.github.io/selectize.js/js/selectize.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    	$('#admin').selectize({
	        create: false,
	        sortField: 'text'
	    });

	    $("body").on("click" , ".deleteCode", function(){
            if(confirm("Are you sure you want to delete this access code? This cannot be undone.")){
                var code = $(this).attr("data-code");

                $.get('/admin/organizations/codes/delete/' + code, function(data, status){
                    
                });
                $(this).parent().parent().remove();
            }
        });
    });
    </script>

    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script src="/assets/plugins/select2/select2.min.js" type="text/javascript" ></script>

    <script type="text/javascript">
            $(document).ready(function() {
                $("#home1-tab").click(function(){
                    $("#profile1").removeClass("active");
                });
                $(".select2").select2();
                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

            $(".change-organization-courses").on("change", function(){

                var courses = $(this).serialize();

                console.log(courses);
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $("#organizationProfile input:eq(0)").val()
                  }
                });

                $.ajax({
                  type: "POST",
                  url: "/admin/organizations/courses/update/{{ $organization->id }}",
                  data: courses,
                  dataType: "text/javascript"
                });
            });
            $(".change-course").on("change", function(){
                var code = $(this).attr("data-code");

                var courses = $(this).serialize();
                
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $("#organizationProfile input:eq(0)").val()
                  }
                });

                $.ajax({
                  type: "POST",

                  url: "/admin/organizations/codes/updateCourses/" + code,
                  data: courses,
                  dataType: "text/javascript"
                });
                
            });
        </script>

@endsection