@extends('layouts.app')

@section('css')
	<!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/custombox/custombox.css" rel="stylesheet">
    <link href="/assets/plugins/select2/select2.min.css"  rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />
	<style>
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
    .select2-container{
        z-index:10002;
    }
    .select2-selection{
        text-align: left;
    }
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">{{ $page }}</li>
    <li class="breadcrumb-item active">All</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
                <div class="row">
                <div class="btn-group" style="margin-bottom: 20px;">
                    <a href="#create" class="btn btn-primary waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Organization</a>
                </div>
    				<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Admin</th>
                            <th class="text-center">Users</th>
                            <th class="text-center">Codes</th>
                            <th class="text-center">Notifications</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($organizations as $organization)
                            <tr>
                                <td>{{ $organization->name }}</td>
                                <td>{{ $organization->admin()->profile->first_name or "" }} {{ $organization->admin()->profile->last_name or "" }}</td>
                                <td width="10%" class="text-center">{{ usersByOrgCode( $organization->id ) }}</td>
                                <td width="10%" class="text-center">{{ $organization->codes()->count() }}</td>
                                <td width="10%" class="text-center"><input class="switchery" type="checkbox" data-organization="{{ $organization->id }}" data-plugin="switchery" data-color="#1AB394" data-secondary-color="#ED5565" @if(notificationsEnabled($organization->id)) checked="checked" @endif /></td>
                                <td width="5%" class="text-center"><a href="{{ route('organization', ['id' => $organization->id ]) }}"><i class="fa fa-external-link"></a></td>
                            </tr>
                        @endforeach
                            
                        </tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
        <div id="create" class="modal-demo">
            <button type="button" class="close" onclick="Custombox.close();">
                <span>&times;</span><span class="sr-only">Close</span>
            </button>
            <h4 class="custom-modal-title">Create New Organization</h4>
            <div class="custom-modal-text">
                <form method="POST" action="{{ route('new-organization') }}">
                    @csrf
                    <div class="form-group">
                    <select class="select2 text-left" name="admin" data-placeholder="Select Organization Admin (Not Required)">
                        <option value=""></option>
                        @foreach($users as $user)
                            @if(isset($user->profile->first_name))
                                <option value="{{ $user->id }}">{{ $user->profile->first_name }} {{ $user->profile->last_name }} | {{ $user->email }}</option>
                            @endif
                        @endforeach
                    </select>
                    </div>
                    <div class="form-group-custom text-left">
                        <input type="text" required="required" maxlength="255" name="name">
                        <label class="control-label">Organization Name</label><i class="bar"></i>
                    </div>
                    <div class="form-group-custom text-left">
                        <input type="text" required="required" maxlength="5" name="prefix">
                        <label class="control-label">Organization prefix (i.e ARTBA, LANE, SUPER,)</label><i class="bar"></i>
                    </div>
                    <div class="form-group-custom text-left">
                        <input type="number" required="required" value="1" step="1" min="0" name="codes">
                        <label class="control-label">Number of access codes to create</label><i class="bar"></i>
                    </div>
                    <div class="checkbox text-left" style="margin-bottom: 30px;">
                        <input type="checkbox" name="alertadmin" checked=""> Automatically alert the organization admin of this action
                    </div>
                    <div class="form-group-custom">
                        <input type="submit" class="btn btn-success" name="">
                    </div>
                </form>
            </div>
        </div>

@endsection

@section('js')
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script src="/assets/plugins/select2/select2.min.js" type="text/javascript" ></script>
    <script type="text/javascript">
            $(document).ready(function() {
                $(".select2").select2();
                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

                $("body").on("click", ".switchery", function(){
                    var organization = $(this).parent().find("input.switchery").attr("data-organization");
                    $.get('/admin/organizations/notifications/' + organization, function(data, status){
                    });
                });
            } );
        </script>
@endsection