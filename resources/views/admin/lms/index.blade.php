@extends('layouts.app')

@section('css')
	<!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

	<style>
	.dataTables_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">LMS Console</li>
    <li class="breadcrumb-item active">Home</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
        <div class="">
            <ul class="nav nav-tabs nav-bordered">
                <li class="nav-item">
                    <a href="#test-results" data-toggle="tab" class="nav-link active" id="test-results-tab">
                        Edvance360 Users
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#v-profile" data-toggle="tab" class="nav-link" id="export-tab">
                        Edvance360 Courses
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active"  id="test-results">
                    <table id="datatable1" class="table table-striped table-bordered datatable-buttons" cellspacing="0" width="100%">
                        <thead>
                            <tr>   
                                <th>User ID</th>
                                <th>Student</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>SCTPP</th>
                                <th>Certificate</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($students as $student)
                                @if($student->userId != "" && $student->status == 1)
                                    <tr>
                                        <td>{{ $student->userId }}</td>
                                        <td>{{ $student->firstName . " " . $student->lastName }}</td>
                                        <td>{{ $student->username }}</td>
                                        <td>{{ $student->userRole }}</td>
                                        <td>{{ $student->email }}</td>
                                        <td>{{ $student->companyName }}</td>
                                        <td class="text-center sctpp-ver" data-user="{{ $student->email }}"></td>
                                        <td class="text-center cert-ver" data-user="{{ $student->email }}"></td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="v-profile">

                    <table id="datatable2" class="table table-striped table-bordered datatable-buttons" cellspacing="0" width="100%">
                        <thead>
                            <tr>   
                                <th>Course Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($courses as $course)
                                <tr>
                                    <td>{{ $course->name }}</td>
                                    <td>{!! $course->description !!}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</div>
</div>

@endsection

@section('js')

    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <script type="text/javascript">
            $(document).ready(function() {
            

                //Buttons examples
                var tableOne = $('#datatable1').DataTable({
                    lengthChange: true,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    buttons: ['copy', 'excel', 'pdf'],
                    "fnDrawCallback": function( oSettings ) {
                        getFields()
                    }
                });

                var tableTwo = $('#datatable2').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables
                    
                    $("#datatable1_wrapper").prepend(tableOne.buttons().container());
                    $("#datatable2_wrapper").prepend(tableTwo.buttons().container());
                

            } );

            $("#test-results-tab").click(function(){
                console.log("click");
                $("#v-profile").removeClass("active");
                //$("#v-profile").hide();
            })

            function getFields(){
                    $("#datatable1 .sctpp-ver").each(function(){
                        var user_id = $(this).attr("data-user");
                        var cell = $(this);
                        var certCell = $(this).parent().find(".cert-ver");
                        $.get('/admin/users/verify/email/' + user_id, function(data, status){
                            
                            
                            if (data != "false" && data.indexOf(',') < 0){
                                cell.html("<a target='_blank' href='/admin/users/view/" + data + "'><i class='fa fa-check'></i></a>");
                                certCell.html("<i class='fa fa-times'></i>");
                            }
                            else{
                                if(data == "false"){
                                    cell.html("<i class='fa fa-times'></i>");
                                    certCell.html("<i class='fa fa-times'></i>");
                                }
                                else{
                                    data = data.split(',');
                                    cell.html("<a target='_blank' href='/admin/users/view/" + data[0] + "'><i class='fa fa-check'></i></a>");
                                    certCell.html("<a target='_blank' href='https://www.credential.net/" + data[1] + "'>" + data[1] + "</a>");
                                }
                            }
                        });

                        
                    });
                }
        </script>
@endsection