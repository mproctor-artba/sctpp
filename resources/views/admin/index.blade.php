@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
	<li class="breadcrumb-item">Analytics</li>
	<li class="breadcrumb-item active">Overview</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-6">
		<div class="card-box">
			<h5 class="card-title"><a href="{{ route('exam') }}">All Exam Results: {{ $examresults }}</a></h5>
			<div class="card-body">
				                <canvas id="exams" height="150"></canvas>

			</div>
		</div>
	</div>
    <div class="col-lg-6">
        <div class="card-box">
            <h5 class="card-title"><a href="{{ route('certificates') }}">All Certificates: {{ $certificates->count() }}</a></h5>
            <div class="card-body">
                <canvas id="certificates" height="150"></canvas>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="card-box">
			<h5 class="card-title"><a href="{{ route('review') }}">All Applications: {{ $applications->count() }}</a></h5>
			<div class="card-body">
				<div id="pie-chart">
                    <canvas id="applications"  height="150"></canvas>
                </div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')
	<script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/waves.js"></script>
    <script src="/assets/js/jquery.slimscroll.js"></script>
    <script src="/assets/js/jquery.scrollTo.min.js"></script>
    <!-- App js -->
    <script src="/assets/js/jquery.core.js"></script>
    <script src="/assets/js/jquery.app.js"></script>
    <script src="https://www.chartjs.org/dist/2.7.3/Chart.bundle.js" type="text/javascript" ></script>
    <script src="https://artbabridgereport.org/js/utils.js" type="text/javascript" ></script>
    <script>
        var color = Chart.helpers.color;
        var colorNames = Object.keys(window.chartColors);

        @include('layouts.charts.applications')

        @include('layouts.charts.certificates')

        @include('layouts.charts.exam-results')

        window.onload = function() {
            var ctx = document.getElementById('certificates').getContext('2d');
            ctx.height = 150;
            window.certificateChart = new Chart(ctx, {
                type: 'bar',
                data: certificateChartData,
                options: {
                    scales: {
                        yAxes: [{
                            id: 'A',
                            type: 'linear',
                            stacked: false,
                            position: 'left',
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

            ctx = document.getElementById('exams').getContext('2d');
            window.certificateChart = new Chart(ctx, {
                type: 'bar',
                data:  examChartData,
                options: {
                    scales: {
                        yAxes: [{
                            id: 'A',
                            type: 'linear',
                            stacked: false,
                            position: 'left',
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

            ctx = document.getElementById('applications').getContext('2d');
            window.certificateChart = new Chart(ctx, {
                type: 'pie',
                data:  applicationChartData
            });

            ctx = document.getElementById('performance').getContext('2d');
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    title: {
                        display: true,
                        text: 'Domain Performance by Score'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });
        };

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>
@endsection