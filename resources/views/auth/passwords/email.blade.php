<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
		<meta name="author" content="Coderthemes">

		<link rel="shortcut icon" href="assets/images/favicon.ico">

		<title>SCTPP | Password Reset</title>

		<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="/assets/js/modernizr.min.js"></script>
	</head>
	<body>

		<div class="account-pages" style="background-image:url('/images/background.jpg')"></div>
		<div class="clearfix"></div>

        <div class="wrapper-page">
            <div class="card-box">
                <div class="panel-heading">
                    <img src="/images/sctpp-logo.png" width="100%" style="margin-bottom: 30px; padding: 0 20px;">
                </div>

                @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-12">
                            <div class="form-group-custom">
                                <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                 <label class="control-label">{{ __('E-Mail Address') }}</label><i class="bar"></i>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div> 
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" style="width:100%;">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                    <div class="row">
                <div class="col-12 text-center">
                    <strong><p class="text-white">
                        Already have account?<a href="{{ route('login') }}" class="text-white m-l-5"><b>Sign In</b></a>
                    </p></strong>
                </div>
            </div>

                </div>
            </div>

        </div>

		<!-- jQuery  -->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/waves.js"></script>
		<script src="/assets/js/jquery.slimscroll.js"></script>
		<script src="/assets/js/jquery.scrollTo.min.js"></script>

		<!-- App js -->
		<script src="/assets/js/jquery.core.js"></script>
		<script src="/assets/js/jquery.app.js"></script>
        <script>
        $("body").trigger("click");
        </script>
	</body>
</html>