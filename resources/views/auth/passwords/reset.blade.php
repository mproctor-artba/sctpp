<!DOCTYPE html>
<html>
	<head>
        <title>SCTPP | Password Reset</title>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="The ARTBA Foundation’s Safety Certification for Transportation Project Professionals™ (SCTPP) program was established in 2016 to make safety top-of-mind for all professionals involved in the planning, design, management, materials delivery and construction of transportation projects from inception through completion." name="description" />
        <meta content="ARTBA Certification Team" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/assets/images/favicon.png">

		<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="/assets/js/modernizr.min.js"></script>
        <style type="text/css">
            form .form-group-custom .control-label{
                top:-1rem;
            }
        </style>
	</head>
	<body>

		<div class="account-pages" style="background-image:url('/images/background.jpg')"></div>
		<div class="clearfix"></div>

        <div class="wrapper-page">
            <div class="card-box">
                <div class="panel-heading">
                    <img src="/images/sctpp-logo.pngg" width="100%" style="margin-bottom: 20px; padding: 0 20px;">
                    <h4 class="text-center"><strong>Sign In</strong></h4>
                </div>

                <div class="p-20">
                    <form method="POST" action="{{ route('password.request') }}" id="form">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group-custom {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="" name="email" value="{{ $email or old('email') }}" required>
                            <label class="control-label" for="email">Email Address</label><i class="bar"></i>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>


                        <div class="form-group-custom {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="" name="password" required>
                            <label class="control-label" for="user-password">Password</label><i class="bar"></i>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group-custom {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input id="password_confirmation" type="password" class="" name="password_confirmation" required>
                            <label class="control-label" for="user-password">Confirm Password</label><i class="bar"></i>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" id="submit" style="width:100%;">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <strong><p class="text-white">
                        Don't have an account? <a href="{{ route('register') }}" class="text-white m-l-5"><b>Sign Up</b></a>
                    </p>
                    </strong>
                </div>
            </div>

        </div>

		<!-- jQuery  -->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/waves.js"></script>
		<script src="/assets/js/jquery.slimscroll.js"></script>
		<script src="/assets/js/jquery.scrollTo.min.js"></script>

		<!-- App js -->
		<script src="/assets/js/jquery.core.js"></script>
		<script src="/assets/js/jquery.app.js"></script>
        <script>
        $("body").trigger("click");

        $("#form").submit(function(){
            var email = $("#email").val();
            
            if(email != "" && $("#password").val() != "" && $("#password_confirmation").val() != "" && $("#password").val() == $("#password_confirmation").val()){
                $.get("{{ route('pushResetComplete', ['email' => NULL]) }}/" + email, function(data, status){
                    console.log("complete");
                });
            }

            return true;
        });

        $("#submit").click(function() {
            
        });
        </script>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83551434-1', 'auto');
  ga('send', 'pageview');

</script>
	</body>
</html>