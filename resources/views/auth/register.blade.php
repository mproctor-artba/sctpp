<!DOCTYPE html>
<html>
    <head>
        <title>SCTPP | Register</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="The ARTBA Foundation’s Safety Certification for Transportation Project Professionals™ (SCTPP) program was established in 2016 to make safety top-of-mind for all professionals involved in the planning, design, management, materials delivery and construction of transportation projects from inception through completion." name="description" />
        <meta content="ARTBA Certification Team" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/assets/images/favicon.png">

        <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="/css/theme-overide.css" rel="stylesheet" type="text/css" />
        <script src="/assets/js/modernizr.min.js"></script>
    </head>
    <body>

        <div class="account-pages" style="background-image:url('/images/background2.jpg'); background-position-y:bottom;"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="card-box">
                <div class="panel-heading">
                    <img src="/images/sctpp-logo.png" width="100%" style="margin-bottom: 20px; padding: 0 20px;">
                    <h4 class="text-center"><strong>Register</strong></h4>
                </div>

                <div class="p-20">
                    <div class="alert alert-primary text-center">
                        <p><strong>The SCTPP Program is undergoing changes and is no longer accepting new registrations.</strong></p>
                    </div>
                    <div class="form-group-custom mb-0">
                            <div class="" style="padding:0;">
                                <br>
                                <a class="btn btn-link" href="{{ route('login') }}" style="width:100%;">
                                    Back to Login
                                </a>
                            </div>
                        </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 text-center">
                    <strong><p class="text-white">
                        Already have account?<a href="{{ route('login') }}" class="text-white m-l-5"><b>Sign In</b></a>
                    </p></strong>
                </div>
            </div>

        </div>

        <!-- jQuery  -->
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/waves.js"></script>
        <script src="/assets/js/jquery.slimscroll.js"></script>
        <script src="/assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="/assets/js/jquery.core.js"></script>
        <script src="/assets/js/jquery.app.js"></script>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83551434-1', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>