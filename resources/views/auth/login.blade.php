<!DOCTYPE html>
<html>
	<head>
        <title>SCTPP | Login</title>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="The ARTBA Foundation’s Safety Certification for Transportation Project Professionals™ (SCTPP) program was established in 2016 to make safety top-of-mind for all professionals involved in the planning, design, management, materials delivery and construction of transportation projects from inception through completion." name="description" />
        <meta content="ARTBA Certification Team" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/assets/images/favicon.png">

		<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="/css/theme-overide.css" rel="stylesheet" type="text/css" />
        <script src="/assets/js/modernizr.min.js"></script>
        <style type="text/css">
            form .form-group-custom .control-label{
                top:-1rem;
            }
            .invalid-feedback{
                display: block !important;
            }
            .hide{
                display:none;
            }
        </style>
	</head>
	<body>

		<div class="account-pages" style="background-image:url('/images/background.jpg')"></div>
		<div class="clearfix"></div>

        <div class="wrapper-page">
            <div class="card-box">
                <div class="panel-heading">
                    <img src="/images/sctpp-logo.png" width="100%" style="margin-bottom: 20px; padding: 0 20px;">
                    <h4 class="text-center"><strong>Sign In</strong></h4>
                </div>

                <div class="p-20">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        @if ($errors->has('email') || $errors->has('password'))
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="alert alert-danger alert-dismissable" style="margin-top: -10px;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        {{ $errors->first('email') }} 
                                        @if($errors->has('password'))
                                            {{ $errors->first('password') }}
                                        @endif
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="form-group row hide passwordResetNeeded">
                            <div class="col-md-12">
                                <div class="alert alert-danger alert-dismissable" style="margin-top: -10px;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <a href="{{ route('password.request') }}" style="color:white;">Your password has expired. Click here to reset it</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-group-custom">
                                    <input id="email" type="text" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                    <label class="control-label">{{ __('E-Mail Address') }}</label><i class="bar"></i>
                                </div>
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            
                            <div class="col-md-12">
                            <div class="form-group-custom">
                                <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
<label class="control-label">{{ __('Password') }}</label><i class="bar"></i>
</div>

                            </div>
                            <div class="checkbox col-md-12">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                        </div>

                        <div class="form-group-custom mb-0">
                            <div class="" style="padding:0;">
                                <button type="submit" class="btn btn-primary" style="width:100%;">
                                    {{ __('Login') }}
                                </button>
                                <br><br>
                                <a class="btn btn-link" href="{{ route('register') }}" style="width:100%;">
                                    {{ __('Create an Account') }}
                                </a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <strong><p class="text-white">
                        <b><a href="{{ route('password.request') }}" class="text-white m-l-5">Forgot your password?</b></a>
                    </p>
                    </strong>
                </div>
            </div>

        </div>

		<!-- jQuery  -->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/waves.js"></script>
		<script src="/assets/js/jquery.slimscroll.js"></script>
		<script src="/assets/js/jquery.scrollTo.min.js"></script>

		<!-- App js -->
		<script src="/assets/js/jquery.core.js"></script>
		<script src="/assets/js/jquery.app.js"></script>
        <script>
        $("body").trigger("click");


        </script>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83551434-1', 'auto');
  ga('send', 'pageview');

</script>
	</body>
</html>