<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="The ARTBA Foundation’s Safety Certification for Transportation Project Professionals™ (SCTPP) program was established in 2016 to make safety top-of-mind for all professionals involved in the planning, design, management, materials delivery and construction of transportation projects from inception through completion.">
        <meta name="author" content="ARTBA Certification Team">

        <link rel="shortcut icon" href="/assets/images/favicon.png">

        <title>SCTPP | About</title>

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">

        <!-- Bootstrap core CSS -->
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Owl Carousel CSS -->
        <link href="/css/owl.carousel.css" rel="stylesheet">
        <link href="/css/owl.theme.default.min.css" rel="stylesheet">

        <!-- Icon CSS -->
        <link href="/css/font-awesome.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="/css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->

        <style type="text/css">
            .bg-img-1{
                background: url('/images/bg-header.jpg') fixed;
                background-size: cover;

            }
            .bg-img-2{
                background: url('/images/safe_construction.jpg') fixed;
                background-size: cover;

            }
            .hide{
                display: none;
            }
            .section{
                padding-top: 50px;
                padding-bottom: 40px;
            }
            .pricing-column .plan-stats{
                padding: 0px 20px 15px;
            }
            @media (max-width: 767px) {
                .logo img{
                    position: absolute;
                    margin-top:-10px;
                    width:300px;
                }
            }
            .bg-overlay{
                background-color: rgba(60,64,70,0.7);
            }
            .text-muted{
                color: black !important;
            }
            .home-wrapper h4 {
                color: rgba(255, 255, 255, 0.9) !important;
            }
            .features-box{
                padding: 15px;
            }
        </style>

    </head>


    <body data-spy="scroll" data-target="#navbar-menu">

        <!-- Navbar -->
        <nav class="navbar navbar-custom navbar-expand-lg navbar-light">
            <div class="container">
                <a class="navbar-brand logo" href="https://sctpp.org"><img src="/images/artba-tdf-2.jpg" style="max-width: 350px;"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsMenu" aria-controls="navbarsMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsMenu">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#why">Why Certify</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#eligibility">Eligibility</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#discount">Discount</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('register') }}" class="btn btn-custom navbar-btn">Start Application</a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
        <!-- End navbar-custom -->



        <!-- HOME -->
        <section class="home bg-img-1" id="home">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="home-fullscreen">
                            <div class="full-screen">
                                <div class="home-wrapper home-wrapper-alt" style="padding-top: 150px;">
                                    <h1 class="text-white" style="font-size: 3rem;">Safety Certification for <br>Transportation Project Professionals&trade;</h1>
                                    <h4 class="" style="width:82%;">Our vision is to ensure the safety and well-being of construction workers, motorists, truck drivers, pedestrians and their families by making transportation project sites worldwide zero-incident zones.</h4>
                                    <a href="{{ route('register') }}" target="_blank" class="btn btn-white-bordered">Get Certified. Save Lives&trade;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END HOME -->

        <!-- Features -->
        <section class="section" id="why">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3 class="title">Why Invest in Safety Certification?</h3>
                        <p class="text-muted sub-title">Certification demonstrates your knowledge of internationally-recognized core competencies for<br> safety awareness and risk management on transportation projects.</p>
                    </div>
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-sm-4">
                        <div class="features-box">
                            <i class="fa fa-diamond"></i>
                            <h6 class="m-t-20">Safer Project Sites</h6>
                            <p class="text-muted">Show that the safety of people in and around your transportation project sites is <b>a top priority</b>.</p>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="features-box">
                            <i class="fa fa-trophy"></i>
                            <h6 class="m-t-20">Competitive Edge</h6>
                            <p class="text-muted">Demonstrate your <b>commitment to reducing liability</b> and making your project sites incident free.</p>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="features-box">
                            <i class="fa fa-signal"></i>
                            <h6 class="m-t-20">Flexible Requirements</h6>
                            <p class="text-muted">Applicants can choose from three different eligibility options accommodated to their <b>educational backgrounds and work experience</b>.</p>
                        </div>
                    </div>

                </div> <!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- end Features -->



        <!-- Features Alt -->
        <section class="section p-t-0">
            <div class="container">

                <div class="row">
                    <div class="col-sm-5">
                        <div class="feat-description m-t-20">
                            <h4>You're Next! Get Certified Within 30 Days</h4>
                            <p class="text-muted">Every step of the certification process is completed online, providing individuals more flexibility and shorter wait times. Our application takes only a few minutes to complete and provides firms or agencies with the flexibility they need to process hundreds of employees at a time.</p>

                            <a href="{{ route('register') }}" class="btn btn-custom">Start Application</a>
                        </div>
                    </div>

                    <div class="col-sm-6 ml-auto">
                        <img src="/images/sctpp-portal-cert.jpg" alt="img" class="img-fluid m-t-20">
                    </div>

                </div><!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- end features alt -->

                <!-- PRICING -->
        <section class="section" id="eligibility">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3 class="title">Applicants must meet one of the three eligibility requirements</h3>
                        <p class="text-muted sub-title"></p>
                    </div>
                </div> <!-- end row -->


                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="row">

                            <!--Pricing Column-->
                            <article class="pricing-column col-lg-4 col-md-4">
                                <div class="inner-box">
                                    <div class="plan-header text-center">
                                        <h2 class="plan-price">Option 1</h2>
                                    </div>
                                    <ul class="plan-stats list-unstyled">
                                        <li>Three years’ full-time or equivalent experience<sup>*</sup> in the transportation construction industry.</li>
                                        <li>Completion of an OSHA-10, OSHA-30, OSHA-500 OR OSHA-510 course.</li>
                                    </ul>
                                </div>
                            </article>


                            <!--Pricing Column-->
                            <article class="pricing-column col-lg-4 col-md-4">
                                <div class="inner-box active">
                                    <div class="plan-header text-center">
                                        <h2 class="plan-price">Option 2</h2>
                                    </div>
                                    <ul class="plan-stats list-unstyled">
                                        <li>Baccalaureate degree in engineering or construction management.</li>
                                        <li>Two years’ experience<sup>*</sup> in the transportation construction industry. Experience must be within five years of applying for certification.</li>
                                        <li>Completion of an OSHA-10, OSHA-30, OSHA-500 OR OSHA-510 course.</li>
                                    </ul>
                                </div>
                            </article>


                            <!--Pricing Column-->
                            <article class="pricing-column col-lg-4 col-md-4">
                                <div class="inner-box">
                                    <div class="plan-header text-center">
                                        <h2 class="plan-price">Option 3</h2>
                                    </div>
                                    <ul class="plan-stats list-unstyled">
                                        <li>Associate or technical degree in safety.</li>
                                        <li>Two years’ experience<sup>*</sup> in the transportation construction industry. Experience must be within five years of applying for certification.</li>
                                    </ul>
                                </div>
                            </article>
                            <div class="col-md-12">
                                <p><em>* Experience is defined as a job classification including, but not limited to, laborer, operator, foreperson, superintendent, project manager, construction manager, engineer, safety professional, risk manager, inspector, surveyor, or estimator. Internships qualify as experience. Experience must be within five years of applying for certification.</em></p>
                            </div>
                        </div>
                    </div><!-- end col -->
                </div>
                 <!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- End Pricing -->

        


        <!-- Features Alt -->
        <section class="section" id="discount" style="padding-bottom: 0;">
            <div class="container">

                <div class="row">
                    <div class="col-sm-7">
                        <a href="https://puttingsafetyfirst.org"><img src="/images/sctpp-audience.jpg" alt="img" class="img-fluid"></a>
                    </div>

                    <div class="col-sm-5 ml-auto">
                        <div class="feat-description">
                            <h4>Join the Safety Movement and Save 20%</h4>
                            <p class="text-muted">Over 200 transportation construction firms and state DOTs have submitted candidates for certification. Act now and take advantage of our 20 percent discount on application and testing fees. <a href="https://store.artba.org/product/sctpp-application-and-testing-fee/" target="_blank">Organizations that want to sponsor</a> one or more employees can order access codes to distribute to their applicants.</p>

                            <a href="{{ route('register') }}" class="btn btn-custom">Start Application</a>
                        </div>
                    </div>

                </div><!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- end features alt -->


        <!-- Features Alt -->
        <section class="section hide">
            <div class="container">

                <div class="row">
                    <div class="col-sm-5">
                        <div class="feat-description">
                            <h4>Praesent et viverra massa non varius magna eget nibh vitae velit posuere efficitur.</h4>
                            <p class="text-muted">Ubold is a fully featured premium admin template built on top of awesome Bootstrap 4.0.0-beta, modern web technology HTML5, CSS3 and jQuery. It has many ready to use hand crafted components. The theme is fully responsive and easy to customize. The code is super easy to understand and gives power to any developer to turn this theme into real web application. </p>

                            <a href="" class="btn btn-custom">Learn More</a>
                        </div>
                    </div>

                    <div class="col-sm-6 ml-auto">
                        <img src="/images/mac_3.png" alt="img" class="img-fluid">
                    </div>

                </div><!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- end features alt -->


        <!-- Testimonials section -->
        <section class="section bg-img-1 hide">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-10 col-md-8">
                        <div class="owl-carousel text-center">
                            <div class="item">
                                <div class="testimonial-box">
                                    <h4>Excellent support for a tricky issue related to our customization of the template. Author kept us updated as he made progress on the issue and emailed us a patch when he was done.</h4>
                                    <img src="images/user.jpg" class="testi-user rounded-circle" alt="testimonials-user">
                                    <p>- Ubold User</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-box">
                                    <h4>Flexible, Everything is in, Suuuuuper light, even for the code is much easier to cut and make it a theme for a productive app..</h4>
                                    <img src="images/user2.jpg" class="testi-user rounded-circle" alt="testimonials-user">
                                    <p>- Ubold User</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-box">
                                    <h4>Not only the code, design and support are awesome, but they also update it constantly the template with new content, new plugins. I will buy surely another coderthemes template!</h4>
                                    <img src="images/user3.jpg" class="testi-user rounded-circle" alt="testimonials-user">
                                    <p>- Ubold User</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- End Testimonials section -->


        <!-- Clients -->
        <section class="section p-t-0 hide" id="">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3 class="title">Trusted by Thousands</h3>
                        <p class="text-muted sub-title">The clean and well commented code allows easy customization of the theme.It's <br/> designed for describing your app, agency or business.</p>
                    </div>
                </div>
                <!-- end row -->

                <div class="row text-center">
                    <div class="col-sm-12">
                        <ul class="list-inline client-list">
                            <li class="list-inline-item"><a href="" title="Microsoft"><img src="images/clients/microsoft.png" alt="clients"></a></li>
                            <li class="list-inline-item"><a href="" title="Google"><img src="images/clients/google.png" alt="clients"></a></li>
                            <li class="list-inline-item"><a href="" title="Instagram"><img src="images/clients/instagram.png" alt="clients"></a></li>
                            <li class="list-inline-item"><a href="" title="Converse"><img src="images/clients/converse.png" alt="clients"></a></li>
                        </ul>
                    </div> <!-- end Col -->

                </div><!-- end row -->

            </div>
        </section>
        <!--End  Clients -->


        <!-- FOOTER -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p class="copyright" style="color:white;">© American Road & Transportation Builders Association {{ Date('Y') }}. All rights reserved.</p>
                    </div>
                    <div class="col-md-3 ml-auto hide">
                        <ul class="social-icons text-md-right">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </footer>
        <!-- End Footer -->
        

        <!-- Back to top -->    
        <a href="#" class="back-to-top" id="back-to-top"> <i class="fa fa-angle-up"></i> </a>


        <!-- js placed at the end of the document so the pages load faster -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/popper.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>

        <!-- Jquery easing -->                                                      
        <script type="text/javascript" src="/js/jquery.easing.1.3.min.js"></script>

        <!-- Owl Carousel -->                                                      
        <script type="text/javascript" src="/js/owl.carousel.min.js"></script>

        <!--common script for all pages-->
        <script src="/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:false,
                autoplay: true,
                autoplayTimeout: 4000,
                responsive:{
                    0:{
                        items:1
                    }
                }
            })
        </script>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83551434-1', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>