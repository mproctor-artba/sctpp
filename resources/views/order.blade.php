<!DOCTYPE html>
<html>
	<head>
        <title>SCTPP | Order</title>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="The ARTBA Foundation’s Safety Certification for Transportation Project Professionals™ (SCTPP) program was established in 2016 to make safety top-of-mind for all professionals involved in the planning, design, management, materials delivery and construction of transportation projects from inception through completion." name="description" />
        <meta content="ARTBA Certification Team" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/assets/images/favicon.png">

		<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="/css/theme-overide.css" rel="stylesheet" type="text/css" />
        <script src="/assets/js/modernizr.min.js"></script>
        <link rel="stylesheet" href="https://selectize.github.io/selectize.js/css/selectize.default.css" data-theme="default">
        <style type="text/css">
            form .form-group-custom .control-label{
                top:-1rem;
            }
            .invalid-feedback{
                display: block !important;
            }
            .hide{
                display:none;
            }
    .tab-content{
        width:100%;
    }
/**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
.StripeElement {
    width: 100%;
  background-color: white;
  height: 40px;
  padding: 10px 12px;
  border-radius: 4px;
  border: 1px solid transparent;
  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}


@-webkit-keyframes rotating /* Safari and Chrome */ {
  from {
    -webkit-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes rotating {
  from {
    -ms-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -webkit-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  to {
    -ms-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -webkit-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
.fa-refresh {
  -webkit-animation: rotating 2s linear infinite;
  -moz-animation: rotating 2s linear infinite;
  -ms-animation: rotating 2s linear infinite;
  -o-animation: rotating 2s linear infinite;
  animation: rotating 2s linear infinite;
}
.select2-selection{
        text-align: left;
    }
    .selectize-control.single .selectize-input{
            background-color: white !important;
            background-image: none !important;
            border: none;
            border-bottom: 1px solid #999;
            border-radius: 0 !important;
    }
    .selectize-control{
        padding-top: 2px !important;
        padding-left: 0 !important;
    }
    .selectize-control .item{
        font-size: 1rem !important;
        color: #565656 !important;
    }

</style>
	</head>
	<body>

		<div class="account-pages" style="background-image:url('/images/background.jpg')"></div>
		<div class="clearfix"></div>

        <div class="wrapper-page" style="width:840px;">
            <div class="card-box">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="{{ URL('/images/sctpp-ansi.png') }}" width="100%" style="max-width: 378px; margin-bottom: 20px; padding: 40px 20px 0 20px;">
                        </div>
                        <div class="col-md-6">
                            <h4 class="text-center"><strong>Access Code Order Form</strong></h4>
                            <p>Organizations can cover the application and testing fees for their candidates with this product by purchasing one or more access codes. <strong>Each access code is $400</strong> and can only be used once. Please check to make sure each candidate meets one of the three <a href="https://www.sctpp.org/get-certified/certification/" target="_blank">eligibility requirements</a>.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if (session('confirmation'))
                                <div class="alert alert-success">
                                    {{ session('confirmation') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="p-20">
                    <form id="payment-form" method="POST" action="{{ route('submitOrder') }}">
                        {{ CSRF_FIELD() }}
                        <div class="form-row">
                            <div class="form-group-custom text-left col-md-12">
                                
                                
                            </div>
                        </div>
                        <div class="form-row" style="margin-bottom: 5px;">
                            <div class="col-md-6">
                                <div class="form-group-custom">
                                    <input id="name" type="text" class="" name="name" value="" required>
                                    <label class="control-label">Your Name</label><i class="bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group-custom">
                                    <input id="email" type="text" class="" name="email" value="" required>
                                    <label class="control-label">Your Email</label><i class="bar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-row" style="margin-bottom: 5px;">
                            <div class="col-md-6">
                                <div class="form-group-custom">
                                    <input id="title" type="text" class="" name="title" value="" required>
                                    <label class="control-label">Your Title</label><i class="bar"></i>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group-custom">
                                    <select id="company" class="demo-default required material" placeholder="Please Select or Enter Company Name" name="organization" required="">
                                        <option value="">Please Select or Enter Company Name</option>
                                        @foreach($organizations as $organization)
                                                <option value="{{ $organization->id }}">{{ $organization->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-row" style="margin-bottom: 5px;">
                            <div class="form-group-custom text-center col-md-6">
                                <select class="form-control material required option" name="quantity" id="quantity" required="">
                                    <option value="">How many candidates do you have?</option>
                                    @for($i = 1; $i < 11; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-6"> 
                                <p>Please <a href="mailto:certificationteam@sctpp.org">contact us</a> if you have more than 10 candidates.</p>
                            </div>
                        </div>
                        <div class="form-row" style="margin-bottom: 20px;">
                            <div id="card-element">
                              <!-- a Stripe Element will be inserted here. -->
                            </div>
                            <!-- Used to display form errors -->
                            <div id="card-errors" role="alert"></div>
                        </div>
                        <div class="form-row action">
                          <div class="form-group">
                            <button type="submit" class="btn btn-primary submit"><i class="icon"></i> Place Order <span class="btn-label btn-label-right"><i class="fa fa-dollar"></i><span id="total">0</span></button>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

		<!-- jQuery  -->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/waves.js"></script>
		<script src="/assets/js/jquery.slimscroll.js"></script>
		<script src="/assets/js/jquery.scrollTo.min.js"></script>

		<!-- App js -->
		<script src="/assets/js/jquery.core.js"></script>
		<script src="/assets/js/jquery.app.js"></script>
    <script src="https://selectize.github.io/selectize.js/js/selectize.js"></script>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-83551434-1', 'auto');
          ga('send', 'pageview');

          $("#quantity").on("change", function(){
            var val = $(this).val();
            console.log(val);
            $("#total").text(val * 400);
          });

          $('#company').selectize({
        create: true,
        sortField: 'text'
    });
        </script>

<script src="https://js.stripe.com/v3/" id="striping1"></script>
<script type="text/javascript" id="striping2">
    //Stripe.setPublishableKey('');
// Create a Stripe client
var stripe = Stripe('{{ env('STRIPE_KEY') }}');

// Create an instance of Elements
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
$(".submit .icon").addClass("fa fa-refresh");

  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server
      
    $("#payment-form").append('<input type="hidden" name="stripeToken" value="' + result.token.id + '">');
    $("#payment-form").append('<input type="hidden" name="cardnumber" value="' + result.token.card.last4+ '">');
    $("#payment-form").append('<input type="hidden" name="cardID" value="' + result.token.card.id+ '">');
      

    $("#payment-form").submit();
    }
  });
});
</script>
	</body>
</html>