@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
    form .form-group-custom .control-label{
        position: absolute;
        top: 0.25rem;
        pointer-events: none;
        padding-left: 0.125rem;
        z-index: 1;
        color: #98a6ad;
        font-weight: normal;
        -webkit-transition: all 0.28s ease;
        transition: all 0.28s ease;
    }
    .page-title{
        display: none;
    }
    span.text-primary{
        cursor: pointer;
    }
	</style>
@endsection

@section('content')

<div class="row">
<div class="col-lg-10 offset-md-1">
<form method="POST" action="{{ route('saveEligibility') }}">
@csrf
    <div class="tabs-vertical-env">
        <ul class="nav tabs-vertical">
            <li class="nav-item">
                <a href="#v-messages" data-toggle="tab" aria-expanded="false" class="nav-link active v-messages">
                    Select Eligibility
                </a>
            </li>
            <li class="nav-item">
                <a href="#v-ethics" data-toggle="tab" aria-expanded="false" class="nav-link v-ethics">
                    Code of Ethics
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="v-messages">
                <h4 class="m-t-0 header-title"><b>Candidates must meet one of the following eligibility requirements:</b></h4>
				   
				<b><span class="text-primary option" data-val="1">Option 1</span></b>
                <ul>
				<li>Proof of three years’ full-time or equivalent experience* in the transportation construction industry. Transportation construction is occupational experience that includes but is not limited to construction of highways, roads, bridges, airports, rail systems, tunnels, utilities and ports.</li>
                <li>Completion of an OSHA-10, OSHA-30, OSHA-500, OSHA-510 course OR approved equivalent. Approved equivalency includes Associate Safety Professional (ASP), Certified Safety Professional (CSP), Safety Management Specialist (SMS), or Safety Trained Supervisor for Construction (STSC).</li>
                </ul>

				<b><span class="text-primary option" data-val="2">Option 2</span></b>

                <ul>
				<li>Baccalaureate degree in engineering or construction management with two years’ experience* in the transportation construction industry. Experience must be within five years of applying for certification.</li>
                <li>Completion of an OSHA-10, OSHA-30, OSHA-500, OSHA-510 course OR approved equivalent. Approved equivalency includes Associate Safety Professional (ASP), Certified Safety Professional (CSP), Safety Management Specialist (SMS), or Safety Trained Supervisor for Construction (STSC).</li>
                </ul>
				<b><span class="text-primary option" data-val="3">Option 3</span></b>

				<ul><li>Associate or technical degree in safety with two years’ experience* in the transportation construction industry.</li></ul>

<hr></hr>
				<p><em>* Experience must be within five years of applying for certification. Experience is defined as a job classification including, but not limited to, laborer, operator, foreperson, superintendent, project manager, construction manager, engineer, safety professional, risk manager, inspector, surveyor, or estimator. Internships qualify as experience. Experience must be within five years of applying for certification
</em>
</p>
<p><em>
† Individuals or organizations that desire to have a course considered as equivalent to an accepted OSHA course must submit documentation to the certification staff for review and approval.  Fees for course equivalency evaluation must be paid at the time documentary evidence is submitted.

For a course to be equated to OSHA courses, it must meet the following criteria:
<ul style="padding-left:0;">
    <ol>1. At least 10 hours of classroom instructional time;</ol>
    <ol>2. Instruction must include a minimum number of hours on the following topics:</ol>
    <ol>
        <ul>
            <li>Falls – 1.5 hours</li>
            <li>Electrocution – 1 hour</li>
            <li>Struck-by – 1.5 hours</li>
            <li>Caught-In – 1 hour.</li>
            <li>Topics for the remaining five (5) hours must align with items designated on the Exam Blueprint.</li>
        </ul>
    </ol>
    <ol>3. Courses must be developed and taught by a government entity, accredited college or university, national trade association or labor union.</ol>
    <ol>4. Evidence of course completion must be submitted. Acceptable forms of evidence include training sign-in sheets, certificates of completion and employment records.  All evidentiary documentation must be signed and certified by the training provider, employer or other person other than the candidate.</ol>
</ul>
</em></p>

				<div class="form-group">
					<select id="optionSelect" class="form-control material required" name="eligibility" required="" style="border-bottom: 4px solid #4285f4 !important; max-width: 400px;">
						<option value="">Select how you would like to apply</option>
						<option value="1" @if(isset($application->eligibility) && $application->eligibility == 1)selected @endif>Option 1</option>
						<option value="2" @if(isset($application->eligibility) && $application->eligibility == 2)selected @endif>Option 2</option>
						<option value="3" @if(isset($application->eligibility) && $application->eligibility == 3)selected @endif>Option 3</option>
					</select>
				</div>
                <div class="form-group">
                    <button class="btn btn-primary next" type="button">Next <span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></button>
                </div>
            </div>
            <div class="tab-pane" id="v-ethics">
                <h4 class="m-t-0 header-title"><b>SCTPP&trade; Code of Ethics</b></h4>
                <br>
                <p>The Code of Ethics of the Certification Commission requires certified persons to uphold the rules and  requirements for Safety Certification for Transportation Project Professionals&trade; (SCTPP). The Code of Ethics allows for the proper discharge of the responsibilities of those they serve, and for the protection and integrity of the credential. Agreement to uphold and abide by the Code of Ethics is a requirement for earning and  maintaining SCTPP certification. Implicit in this agreement is an obligation not only to comply with the  mandates and requirements of all applicable laws and regulations, but to act in an ethical manner in all  professional services and activities. Certified persons who fail to comply with the Code of Ethics are subject to disciplinary procedures which may result in sanctions, including but not limited to revocation of certification status. The Code of Ethics is not set forth to determine behaviors resulting in criminal or civil liability, nor are they intended to resolve matters of market competition.</p>
                <p>
                As a safety certified transportation project professional &trade;, I agree to uphold and abide by the following tenets:
                </p>
                <ol>
                    <li>Perform duties in a safe, ethical and competent manner.</li>
                    <li>Provide complete and accurate information when applying for certification and recertification.</li>
                    <li>Represent qualifications with honesty and integrity.</li>
                    <li>Abide by and uphold the policies of the Certification Commission.</li>
                    <li>Truthfully represent the scope of the certification.</li>
                    <li>Safeguard the integrity of the certification and refrain from behavior that would bring the certification body into disrepute.</li>
                    <li>Use the logo and certification marks only in an authorized and approved manner.</li>
                    <li>Pay all fees and provide information required by the Certification Commission.</li>
                    <li>Should certification be suspended or withdrawn, agree to discontinue claims to certification, including references to the Certification Commission, and return the certificate and related items to SCTPP personnel.</li>
                </ol>
                
                <p>The following sanctions may be imposed for failure to abide by the code of ethics. The sanction shall be based on the severity of the violation, and shall include, but not be limited to:</p>
                
                <ul>
                    <li>Cease and Desist</li>
                    <li>Written reprimand</li>
                    <li>Written reprimand with remediation</li>
                    <li>Censure</li>
                    <li>Suspension</li>
                    <li>Revocation</li> 
                    <li>Permanent revocation</li>
                </ul>
                <p>In addition to imposing sanctions, ARTBA Foundation shall have the authority to report sanctions to legal and regulatory authorities, and other credentialing organizations as appropriate.</p>
                <label><input type="checkbox" name="ethics" value="1" required="" id="ethics" aria-required="true" @if(isset($application->eligibility))checked @endif> I have read the Code of Ethics and agree to uphold and abide by the Code.</label>
                <br><br>
                @if(isset(Auth::user()->application->status))
                    @if(Auth::user()->application->status <= 1 || Auth::user()->application->status == 3)
                        <button type="submit" class="btn btn-primary">Save @if(Auth::user()->isNew()) and Move to Step 3<span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span> @else and Next @endif</button>
                    @endif
                @else
                        <button type="submit" class="btn btn-primary">Save @if(Auth::user()->isNew()) and Move to Step 3<span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span> @else and Next @endif</button>

                @endif
                <p class="help-text text-danger"><strong></strong></p>

            </div>
        </div>
    </div>
    </form>
    </div>
</div>

@endsection

@section('js')
    <script type="text/javascript">
    var option = $("#optionSelect").val();

    if(option == ""){
        $(".text-danger strong").text("You must select an eligbility option submit");
    }

    $("#optionSelect").on("change", function(){
        option = $(this).val();

        if(option != ""){
            $(".text-danger strong").text("");
        } else {
            $(".text-danger strong").text("You must select an eligbility option submit");
        }
    });
    
        $(".next").click(function(){
            $(".v-messages").removeClass("active");
            $("#v-messages").removeClass("active");
            $(".v-ethics").addClass("active");
            $("#v-ethics").addClass("active");
        })

        $(".option").click(function(){
            $("#optionSelect").val($(this).attr("data-val"));
        });
    </script>
@endsection