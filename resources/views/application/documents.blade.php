@extends('layouts.app')

@section('css')
	<link href="/assets/plugins/dropzone/dropzone.css" rel="stylesheet" type="text/css" />
	<style>
	.tab-content{
		width:100%;
	}
	.page-title{
		display: none;
	}
	.col-md-4{
		display: inline-block;
		max-width: 32%;
	}
	.widget-bg-color-icon .bg-icon{
		margin: 0 auto;
	}
    .col-md-6.flex{
        display: -webkit-inline-box;
        max-width:49% !important;
    }
	</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-7">
            <div class="col-md-12">
                <h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>My Documents</b></h4>
            </div>
            @foreach($documents as $document)
            	<div class="col-md-6 flex">
                    <div class="widget-bg-color-icon card-box text-center">
                        <iframe src="/uploads/documents/{{ $document->name }}" style="width:100%; height:400px; border: 0 !important;"></iframe>
                        <div class="text-center">
                            <h3 class="text-dark"><a href="/uploads/documents/{{ $document->name }}" target="_blank"><span class="counter">
                            @if($document->type == 1) Resume @elseif($document->type == 2) OSHA @elseif($document->type == 3) Diploma @endif
                            </span></a></h3>
                            @if($application->status <= 1 || $application->status == 3)
                                <button class="btn btn-danger deleteDocument" data-name="{{ $document->name }}"><i class="fa fa-times"></i> DELETE</button>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-md-5">
            <div class="col-md-12">
                <h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b></b></h4>
            </div>
        	<div class="card-box">
        		<div class="card-body">
        			<form method="POST" action="{{ route('upload') }}" enctype="multipart/form-data">
        			@csrf
                    <input type="hidden" name="eligibility" value="{{ $application->eligibility }}">
        				
                        @if($application->status <= 1 || $application->status == 3)
                        <h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Option {{ $application->eligibility }} Documents Required</b></h4>
                        <h5 class="text-danger"><strong>Only PDF files under 2MB will be accepted</strong></h5>
                        @if($application->eligibility == 1)
                        
                        <div class="form-group">
                            <label class="control-label">Upload your resume showcasing three years’ full-time or equivalent experience* in transportation construction industry. Transportation construction is occupational experience that includes but is not limited to construction of highways, roads, bridges, airports, rail systems, tunnels, utilities and ports.</label>
                            <input type="file" name="resume" class="filestyle" data-iconname="fa fa-cloud-upload" data-buttontext="Upload Resume">
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label">Upload your Certificate (Completion of an OSHA-10, OSHA-30, OSHA-500 OR OSHA-510 course.) </label>
                            <input type="file" name="osha" class="filestyle" data-iconname="fa fa-cloud-upload" data-buttontext="Upload Certificate" required="">
                        </div>
                        @elseif($application->eligibility == 2)
                        	<div class="form-group">
                            <label class="control-label">Upload your diploma (bachelor's degree in engineering or construction management.)</label>
                            <input type="file" name="diploma" class="filestyle" data-iconname="fa fa-cloud-upload" data-buttontext="Upload Diploma" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Upload your resume (reflecting two years’ experience* in the transportation construction industry.)</label>
                            <input type="file" name="resume" class="filestyle" data-iconname="fa fa-cloud-upload" data-buttontext="Upload Resume" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Upload your Certificate (Completion of an OSHA-10, OSHA-30, OSHA-500 OR OSHA-510 course.)</label>
                            <input type="file" name="osha" class="filestyle" data-iconname="fa fa-cloud-upload" data-buttontext="Upload Certificate" required="">
                        </div>
                        @elseif($application->eligibility == 3)
                        <div class="form-group">
                            <label class="control-label">Upload your diploma (associate or technical degree in safety)</label>
                            <input type="file" name="diploma" class="filestyle" data-iconname="fa fa-cloud-upload" data-buttontext="Upload Diploma" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Upload your resume (reflecting two years’ experience* in the transportation construction industry)</label>
                            <input type="file" name="resume" class="filestyle" data-iconname="fa fa-cloud-upload" data-buttontext="Upload Resume" required="">
                        </div>
                        @endif
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary">Upload Documents</button>
                        @if(Auth::user()->isNew())
                        <br><br><p>Move to Step 4 once you are done uploading all of your documents.</p>
                        @endif
                        </div>
                        @elseif($application->status == 2)
                            <h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Document management temporarily disabled until a decision on your application has been made.</h4>
                        @endif
                    </form>
        		</div>
        	</div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Page Specific JS Libraries -->
    <script src="/assets/plugins/bootstrap-filestyle/bootstrap-filestyle.min.js" type="text/javascript"></script>
    <script>
        $('.filestyle').change( function(){
                var ext = $(this).val().split('.').pop().toLowerCase();
                console.log(ext);
                if(ext != "pdf" && ext != ""){
                    $.Notification.notify('error','bottom right', 'Incorrect File Format', 'Only PDF documents are allowed for submission.');
                    $(this).val(""); 
                }
                if((this.files[0].size / 1000000) > 2.1){
                    $.Notification.notify('error','bottom right', 'File Too Large', 'Only attachments that are 2MB or smaller are allowed for submission');
                    $(this).val(""); 
                }
        });
        $(".deleteDocument").click(function(){
            if(confirm("Are you sure you want to delete this document?")){
                var filename = $(this).attr("data-name");

                if(filename != ""){
                    $(this).parent().parent().parent().remove();
                    $.get("{{ route('removeDocument', ['name' => NULL]) }}/" + filename, function(data, status){
                        $.Notification.notify('success','bottom right', 'Done', 'Your document has been removed.');
                    });
                }
            }
        });

        // if the user has uploaded at least one document
        @if($documents->count() > 0)
        $("input").each(function(){
            $(this).removeAttr('required');
        });
        @endif
    </script>

@endsection