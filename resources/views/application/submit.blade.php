@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
/**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
.StripeElement {
	width: 100%;
  background-color: white;
  height: 40px;
  padding: 10px 12px;
  border-radius: 4px;
  border: 1px solid transparent;
  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}


@-webkit-keyframes rotating /* Safari and Chrome */ {
  from {
    -webkit-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes rotating {
  from {
    -ms-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -webkit-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  to {
    -ms-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -webkit-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
.fa-refresh {
  -webkit-animation: rotating 2s linear infinite;
  -moz-animation: rotating 2s linear infinite;
  -ms-animation: rotating 2s linear infinite;
  -o-animation: rotating 2s linear infinite;
  animation: rotating 2s linear infinite;
}
</style>
@endsection

@section('content')

<div class="card-box col-md-6 offset-md-3">
	<div class="card-body">
		<div class="row" >
			<h3 class="m-t-0 header-title col-md-12" style="margin-bottom: 20px; font-size: 24px;"><b>Application Fee: $<span id="price">400</span></b></h3>
		</div>
			<form id="payment-form" method="POST" action="{{ route('submitApplication') }}">
				{{ CSRF_FIELD() }}
				<div class="form-row">
					<div class="form-group-custom text-left col-md-12">
						<p>Your application will only be reviewed when all steps of the application have been completed and upon payment has been received.<br>
						<em class="text-muted">Note: This fee is partialy refundable</em></p>
					</div>
					<div class="form-group-custom text-left col-md-12" style="margin-bottom: 10px;">
		                <p>If your employer provided you with an access code, enter it here:</p>
		            </div>
	            </div>
				<div class="form-row">
					<div class="form-group-custom input-group text-left col-md-12">
		        <input type="text" name="code" id="code" placeholder="Access Code">
            <i class="bar" style="position:absolute; width:84%; top:35px; z-index: 0;"></i>
            <div class="input-group-append">
              <button class="btn btn-dark waves-effect waves-light" type="button" id="verify" style="cursor:pointer;"><i class="fa fa-search"></i> Verify</button>
            </div>
		      </div>
        </div>
        <div class="form-row" style="margin-bottom: 5px;">
          
        </div>
        <div class="form-row" style="margin-bottom: 20px;">

            <div id="card-element">
            <p>OR, enter a credit to pay for our own application:</p>
              <!-- a Stripe Element will be inserted here. -->
            </div>
            <!-- Used to display form errors -->
            <div id="card-errors" role="alert"></div>
        </div>
        <div class="form-row action">
          <div class="form-group">
            <button type="submit" class="btn btn-primary submit">Submit Application for Review <span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></button>
          </div>
        </div>
            </form>
            <div id="card-container" style="display: none;"></div>
        </div>
	</div>
</div>

@endsection

@section('js')
<script src="https://js.stripe.com/v3/" id="striping1"></script>
<script type="text/javascript" id="striping2">
    //Stripe.setPublishableKey('');
// Create a Stripe client
var stripe = Stripe('{{ env('STRIPE_KEY') }}');

// Create an instance of Elements
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {


  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server
      $("i.fa").removeClass("fa-arrow-right");
      $(".submit i").addClass("fa fa-refresh");
    $("#payment-form").append('<input type="hidden" name="stripeToken" value="' + result.token.id + '">');
    $("#payment-form").append('<input type="hidden" name="cardnumber" value="' + result.token.card.last4+ '">');
    $("#payment-form").append('<input type="hidden" name="cardID" value="' + result.token.card.id+ '">');
      

    $("#payment-form").submit();
    }
  });
});

$("#verify").click(function(){
      var code = $("#code").val();

      $.get('/application/access/' + code, function(data, status){
        if(data == "OK"){
          $("#verify").removeClass();
          $("#verify").addClass("btn btn-success");
          $("#verify").html("<i class='fa fa-check'></i> Verified"); 
          $("#card-element").remove();
          $(".submit").remove();
          $("#price").text("0");
          $(".action").append('<a href="/application/waived/' + code  + ' " class="btn btn-primary submit">Submit Application for Review <span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></button>');
        }
        else {
          $("#verify").removeClass();
          $("#verify").addClass("btn btn-danger");
          $("#verify").html("<i class='fa fa-times'></i> Invalid"); 
        }
      });
});
$("#code").focusout(function() {
  if($(this).val() == ""){
    $("#verify").removeClass();
    $("#verify").addClass("btn btn-dark");
    $("#verify").html("<i class='fa fa-search'></i> Verify"); 
  }
});
</script>

@endsection