@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">{{ $page }}</li>
    <li class="breadcrumb-item active">{{ $title }}</li>
</ol>
@endsection

@section('content')


@endsection

@section('js')


@endsection