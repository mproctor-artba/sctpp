@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	button[type="submit"].btn{
		width: 300px;
		margin:0 auto;
		text-decoration: none !important;
	}
	.card-footer{
		border:none;
		background: none;
	}
	.card-box.selected{
		border: 3px solid #007bff;
	}
	</style>
@endsection

@section('content')
<form method="POST" action="{{ route('recertify') }}">
@csrf
	<div class="row">
		<div class="col-md-6">
			<div class="card-box @if(Auth::user()->step11() == 1) selected @endif">
				<div class="card-body" style="min-height: 430px;">
					<div class="row" style="margin-bottom: 20px;">
						<p style="font-size:24px; width: 100%;">Option 1: Submit 30 Professional Development Hours</p>
						<p>Certified persons seeking recertification can select this route which requires:</p>
						<ul>
							<li>Must work at least two years total within the three-year certification cycle</li>
							<li>Agree to abide by and uphold the Code of Ethics.</li>
							<li>Complete 30 points in training in safety related to the competency requirements of the transportation project professional.
							<br>
					</div>
					
				</div>
				<div class="card-footer">
				@if(Auth::user()->step11() == NULL)
					<div class="row text-center">
						<h4 class="text-center block-center" style="display: block; margin: 10px auto;">$190</h4>
					</div>
					<div class="row text-center">
						<button type="submit" name="recertOption" class="btn btn-primary" value="1">Select Option 1</button>
					</div>
				@elseif(Auth::user()->step11() == 1 || Auth::user()->step11() == 2 )
						<div class="row text-center">
							<h4 class="text-center block-center" style="display: block; margin: 10px auto;">&nbsp;</h4>
						</div>
						<div class="row text-center">
							<button type="submit" name="recertOption" class="btn btn-primary disabled" value="1" disabled="">Select Option 1</button>
						</div>
				@endif
				</div>
			</div>
		</div>
		<div class="col-md-6">
	</div>
</div>
</form>
@endsection

@section('js')


@endsection