@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	.transparent td{
        padding:0;
    }
    .transparent tbody tr:nth-of-type(odd){
        background: transparent;
    }
	</style>
@endsection

@section('content')

@if(Auth::user()->isAdmin())
<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
				<div class="row">
                	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Review Application</b></h4>
                </div>
                <div class="row">
                	<form method="POST" class="form" style="width:100%;" action="{{ route('decideRecertification', ['id' => $application->id]) }}" id="decide">
                		@csrf
                		<div class="form-row" style="margin-bottom: 20px;">
                			<div class="col-md-12">
                			<textarea class="form-control" width="100%" name="reason"></textarea>
                			</div>
                		</div>
                		<div class="form-row" style="padding:0;">
                			<div class="col-md-6" align="right">
                				<button type="submit" name="decision" value="0" class="btn btn-danger waves-effect waves-light pull-left decide"><span class="btn-label"><i class="fa fa-times"></i></span> Reject</button>
                			</div>
                			<div class="col-md-6">
                				<button type="submit" name="decision" value="1" class="btn btn-success waves-effect waves-light pull-right decide"><span class="btn-label"><i class="fa fa-check"></i></span> Approve for Recertification</button>
                			</div>
                		</div>
                	</form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
				<div class="row">
                	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Application Notes</b></h4>
                </div>
                <div class="row">
                <table class="table">
                	<thead>
                		<tr>
                			<th width="15%">Admin</th>
                			<th width="10%">Decision</th>
                			<th>Reason</th>
                			<th width="20%">Date</th>
                		</tr>
                	</thead>
                	<tbody>
                		@foreach($application->decisions as $decision)
                		<tr>
                			<td>
                				{{ $decision->user->profile->first_name . " " . $decision->user->profile->last_name }}
                			</td>
                			<td>
                				@if($decision->decision == 0)
                					Denied
                				@elseif($decision->decision == 1)
                					Approved
                				@endif
                			</td>
                			<td>{{ $decision->reason }}</td>
                			<td>{{ convertTimestamp($decision->date) }}</td>
                		</tr>
                	@endforeach
                	</tbody>
                </table>

                <ul style="padding: 0;">
                	
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endif


<div class="row">
	<div class="col-lg-6">
		<div class="card-box">
			<div class="card-body">
                <div class="row">
                	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Candidate Info</b></h4>
                </div>
                <div class="row">
                	<ul style="list-style: none; padding: 0;">
                		<li>Name:	{{ $application->user->profile->first_name }} {{ $application->user->profile->middle }} {{ $application->user->profile->last_name }}</li>
                		<li>Email:	{{ $application->user->email }} </li>
                		<li>Phone:	{{ $application->user->profile->phone }}</li>
                		<li>Company:	{{ $application->user->profile->company }}</li>
                		<li>Job Title:	{{ $application->user->profile->title }}</li>
                		<li>Address: {{ $application->user->profile->address() }}</li>
                	</ul>
                </div>
            </div>
        </div>
    </div>
	<div class="col-lg-6">
		<div class="card-box">
			<div class="card-body">
                <div class="row">
                	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Candidate Eligibility</b></h4>
                </div>
                <div class="row">
                	<p>This application requires the candidate to @if($application->recert_option == 1 || $application->recert_option == 3) submit 30 or more Professional Development Hours @else to sit for the exam @endif in order to renew their SCTPP certification.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
				<h4 class="m-t-0 header-title"><b>Experiences</b></h4>
                <br>
                @if($experiences->count() > 0)
	    <div class="row">
			<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
	            <thead>
	            <tr>
	                <th>Organization</th>
	                <th width="15%">Dates</th>
	                <th>Experience</th>
	                <th width="20%">Supervisor</th>
	                <th width="15%">Status</th>
	                <th width="5%"></th>
	            </tr>
	            </thead>
	            <tbody>
	            	@foreach($experiences as $experience)
	            		<tr>
	            			<td>{{ $experience->organization }}</td>
	            			<td>
		            			<ul style="padding: 0; list-style: none;">
		            				<li>Start: {{ $experience->start }}</li>
		            				<li>End: {{ $experience->finished }}</li>
		            			</ul>
	            			</td>
	            			<td>
		            			<ul style="padding: 0; list-style: none;">
			            			@foreach(explode(",", $experience->experiences) as $exp)
			            				<li>{{ $exp }}</li>
			            			@endforeach
		            			</ul>
	            			</td>
	            			<td>
	            				<ul style="padding: 0; list-style: none;">
	            				<li>Reference: {{ $experience->reference }}</li>
	            				<li>Email: {{ $experience->email }}</li>
	            				<li>Phone: {{ $experience->phone }}</li>
	            			</ul>
	            			</td>
	            			<td class="text-center">
	            				@if($experience->status == 0)
	            					Response Pending <br>
	            					<label class="btn btn-info sendReminder" data-tracker="{{ $experience->secret }}"><i class="md md-email"></i> Send Reminder</label>
	            					<br>
                                        <small style="font-size:70%;">Last Contacted: {{ lastContacted($experience->id) }}</small></td>
	            				@elseif($experience->status == 1)
	            					<i class="md md-check"></i> Complete
	            				@endif
	            			</td>
	            			<td class="text-center">
	            				@if($experience->status == 0)
	            			<button class="btn btn-danger removeReference" data-reference="{{ $experience->id }}"><i class="fa fa-times"></i></button>
	            				@endif
	            			</td>
	            		</tr>
	            	@endforeach
	            	@if($experiences->count() > 0)
	            		<style type="text/css">.step5{ display:block !important; }</style>
	            	@endif
	            </tbody>
	        </table>
        </div>
        @endif
			</div>
		</div>
	</div>
</div>
@if($application->user->step11() == 1 || $application->user->step11() == 3)
<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
                @include('layouts.charts.pdh-progress')
                <br>

			<h4 class="m-t-0 header-title"><b>Professional Development Hours</b></h4>
				@include('layouts.tables.pdh')
			</div>
		</div>
	</div>
</div>
@endif()
<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
				<h4 class="m-t-0 header-title"><b>SCTPP&trade; Code of Ethics</b></h4>
                <br>
                <p>The Code of Ethics of the Certification Commission requires certified persons to uphold the rules and  requirements for Safety Certification for Transportation Project Professionals&trade; (SCTPP). The Code of Ethics allows for the proper discharge of the responsibilities of those they serve, and for the protection and integrity of the credential. Agreement to uphold and abide by the Code of Ethics is a requirement for earning and  maintaining SCTPP certification. Implicit in this agreement is an obligation not only to comply with the  mandates and requirements of all applicable laws and regulations, but to act in an ethical manner in all  professional services and activities. Certified persons who fail to comply with the Code of Ethics are subject to disciplinary procedures which may result in sanctions, including but not limited to revocation of certification status. The Code of Ethics is not set forth to determine behaviors resulting in criminal or civil liability, nor are they intended to resolve matters of market competition.</p>
                <p>
                As a safety certified transportation project professional &trade;, I agree to uphold and abide by the following tenets:
                </p>
                <ol>
                    <li>Perform duties in a safe, ethical and competent manner.</li>
                    <li>Provide complete and accurate information when applying for certification and recertification.</li>
                    <li>Represent qualifications with honesty and integrity.</li>
                    <li>Abide by and uphold the policies of the Certification Commission.</li>
                    <li>Truthfully represent the scope of the certification.</li>
                    <li>Safeguard the integrity of the certification and refrain from behavior that would bring the certification body into disrepute.</li>
                    <li>Use the logo and certification marks only in an authorized and approved manner.</li>
                    <li>Pay all fees and provide information required by the Certification Commission.</li>
                    <li>Should certification be suspended or withdrawn, agree to discontinue claims to certification, including references to the Certification Commission, and return the certificate and related items to SCTPP personnel.</li>
                </ol>
                
                <p>The following sanctions may be imposed for failure to abide by the code of ethics. The sanction shall be based on the severity of the violation, and shall include, but not be limited to:</p>
                
                <ul>
                    <li>Cease and Desist</li>
                    <li>Written reprimand</li>
                    <li>Written reprimand with remediation</li>
                    <li>Censure</li>
                    <li>Suspension</li>
                    <li>Revocation</li> 
                    <li>Permanent revocation</li>
                </ul>
                <p>In addition to imposing sanctions, ARTBA Foundation shall have the authority to report sanctions to legal and regulatory authorities, and other credentialing organizations as appropriate.</p>
                <label>
                @if(!Auth::user()->isAdmin())
                    <input type="checkbox" name="ethics" value="1" required="" id="ethics" aria-required="true" @if(isset($application->eligibility))checked @endif> 
                @else 
                    <i class="fa fa-check"></i> 
                @endif I have read the Code of Ethics and agree to uphold and abide by the Code.</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<h4 class="m-t-0 header-title"><b>SCTPP&trade; Terms & Conditions</b></h4>
                		<br>
						<h5 class="m-t-0 header-title"><b>Please read each statement carefully and check the corresponding box to denote your understanding before proceeding.</b></h5>
	                    @if(!Auth::user()->isAdmin())
	                       <input type="checkbox" name="read" value="1" required="" id="read" aria-required="true">
                        @else
                            <i class="fa fa-check"></i>
                        @endif
	                      &nbsp; &nbsp; <span>I acknowledge that I have read, understand, and agree to abide by the terms outlined in this <a href="https://sctpp.org/documents/recertification" target="_blank">SCTPP Recertification Handbook</a>.
	                      </span>
                    </div>
                    <div class="col-md-12">
                      @if(!Auth::user()->isAdmin())
                           <input type="checkbox" name="sta" value="1" required="" id="sta" aria-required="true">
                        @else
                            <i class="fa fa-check"></i>
                        @endif
                      &nbsp; &nbsp; <span>I fully understand that it is an application for certification and does not guarantee certification.
                      </span>
                    </div>
                    <div class="col-md-12">

                      @if(!Auth::user()->isAdmin())
                           <input type="checkbox" name="under" value="1" required="" id="under" aria-required="true">
                        @else
                            <i class="fa fa-check"></i>
                        @endif

                      &nbsp; &nbsp; <span>I understand that the Certification Commission reserves the right to revise or update this application, the Code of Ethics and recertification requirements, and it is my responsibility to be aware of these current requirements.  I further understand that I am obligated to immediately inform the Certification Commission of changed circumstances that may materially affect my application, including my ability to continue to fulfill certification requirements.  I further understand that it is my responsibility to provide any requested documentation in connection with this application.
                      </span>
                    </div>
                    <div class="col-md-12">
                      @if(!Auth::user()->isAdmin())
                           <input type="checkbox" name="attest" value="1" required="" id="attest" aria-required="true">
                        @else
                            <i class="fa fa-check"></i>
                        @endif

                      &nbsp; &nbsp; <span>I hereby attest that I am taking the examination solely for the purposes of earning the certification. I further understand that I am prohibited from transmitting information regarding examination questions or content in any form to any person or entity, and understand that failure to comply with this prohibition may result in my certification being revoked and/or legal action being taken against me.
                      </span>
                    </div>
                    <div class="col-md-12">
                      @if(!Auth::user()->isAdmin())
                           <input type="checkbox" name="agree" value="1" required="" id="agree" aria-required="true">
                        @else
                            <i class="fa fa-check"></i>
                        @endif

                      &nbsp; &nbsp; <span>I understand and agree that if I am certified following acceptance of this application and successful completion of the examination, such certification does not constitute a warranty or guarantee of my fitness or competency to practice as a certified professional. If I am certified, I authorize the Certification Commission to include my name in a list of certified individuals and agree to use the certification and related trade names, trademarks, and logos only as permitted by program policies.  I understand and agree that the Certification Commission may also use anonymous and aggregate application and examination data for statistical and research purposes.
                      </span>
                    </div>
                    <div class="col-md-12">
                        @if(!Auth::user()->isAdmin())
                           <input type="checkbox" name="backgroundcheck" value="1" required="" id="backgroundcheck" aria-required="true">
                        @else
                            <i class="fa fa-check"></i>
                        @endif
                      &nbsp; &nbsp; <span>I authorize ARTBA's Certification Team to conduct a verification of the employment experience provided with my application.</span>
                    </div>
                    <div class="col-md-12">
                      @if(!Auth::user()->isAdmin())
                           <input type="checkbox" name="accommodation" value="1" required="" id="accommodation" aria-required="true">
                        @else
                            <i class="fa fa-check"></i>
                        @endif
                      &nbsp; &nbsp; <span>I understand that I may request exam accommodation for special needs as outlined on Page 17 of the SCTPP Candidate Handbook.</span>
                    </div>
                    <div class="col-md-12">
                        @if(!Auth::user()->isAdmin())
                           <input type="checkbox" name="bylaw" value="1" required="" id="bylaw" aria-required="true">
                        @else
                            <i class="fa fa-check"></i>
                        @endif
                      &nbsp; &nbsp; <span>I understand that ARTBA may be required to release confidential information to legal authorities if required by the law. I accept email to be an acceptable form of notification to inform me if ARTBA is required to release my information.</span>
                    </div>
                    <div class="col-md-12">
                        @if(!Auth::user()->isAdmin())
                           <input type="checkbox" name="notmisrepresent" value="1" required="" id="notmisrepresent" aria-required="true">
                        @else
                            <i class="fa fa-check"></i>
                        @endif
                      &nbsp; &nbsp; <span>I agree to not misrepresent the scope or purpose of the SCTPP certification, make statements that are misleading or unauthorized, or to use the certification in a manner that will bring the SCTPP into disrepute.</span>
                    </div>
                    <div class="col-md-12">
                        @if(!Auth::user()->isAdmin())
                           <input type="checkbox" name="agreeDiscontinue" value="1" required="" id="agreeDiscontinue" aria-required="true">
                        @else
                            <i class="fa fa-check"></i>
                        @endif
                      &nbsp; &nbsp; <span>I agree to discontinue use of- or reference to possession of the SCTPP certification if I am suspended or the certification is withdrawn.  I will return SCTPP certificates and other marks to the SCTPP staff or Commission upon suspension or revocation of my certification.</span>
                    </div>
                    <div class="col-md-12">
	                    <br>
	                      <h4 class="m-t-0 header-title text-danger"><b>Applicant Signature: (use your cursor to draw your signature below)</h4>
	                      <br>
	                      
	                      @if(!empty($application->signature))
	                      <img src="data:image/svg+xml;base64,{{ $application->signature }}" style="margin:50px 0; border-bottom:2px solid silver;">
	                      @else
	                      <button class="btn btn-danger" type="button" id="clear"><i class="md md-refresh"></i> Clear Signature Pad</button>
	                      <input type="hidden" name="signature" id="sig">
	                      <div id="signature"></div>
	                      @endif
	                </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script src="/assets/js/jSignature.min.js"></script>
<script type="text/javascript">
	$("#signature").jSignature();
	$("body").on("click", "#clear", function(){
            $("#signature").jSignature("reset");
    });
    $("form").mousemove(function(){
    	var blank = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMCIgaGVpZ2h0PSIwIj48L3N2Zz4=";

    	var datapair = $("#signature").jSignature("getData", "svgbase64");
    	if(datapair != null){
    	$("#sig").val(datapair[1]);

    	if($("#sig").val() != blank && $('.required:checked').length == $('.required').length) {
    		$(".submit").removeClass("disabled");

    	} else {
    		$(".submit").addClass("disabled");
    	}
    }
            
    });
    $(".submit").click(function(){
    	var blank = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMCIgaGVpZ2h0PSIwIj48L3N2Zz4=";

    	var datapair = $("#signature").jSignature("getData", "svgbase64");
    	$("#sig").val(datapair[1]);

    	if($("#sig").val() != blank) {
    		if ($('.required:checked').length == $('.required').length) {
    			$("#terms").submit();
    		} else {
    			$.Notification.notify('error','bottom right', 'Full Agreement Required', 'Applicants must agree to all terms of the SCTPP&trade; Program');
    		}
    		
    	} else {
    		$.Notification.notify('error','bottom right', 'Missing Signature!', 'You must apply your signature in order to mark your agreement to the terms of the SCTPP&trade; Program');
    	}
    });

    $(".sendReminder").click(function(){
            var tracker = $(this).attr("data-tracker");
            if(confirm("Are you sure you want to send this contact a reminder email?")){
                $.get("{{ route('remindReference', ['secret' => NULL]) }}/" + tracker, function(data, status){
                    $.Notification.notify('success','bottom right', 'Done', 'This reference has been sent a reminder email.');
                });
            }
        });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#req0-progress').css('width',(({{ $allhours }}/30) * 100) + "%");
        $('#req0-progress-complete').css('width',(({{ $approvedSum }}/30) * 100) + "%");


        $("#totalHours").text({{$approvedSum}} + "/30");
    });
</script>
@endsection