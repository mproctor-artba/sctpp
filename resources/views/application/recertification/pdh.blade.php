@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="/assets/plugins/dropify/dropify.min.css">
<link rel="stylesheet" type="text/css" href="https://l-lin.github.io/font-awesome-animation/dist/font-awesome-animation.min.css">
<style type="text/css">
form .card-header{
	min-height: 70px;
}
		.form-row{
			margin-bottom: 10px;
		}
		.pull-right{
			position: inherit;
			right:0;
			top:0;
		}
		#pdh{
			margin-top:10px;
		}
		.pdhTitle{
			margin: 0;
    margin-top: 10px;
    margin-bottom: -29px;
		}
		.pdhTitle a{
			padding-left: 10px;
		}
		.card .card-header a.collapsed:before {
    /* symbol for "opening" panels */
    
    content: "\f054";   /* adjust as needed, taken from bootstrap.css */
    font-family: FontAwesome !important;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
    float: left;        /* adjust as needed */
    color: grey;         /* adjust as needed */
}
.card-header a.text-dark:before {
    /* symbol for "collapsed" panels */
    font-family: FontAwesome !important;
    content: "\f077";    /* adjust as needed, taken from bootstrap.css */
}
.dropdown.bootstrap-select{
max-width: 200px;
float:right;
margin-right: 20px;
	}
	.dropdown-menu.show{
		max-height: 200px;
		overflow-y: auto;
		min-width: inherit !important;
		max-width: 240px !important;
	}
.req0{
	padding: .5rem .75rem;
}
.req0:before{
	font-family: FontAwesome !important;
	content: "\f054";
}
.activityTitle{
	font-size: 22px;
}
.condensed-list{
	list-style: none;
	padding-left: 0;
}
.progress .bg-success{
	background-color: #2bbbad !important;
}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<div class="progress progress-xxl">
		                    <div id="req0-progress"  class="progress-bar bg-info" role="progressbar" style="" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
		                </div>
						<label><strong>Total Progress:</strong> <span id="totalHours">0/30</span> Hours</label>
	                </div>
                </div>
                <div class="row">
                	<div class="col-md-12">
                		<ul class="condensed-list">
                			<li></li>
							<li>All hours must be submitted no later than @php $days = Auth::user()->certificate->daysToRecertify() - 15; @endphp {{ date('m-d-Y', strtotime("+$days days")) }}</li>
						</ul>
					</div>
					<div class="col-md-1">
						<button type="button" class="clone btn btn-info" style="cursor: pointer;">Add New</button>
					</div>
					<div class="col-md-2">
						<a href="https://sctpp.org/wp-content/uploads/2019/04/Recertification-PDH-Requirements.pdf" target="_blank" class="btn btn-dark" alt="PDH Submission Guide">PDH Submission Guide</a>
					</div>
					@if(Auth::user()->pdhs->sum('hours') >= 30)
					<div class="col-md-2">
						<div class="btn-group">
							<a href="{{ route('recertify-submit') }}" class="btn btn-primary waves-effect waves-light">Ready for Submission <span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></a>
				        </div>
					</div>
					@endif
                </div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		@CSRF
		<div id="pdh">
			@foreach($pdhs as $pdh)
				@include('layouts.pdh', $pdh)
			@endforeach
			@if(empty($pdh))
				@include('layouts.pdh')
			@endif
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="/assets/plugins/dropify/dropify.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
var regex = /^(.+?)(\d+)$/i;
var cloneIndex = $(".clonedInput").length;

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': "{{ csrf_token() }}"
  }
});

function clone(){

    $.get("{{ route('getPDHForm') }}", function(data, status){
        $("#pdh").append(data);
        $('.dropify').dropify({
        	allowedFiles: 'pdf'
        });
    });

}

function remove(pdhID){
	if(confirm("Are you sure you want to remove this record?")){
		

		$.ajax({
            type: "GET",
            url: "{{ route('removePDHform') }}/" + pdhID,
        });

 		
 		sumPDH();
	}
}

$("button.clone").on("click", clone);

$("body").on("click", ".remove", function(){
	var pdhID = $(this).attr("data-pdhid");
	remove(pdhID);
	$(this).parent().parent().remove();
});

$("body").on("keyup", ".activityName", function(){
	var activityTitle = $(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().find(".activityTitle");


	activityTitle.text($(this).val());
	
});



$("body").on("change", "select", function(){
	sumPDH();
});

function sumPDH(){
	var i;
	var progress = parseInt(0);
	var progressBar = [];
	var hours = parseInt(0);
	var category = "";

	// ad hoc to finish asap

	var progressA = parseInt(0);
	var progressB = parseInt(0);
	var progressC = parseInt(0);
	var progressD = parseInt(0);
	var progressE = parseInt(0);

	$(".req0 option:selected").each(function(){
		var father = $(this).parent().parent().parent().parent().parent().parent().parent().parent().parent();
		hours = parseInt($(this).val());
		if(!isNaN(hours)){
			progress += hours
		} else {
			hours = 0;
		}

		father.find(".currentHours").text(hours);
	});

	$('#req0-progress').css('width',((progress/30) * 100) + "%");


	$("#totalHours").text(progress + "/30");



	$("#page-title").text("Manage PDHs for Recertification");
	
	
	$("title").text("SCTPP | PDH Submission Form");
	if((progress/30 * 100) >= 100){
		$("#req0-progress").addClass("bg-success");
		}

}

$(".dropify").dropify({messages:{default:"Drag and drop a file here or click",replace:"Drag and drop or click to replace",remove:"Remove",error:"Please select another file"},error:{fileSize:"The file size is too big (2MB max)"}});

$("body").on("change", ".required", function(){
	var father = $(this).parent().parent().parent().parent().parent().parent().parent().parent().parent();
	var requiredFields = father.find(".pre-validated").find(".required");
	var errors = 0;
	$(requiredFields).each(function(){
		if($(this).val() == "" || $(this).hasClass("validateDate") && validateDate($(this).val())){
			completed = false;
			$(this).addClass("parsley-error");
			if($(this).hasClass("dropify")){
				$(this).parent().parent().parent().find(".dropify-wrapper").addClass("parsley-error");
			}

			$(this).closest(".card").addClass("parsley-error");

			errors++;
		} else {
			$(this).removeClass("parsley-error");
			if($(this).hasClass("dropify")){
				$(this).parent().parent().parent().find(".dropify-wrapper").removeClass("parsley-error");
			}
		}
		
	});
	if(errors == 0){
		$(this).closest(".card").removeClass("parsley-error");
	}
});

$("body").on("click", "button.savePDH", function(){
	var formID = $(this).attr("data-form");
	var formdata = new FormData($("#pdhForm-" + formID)[0]);
	var errors = 0;
	var completed = true;
	var requiredFields = $("#pdhForm-" + formID).find(".required");

	$(requiredFields).each(function(){
		if($(this).val() == "" || $(this).hasClass("validateDate") && validateDate($(this).val())){
			completed = false;
			$(this).addClass("parsley-error");

			if($(this).hasClass("dropify")){
				$(this).parent().parent().parent().find(".dropify-wrapper").addClass("parsley-error");
			}
			

			$(this).closest(".card").addClass("parsley-error");

			errors++;
		} else {
			$(this).removeClass("parsley-error");
			
			if($(this).hasClass("dropify")){
				$(this).parent().parent().parent().find(".dropify-wrapper").removeClass("parsley-error");
			}
			
		}

	});

	if(errors == 0){
		$(this).closest(".card").removeClass("parsley-error");
	}

	$("#pdhForm-" + formID).addClass("pre-validated");

	if(completed == false){
        $.Notification.notify('error','bottom right', 'Missing Required Fields', 'Please correct the highlighted fields and try again.');
        var headerText = $("#pdhForm-" + formID).find("a.text-dark").text();
                	$("#pdhForm-" + formID).find("a.text-dark").html('<i title="Pending Review" class="fa fa-warning text-danger"></i><span class="activityTitle">' + headerText + '</span>');
	} else {
	    $.ajax({
            type: "POST",
            url: "{{ route('savePDHform') }}",
            data: formdata,
            cache: false,
		    processData: false,
			contentType: false,
			success: function(data) { // je récupère la réponse du fichier PHP
                if(data == 200){
                	$.Notification.notify('success','bottom right', 'PDH Uploaded', 'Your submission has been saved successfully!');
                	var headerText = $("#pdhForm-" + formID).find("a.text-dark").text();
                	$("#pdhForm-" + formID).find("a.text-dark").html('<i title="Pending Review" class="fa fa-clock-o text-info"></i><span class="activityTitle">' + headerText + '</span>');
                } else {
                	$.Notification.notify('error','bottom right', 'Error!', 'We could not process your upload at this time. Please check all inputs and try again.');
                	var headerText = $("#pdhForm-" + formID).find("a.text-dark").text();
                	$("#pdhForm-" + formID).find("a.text-dark").html('<i title="Pending Review" class="fa fa-warning text-danger"></i><span class="activityTitle">' + headerText + '</span>');
                }
            }  
        });
	}
});

function validateDate(date){

	var re = new RegExp("/^\d{1,2}\/\d{1,2}\/\d{4}$/");
	console.log(date);
	if (re.test(date)) {
		console.log(date);
		console.log("haha");
		return true;
	}
	console.log("nope");
	return false;
}

setTimeout(sumPDH, 200);



$('body').on('focus',".datepicker", function(){
    $(this).datepicker();
});
</script>
@endsection