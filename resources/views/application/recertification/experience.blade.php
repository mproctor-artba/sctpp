@extends('layouts.app')

@section('css')
	<link href="/assets/plugins/custombox/custombox.css" rel="stylesheet">
	<link href="/assets/plugins/select2/select2.min.css"  rel="stylesheet" type="text/css" />
	<style>
	.tab-content{
		width:100%;
	}
	.page-title{
		display: none;
	}
	.select2-container{
		z-index:10002;
	}
	</style>
@endsection

@section('content')

<div class="card-box">
	<div class="card-body">
		<div class="row" >
		<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Please provide references that can validate at least two years' experience in the transportation construction industry during the recertification period. Note: All references must be verified before your application is reviewed.</b></h4>
		</div>
		<div class="row">
		<p></p>
			<p>
		</div>

		<div class="row" style="display: block">
			<div class="btn-group" style="margin-bottom: 20px;">
			<a href="#create" class="btn btn-success waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Experience</a>
	        </div>
	        @if($experiences->count() > 0)
	        <div class="btn-group pull-right step5" style="display:none; margin-bottom: 20px;">
				<a href="@if(Auth::user()->application->recert_option == 1 || Auth::user()->application->recert_option == 3) {{ route('recertify-pdh') }} @else {{ route('recertify-submit') }} @endif" class="btn btn-primary waves-effect waves-light">Move to Next Step<span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></a>
	        </div>

	        @endif
	    </div>
	    @if($experiences->count() > 0)
	    <div class="row">
			<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
	            <thead>
	            <tr>
	                <th>Organization</th>
	                <th width="15%">Dates</th>
	                <th>Experience</th>
	                <th width="20%">Supervisor</th>
	                <th width="15%">Status</th>
	                <th width="5%"></th>
	            </tr>
	            </thead>
	            <tbody>
	            	@foreach($experiences as $experience)
	            		<tr>
	            			<td>{{ $experience->organization }}</td>
	            			<td>
		            			<ul style="padding: 0; list-style: none;">
		            				<li>Start: {{ $experience->start }}</li>
		            				<li>End: {{ $experience->finished }}</li>
		            			</ul>
	            			</td>
	            			<td>
		            			<ul style="padding: 0; list-style: none;">
			            			@foreach(explode(",", $experience->experiences) as $exp)
			            				<li>{{ $exp }}</li>
			            			@endforeach
		            			</ul>
	            			</td>
	            			<td>
	            				<ul style="padding: 0; list-style: none;">
	            				<li>Reference: {{ $experience->reference }}</li>
	            				<li>Email: {{ $experience->email }}</li>
	            				<li>Phone: {{ $experience->phone }}</li>
	            			</ul>
	            			</td>
	            			<td class="text-center">
	            				@if($experience->status == 0)
	            					Response Pending <br>
	            					<label class="btn btn-info sendReminder" data-tracker="{{ $experience->secret }}"><i class="md md-email"></i> Send Reminder</label>
	            					<br>
                                        <small style="font-size:70%;">Last Contacted: {{ lastContacted($experience->id) }}</small></td>
	            				@elseif($experience->status == 1)
	            					<i class="md md-check"></i> Complete
	            				@endif
	            			</td>
	            			<td class="text-center">
	            				@if($experience->status == 0)
	            			<button class="btn btn-danger removeReference" data-reference="{{ $experience->id }}"><i class="fa fa-times"></i></button>
	            				@endif
	            			</td>
	            		</tr>
	            	@endforeach
	            	@if($experiences->count() > 0)
	            		<style type="text/css">.step5{ display:block !important; }</style>
	            	@endif
	            </tbody>
	        </table>
        </div>
        @endif
	</div>
</div>


		<!-- Modal -->
        <div id="create" class="modal-demo">
            <button type="button" class="close" onclick="Custombox.close();">
                <span>&times;</span><span class="sr-only">Close</span>
            </button>
            <h4 class="custom-modal-title">Add Work Experience</h4>
            <div class="custom-modal-text">
                <form method="POST" action="{{ route('saveRecertifyExperience') }}">
                	@csrf
                	<input type="hidden" name="type" value="2">
	                <div class="form-row">
	                	<div class="form-group-custom text-left col-md-12">
	                        <input type="text" required="" name="organization" value="">
	                        <label class="control-label">Organization Name</label><i class="bar"></i>
	                    </div>
	                </div>
	                <div class="form-row">
	                	<div class="form-group-custom text-left col-md-6">
	                        <select required="" name="startMonth" class="form-control material">
	                        	<option value="">Please Select</option>
		                        @for($i = 1; $i <= 12; $i++)
		                        	@if($i < 10)
		                        		<option value="0{{ $i }}">{{ $i }}</option>
		                        	@else
		                        		<option value="{{ $i }}">{{ $i }}</option>
		                        	@endif
		                        @endfor
	                        </select>
	                        <label class="control-label">Start Month</label><i class="bar"></i>
	                    </div>
	                    <div class="form-group-custom text-left col-md-6">
	                        <input type="number" required="" name="startYear" value="" step="1" minlength="4" maxlength="4" min="1950" max="{{ Date('Y') }}">
	                        <label class="control-label">Start Year (YYYY)</label><i class="bar"></i>
	                    </div>
	                </div>
	                <div class="form-row">
	                	<div class="form-group col-md-12 text-left">
	                		<div class="checkbox">
                                <input id="currently" type="checkbox" name="currently" value="1">
                                <label for="currently">
                                    I currently work here
                                </label>
                            </div>
	                	</div>
	                </div>
	                <div class="form-row endtime">
	                	<div class="form-group-custom text-left col-md-6">
	                        <select required="" name="endMonth" class="form-control material">
	                        	<option value="">Please Select</option>
		                        @for($i = 1; $i <= 12; $i++)
		                        	@if($i < 10)
		                        		<option value="0{{ $i }}">{{ $i }}</option>
		                        	@else
		                        		<option value="{{ $i }}">{{ $i }}</option>
		                        	@endif
		                        @endfor
	                        </select>
	                        <label class="control-label">End Month</label><i class="bar"></i>
	                    </div>
	                    <div class="form-group-custom text-left col-md-6">
	                        <input type="number" required="" name="endYear" value="" step="1" minlength="4" maxlength="4" min="1950" max="{{ Date('Y') }}">
	                        <label class="control-label">End Year (YYYY)</label><i class="bar"></i>
	                    </div>
	                </div>
	                <div class="form-row" style="margin-bottom: 20px;">
	                	<div class="form-group-custom text-left col-md-12">
	                        <label class="control-label">Experiences</label>
	                    </div>
	                    <div class="col-md-12">
	                    	<select class="select2 select2-multiple" multiple="multiple" name="experiences[]" multiple data-placeholder="Choose ..." required="">
	                    		<option>Construction Manager</option>
	                    		<option>Designer</option>
	                    		<option>Engineer</option>
	                    		<option>Foreperson</option>
	                            <option>Laborer</option>
	                            <option>Operator</option>
	                            <option>Owner / Executive</option>
	                            <option>Project Manager</option>
	                            <option>Risk Manager</option>
	                            <option>Safety Professional</option>
	                            <option>Superintendent</option>
	                        </select>
	                    </div>
	                </div>
	                <div class="form-row">
	                	<div class="form-group-custom text-left col-md-4">
	                        <input type="text" required="" name="reference" value="">
	                        <label class="control-label">Reference Name</label><i class="bar"></i>
	                    </div>
	                    <div class="form-group-custom text-left col-md-4">
	                        <input type="text" required="" name="email" value="">
	                        <label class="control-label">Email</label><i class="bar"></i>
	                    </div>
	                    <div class="form-group-custom text-left col-md-4">
	                        <input type="number" required="" name="phone" value="">
	                        <label class="control-label">Phone</label><i class="bar"></i>
	                    </div>
	                </div>
	                <div class="form-row text-left">
	                	<em>Note: Once you press submit, your reference will receive an email from certificationteam@sctpp.org asking them to verify your experience. Please be sure beforehand that they are expecting an email from us. Generally, we recommend providing the contact information for someone that currently works in Human Resources.</em>
	                	<br>
	                </div>
	                <div class="form-row text-left">
	                	<button class="btn btn-primary">Submit</button>
	                </div>
                </form>
            </div>
        </div>

@endsection

@section('js')

<!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script src="/assets/plugins/select2/select2.min.js" type="text/javascript" ></script>
    <script type="text/javascript">
    	$(".select2").select2();

    	$("#currently").click(function(){
    		if($(this).is(":checked")){
    			$(".endtime ").hide();
    			$(".endtime select").each(function(){
    				$(this).removeAttr("required");
    			});
    			$(".endtime input").each(function(){
    				$(this).removeAttr("required");
    			});

    		} else {
    			$(".endtime ").show();
    			$(".endtime select").each(function(){
    				$(this).attr("required", "required");
    			});
    			$(".endtime input").each(function(){
    				$(this).attr("required", "required");
    			});
    		}
    	});

    	$(".removeReference").click(function(){
    		var reference = $(this).attr("data-reference");
    		if(confirm("Are you sure you want to remove this reference from your application?")){
    			$(this).parent().parent().remove();
                $.get("{{ route('removeExperience', ['reference' => NULL]) }}/" + reference, function(data, status){
                    $.Notification.notify('success','bottom right', 'Done', 'Your reference has been removed.');
                });
    		}
    	});

    	$(".sendReminder").click(function(){
    		var tracker = $(this).attr("data-tracker");
    		if(confirm("Are you sure you want to send this contact a reminder email?")){
                $.get("{{ route('remindReference', ['secret' => NULL]) }}/" + tracker, function(data, status){
                    $.Notification.notify('success','bottom right', 'Done', 'Your reference has been sent a reminder email.');
                });
    		}
    	});

    </script>
@endsection