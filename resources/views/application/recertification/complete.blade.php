@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	a.btn{
		width: 300px;
		margin:0 auto;
		text-decoration: none !important;
	}
	</style>
@endsection

@section('content')

<div class="card-box col-md-6 offset-md-3">
	<div class="card-body">
		<div class="row" >
			<h4 class="m-t-0 header-title col-md-12 text-center" style="margin-bottom: 20px; padding: 20px;"><b><i class="md md-check text-success" style="font-size:96px;"></i></b></h4>
		</div>
		<div class="row text-center" style="margin-bottom: 20px;">
			<p style="font-size:38px; width: 100%;">Thank You!</p>
			<p>Please give us up to 15 business days to review your submission. Only submissions with no outstanding documents or references will be considered.</p>
		</div>
		<div class="row text-center" style="margin-bottom: 20px;">
			<a class="btn btn-primary" href="{{ route('logout') }}">Log Out</a>
		</div>
		<div class="row text-center">
			<a class="btn btn-link" href="/">Return to Profile</a>
		</div>
	</div>
</div>


@endsection

@section('js')


@endsection