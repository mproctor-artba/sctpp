@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	a.btn{
		width: 300px;
		margin:0 auto;
		text-decoration: none !important;
	}
	</style>
@endsection

@section('content')

<div class="card-box col-md-6 offset-md-3">
	<div class="card-body">
		<div class="row" >
			<h4 class="m-t-0 header-title col-md-12 text-center" style="margin-bottom: 20px; padding: 20px;"><b><i class="md md-book text-success" style="font-size:96px;"></i></b></h4>
		</div>
		<form method="POST" action="{{ route('enroll') }}">
		@csrf
		<div class="text-center" style="margin-bottom: 20px;">
			<h3 style="width: 100%;">You are eligible to enroll into the following courses:</h3>
			<div style="background: whitesmoke; padding:10px; border-radius: 10px; margin:20px 0;">
			<ul style="text-align:left; width: 500px; margin:0 auto; font-size: 18px;">
				@foreach($courseNames as $courseName)
					<li>{{ $courseName }}</li>
				@endforeach
			</ul>
			</div>
			<h4>To enroll into the above courses, type your password below.</h4>

			<input type="password" class="form-control" name="password" style="width:300px; margin:0 auto;">
		</div>
		<div class="text-center" style="margin-bottom: 50px; width: 100%">
			<button class="btn btn-primary" style="min-width: 300px;">ENROLL NOW</button>
		</div>
		</form>
		<div class="text-center" style="width: 100%;">
			<a class="btn btn-link" href="{{ route('complete') }}" style="" style="max-width: 200px;">SKIP ENROLLMENT</a>
		</div>
	</div>
</div>


@endsection

@section('js')


@endsection