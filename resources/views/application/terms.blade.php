@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	</style>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
				<div class="row">
				<form method="POST" action="{{ route('saveTerms') }}" id="terms">
					@csrf
					<div class="col-lg-12">
						<h4 class="m-t-0 header-title"><b>Please read each statement carefully and check the corresponding box to denote your understanding before proceeding.</b></h4>
	                    
	                      <input type="checkbox" name="read" value="1" required="" id="read" class="required" aria-required="true" @if(Auth::user()->step5())checked @endif>
	                      &nbsp; &nbsp; <span>I acknowledge that I have read, understand, and agree to abide by the terms outlined in this <a href="https://sctpp.org/documents/handbook" target="_blank">SCTPP Candidate Handbook</a>.
	                      </span>
	                    </div>
	                    <div class="col-md-12">
	                      <input type="checkbox" name="sta" value="1" required="" class="required" aria-required="true" @if(Auth::user()->step5())checked @endif>
	                      &nbsp; &nbsp; <span>I fully understand that it is an application for certification and does not guarantee certification.
	                      </span>
	                    </div>
	                    <div class="col-md-12">
	                      <input type="checkbox" name="under" value="1" required="" class="required" aria-required="true" @if(Auth::user()->step5())checked @endif>
	                      &nbsp; &nbsp; <span>I understand that the Certification Commission reserves the right to revise or update this application, the Code of Ethics and recertification requirements, and it is my responsibility to be aware of these current requirements.  I further understand that I am obligated to immediately inform the Certification Commission of changed circumstances that may materially affect my application, including my ability to continue to fulfill certification requirements.  I further understand that it is my responsibility to provide any requested documentation in connection with this application.
	                      </span>
	                    </div>
	                    <div class="col-md-12">
	                      <input type="checkbox" name="attest" value="1" required="" class="required" aria-required="true" @if(Auth::user()->step5())checked @endif>
	                      &nbsp; &nbsp; <span>I hereby attest that I am taking the examination solely for the purposes of earning the certification.  I further understand that I am prohibited from transmitting information regarding examination questions or content in any form to any person or entity, and understand that failure to comply with this prohibition may result in my certification being revoked and/or legal action being taken against me.
	                      </span>
	                    </div>
	                    <div class="col-md-12">
	                      <input type="checkbox" name="agree" value="1" required="" class="required" aria-required="true" @if(Auth::user()->step5())checked @endif>
	                      &nbsp; &nbsp; <span>I understand and agree that if I am certified following acceptance of this application and successful completion of the examination, such certification does not constitute a warranty or guarantee of my fitness or competency to practice as a certified professional. If I am certified, I authorize the Certification Commission to include my name in a list of certified individuals and agree to use the certification and related trade names, trademarks, and logos only as permitted by program policies.  I understand and agree that the Certification Commission may also use anonymous and aggregate application and examination data for statistical and research purposes.
	                      </span>
	                    </div>
	                    <div class="col-md-12">
	                        <input type="checkbox" name="backgroundcheck" class="required" required="" aria-required="true" @if(Auth::user()->step5())checked @endif>
	                      &nbsp; &nbsp; <span>I authorize ARTBA's Certification Team to conduct a verification of the employment experience provided with my application.</span>
	                    </div>
	                    <div class="col-md-12">
	                      <input type="checkbox" value="1" required="" class="required" aria-required="true" @if(Auth::user()->step5())checked @endif name="accomodation">
	                      &nbsp; &nbsp; <span>I understand that I may request exam accommodation for special needs as outlined on Page 17 of the SCTPP Candidate Handbook.</span>
	                    </div>
	                    <div class="col-md-12">
	                        <input type="checkbox" name="bylaw" class="required" required="" aria-required="true"  @if(Auth::user()->step5())checked @endif >
	                      &nbsp; &nbsp; <span>I understand that ARTBA may be required to release confidential information to legal authorities if required by the law. I accept email to be an acceptable form of notification to inform me if ARTBA is required to release my information.</span>
	                    </div>
	                    <div class="col-md-12">
	                        <input type="checkbox" name="notmisrepresent" class="required" required="" aria-required="true" @if(Auth::user()->step5())checked @endif>
	                      &nbsp; &nbsp; <span>I agree to not misrepresent the scope or purpose of the SCTPP certification, make statements that are misleading or unauthorized, or to use the certification in a manner that will bring the SCTPP into disrepute.</span>
	                    </div>
	                    <div class="col-md-12">
	                        <input type="checkbox" name="agreeDiscontinue" class="required" required="" aria-required="true" @if(Auth::user()->step5())checked @endif>
	                      &nbsp; &nbsp; <span>I agree to discontinue use of- or reference to possession of the SCTPP certification if I am suspended or the certification is withdrawn.  I will return SCTPP certificates and other marks to the SCTPP staff or Commission upon suspension or revocation of my certification.</span>
	                    </div>
	                    <div class="col-md-12">
	                    <br>
	                      <h4 class="m-t-0 header-title text-danger"><b>Applicant Signature: (use your cursor to draw your signature below)</h4>
	                      <br>
	                      
	                      @if(!empty($application->signature))
	                      <img src="data:image/svg+xml;base64,{{ $application->signature }}" style="margin:50px 0; border-bottom:2px solid silver;">
	                      @else
	                      <button class="btn btn-danger" type="button" id="clear"><i class="md md-refresh"></i> Clear Signature Pad</button>
	                      <input type="hidden" name="signature" id="sig">
	                      <div id="signature"></div>
	                      @endif
	                </div>
	                @if(empty($application->signature))
	                <div class="col-md-12">
	                	<button type="button" class="btn btn-primary submit disabled">Save and Move to Step 6 <span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></button>
	                </div>
	                @endif
	                </form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')
<script src="/assets/js/jSignature.min.js"></script>
<script type="text/javascript">
	$("#signature").jSignature();
	$("body").on("click", "#clear", function(){
            $("#signature").jSignature("reset");
    });
    $("form").mousemove(function(){
    	var blank = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMCIgaGVpZ2h0PSIwIj48L3N2Zz4=";

    	var datapair = $("#signature").jSignature("getData", "svgbase64");
    	if(datapair != null){
    	$("#sig").val(datapair[1]);

    	if($("#sig").val() != blank && $('.required:checked').length == $('.required').length) {
    		$(".submit").removeClass("disabled");

    	} else {
    		$(".submit").addClass("disabled");
    	}
    }
            
    });
    $(".submit").click(function(){
    	var blank = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMCIgaGVpZ2h0PSIwIj48L3N2Zz4=";

    	var datapair = $("#signature").jSignature("getData", "svgbase64");
    	$("#sig").val(datapair[1]);

    	if($("#sig").val() != blank) {
    		if ($('.required:checked').length == $('.required').length) {
    			$("#terms").submit();
    		} else {
    			$.Notification.notify('error','bottom right', 'Full Agreement Required', 'Applicants must agree to all terms of the SCTPP&trade; Program');
    		}
    		
    	} else {
    		$.Notification.notify('error','bottom right', 'Missing Signature!', 'You must apply your signature in order to mark your agreement to the terms of the SCTPP&trade; Program');
    	}
    });
</script>
@endsection