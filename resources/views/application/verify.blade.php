@extends('layouts.email-primary')

@section('css')
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="/css/theme-overide.css" rel="stylesheet" type="text/css" />
        <script src="/assets/js/modernizr.min.js"></script>
@endsection

@section('content')
<form method="POST" action="{{ route('submitVerify', ['secret' => $experience->secret, 'spoof' => generateRandomString() ]) }}">
			@csrf
			@php ($name = $profile->first_name . " " . $profile->last_name)
	<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
		
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align:center;" align="center" valign="top">
			<img src="{{ URL('/images/sctpp-logo.png') }}" width="100%" style="max-width: 400px;">
			</td>
		</tr>
		<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
			<td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
			
			<strong>Select all of the statements that are true:</strong>
			<br>
			<ul style="list-style: none; padding:0; ">
				<li><input type="checkbox" required=""> I certify that I am the appropriate person at {{ $experience->organization }} to verify the work experience of {{ $name }} at {{ $experience->organization }}.</li>
				<li><input type="checkbox" required=""> I certify that {{ $name }} was employed at {{ $experience->organization }} from {{ $experience->start }} to {{ $experience->finished }}.</li>
				<li><input type="checkbox" required=""> I certify that {{ $name }} has served in roles that allowed them to accumulate experience in all of the stated areas:
				<ul>
					@foreach(explode(",", $experience->experiences) as $exp)
        				<li>{{ $exp }}</li>
        			@endforeach
				</ul>
				</li>
				<li><input type="checkbox" required=""> I certify that the accumulated occupational experience may include, but is not limited to, construction of highways, roads, bridges, airports, rail systems, utilities, or ports.</li>
				<li><input type="checkbox" required=""> I agree to be contacted by the ARTBA Certification Team to answer questions about the experience of {{ $name }}</li>
				<li><input type="checkbox" required=""> I understand that this submission may be subjected to an audit and if found to be untruthful, may lead to the suspension or revokation of {{ $name }}'s certification.</li>
			</ul>
			</form>
			</td>
		</tr>
	<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
			
		</td>
	</tr><tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" itemprop="handler" itemscope itemtype="http://schema.org/HttpActionHandler" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
			<button type="submit" class="btn btn-primary" itemprop="url" style="width:100%;">Submit Approval</button>
		</td>
	</tr><tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
	</tr></table>
	</form>
@endsection