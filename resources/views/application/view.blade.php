@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
	@if(Auth::user()->isAdmin())<li class="breadcrumb-item"><a href="{{ route('review') }}">Applications</a></li>@endif
	<li class="breadcrumb-item active">{{ $application->user->profile->first_name . " " . $application->user->profile->last_name }}</li>
</ol>
@endsection

@section('content')
@if(Auth::user()->isAdmin() && $application->status <= 3)
<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
				<div class="row">
                	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Review Application</b></h4>
                </div>
                <div class="row">
                	<form method="POST" class="form" style="width:100%;" action="{{ route('decide', ['id' => $application->id]) }}" id="decide">
                		@csrf
                		<div class="form-row" style="margin-bottom: 20px;">
                			<div class="col-md-12">
                			<textarea class="form-control" width="100%" name="reason"></textarea>
                			</div>
                		</div>
                		<div class="form-row" style="padding:0;">
                			<div class="col-md-6" align="right">
                				<button type="submit" name="decision" value="0" class="btn btn-danger waves-effect waves-light pull-left decide"><span class="btn-label"><i class="fa fa-times"></i></span> Reject Application</button>
                			</div>
                			<div class="col-md-6">
                				<button type="submit" name="decision" value="1" class="btn btn-success waves-effect waves-light pull-right decide"><span class="btn-label"><i class="fa fa-check"></i></span> Approve Application</button>
                			</div>
                		</div>
                	</form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
				<div class="row">
                	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Application Notes</b></h4>
                </div>
                <div class="row">
                <table class="table">
                	<thead>
                		<tr>
                			<th width="15%">Admin</th>
                			<th width="10%">Decision</th>
                			<th>Reason</th>
                			<th width="20%">Date</th>
                		</tr>
                	</thead>
                	<tbody>
                		@foreach($application->decisions as $decision)
                		<tr>
                			<td>
                				{{ $decision->user->profile->first_name . " " . $decision->user->profile->last_name }}
                			</td>
                			<td>
                				@if($decision->decision == 0)
                					Denied
                				@elseif($decision->decision == 1)
                					Approved
                				@endif
                			</td>
                			<td>{{ $decision->reason }}</td>
                			<td>{{ convertTimestamp($decision->date) }}</td>
                		</tr>
                	@endforeach
                	</tbody>
                </table>

                <ul style="padding: 0;">
                	
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row">
	<div class="col-lg-6">
		<div class="card-box">
			<div class="card-body">
                <div class="row">
                	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Candidate Info</b></h4>
                </div>
                <div class="row">
                	<ul style="list-style: none; padding: 0;">
                		<li>Name:	{{ $application->user->profile->first_name }} {{ $application->user->profile->middle }} {{ $application->user->profile->last_name }}</li>
                		<li>Email:	{{ $application->user->email }} </li>
                		<li>Phone:	{{ $application->user->profile->phone }}</li>
                		<li>Company:	{{ $application->user->profile->company }}</li>
                		<li>Job Title:	{{ $application->user->profile->title }}</li>
                		<li>Address: {{ $application->user->profile->address() }}</li>
                	</ul>
                </div>
            </div>
        </div>
    </div>
	<div class="col-lg-6">
		<div class="card-box">
			<div class="card-body">
                <div class="row">
                	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Candidate Eligibility</b></h4>
                </div>
                <div class="row">
                	@if($application->eligibility == 1)
                		<b>[OPTION 1 SELECTED]</b>
                		<p>Proof of three years’ full-time or equivalent experience* in transportation construction industry. Transportation construction is occupational experience that includes but is not limited to construction of highways, roads, bridges, airports, rail systems, tunnels, utilities and ports.
				Upload your Certificate (Completion of an OSHA-10, OSHA-30, OSHA-500 OR OSHA-510 course.)</p>
                	@elseif($application->eligibility == 2)
                		<b>[OPTION 2 SELECTED]</b>
                		<p>Baccalaureate degree in engineering or construction management with two years’ experience* in transportation construction industry. Experience must be within five years of applying for certification.
				Completion of an OSHA-10, OSHA-30, OSHA-500 OR OSHA-510 course.</p>
                	@elseif($application->eligibility == 3)
                		<b>[OPTION 3 SELECTED]</b>
                		<p>Associate or technical degree in safety with two years’ experience in transportation construction industry*. Experience must be within five years of applying for certification.</p>
                	@endif

                	<p><em>* Experience is defined as a job classification including, but not limited to, laborer, operator, foreperson, superintendent, project manager, construction manager, engineer, safety professional, risk manager, inspector, surveyor, or estimator. Internships qualify as experience. Experience must be within five years of applying for certification.</em></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
				<div class="col-lg-12">
						<h4 class="m-t-0 header-title"><b>Candidate Agreement and Signature</b></h4>
	                    
	                      <strong>[<i class="fa fa-check"></i>] </strong>
	                      &nbsp; &nbsp; <span>I acknowledge that I have read, understand, and agree to abide by the terms outlined in this <a href="https://sctpp.org/documents/handbook" target="_blank">SCTPP Candidate Handbook</a>.
	                      </span>
	                    </div>
	                    <div class="col-md-12">
	                      <strong>[<i class="fa fa-check"></i>] </strong>
	                      &nbsp; &nbsp; <span>I fully understand that it is an application for certification and does not guarantee certification.
	                      </span>
	                    </div>
	                    <div class="col-md-12">
	                      <strong>[<i class="fa fa-check"></i>] </strong>
	                      &nbsp; &nbsp; <span>I understand that the Certification Commission reserves the right to revise or update this application, the Code of Ethics and recertification requirements, and it is my responsibility to be aware of these current requirements.  I further understand that I am obligated to immediately inform the Certification Commission of changed circumstances that may materially affect my application, including my ability to continue to fulfill certification requirements.  I further understand that it is my responsibility to provide any requested documentation in connection with this application.
	                      </span>
	                    </div>
	                    <div class="col-md-12">
	                      <strong>[<i class="fa fa-check"></i>] </strong>
	                      &nbsp; &nbsp; <span>I hereby attest that I am taking the examination solely for the purposes of earning the certification.  I further understand that I am prohibited from transmitting information regarding examination questions or content in any form to any person or entity, and understand that failure to comply with this prohibition may result in my certification being revoked and/or legal action being taken against me.
	                      </span>
	                    </div>
	                    <div class="col-md-12">
	                      <strong>[<i class="fa fa-check"></i>] </strong>
	                      &nbsp; &nbsp; <span>I understand and agree that if I am certified following acceptance of this application and successful completion of the examination, such certification does not constitute a warranty or guarantee of my fitness or competency to practice as a certified professional. If I am certified, I authorize the Certification Commission to include my name in a list of certified individuals and agree to use the certification and related trade names, trademarks, and logos only as permitted by program policies.  I understand and agree that the Certification Commission may also use anonymous and aggregate application and examination data for statistical and research purposes.
	                      </span>
	                    </div>
	                    <div class="col-md-12">
	                        <strong>[<i class="fa fa-check"></i>] </strong>
	                      &nbsp; &nbsp; <span>I authorize ARTBA's Certification Team to conduct a verification of the employment experience provided with my application.</span>
	                    </div>
	                    <div class="col-md-12">
	                      <strong>[<i class="fa fa-check"></i>] </strong>
	                      &nbsp; &nbsp; <span>I understand that I may request exam accommodation for special needs as outlined on Page 17 of the SCTPP Candidate Handbook.</span>
	                    </div>
	                    <div class="col-md-12">
	                        <strong>[<i class="fa fa-check"></i>] </strong>
	                      &nbsp; &nbsp; <span>I understand that ARTBA may be required to release confidential information to legal authorities if required by the law. I accept email to be an acceptable form of notification to inform me if ARTBA is required to release my information.</span>
	                    </div>
	                    <div class="col-md-12">
	                        <strong>[<i class="fa fa-check"></i>] </strong>
	                      &nbsp; &nbsp; <span>I agree to not misrepresent the scope or purpose of the SCTPP certification, make statements that are misleading or unauthorized, or to use the certification in a manner that will bring the SCTPP into disrepute.</span>
	                    </div>
	                    <div class="col-md-12">
	                        <strong>[<i class="fa fa-check"></i>] </strong>
	                      &nbsp; &nbsp; <span>I agree to discontinue use of- or reference to possession of the SCTPP certification if I am suspended or the certification is withdrawn.  I will return SCTPP certificates and other marks to the SCTPP staff or Commission upon suspension or revocation of my certification.</span>
	                    </div>
	                    <div class="col-md-12">
	                      <label>Applicant Signature (use your cursor to draw your signature below)</label>
	                      <br>
	                      
	                      @if(!empty($application->signature))
	                      <img src="data:image/svg+xml;base64,{{ $application->signature }}" style="margin:50px 0; border-bottom:2px solid silver;">
	                      @else
	                      <button class="btn btn-danger" type="button" id="clear"><i class="md md-refresh"></i> Clear Signature Pad</button>
	                      <input type="hidden" name="signature" id="sig">
	                      <div id="signature"></div>
	                      @endif
	                </div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
                <div class="row">
                	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Candidate Documents</b></h4>
                </div>
                <div class="row">
                	<ul style="list-style: none; width: 100%;">
                	@foreach($application->user->documents as $document)
                		<li style="position:relative; display: inline-block; min-height: 400px; text-align: center;" ><div onclick=" openInNewTab('/uploads/documents/{{ $document->name }}')"><iframe src="/uploads/documents/{{ $document->name }}" style="width:100%; min-height: 400px;" ></iframe><button class="btn btn-primary">Open Document</button></div></li>
                	@endforeach
                	</ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
                <div class="row">
                	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>Candidate Experience</b></h4>
                </div>
                <div class="row">
                	<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
	            <thead>
	            <tr>
	                <th>Organization</th>
	                <th width="15%">Dates</th>
	                <th>Experience</th>
	                <th width="20%">Supervisor</th>
	                <th width="15%">Status</th>
	            </tr>
	            </thead>
	            <tbody>
	            	@foreach($application->user->experiences as $experience)
	            		<tr>
	            			<td>{{ $experience->organization }}</td>
	            			<td>
		            			<ul style="padding: 0; list-style: none;">
		            				<li>Start: {{ $experience->start }}</li>
		            				<li>End: {{ $experience->finished }}</li>
		            				<li>Length: 1 year</li>
		            			</ul>
	            			</td>
	            			<td>
		            			<ul style="padding: 0; list-style: none;">
			            			@foreach(explode(",", $experience->experiences) as $exp)
			            				<li>{{ $exp }}</li>
			            			@endforeach
		            			</ul>
	            			</td>
	            			<td>
	            				<ul style="padding: 0; list-style: none;">
	            				<li>Reference: {{ $experience->reference }}</li>
	            				<li>Email: {{ $experience->email }}</li>
	            				<li>Phone: {{ $experience->phone }}</li>
	            			</ul>
	            			</td>
	            			<td>
	            				@if($experience->status == 0)
	            					<label class="btn btn-info sendReminder" data-tracker="{{ $experience->secret }}"><i class="md md-email"></i> Send Reminder</label>
	            				@elseif($experience->status == 1)
	            					<label class=""><b><i class="md md-check"></i></b> Verification Complete</label>
	            				@endif
	            			</td>
	            		</tr>
	            	@endforeach
	            </tbody>
	        </table>
                </div>
            </div>
        </div>
    </div>
</div>

@if(isset($application->user->certificate->certificate))
<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
				<div class="row">
                	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;"><b>SCTPP Certificate</b></h4>
                </div>
				<iframe src="https://pdf.credential.net/{{ $application->user->certificate->certificate }}.pdf" width="100%" style="min-height:600px;"></iframe>
			</div>
		</div>
	</div>
</div>
@endif

@endsection

@section('js')
	<script>
		function openInNewTab(url) {
  var win = window.open(url, '_blank');
  win.focus();
}

		$(".sendReminder").click(function(){
    		var tracker = $(this).attr("data-tracker");
    		if(confirm("Are you sure you want to send this contact a reminder email?")){
                $.get("{{ route('remindReference', ['secret' => NULL]) }}/" + tracker, function(data, status){
                    $.Notification.notify('success','bottom right', 'Done', 'This reference has been sent a reminder email.');
                });
    		}
    	});
	</script>
@endsection