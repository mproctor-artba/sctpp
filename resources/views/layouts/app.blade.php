<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>SCTPP | {{ ucfirst($page) }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="The ARTBA Foundation’s Safety Certification for Transportation Project Professionals™ (SCTPP) program was established in 2016 to make safety top-of-mind for all professionals involved in the planning, design, management, materials delivery and construction of transportation projects from inception through completion." name="description" />
        <meta content="ARTBA Certification Team" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/assets/images/favicon.png">

        <!-- App css -->
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/custombox/custombox.css" rel="stylesheet">
        <link href="/css/theme-overide.css" rel="stylesheet" type="text/css" />
        <script src="/assets/js/modernizr.min.js"></script>
        @yield('css')
    </head>


    <body>
        <!-- Navigation Bar-->
        @if(Auth::user()->isAdmin())
            @include('layouts.admin-nav')
        @else
            @if(Request::route()->getPrefix() == "application/recertification")
                 @include('layouts.recertification-nav')
            @else
                @include('layouts.user-nav')
            @endif
        @endif

        
        <!-- End Navigation Bar-->

        <div class="wrapper">
            <div class="container-fluid">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        @yield('crumbs')
                    </div>
                    <h4 id="page-title" class="page-title">&nbsp;</h4>
                </div>
                @yield('content')
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        @include('layouts.footer')
        <!-- End Footer -->

        <!-- jQuery  -->
        <script src="/assets/plugins/switchery/js/switchery.min.js"></script>
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/waves.js"></script>
        <script src="/assets/js/jquery.slimscroll.js"></script>
        <script src="/assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="/assets/js/jquery.core.js"></script>
        <script src="/assets/js/jquery.app.js"></script>
        <script src="/assets/plugins/notifyjs/notify.min.js"></script>
        <script src="/assets/plugins/notifyjs/notify-metro.js"></script>
        <script src="/assets/plugins/custombox/custombox.min.js"></script>
        <script src="/assets/plugins/custombox/legacy.min.js"></script>
        <script text="">
            $(document).ready(function() {
                $("#page-title").html("{!! $title !!}");

                $(".incompleteSteps").click(function(){
                    $.Notification.notify('error','bottom right', 'Previous Steps Incomplete', 'You will be able to access this tab once all previous steps are complete.');
                });

                @if(session('message') !== null)
                    $.Notification.notify('{{ session("message")["alert"] }}','bottom right', '{{ session("message")["header"] }}', '{{ session("message")["body"] }}');
                @endif
            });
        </script>
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83551434-1', 'auto');
  ga('send', 'pageview');

</script>
        @yield('js')
    </body>
</html>