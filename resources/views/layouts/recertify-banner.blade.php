@if(isset(Auth::user()->certificate))
    @if(Auth::user()->certificate->daysToRecertify() > 0)
    <div class="card text-white bg-success text-xs-center" style="margin-bottom: 10px;">
        <div class="card-body">
            <div class="row">
                <div class="col-md-10">
                    <h4 class="text-white">Congratulations, You're Safety Certified!</h4>
                    <p>Your SCTPP certificate expires on {{ Date("m-d-Y",Auth::user()->certificate->expiration()) }}</p>
                    <footer><a href="{{ Auth::user()->certificate->link() }}" style="color: white; border: 1px solid white; padding: 10px 20px; border-radius: 3px;" target="_blank"><strong>View Your Certificate <i class="fa fa-certificate"></i></strong></a>
                    </footer>
                </div>
                <div class="col-md-2 text-center">
                    <h2 class="text-white">{{ number_format(Auth::user()->certificate->daysToRecertify()) }}</h2>
                    <p>Days left to recertify</p>
                    @if(Auth::user()->step11() == NULL)
                        <a class="btn btn-white" href="{{ route('recertify') }}"> Get Started</a>
                    @else
                        @if(Auth::user()->application->stage == NULL || Auth::user()->application->stage > 1 && Auth::user()->application->stage < 4)
                        <a class="btn btn-white" href="{{ route('resumeApp') }}"> Resume Application</a>
                        @else
                        <a class="btn btn-white" href="{{ route('resumeApp') }}"><i class="fa fa-check"></i> Application Submitted</a>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="card text-white bg-danger text-xs-center" style="margin-bottom: 10px;">
        <div class="card-body">
            <div class="row">
                <div class="col-md-10">
                    <h4 class="text-white">You're Safety Certified!</h4>
                    <p>Your SCTPP certificate expired on {{ Date("m-d-Y",Auth::user()->certificate->expiration()) }}</p>
                    <footer><a href="{{ Auth::user()->certificate->link() }}" style="color: white; border: 1px solid white; padding: 10px 20px; border-radius: 3px;" target="_blank"><strong>View Your Certificate <i class="fa fa-certificate"></i></strong></a>
                    </footer>
                </div>
                <div class="col-md-2 text-center">
                    <h2 class="text-white">{{ number_format(Auth::user()->certificate->daysToRecertify()) }}</h2>
                    <p>Days left to recertify</p>
                    @if(Auth::user()->step11() == NULL)
                        <a class="btn btn-white" href="{{ route('recertify') }}"> Get Started</a>
                    @else
                        @if(Auth::user()->application->stage == NULL || Auth::user()->application->stage > 1 && Auth::user()->application->stage < 4)
                        <a class="btn btn-white" href="{{ route('resumeApp') }}"> Resume Application</a>
                        @else
                        <a class="btn btn-white" href="{{ route('resumeApp') }}"><i class="fa fa-check"></i> Application Submitted</a>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endif
@endif