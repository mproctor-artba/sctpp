var certificateChartData = {
	labels: ['2016', '2017', '2018', '2019', '2020', '2021'],
	datasets: [{
		label: 'Total Certificates',
		borderColor: "#5fbeaa",
		borderWidth: 1,
		fill: "rgba(57,175,209,0.2)",
		backgroundColor: "rgba(95, 190, 170, 0.5)",
		data: [
			{{ $certificates->where('created_at','>=','2016-01-01 00:00:00')->where('created_at','<=','2016-12-31 11:59:59')->count() }}, {{ $certificates->where('created_at','>=','2017-01-01 00:00:00')->where('created_at','<=','2017-12-31 11:59:59')->count() }}, {{ $certificates->where('created_at','>=','2018-01-01 00:00:00')->where('created_at','<=','2018-12-31 11:59:59')->count() }}, {{ $certificates->where('created_at','>=','2019-01-01 00:00:00')->where('created_at','<=','2019-12-31 11:59:59')->count() }},{{ $certificates->where('created_at','>=','2020-01-01 00:00:00')->where('created_at','<=','2020-12-31 11:59:59')->count() }}, {{ $certificates->where('created_at','>=','2021-01-01 00:00:00')->where('created_at','<=','2021-12-31 11:59:59')->count() }}
		]
	},
	{
	label: 'Total Recertifications',
	borderColor: "rgba(0, 123, 255, 1)",
	borderWidth: 1,
	backgroundColor: "rgba(0, 123, 255, 0.3)",
	data: [0,0,0,
	{{ $applications->where('stage', 5)->where('updated_at','>=','2019-01-01 00:00:00')->where('updated_at','<=','2019-12-31 11:59:59')->count() }},
	{{ $applications->where('stage', 5)->where('updated_at','>=','2020-01-01 00:00:00')->where('updated_at','<=','2020-12-31 11:59:59')->count() }},
	{{ $applications->where('stage', 5)->where('updated_at','>=','2021-01-01 00:00:00')->where('updated_at','<=','2021-12-31 11:59:59')->count() }}]
	}],
	  options: {
	    
	  }
};
