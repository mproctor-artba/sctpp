var barChartData = {
	labels: ['50-60','61-70','71-80','81-90','91-100'],
	datasets: [{
		label: 'Below Standard',
		backgroundColor: "rgba(239, 69, 84, 0.5)",
		borderColor: "#ef4554",
        borderWidth: 1,
		data: [
			randomScalingFactor(),
			randomScalingFactor(),
			randomScalingFactor(),
			randomScalingFactor(),
			randomScalingFactor()
		]
	}, {
		label: 'Near Standard',
		backgroundColor: "rgba(0, 123, 255, 0.3)",
		borderColor: "rgba(0, 123, 255, 1)",
		borderWidth: 1,
		data: [
			randomScalingFactor(),
			randomScalingFactor(),
			randomScalingFactor(),
			randomScalingFactor(),
			randomScalingFactor()
		]
	}, {
		label: 'Above Standard',
		backgroundColor: "rgba(95, 190, 170, 0.5)",
		borderColor: "#5fbeaa",
		borderWidth: 2,
		data: [
			randomScalingFactor(),
			randomScalingFactor(),
			randomScalingFactor(),
			randomScalingFactor(),
			randomScalingFactor()
		]
	}]
};