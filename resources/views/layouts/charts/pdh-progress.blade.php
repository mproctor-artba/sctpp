<div class="row">
	<div class="col-md-12">
		<div class="progress progress-xxl">
            <div id="req0-progress"  class="progress-bar bg-info" role="progressbar" style="background-color:orange !important;" aria-valuenow="{{ $approvedSum }}" aria-valuemin="0" aria-valuemax="100"><div id="req0-progress-complete"  class="progress-bar bg-success" role="progressbar" style="position: absolute; height: 10px; border-radius: 20px;" aria-valuenow="{{ $approvedSum }}" aria-valuemin="0" aria-valuemax="100"></div></div>
        </div>
		<label><strong>Total Progress:</strong> <span id="totalHours">{{ $approvedSum }}/30</span> Hours</label>
    </div>
</div>