var examChartData = {
	labels: ['2016', '2017', '2018', '2019', '2020', 2021],
	datasets: [
        {
            label: "Passed",
            backgroundColor: "rgba(95, 190, 170, 0.5)",
            borderColor: "#5fbeaa",
            borderWidth: 1,
            hoverBackgroundColor: "rgba(95, 190, 170, 0.8)",
            hoverBorderColor: "#5fbeaa",
            data: {{ json_encode($results["passed"]) }}
        },
        {
            label: "Failed",
            backgroundColor: "rgba(239, 69, 84, 0.5)",
            borderColor: "#ef4554",
            borderWidth: 1,
            hoverBackgroundColor: "rgba(239, 69, 84, 0.8)",
            hoverBorderColor: "#ef4554",
            data: {{ json_encode($results["failed"]) }}
        },
        {
            label: "No show",
            backgroundColor: "rgba(255, 136, 0, 0.5)",
            borderColor: "#ff8800",
            borderWidth: 1,
            hoverBackgroundColor: "rgba(255, 136, 0, 0.8)",
            hoverBorderColor: "#ff8800",
            data: {{ json_encode($results["noshow"]) }}
        }
	]
};