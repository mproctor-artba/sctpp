var applicationChartData = {
    labels: [
        "Incomplete {{ $progress['percentages'][0] }}",
        "In Progress {{ $progress['percentages'][1] }}",
        "Finishing Up {{ $progress['percentages'][2] }}",
        "Denied {{ $progress['percentages'][3] }}",
        "Awaiting Exam {{ $progress['percentages'][4] }}"
    ],
    datasets: [
        {
            data: {{ json_encode($progress["values"]) }},
            backgroundColor: [
                "#ff8800",
                "#ef4554",
                "#5fbeaa",
                "#566676",
                "#007bff"
            ],
            hoverBackgroundColor: [
                "#ff8800",
                "#ef4554",
                "#5fbeaa",
                "#566676",
                "#007bff"
            ],
            hoverBorderColor: "#fff"
        }]
};