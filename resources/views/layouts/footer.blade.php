<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                © {{ Date('Y') }} ARTBA Certification Team
            </div>
        </div>
    </div>
</footer>