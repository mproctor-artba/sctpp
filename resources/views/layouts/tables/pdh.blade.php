<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Applicant</th>
            <th class="text-center">Hours</th>
            <th>Detail</th>
            <th class="text-center">Evidence</th>
            <th>Decision</th>
        </tr>
    </thead>
    <tbody>
        @foreach($pdhs as $pdh)
            <tr>
                <td>{{ getFullName($pdh->user_id) }} </td>
                <td class="text-center">{{ $pdh->hours }}</td>
                <td>
                    <ul style="padding:0 0 0 5px; margin:0 0 0 5px;">
                        <li>Name: {{ $pdh->name }}</li>
                        <li>Location: {{ $pdh->city }}, {{ $pdh->state }}</li>
                        <li>Organization: {{ $pdh->organization }}</li>
                        <li>Contact Info: 
                            <ul>
                                <li>{{ $pdh->contactName }}</li>
                                <li>{{ $pdh->email }}</li>
                                <li> {{ $pdh->phone }}</li>
                            </ul>
                        </li>
                        <li>Activity: {{ convertRecertActivity($pdh->activity) }}</li>
                        <li>Date: {{ $pdh->activityDate }}</li>
                        @if($pdh->status == 1)
                            @foreach($pdh->decisions as $decision)
                                    <li><strong>Denied:</strong> {{ $decision->reason }}</li>
                                    @php break; @endphp
                            @endforeach
                        @endif
                    </ul>
                    @if(Auth::user()->isAdmin())
                        <br>
                        @php $decisions = $pdh->decisions; @endphp
                        @if(sizeof($decisions) > 0)
                            Admin Notes:
                            
                                @foreach($decisions as $decision)
                                    @if(!empty($decision->reason))
                                    <li style="margin-left:32px;">{{ $decision->reason }}</li>
                                    @endif
                                @endforeach
                            
                        @endif
                    @endif
                </td>
                <td class="text-center">
                    <a href="/uploads/pdhs/{{ $pdh->file }}" target="_blank"><i class="fa fa-file" style="font-size: 24px;"></i></a>
                </td>
                <td>
                	@if(Auth::user()->isAdmin())
                    <form method="POST" action="{{ route('decide-pdh') }}">
                        @csrf
                        <input type="hidden" name="pdh_id" value="{{ $pdh->id }}">
                        
                        <table width="100%" class="transparent">
                            <tbody>
                                @if($pdh->status == 0)
                                    <tr>
                                        <td colspan="2"><textarea class="form-control" name="reason" style="width:90%;"></textarea></td>
                                    </tr>
                                @endif
                                <tr>
                                    @if($pdh->status == 0)
                                        <td><button type="submit" class="btn btn-success" name="decision" value="2"><i class="fa fa-check"></i></button></td>
                                    <td><button type="submit" class="btn btn-danger" name="decision" value="1"><i class="fa fa-times"></i></button></td>

                                    @elseif($pdh->status == 1)
                                        <td colspan="2" class="text-center"><i class="fa fa-times text-danger"></i></td>
                                    @elseif($pdh->status == 2)
                                        <td colspan="2" class="text-center"><i class="fa fa-check text-success"></i></td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </form>
                    @else
                        @if($pdh->status == 0)
                             <i class="fa fa-clock-o text-info"></i> Pending
                        @elseif($pdh->status == 1)
                            <i class="fa fa-times text-danger"></i> Denied
                        @elseif($pdh->status == 2)
                            <i class="fa fa-check text-success"></i> Approved
                        @endif
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>