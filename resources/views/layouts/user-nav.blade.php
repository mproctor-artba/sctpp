<header id="topnav">
            <div class="navbar-custom">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->

                        <ul class="navigation-menu">
                        <a href="{{ route('home') }}" class="logo" style="color:white; font-size:20px; position: absolute; left: 100px; top: 50%; margin-top: -12px;">
                            SCTPP
                        </a>
                        @if(Auth::user()->isOrgAdmin())
                                <li class="has-submenu @if($page == 'Organization')active @endif">
                                    <a href="{{ route('myorgs') }}" class="hoverless">My Organization</a>
                                    <ul class="submenu">
                                        <li class="@if($page == 'Profile')active @endif">
                                            <a href="{{ route('myorgs') }}" class="hoverless">Org Profile</a>
                                        </li>
                                        <li class="@if($page == 'Document-Center')active @endif">
                                            <a href="{{ route('myorgdocuments') }}" class="hoverless">Documents Center</a>
                                        </li>
                                        <li class="@if($page == 'Analytics')active @endif">
                                            <a href="{{ route('myorganalytics') }}" class="hoverless">Analytics</a>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        @if(Auth::user()->isNew())
                            <li class="has-submenu @if($page == 'profile')active @endif">
                                <a href="{{ route('profile') }}" class="hoverless">
                                @if(Auth::user()->step1())
                                <i class="md md-check"></i>
                                @else
                                <i class="md">Step 1</i>
                                @endif
                                Profile</a>
                            </li>

                            <li class="has-submenu @if($page == 'Eligibility')active @endif">
                                <a @if(Auth::user()->step1())href="{{ route('eligibility') }}"@else class="incompleteSteps"@endif class="hoverless">
                                @if(Auth::user()->step2())
                                <strong><i class="md md-check"></i></strong>
                                @else
                                <i class="md">Step 2</i>
                                @endif
                                @if(!Auth::user()->step1()) <i class="md md-lock inline"></i> @endif Eligibility</a>
                            </li>

                            <li class="has-submenu @if($page == 'Documents')active @endif">
                                <a @if(Auth::user()->step2())href="{{ route('documents') }}"@else class="incompleteSteps"@endif class="hoverless">
                                @if(Auth::user()->step3())
                                <strong><i class="md md-check"></i></strong>
                                @else
                                <i class="md">Step 3</i>
                                @endif
                                @if(!Auth::user()->step2()) <i class="md md-lock inline"></i> @endif Documents</a>
                            </li>

                            <li class="has-submenu @if($page == 'Experience')active @endif">
                                <a @if(Auth::user()->step3())href="{{ route('experience') }}"@else class="incompleteSteps"@endif class="hoverless">
                                @if(Auth::user()->step4())
                                <strong><i class="md md-check"></i></strong>
                                @else
                                <i class="md">Step 4</i>
                                @endif
                                @if(!Auth::user()->step3()) <i class="md md-lock inline"></i> @endif Add Experience</a>
                            </li>

                            <li class="has-submenu @if($page == 'Terms')active @endif">
                                <a @if(Auth::user()->step4())href="{{ route('terms') }}"@else class="incompleteSteps"@endif class="hoverless">
                                @if(Auth::user()->step5())
                                <strong><i class="md md-check"></i></strong>
                                @else
                                <i class="md">Step 5</i>
                                @endif
                                @if(!Auth::user()->step4()) <i class="md md-lock inline"></i> @endif Agree to Terms</a>
                            </li>

                            <li class="has-submenu @if($page == 'Fees')active @endif">
                                <a @if(Auth::user()->step5())href="{{ route('submit') }}"@else class="incompleteSteps"@endif class="hoverless">
                                @if(Auth::user()->step6())
                                <strong><i class="md md-check"></i></strong>
                                @else
                                <i class="md">Step 6</i>
                                @endif
                                @if(!Auth::user()->step5()) <i class="md md-lock inline"></i> @endif Submit</a>
                            </li>
                            @else
                            <li class="has-submenu @if($page == 'profile')active @endif">
                                <a href="{{ route('profile') }}" class="hoverless">Profile</a>
                            </li>
                            @if(isset(Auth::user()->application) &&  Auth::user()->application->status == 3)
                                <li class="has-submenu @if($page == 'Eligibility')active @endif">
                                    <a href="{{ route('eligibility') }}" class="hoverless">Eligibility</a>
                                </li>
                            @endif

                            <li class="has-submenu @if($page == 'Documents')active @endif">
                                <a href="{{ route('documents') }}" class="hoverless">Documents</a>
                            </li>

                            <li class="has-submenu @if($page == 'Experience')active @endif">
                                <a href="{{ route('experience') }}" class="hoverless">Experience</a>
                            </li>
                            
                            @if(isset(Auth::user()->application) && Auth::user()->application->status == 3)
                                <li class="has-submenu active" style="background-color: #ef4554 !important; background: #ef4554 !important;">
                                    <a href="#" style="background: #ef4554 !important; color:white !important; border-bottom: 1px solid #ef4554 !important;" onclick="if(confirm('Click OK you resubmit your application to the ARTBA Certification Team for review.')) window.location.href = '{{ route('resubmit') }}';">Resubmit</a>
                                </li>
                            @endif
                            @if(isset(Auth::user()->application) &&  Auth::user()->application->status == 6 || isset(Auth::user()->application) && Auth::user()->application->status == 7)
                                <li class="has-submenu active" style="background-color: #ef4554 !important; background: #ef4554 !important;">
                                    <a href="{{ route('retest') }}" style="background: #ef4554 !important; color:white !important; border-bottom: 1px solid #ef4554 !important;">Retake Exam</a>
                                </li>
                            @endif
                        @endif

                        </ul>
                        <ul class="navigation-menu float-right right-menu">

                            <li class="has-submenu @if(Auth::user()->isNew()) isnew @endif"><a href="{{ route('logout') }}" style="background: #ef4554 !important;">@if(Auth::user()->isNew())<i class="fa fa-sign-out"></i>@endif Logout</a></li>
                            

                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>