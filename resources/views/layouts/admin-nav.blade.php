<header id="topnav">
            <div class="navbar-custom">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->

                        <ul class="navigation-menu">
                        <a href="{{ route('home') }} class="logo" style="color:white; font-size:20px; position: absolute; left: 100px; top: 50%; margin-top: -12px;">
                            SCTPP
                        </a>
                        <a href="{{ route('home') }}" class="logo" style="color:white; font-size:20px; position: absolute; left: 100px; top: 50%; margin-top: -12px;">
                            SCTPP
                        </a>
                            <li class="has-submenu @if($page == 'Analytics')active @endif">
                                <a href="{{ route('overview') }}" class="hoverless"><i class="md"></i>Analytics</a>
                                <ul class="submenu">
                                    <li>
                                        <a href="{{ route('overview') }}">Overview</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('performance') }}">Performance</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('demographics') }}">Demographics</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('emails') }}">Email Log</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('sisense') }}">Sisense Dashboard (Beta)</a>
                                    </li>
                                </ul>
                            </li>

                            @if(Auth::user()->canReview())

                            <li class="has-submenu @if($page == 'Users')active @endif">
                                <a href="{{ route('users') }}" class="hoverless"><i class="md"></i>User Management</a>
                                <ul class="submenu">
                                    <li>
                                        <a href="{{ route('users') }}">All Users</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('references') }}">References</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('pdh-submissions') }}">PDH Submissions</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('review') }}">Review Applications</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('reviewHistory') }}">Decision History</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('exam') }}">Examination</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('certificates') }}">Certificates</a>
                                    </li>
                                </ul>
                            </li>

                            @endif

                            <li class="has-submenu @if($page == 'Organizations')active @endif">
                                <a href="{{ route('organizations') }}" class="hoverless"><i class="md"></i>Organizations</a>
                                <ul class="submenu">
                                    <li><a href="{{ route('organizations') }}">All Organizations</a></li>
                                    <li><a href="{{ route('access-codes') }}">Access Codes</a></li>
                                </ul>
                            </li>
                            <!--
                            <li class="has-submenu @if($page == 'LMS')active @endif">
                                <a href="{{ route('lms') }}" class="hoverless"><i class="md"></i>LMS</a>
                            </li>
                            -->
                            <li class="has-submenu @if($page == 'Account')active @endif">
                                <a href="{{ route('account', ['id' => Auth::user()->id]) }}" class="hoverless"><i class="md"></i>My Account</a>
                            </li>
                        </ul>
                        <ul class="navigation-menu float-right right-menu">
                            <li class="has-submenu"><a href="{{ route('logout') }}" style="background: #ef4554 !important; padding-top:23px !important; padding-bottom: 23px !important;">Logout</a></li>
                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>