@php
$toggleBox = rand(10000000,999999999);
@endphp
<div class="card mb-1 pdh-card">
	<form method="POST" enctype="multipart/form-data" id="pdhForm-{{ $toggleBox }}">
		<div class="card-header" id="headingOne">
			<h4 class="pdhTitle">
				<a class="text-dark @if(isset($pdh->id)) collapsed @endif" data-toggle="collapse" href="#collapse{{ $toggleBox }}" aria-expanded="true">@if(isset($pdh) && $pdh->status == 2) <i title="PDH Approved" class="fa fa-check text-success"></i> @elseif(isset($pdh) && $pdh->status == 1) <i title="PDH Denied" class="fa fa-times text-danger"></i> @elseif(isset($pdh) && $pdh->status == 0) <i title="Pending Review" class="fa fa-clock-o text-info"></i> @endif <span class="activityTitle">{{ $pdh->name or 'New PDH Activity' }} 
				@if(isset($pdh->content)) | {{ convertRecertContent($pdh->content) }} @endif
				</span>
				</a>
			</h4>
			<button type="button" class="remove btn btn-danger pull-right" data-pdhID="{{ $pdh->id or $toggleBox }}">Remove</button>
		    <h4 class="pull-right" style="margin-right: 10px;"><span class="currentHours">0</span> PDHs</h4>
		</div>
		<div id="collapse{{ $toggleBox }}" class="collapse @if(!isset($pdh->id)) show @endif" aria-labelledby="headingOne" data-parent="#accordion">
			<div class="card-body">
				<div id="clonedInput1" class="clonedInput">
					<input type="hidden" name="pdhID" value="{{ $pdh->id or $toggleBox }}">
					<div class="row">
						<div class="col-md-10">
							<div class="form-row">
								<div class="col-md-4">
									<label>Activity Name</label>
									<input type="text" class="form-control activityName required" required="" name="activityName" value="{{ $pdh->name or '' }}">
								</div>
								<div class="col-md-2">
									<label>Activity Date</label>
									<input type="text" class="form-control required validateDate datepicker" required="" name="activityDate" value="{{ $pdh->activityDate or '' }}" max="2222-05-26" class="date" placeholder="mm/dd/yyyy" style="font-size: 13px;">
								</div>
								<div class="col-md-2">
									<label>Select Hours Earned</label>
									<select class="req0 form-control required" data-style="btn-primary" style="margin-right: 20px;" required="" name="pdhsEarned">
										<option value="">Please Select</option>
										@for($i = 1; $i <= 25; $i++)
											<option value="{{ $i }}" @if(isset($pdh->hours) && $pdh->hours == $i ) selected @endif>{{ $i }} PDHs</option>
										@endfor
									</select>
								</div>
								<div class="col-md-2">
									<label>City</label>
									<input type="text" name="city" class="form-control required" value="{{ $pdh->city or '' }}">
								</div>
								<div class="col-md-2">
									<label>State</label>
									<select class="form-control required" name="state" required="">
										<option value="">Please Select</option>
										@foreach($states as $state)
											<option value="{{ $state->abbr }}" @if(isset($pdh->state) && $pdh->state == $state->abbr ) selected @endif>{{ $state->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-4">
									<label>Sponsoring Organization</label>
									<input type="text" class="form-control required" name="sponsor" value="{{ $pdh->organization or '' }}">
								</div>
								<div class="col-md-3">
									<label>Activity Contact</label>
									<input type="text" class="form-control required" name="contactName" value="{{ $pdh->contactName or '' }}">
								</div>
								<div class="col-md-2">
									<label>Activity Contact Phone</label>
									<input type="tel" class="form-control required" name="telephone" value="{{ $pdh->phone or '' }}">
								</div>
								<div class="col-md-3">
									<label>Activity Contact Email</label>
									<input type="email" class="form-control required" name="email" value="{{ $pdh->email or '' }}">
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-6">
									<label>Activity Type</label>
									<select class="form-control required" required="" name="activityType">
										<option value="">Please Select</option>
										<option value="1" @if(isset($pdh->type) && $pdh->type == 1 ) selected @endif>College-level classroom course in relevant material completed with passing grade</option>
										<option value="2" @if(isset($pdh->type) && $pdh->type == 2 ) selected @endif>Continuing Education Course complete with certificate</option>
										<option value="3" @if(isset($pdh->type) && $pdh->type == 3 ) selected @endif>Short courses, tutorials and distance-education courses through video, webinar or internet learning</option>
										<option value="4" @if(isset($pdh->type) && $pdh->type == 4 ) selected @endif>Attended qualifying seminars, industry courses, workshops, or professional presentations, meetings, conventions or conferences</option>
										<option value="5" @if(isset($pdh->type) && $pdh->type == 5 ) selected @endif>Presented at qualifying seminars, industry courses, workshops, or professional presentations, meetings, conventions or conferences</option>
										<option value="6" @if(isset($pdh->type) && $pdh->type == 6 ) selected @endif>Published paper or article on transportation construction health and safety or traffic control and roadway maintenance</option>
										<option value="7" @if(isset($pdh->type) && $pdh->type == 7 ) selected @endif>Published book on safety- transportation, construction, environmental, occupational</option>
									</select>
								</div>			
							</div>
						</div>
						<div class="col-md-2">
							<label>Attach Evidence (PDFs only)</label>
							@if(isset($pdh->file))
								<a href="/uploads/pdhs/{{ $pdh->file }}" target="_blank">View Document</a>
								<br><br>Replace file below
								<input type="file" name="activityDocument">
							@else
								<input type="file" class="required dropify test-control" data-height="200" name="activityDocument" data-allowed-file-extensions="pdf" data-max-file-size="2M"/>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							@if(isset($pdh) && $pdh->status == 2)
							<p>Note: This PDH evidence has already been approved. Clicking save will reset the approval status and require an admin to review it.</p>
							@elseif(isset($pdh) && $pdh->status == 1)
							<p><strong style="font-size:18px;">Denied:</strong> {{ $pdh->lastestDecision()->reason }} Please save necessary changes and click save to have your PDH evidence reconsidered.</p>
							@endif
							<button class="btn btn-success savePDH" type="button" data-form="{{ $toggleBox }}">SAVE</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>