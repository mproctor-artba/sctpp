<header id="topnav">
    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->

                <ul class="navigation-menu">
                <a href="{{ route('home') }}" class="logo" style="color:white; font-size:20px; position: absolute; left: 100px; top: 50%; margin-top: -12px;">
                    SCTPP
                </a>
                    <li class="has-submenu @if($page == 'eligibility')active @endif">
                        <a href="{{ route('recertify') }}" class="hoverless">
                        @if(Auth::user()->step11() >= 1)
                                <i class="md md-check"></i>
                                @else
                                <i class="md">Step 1</i>
                                @endif
                        Eligibility</a>
                    </li>
                    <li class="has-submenu @if($page == 'checkout')active @endif">
                        <a href="{{ route('recertify-checkout') }}" class="hoverless">

                        @if(Auth::user()->step12())
                                <i class="md md-check"></i>
                                @else
                                <i class="md">Step 2</i>
                                @endif
                        Payment</a>
                    </li>
                    <li class="has-submenu @if($page == 'experience')active @endif">
                        <a href="{{ route('recertify-experience') }}" class="hoverless">
                        @if(Auth::user()->step13())
                                <i class="md md-check"></i>
                                @else
                                <i class="md">Step 3</i>
                                @endif
                        Experience</a>
                    </li>

                    @if(Auth::user()->step11() == 1 || Auth::user()->step11() == 3)
                    <li class="has-submenu @if($page == 'recertifyPDF')active @endif">
                        <a href="{{ route('recertify-pdh') }}" class="hoverless">
                        @if(Auth::user()->pdhs->sum('hours') >= 30)
                        <i class="md md-check"></i>
                        @else
                        <i class="md">Step 4</i>
                        @endif
                        PDH Documents</a>
                    </li>
                    <li class="has-submenu @if($page == 'recertify-submit')active @endif">
                        <a href="@if(Auth::user()->pdhs->sum('hours') >= 30){{ route('recertify-submit')}}@else #@endif" class="@if(Auth::user()->pdhs->sum('hours') < 30)incompleteSteps @endif hoverless">
                        @if(Auth::user()->application->stage >= 4)
                        <i class="md md-check"></i>
                        @else
                        <i class="md">Step 5</i>
                        @endif
                        Submit</a>
                    </li>
                    @elseif(Auth::user()->step11() == 2 || Auth::user()->step11() == 4)
                    <li class="has-submenu @if($page == 'recertify-submit')active @endif">
                        <a href="@if(Auth::user()->step13()){{ route('recertify-submit') }}@else #@endif" class="@if(!Auth::user()->step13()) incompleteSteps @endif hoverless">
                        @if(Auth::user()->application->stage >= 4)
                        <i class="md md-check"></i>
                        @else
                        <i class="md">Step 4</i>
                        @endif
                        Submit</a>
                    </li>
                    @endif
                </ul>
                <ul class="navigation-menu float-right right-menu">
                    <li class="has-submenu @if(Auth::user()->isNew()) isnew @endif"><a href="{{ route('logout') }}" style="background: #ef4554 !important;"><i class="fa fa-sign-out"></i> Logout</a></li>
                </ul>
                <!-- End navigation menu -->
            </div> <!-- end #navigation -->
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>