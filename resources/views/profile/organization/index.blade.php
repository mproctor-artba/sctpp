@extends('layouts.app')

@section('css')
	<link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
	<style>
	.tab-content{
		width:100%;
	}
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
	#datatable-buttons_filter{
		text-align: right;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">Organizations</li>
    <li class="breadcrumb-item active">{{ $organization->name }}</li>
</ol>
@endsection

@section('content')

	<div class="row">
		<div class="col-lg-12">
			<div class="card-box">
				<div class="card-body">
					<div class="row">
						<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" style="width:100%">
		                    <thead>
		                    <tr>
		                        <th>Code</th>
		                        <th>Used By</th>
		                        <th class="text-center">Progress</th>
		                        <th width="10%" class="text-center"></th>
		                    </tr>
		                    </thead>
		                    <tbody>
		                        @foreach($organization->codes as $code)
		                        <tr>
		                            <td>{{ $code->code }}</td>
		                            <td>
		                            @if(is_object($code->user))
		                                {{ $code->user->profile->first_name . " " . $code->user->profile->last_name }} 
		                            @endif
		                            </td>
		                            <td class="text-center" width="150">
		                            	@if(isset($code->user))
		                            		{{ $code->user->currentStep() }}
		                            	@else
		                            		N/A
		                            	@endif
		                            </td>
		                            <td class="text-center">
		                            @if(isset($code->user))
		                            	<table >
                                    <tr style="background: none !important;">
                                        <td style="border:none !important;"><a href="mailto:{{ $code->user->email }}" class="text-center"><i class="fa fa-envelope-o"></i></a></td>
                                        </tr>
                                    </table>
                                    @endif
		                            </td>
		                        </tr>
		                        @endforeach
		                        @foreach($monitoredUsers as $monitoredUser)
		                        	<tr>
		                        	<td>N/A</td>
		                            <td>{{ $monitoredUser->profile->first_name . " " . $monitoredUser->profile->last_name }}</td>
		                            <td class="text-center" width="150">
		                            	{{ $monitoredUser->currentStep() }}
		                            </td>
		                            <td class="text-center">
		                            	<table >
		                                    <tr style="background: none !important;">
		                                        <td style="border:none !important;"><a href="mailto:{{ $monitoredUser->email }}" class="text-center"><i class="fa fa-envelope-o"></i></a></td>
		                                    </tr>
		                                </table>
		                            </td>
		                            </tr>
		                        @endforeach
		                    </tbody>
	                	</table>
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection

@section('js')
	    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script type="text/javascript">
            $(document).ready(function() {
            	$("#page-title").html("{{ $organization->name }}");
                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'csv', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');


            } );
        </script>

@endsection