@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
	#datatable-buttons_filter{
		text-align: right;
	}
    #faq li{
        padding: 5px 0;
    }
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">Accounts</li>
    <li class="breadcrumb-item">{{ $organization->name }}</li>
    <li class="breadcrumb-item active">{{ $page }}</li>
</ol>
@endsection

@section('content')

	<div class="row">
		<div class="col-lg-6">
			<div class="card-box">
				<div class="card-body">
					<div class="row">
                        <form method="POST" style="width:100%;">
                            @CSRF
                            <div class="form-group">
                                <h4>How can we help?</h4>
                                <em>Note: This is not a complaints form. <a href="https://sctpp.org/contacts/complaints/" target="_blank">Click here</a> to file an official complaint.</em>
                            </div>
                            <div class="form-group">
                                <label></label>
                                <select class="form-control" name="subject" required="">
                                    <option value="">Select a Topic</option>
                                    <option value="">Applications</option>
                                    <option value="">Billing</option>
                                    <option value="">Analytics / Custom Report</option>
                                    <option value="">Technical Issue</option>
                                    <option value="">Feature Request</option>
                                    <option>Other</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit Message</button>
                            </div>
                        </form>
					</div>
				</div>
			</div>
		</div>
        <div class="col-lg-6">
            <div class="card-box">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group">
                            <h4>FAQs</h4>
                            <ul style="list-style: none; padding:0;" id="faq">
                                <li><b>Q: Can I see how well our employees did on their exam?</b></li>
                                <li>A: The ARTBA Certification Team <b>does not</b> reveal test score data to anyone other than the examinee.</li>
                                <li><b>Q: Why is it taking so long for our employees to get reviewed?</b></li>
                                <li>A: The ARTBA Certification Team manually reviews every application and, depending on volume, may result in delays. Please allow us 15 business days to submit a decision for each application.</li>


                            </ul>
                            For additional SCTPP&trade; related FAQs, please <a href="https://sctpp.org/faqs/" target="_blank">click here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>

@endsection

@section('js')
	<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script type="text/javascript">
            $(document).ready(function() {
            	$("#page-title").html("Contact Support");
                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');


            } );
        </script>

@endsection