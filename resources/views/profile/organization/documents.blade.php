@extends('layouts.app')

@section('css')
	<link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/plugins/dropzone/dropzone.css" rel="stylesheet" type="text/css" />
	<style>
	.tab-content{
		width:100%;
	}
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
	#datatable-buttons_filter{
		text-align: right;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">Accounts</li>
    <li class="breadcrumb-item">{{ $organization->name }}</li>
    <li class="breadcrumb-item active">{{ $page }}</li>
</ol>
@endsection

@section('content')

	<div class="row">
		<div class="col-lg-12">
			<div class="card-box">
				<div class="card-body">
					<div class="row">
						<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" style="width:100%">
		                    <thead>
		                    <tr>
		                        <th>User</th>
		                        <th>Title</th>
		                        <th>Progress</th>
		                        <th>Documents</th>
		                        <th width="10%" class="text-center"></th>
		                    </tr>
		                    </thead>
		                    <tbody>
		                        @foreach($organization->codes as $code)
		                        	@if(isset($code->user))
				                        <tr>
				                            <td>{{ $code->user->profile->first_name . " " . $code->user->profile->last_name}}</td>
				                            <td>{{ $code->user->profile->title }}</td>
				                            <td>{{ $code->user->currentStep() }}</td>
				                            <td>
				                            	<ul style="list-style: none; margin: 0; padding:0;">
				                            		@foreach($code->user->documents as $document)
				                            			<li>
				                            			@if($document->type == 1)
				                            				<b>[RESUME]:</b>
				                            			@elseif($document->type == 2)
				                            				<b>[OSHA]:</b>
				                            			@elseif($document->type == 3)
				                            				<b>[DIPLOMA]:</b>
				                            			@elseif($document->type == 4)
				                            				<b>[PDH]:</b>
				                            			@endif <a href="/uploads/documents/{{ $document->name }}" target="_blank" >{{ $document->name }}</a>
				                            			@if($document->type == 4)
				                            				<button class="deleteDocument label label-danger" data-name="{{ $document->name }}"><i class="fa fa-trash"></i></button>
				                            			@endif
				                            			</li>
				                            		@endforeach
				                            	</ul>
				                            </td>
				                            <td><a href="#create-{{ $code->user->id }}" class="btn btn-success waves-effect waves-light docupload" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a" data-user="{{ $code->user->id }}"><i class="fa fa-upload"></i> Documents</button></td>
				                            <!-- modal for file drop -->
				                        <div id="create-{{ $code->user->id }}" class="modal-demo">
								            <button type="button" class="close" onclick="Custombox.close();">
								                <span>&times;</span><span class="sr-only">Close</span>
								            </button>
								            <h4 class="custom-modal-title">Add Work Experience</h4>
								            <div class="custom-modal-text">
								                <form method="POST" action="{{ route('import') }}" class="dropzone" id="dropzone-{{ $code->user->id }}">
								                     
								                        <div class="form-row">
								                            <div class="fallback">
								                            	<input type="file" name="file[]" multiple="">
								                            </div>
								                        </div>
								                    </form>
								            </div>
								        </div>
								        
				                        </tr>
				                        
		                        	@endif
		                        @endforeach
		                    </tbody>
	                	</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
        


@endsection

@section('js')
	<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="/assets/plugins/dropzone/dropzone.js"></script>
    <!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script type="text/javascript">
            $(document).ready(function() {
            	$("#page-title").html("{{ "Organization " . $page }}");
                // Default Datatable
                $('#datatable').DataTable();

                var config = {
		            autoProcessQueue: true,
		            uploadMultiple: true,
		            parallelUploads: 15,
		            maxFiles: 15
		        };

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');

                $(".deleteDocument").click(function(){
		            if(confirm("Are you sure you want to delete this document?")){
		                var filename = $(this).attr("data-name");

		                if(filename != ""){
		                    $(this).parent().remove();
		                    $.get("{{ route('removeDocument', ['user' => NULL]) }}/" + filename, function(data, status){
		                        $.Notification.notify('success','bottom right', 'Done', 'Document has been removed.');
		                    });
		                }
		            }
		        });

		        

		        $(".docupload").click(function(){
		        	var user = $(this).attr("data-user");
		        	$.getJSON("/user/" + user, function(data){
		        		$("#create-" + user + " .custom-modal-title").text("Upload Documents for " + data["name"]);

		        	});
		        });		        

		        $("form.dropzone").each(function(){
		        	$(this).prepend('<input type="hidden" name="_token" value="{{ csrf_token() }}">');
		        })

		        @foreach($organization->codes as $code)
		            @if(isset($code->user))
		        		var myDropzone{{ $code->user->id }} = new Dropzone("#dropzone-{{ $code->user->id }}", { url: "{{ route('newimport', ['name' => NULL]) }}/{{ $code->user->id }}", acceptedFiles: ".pdf"});
	        		@endif
		        @endforeach
            });
        </script>

@endsection