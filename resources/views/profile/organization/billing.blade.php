@extends('layouts.app')

@section('css')
	<style>
	.tab-content{
		width:100%;
	}
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
	#datatable-buttons_filter{
		text-align: right;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">Accounts</li>
    <li class="breadcrumb-item">{{ $organization->name }}</li>
    <li class="breadcrumb-item active">{{ $page }}</li>
</ol>
@endsection

@section('content')

	<div class="row">
		<div class="col-lg-12">
			<div class="card-box">
				<div class="card-body">
					<div class="row">
                        <form id="payment-form" method="POST" action="{{ route('submitApplication') }}">
                            {{ CSRF_FIELD() }}
                            <div class="form-row">
                                <div class="form-group-custom text-left col-md-12">
                                    <p>Your application will only be reviewed when all steps of the application have been completed and upon payment has been received.<br>
                                    <em class="text-muted">Note: This fee is partialy refundable</em></p>
                                </div>
                                <div class="form-group-custom text-left col-md-12" style="margin-bottom: 10px;">
                                    <p>If your employer provided you with an access code, enter it here:</p>
                                </div>
                            </div>
                    <div class="form-row" style="margin-bottom: 5px;">
                      
                    </div>
                    <div class="form-row" style="margin-bottom: 20px;">

                        <div id="card-element">
                          <!-- a Stripe Element will be inserted here. -->
                        </div>
                        <!-- Used to display form errors -->
                        <div id="card-container" style="display: none;"></div>
                        <div id="card-errors" role="alert"></div>
                    </div>
                    <div class="form-row action">
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary submit">Submit Application for Review <span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></button>
                      </div>
                    </div>

                        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="page-title-box">
        <h4 id="page-title" class="page-title">Billing History</h4>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <div class="card-body">
                    <div class="row">
                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Transaction ID</th>
                                    <th>Admin</th>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                    <th>Date</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center"><i class="fa fa-print"></i></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script type="text/javascript">
            $(document).ready(function() {
            	$("#page-title").html("Order Form");
                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');


            } );
        </script>


<script src="https://js.stripe.com/v3/" id="striping1"></script>
<script type="text/javascript" id="striping2">
    //Stripe.setPublishableKey('');
// Create a Stripe client
var stripe = Stripe('{{ env('STRIPE_KEY') }}');

// Create an instance of Elements
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {


  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server
      $("i.fa").removeClass("fa-arrow-right");
      $(".submit i").addClass("fa fa-refresh");
    $("#payment-form").append('<input type="hidden" name="stripeToken" value="' + result.token.id + '">');
    $("#payment-form").append('<input type="hidden" name="cardnumber" value="' + result.token.card.last4+ '">');
    $("#payment-form").append('<input type="hidden" name="cardID" value="' + result.token.card.id+ '">');
      

    $("#payment-form").submit();
    }
  });
});

$("#verify").click(function(){
      var code = $("#code").val();

      $.get('/application/access/' + code, function(data, status){
        if(data == "OK"){
          $("#verify").removeClass();
          $("#verify").addClass("btn btn-success");
          $("#verify").html("<i class='fa fa-check'></i> Verified"); 
          $("#card-element").remove();
          $(".submit").remove();
          $("#price").text("0");
          $(".action").append('<a href="/application/waived/' + code  + ' " class="btn btn-primary submit">Submit Application for Review <span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></button>');
        }
        else {
          $("#verify").removeClass();
          $("#verify").addClass("btn btn-danger");
          $("#verify").html("<i class='fa fa-times'></i> Invalid"); 
        }
      });
});
$("#code").focusout(function() {
  if($(this).val() == ""){
    $("#verify").removeClass();
    $("#verify").addClass("btn btn-dark");
    $("#verify").html("<i class='fa fa-search'></i> Verify"); 
  }
});
</script>

@endsection