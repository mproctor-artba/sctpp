@extends('layouts.app')

@section('css')
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Multi Item Selection examples -->
    <link href="/assets/plugins/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
	<style>
	.tab-content{
		width:100%;
	}
	#datatable-buttons_wrapper{
		width:100% !important;
		padding: 0 !important;
		max-width: 100% !important;
	}
	#datatable-buttons_filter{
		text-align: right;
	}
	</style>
@endsection

@section('crumbs')
<ol class="breadcrumb hide-phone p-0 m-0">
    <li class="breadcrumb-item">Accounts</li>
    <li class="breadcrumb-item">{{ $organization->name }}</li>
    <li class="breadcrumb-item active">{{ $page }}</li>
</ol>
@endsection

@section('content')

	<div class="row">
		<div class="col-lg-12">
			<div class="card-box">
				<div class="card-body">
                    <h4 class="header-title text-danger"><i class="fa fa-certificate"></i> Certificates Awarded: {{ $passed }}</h4>
                    <br>

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Grade</th>
                                <th>Certificate</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($codes as $code)
                                @if(is_object($code->profile) && is_object($code->profile->results))
                                    @php $profile = $code->profile @endphp
                                    
                                    @foreach($code->profile->results as $result)
                                        <tr>
                                            <td>{{ $profile->first_name . " " . $profile->last_name }}</td>
                                            <td>{{ $profile->city }}</td>
                                            <td>{{ $profile->state }}</td>
                                            <td>{{ strtoupper($result->Grade) }}</td>
                                            <td>@if($result->Score >= 73) <a href="https://www.credential.net/{{ $profile->user->certificate->certificate }}" target="_blank" class="text-center">{{ $profile->user->certificate->certificate }}</a> @else N/A @endif</td>
                                            <td>{{ converttimestamp($result->date) }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            @endforeach
                            
                            @foreach($monitoredUsers as $monitoredUser)
                                @php $profile = $monitoredUser->profile @endphp
                                @foreach($profile->results as $result)
                                <tr>
                                    <td>{{ $profile->first_name . " " . $profile->last_name }}</td>
                                    <td>{{ $profile->city }}</td>
                                    <td>{{ $profile->state }}</td>
                                    <td>{{ strtoupper($result->Grade) }}</td>
                                    <td>@if($result->Score >= 73) <a href="https://www.credential.net/{{ $profile->user->certificate->certificate }}" target="_blank" class="text-center">{{ $profile->user->certificate->certificate }}</a> @else N/A @endif</td>
                                    <td>{{ converttimestamp($result->date) }}</td>
                                </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>


@endsection

@section('js')
	    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Key Tables -->
    <script src="/assets/plugins/datatables/dataTables.keyTable.min.js"></script>

    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>

    <!-- Modal-Effect -->
    <script src="/assets/plugins/custombox/custombox.min.js"></script>
    <script src="/assets/plugins/custombox/legacy.min.js"></script>
    <script type="text/javascript">
            $(document).ready(function() {
            	$("#page-title").html("{{ $page }}");
                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'csv' ,'excel', 'pdf']
                });

                table.unique();

                // Key Tables

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');


            } );
        </script>

@endsection