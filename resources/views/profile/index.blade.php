@extends('layouts.app')

@section('css')
	<link rel="stylesheet" href="https://selectize.github.io/selectize.js/css/selectize.default.css" data-theme="default">
	<style>
	.tab-content, form{
		width:100%;
	}
    form .form-group .control-label{
        position: absolute;
        top: 0.25rem;
        pointer-events: none;
        padding-left: 0.125rem;
        z-index: 1;
        color: #98a6ad;
        font-weight: normal;
        -webkit-transition: all 0.28s ease;
        transition: all 0.28s ease;
    }
    .page-title{
        display: none;
    }
    .select2-selection{
        text-align: left;
    }
    .selectize-control.single .selectize-input{
            background-color: white !important;
            background-image: none !important;
            border: none;
            border-bottom: 1px solid #999;
            border-radius: 0 !important;
    }
    .selectize-control{
        padding-top: 2px !important;
        padding-left: 0 !important;
    }
    .selectize-control .item{
        font-size: 1rem !important;
        color: #565656 !important;
    }
	</style>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card-box">
			<div class="card-body">
                <div class="row">
<form method="POST" action="{{ route('saveProfile') }}">
                @csrf
            	<h4 class="m-t-0 header-title" style="margin-bottom: 20px;">My Profile</h4>
                <div class="form-row clearfix">
                    <div class="form-group col-md-5">
                        <label>First Name</label>
                        <input type="text" name="f_name" value="{{ $profile->first_name or old('f_name') }}" class="form-control material required " required="">
                    </div>
                    <div class="form-group col-md-5">
                        <label>Last Name</label>
                        <input type="text" name="l_name" value="{{ $profile->last_name or  old('l_name') }}" class="form-control material required" required>
                        
                    </div>
                    <div class="form-group col-md-2">
                        <label>Middle Initial</label>
                        <input type="text" name="middle" value="{{ $profile->middle or old('middle') }}" class="form-control material">
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-group col-md-4">
                        <label>Job Title</label>
                        <select id="title" class="demo-default required material" placeholder="Select a title..." name="title" required="">
                            <option value="">Please Select or Enter Job Title</option>
                            @foreach($titles as $jobtitle)
                                    <option value="{{ $jobtitle->title }}"
                                    @if(isset($profile->title))
                                        @if($profile->title == $jobtitle->title)
                                            selected
                                        @endif
                                    @elseif(old('title') == $jobtitle->title)
                                        selected
                                    @endif
                                    >{{ $jobtitle->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Company</label>
                        <select id="company" class="demo-default required material" placeholder="Select a company..." name="company" required="">
                            <option value="">Please Select or Enter Company Name</option>
                            @foreach($organizations as $organization)
                                    <option value="{{ $organization->company }}"
                                    @if(isset($profile->company))
                                        @if($profile->company == $organization->company)
                                            selected
                                        @endif
                                    @elseif(old('company') == $organization->company)
                                        selected
                                    @endif
                                    >{{ $organization->company }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Phone Number</label>
                        <input type="text" name="phone" value="{{ $profile->phone or old('phone') }}" minlength="4" class="form-control material required phone" required>
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-group col-md-6">
                        <label>Address</label>
                        <input type="text" name="address" value="{{ $profile->address or old('address') }}" class="form-control material required" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Address Line 2</label>
                        <input type="text" name="address2" value="{{ $profile->address2 or old('address2') }}" class="form-control material">
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-group col-md-5">
                        <label>City</label>
                        <input type="text" name="city" value="{{ $profile->city or old('city') }}" class="form-control material required" required>
                    </div>
                    <div class="form-group col-md-5">
                        <label>State</label>
                        <select class="form-control material required" name="state" required="">
                        <option value="">Please Select</option>
                        @foreach($states as $state)
                            <option value="{{ $state->abbr }}" 
                                @if(old('state') !== null)
                                    @if(old('state') == $state->abbr)
                                        selected=""
                                    @endif
                                @endif
                                @if(is_object($profile))
                                    @if($profile->state == $state->abbr)
                                        selected=""
                                    @endif
                                @endif
                            >
                        {{ $state->name }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Zip</label>
                        <input type="number" name="zip" value="{{ $profile->zip or old('zip') }}" minlength="4" class="form-control material required" required>
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-group col-md-6">
                        <button type="submit" class="btn btn-primary">Save @if(Auth::user()->isNew()) and Move to Step 2 <span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span>@else Changes @endif</button>
                    </div>
                </div>
                </form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
    <script src="https://selectize.github.io/selectize.js/js/selectize.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    $('#title').selectize({
        create: true,
        sortField: 'text'
    });
    $('#company').selectize({
        create: true,
        sortField: 'text'
    });


        $(".phone").keyup(function(){
            var val = $(this).val();
            var last = val.substr(val.length - 1);
            if(Math.floor(last) != last && !$.isNumeric(last)){
                $(this).val(val.slice(0,-1));
            }
            if(last == " "){
                $(this).val(val.slice(0,-1));
            }
        });
    });
    </script>

@endsection